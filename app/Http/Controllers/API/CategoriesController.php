<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Models\Category;
use Validator;
use File;

class CategoriesController extends Controller
{
  public function categories_main()
  {
    $categories = Category::where('cat', 0)->where('deleted_at', null)->get();
    
    $data = $categories->map(function($category) {

        if($category['image'] != null && File::exists(public_path().$category['image']))
        {
            $category->image = asset($category->image);
        }else{
            $category->image = 'Image Not Exists In Path';
        }
       
        return $category;
    });

    return response()->json(['status' => 'success', 'data' => $data]);
  }

  public function show_categories_main(request $request)
  {

    $validator = Validator::make($request->all(), [
          'id'      => 'required|exists:categories,id|integer'
      ],
      [
          'id.exists'     => 'This Id Not Exists',
          'id.integer'    => 'This Id Not Integer',
      ]);

      if($validator->fails())
      {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
      }

      $category = Category::where('id', $request->id)->first();

      if($category->cat != 0)
      {
          return response()->json(['status' => 'error', 'message' => 'This Not Category Main Be Category Sub']);
      }
      return response()->json(['status' => 'success', 'category' => $category]);
  }
  public function add_category_main(Request $request)
  {
    $validator = Validator::make($request->all(), [
          'title_en' => 'required',
          'title_ar' => 'required',
          'image' => 'sometimes|nullable|mimes:jpeg,png,jpg,gif,svg,webp'
      ],
      [
          'title_en.required'=>'Please Enter Category Name (EN)',
          'title_ar.required'=>'Please Enter Category Name (AR)',
          'image.mimes' => 'Please Choose Valid Image File'
      ]);

      if($validator->fails())
      {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
      }

      $category = new Category;
      $category->title_en  = $request->title_en;
      $category->title_ar  = $request->title_ar;
      $category->cat  = 0;

      if($request->has('image') && $request->image != null)
      {
        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/categories');
        $image->move($destinationPath, $imageName);
        $category->image ='/uploads/categories/'.$imageName;
      }

      $category->save();

      return response()->json(['status' => 'success', 'category' => $category]);
  }


  public function edit_categorys_main(Request $request)
  {
    $validator = Validator::make($request->all(), [
          'title_en' => 'required',
          'title_ar' => 'required',
          'image' => 'sometimes|nullable|mimes:jpeg,png,jpg,gif,svg,webp'
      ],
      [
          'title_en.required'=>'Please Enter Category Name (EN)',
          'title_ar.required'=>'Please Enter Category Name (AR)',
          'image.mimes' => 'Please Choose Valid Image File'
      ]);

      if($validator->fails())
      {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
      }

      $category = Category::where('id', $request->id)->first();
      $category->title_en  = $request->title_en;
      $category->title_ar  = $request->title_ar;

      if($request->has('image') && $request->image != null)
      {
        if (File::exists(public_path().$category->image))
        {
            File::delete(public_path().$category->image);
        }
        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/categories');
        $image->move($destinationPath, $imageName);
        $category->image ='/uploads/categories/'.$imageName;
      }

      $category->save();
      return response()->json(['status' => 'success', 'category' => $category]);
  }

  public function delete_categorys_main(Request $request)
  {

      $validator = Validator::make($request->all(), ['id' => 'required:exists:categories,id']);
      if($validator->fails())
      {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
      }

      $category = Category::where([['id', $request->id], ['deleted_at', null]])->first();
      if($category)
      {
          $category->delete();
          return response()->json(['status' => 'success', 'message' =>'Delete Done']);
      }else{
          return response()->json(['status' => 'error', 'message' =>'This ID Deleted']);
      }

  }

  //======================= Categories Sub ========================
  public function categories_sub()
  {
    $categories = Category::where('cat', '!=', 0)->where('deleted_at', null)->get();
    $data = $categories->map(function($category) {
        if($category['image'] != null && File::exists(public_path().$category['image']))
        {
            $category->image = asset($category->image);
        }else{
            $category->image = 'Image Not Exists In Path';
        }
        return $category;
    });
    return response()->json(['status' => 'success', 'categories' => $data]);
  }

  public function show_categories_sub(request $request)
  {

    $validator = Validator::make($request->all(), [
          'id'             => 'required|exists:categories,id|integer',
      ],
      [
          'id.exists'      => 'This Id Not Exists',
          'id.integer'     => 'This Id Not Integer',
      ]);

      if($validator->fails())
      {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
      }

      $category = Category::where('id', $request->id)->first();

      if($category->cat == 0)
      {
          return response()->json(['status' => 'error', 'message' => 'This is Not a sub Category']);
      }
      return response()->json(['status' => 'success', 'category' => $category]);
  }

  public function add_categorys_sub(Request $request)
  {
    $validator = Validator::make($request->all(), [
          'title_en' => 'required',
          'title_ar' => 'required',
          'cat'      => 'required|exists:categories,id|integer',
          'image'    => 'sometimes|nullable|mimes:jpeg,png,jpg,gif,svg,webp'
      ],
      [
          'title_en.required'        =>'Please Enter Category Name (EN)',
          'title_ar.required'        =>'Please Enter Category Name (AR)',
          'cat.exists'               => 'This Id Not Exists',
          'cat.integer'               => 'This Id Not Integer',
          'image.mimes'             => 'Please Choose Valid Image File'
      ]);

      if($validator->fails())
      {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
      }

      $category = new Category;
      $category->title_en  = $request->title_en;
      $category->title_ar  = $request->title_ar;
      $category->cat  = $request->cat;

      if($request->has('image') && $request->image != null)
      {
        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/categories');
        $image->move($destinationPath, $imageName);
        $category->image ='/uploads/categories/'.$imageName;
      }

      $category->save();

      return response()->json(['status' => 'success', 'category' => $category]);
  }


  public function edit_categorys_sub(Request $request)
  {
    $validator = Validator::make($request->all(), [
          'title_en' => 'required',
          'title_ar' => 'required',
          'cat'      => 'required|exists:categories,id|integer',
          'id'        => 'required|exists:categories,id|integer',
          'image'    => 'sometimes|nullable|mimes:jpeg,png,jpg,gif,svg,webp'
      ],
      [
          'title_en.required'        =>'Please Enter Category Name (EN)',
          'title_ar.required'        =>'Please Enter Category Name (AR)',
          'cat.exists'               => 'This Id Not Exists',
          'cat.integer'              => 'This Id Not Integer',
          'id.integer'               => 'This Id Not Integer',
          'id.exists'                => 'This Id Not Exists',
          'image.mimes'              => 'Please Choose Valid Image File'
      ]);

      if($validator->fails())
      {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
      }

      $category = Category::where('id', $request->id)->first();
      $category->title_en  = $request->title_en;
      $category->title_ar  = $request->title_ar;
      $category->cat  = $request->cat;

      if($request->has('image') && $request->image != null)
      {
        if (File::exists(public_path().$category->image))
        {
            File::delete(public_path().$category->image);
        }
        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/categories');
        $image->move($destinationPath, $imageName);
        $category->image ='/uploads/categories/'.$imageName;
      }

      $category->save();
      return response()->json(['status' => 'success', 'category' => $category]);
  }

  public function delete_categorys_sub(Request $request)
  {

      $validator = Validator::make($request->all(), ['id' => 'required:exists:categories,id']);
      if($validator->fails())
      {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
      }

      $category = Category::where([['id', $request->id], ['deleted_at', null], ['cat', '!=', 0]])->first();
      if($category)
      {
          $category->delete();
          return response()->json(['status' => 'success', 'message' =>'Delete Done']);
      }else{
          return response()->json(['status' => 'error', 'message' =>'This ID Deleted']);
      }

  }
}
