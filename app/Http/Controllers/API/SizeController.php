<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Size;
use Validator;

class SizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sizes()
    {
        $sizes = Size::select('id', 'title')->get();
        return response()->json(['status' => 'success', 'data' => $sizes]);
    }


    public function add_sizes(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ],
        [
            'title.required'  => 'ادخل المقاس'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $size = new Size();
        $size->title = $request->title;
        $size->save();
        return response()->json(['status' => 'success', 'data' => $size]);
    }


    public function edit_sizes(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'id'    => 'required|exists:sizes,id'
        ],
        [
            'title.required'  =>  'ادخل المقاس',
            'id.exists'    =>  'المقاس ليس موجود بقاعدة البيانات'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $size = Size::findOrFail($request->id);
        $size->title = $request->title;
        $size->save();
        return response()->json(['status' => 'success', 'data' => $size]);
    }


    public function delete_sizes(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id'    => 'required|exists:sizes,id'
        ],
        [
            'id.required'  =>  'Enter Id Size',
            'id.exists'    =>  'المقاس ليس موجود بقاعدة البيانات'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $size = Size::findOrFail($request->id);
        $size->delete();
        return response()->json(['status' => 'success', 'message' => 'Deleted Done']);
    }
}
