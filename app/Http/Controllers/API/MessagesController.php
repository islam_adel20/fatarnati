<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Models\Message;
use Carbon\Carbon;
use Validator;

class MessagesController extends Controller
{       
    public function sendMessage(Request $request)
    {
        //===========> Send Message From User To Saller & Saller Services 
        if($request->message_from == 'user')
        {       
         
            $room_id = "";
            /** If Send To Saller */
            if($request->message_to == 'saller')
            {     
                $validator = Validator::make($request->all(), [
                    'saller_id'     => 'required|exists:galleries,id',
                ],
                [
                    'required'          => 'This Required',
                    'saller_id.exists'  => 'This Saller Not Found',
                ]);
        
                if ($validator->fails()) {
                    return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
                }
              
                $check_room = Room::where([['user_id', auth('api')->user()->id], ['saller_id', $request->saller_id]])->first();
                if(!$check_room)
                {     
                    $insert_room = Room::create([
                        'user_id'   => auth('api')->user()->id,
                        'saller_id' => $request->saller_id
                    ]);

                    $room_id = $insert_room->id;
                }else{
                    $room_id = $check_room->id;
                }
            }

            /** If Send To Saller Services */
            if($request->message_to == 'saller_services')
            {   
                $validator = Validator::make($request->all(), [
                    'saller_id'     => 'required|exists:service_sallers,id',
                ],
                [
                    'required'          => 'This Required',
                    'saller_id.exists'  => 'This Saller Not Found',
                ]);
        
                if ($validator->fails()) {
                    return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
                }
                $check_room = Room::where([['user_id', auth('api')->user()->id], ['saller_services_id', $request->saller_id]])->first();
                if(!$check_room)
                {
                    $insert_room = Room::create([
                        'user_id'            => auth('api')->user()->id,
                        'saller_services_id' => $request->saller_id
                    ]);

                    $room_id = $insert_room->id;
                }else{
                
                    $room_id = $check_room->id;
                }
            }

            /** Send Message */
            $message = Message::create([
                'room_id'       => $room_id,
                'sender_id'     => auth('api')->user()->id,
                'message_from'  => $request->message_from,
                'message'       => $request->message,
                'created_at'    => Carbon::now()
            ]);

            return response()->json(['status' => true, 'message' => 'Sent succesfully', 'data' => $message]);
            
         
        }

        //===========> Send Message From Saller To User   
        if($request->message_from == 'saller'){
           
            if($request->message_to == 'user')
            {    
                $validator = Validator::make($request->all(), [
                    'user_id'     => 'required|exists:users,id',
                ],
                [
                    'required'          => 'This Required',
                    'user_id.exists'    => 'This User Not Found',
                ]);
        
                if ($validator->fails()) {
                    return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
                }

                $room_id = "";
                $check_room = Room::where([['saller_id', auth('api_saller')->user()->id], ['user_id', $request->user_id]])->first();
                if(!$check_room)
                {   
                    $insert_room = Room::create([
                        'saller_id'   => auth('api_saller')->user()->id,
                        'user_id'     => $request->user_id
                    ]);

                    $room_id = $insert_room->id;
                   
                }else{
                    $room_id = $check_room->id;
                }

                 /** Send Message */
                $message = Message::create([
                    'room_id'       => $room_id,
                    'sender_id'     => auth('api_saller')->user()->id,
                    'message_from'  => 'saller',
                    'message'       => $request->message,
                    'created_at'    => Carbon::now()
                ]);

                return response()->json(['status' => true, 'message' => 'Sent succesfully', 'data' => $message]);
            }

        }

         //===========> Send Message From Saller To User   
         if($request->message_from == 'saller_services'){
           
            if($request->message_to == 'user')
            {    
                $validator = Validator::make($request->all(), [
                    'user_id'     => 'required|exists:users,id',
                ],
                [
                    'required'          => 'This Required',
                    'user_id.exists'    => 'This User Not Found',
                ]);
        
                if ($validator->fails()) {
                    return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
                }

                $room_id = "";
                $check_room = Room::where([['saller_services_id', auth('api_service_sallers')->user()->id], ['user_id', $request->user_id]])->first();
                if(!$check_room)
                {   
                    $insert_room = Room::create([
                        'saller_services_id'   => auth('api_service_sallers')->user()->id,
                        'user_id'     => $request->user_id
                    ]);

                    $room_id = $insert_room->id;
                   
                }else{
                    $room_id = $check_room->id;
                }

                 /** Send Message */
                $message = Message::create([
                    'room_id'       => $room_id,
                    'sender_id'     => auth('api_service_sallers')->user()->id,
                    'message_from'  => 'saller_services',
                    'message'       => $request->message,
                    'created_at'    => Carbon::now()
                ]);

                return response()->json(['status' => true, 'message' => 'Sent succesfully', 'data' => $message]);
            }
        }
        
    }

     /** Show Saller Services Rooms */
     public function sallerRooms()
     {
         $saller_id = auth('api_saller')->user()->id;
         $rooms = Room::where('saller_id', $saller_id)->get();
         return response()->json(['status' => true, 'rooms' => $rooms]);
     }

    /** Show Saller Services Rooms */
    public function sallerServicesRooms()
    {
        $saller_id = auth('api_service_sallers')->user()->id;
        $rooms = Room::where('saller_services_id', $saller_id)->get();
        return response()->json(['status' => true, 'rooms' => $rooms]);
    }


    
    /** Show Messages By Rooms */
    public function showMessages(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'room_id'     => 'required|exists:rooms,id',
        ],
        [
            'required'          => 'This Required',
            'room_id.exists'    => 'This Room Not Found',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $messages = Message::where('room_id', $request->room_id)->get();
        return response()->json(['status' => true, 'messages' => $messages]);
    }

    /** Show Messages By Rooms */
    public function showSallerMessages(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'room_id'     => 'required|exists:rooms,id',
        ],
        [
            'required'          => 'This Required',
            'room_id.exists'    => 'This Room Not Found',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $messages = Message::where('room_id', $request->room_id)->get();
        return response()->json(['status' => true, 'messages' => $messages]);
    }

    
}