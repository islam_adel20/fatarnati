<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Social;
use Validator;

class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function soical()
    {
        $soical = Social::all();
        return response()->json(['status' => 'success', 'data' => $soical], 200);
    }


    public function add_soical(Request $request)
    {

      $validator = Validator::make($request->all(), [
            'name' => 'required',
            'link' => 'required|url'
        ],
        $messages = [
            'name.required' => 'من فضلك ادخل اسم الميديا',
            'link.required' => 'من فضلك ادخل رابط الميديا',
            'link.url'      => 'يجب ان يكون رابطا'
        ]);

        if($validator->fails())
        {
            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $social = new Social;
        $social->name = $request->name;
        $social->link = $request->link;
        $social->save();
        return response()->json(['status' => 'success', 'data' => $social], 200);
    }



    public function edit_soical(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'name' => 'required',
            'link' => 'required|url',
            'id'   => 'required|exists:socials,id|integer'
        ],
        $messages = [
            'name.required' => 'من فضلك ادخل اسم الميديا',
            'link.required' => 'من فضلك ادخل رابط الميديا',
            'link.url'      => 'يجب ان يكون رابطا',
            'id.integer'    => 'يجب ان يكون رقما',
            'id.exists'            => 'الميديا ليست مسجلة',
        ]);

        if($validator->fails())
        {
            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $social = Social::findOrFail($request->id);
        $social->name = $request->name;
        $social->link = $request->link;
        $social->save();
        return response()->json(['status' => 'success', 'message' => 'Updated Done', 'data' => $social], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete_soical(Request $request)
    {

      $validator = Validator::make($request->all(), [
            'id'   => 'required|exists:socials,id|integer'
        ],
        $messages = [
            'id.integer'    => 'يجب ان يكون رقما',
            'id.exists'     => 'الميديا ليست مسجلة',
        ]);

        if($validator->fails())
        {
            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $social = Social::findOrFail($request->id);
        $social->delete();
        return response()->json(['status' => 'success', 'message' => 'Deleted Done'], 200);
    }
}
