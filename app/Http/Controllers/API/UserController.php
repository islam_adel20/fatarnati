<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cookie;
use App\Models\Country;
use App\Models\User;
use App\Models\Product;
use App\Models\ProductFavorite;
use App\Models\Order;
use App\Models\Gallery;
use App\Models\ServiceSaller;
use App\Models\City;
use Validator;
use File;
use Hash;
use App;
use Mail;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name'      => 'required',
            'last_name'       => 'required',
            'phone'           => 'required',
            'email'           => 'required|email|unique:users,email',
            'password'        => 'required|min:6',
            'password_confirmation' => 'required_with:password|same:password|min:6',
            'map'               => 'sometimes|nullable',
            'soical_name'               => 'sometimes|nullable',
            'city_id'         => 'required|exists:cities,id',
            'region_id'       => 'required|exists:regions,id',
        ],
        [
            'first_name.required'         =>  'Please Enter Your First Name',
            'last_name.required'          =>  'Please Enter Your Last Name',
            'email.required'              =>  'Please Enter Email Address',
            'email.email'                 =>  'Please Enter Valid Email Address',
            'email.unique'                =>  'This Email Address Is Already Registered',
            'password.required'           =>  'Please Enter Password',
            'password.min'                =>  'Password Must Be 6 Charachters At Least',
            'password.confirmed'          =>  'Password & Its Confirmation Not Matching',
            'city_id.required'    =>'Please Enter City',
           'region_id.required'  =>'Please Enter Region',
        ]);

        if ($validator->fails())
        {
            return response()->json(['status' => 'error','message' => 'Invalid Inputs','errors' => $validator->errors()]);
        }

        $activateToken = str_random(10).time();
        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->phone = $request->phone;
        $user->name = $request->first_name." ".$request->last_name;
        if($request->country) {$user->country = $request->country;}
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->activate_token = $activateToken;
        $user->active = 1;
        $user->map = $request->map;
        $user->soical_name = $request->soical_name;
        $user->city  = $request->city_id;
        $user->region_id  = $request->region_id;
        $user->save();

        $token = $user->createToken('ASASK')->accessToken;

        return response()->json([
            'status' => 'success',
            'message' => 'Account Created Successfully.',
            'user_id' => $user->id,
            'token' => $token
        ], 200);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
  			 'email'         =>  'required|email',
             'password'      =>  'required'
          ],
          [
              'email.required'      =>  'Please Enter Email Address',
              'email.email'         =>  'Please Enter Valid Email Address',
              'password.required'   =>  'Please Enter Password'
          ]);

          if ($validator->fails()) {
              return response()->json([
                  'status'      => 'fail',
                  'message'     => 'Invalid Inputs',
                  'errors'      => $validator->errors()
              ]);
          }

        $signinEmail = $request->email;
        $signinPassword = $request->password;

        $user = User::where('email', '=', $signinEmail)->first();

        if (empty($user)) {
            return response()->json([
                'status'  => 'fail',
                'message' => 'Wrong In Email Address Or Password',
            ], 401);
        }
        else if(!Hash::check($signinPassword, $user->password))
        {
            return response()->json([
                'status'  => 'fail',
                'message' => 'Wrong In Email Address Or Password',
            ]);
        }
        else if($user->active == 0)
        {
            return response()->json([
                'status'  => 'fail',
                'message' => 'Please Activate Your Account First',
            ]);
        }
        else
        {
            $token = $user->createToken('ASASK')->accessToken;

            return response()->json([
                'status' => 'success',
                'message' => 'Logged in succfully',
                'user_id' => $user->id,
                'token' => $token
            ], 200);
        }
    }

    public function logout(Request $request)
    {
        $token = auth('api')->user()->token();
        $token->revoke();

        return response()->json([
            'status' => 'success',
            'message' => 'Logged out succfully'
        ], 200);
    }

    public function profileUser()
    {

          $check_user = User::find(auth('api')->user()->id);
          if($check_user)
          {
            $user = User::where('id', $check_user->id)->first();
            return response()->json([
                'status'  => 'success',
                'message' => 'Your Info Loaded Successfully',
                'user_information' => [
                    'id'          => auth('api')->user()->id,
                    'name'        => auth('api')->user()->name,
                    'first_name'  => auth('api')->user()->first_name,
                    'last_name'   => auth('api')->user()->last_name,
                    'email'       => auth('api')->user()->email,
                    'phone'       => auth('api')->user()->phone,
                    'address'     => auth('api')->user()->address,
                    'city'        => auth('api')->user()->city,
                    'region_id'   => auth('api')->user()->region_id
                  ]
            ]);

          }else {
            return response()->json(['status'  => 'error','message' => 'Invild Token']);
          }
    }

      public function editUser(Request $request)
      {
            $check_user = User::find(auth('api')->user()->id);

            if($check_user)
            {
                  $validator = Validator::make($request->all(), [
                      'first_name'      => 'required',
                      'last_name'       => 'required',
                      'phone'           => 'required',
                      'email'           => 'required|email|unique:users,email,'.$check_user->id,
                      'map'             => 'sometimes|nullable',
                      'soical_name'     => 'sometimes|nullable',
                      'city_id'         => 'required|exists:cities,id',
                      'region_id'       => 'required|exists:regions,id',
                  ],
                  [
                      'first_name.required'         =>  'Please Enter Your First Name',
                      'last_name.required'          =>  'Please Enter Your Last Name',
                      'email.required'              =>  'Please Enter Email Address',
                      'email.email'                 =>  'Please Enter Valid Email Address',
                      'email.unique'                =>  'This Email Address Is Already Registered',
                      'city_id.required'    =>'Please Enter City',
                      'region_id.required'  =>'Please Enter Region',
                  ]);

                  if ($validator->fails()) {
                      return response()->json([
                          'status' => 'fail',
                          'message' => 'Invalid Inputs',
                          'errors' => $validator->errors()
                      ], 400);
                  }

                  $user =  User::where('id', $request->id)->first();
                  $user->first_name = $request->first_name;
                  $user->last_name = $request->last_name;
                  $user->phone = $request->phone;
                  $user->name = $request->first_name." ".$request->last_name;
                  if($request->country) {$user->country = $request->country;}
                  $user->email = $request->email;
                  $user->map = $request->map;
                  $user->soical_name = $request->soical_name;
                  $user->city  = $request->city_id;
                  $user->region_id  = $request->region_id;
                  $user->update();
                  return response()->json([
                      'status'      => 'success',
                      'message'     => 'Updated Successfully.',
                      'data'        => $user
                  ], 200);
              }
      }

    //=========================== [product Favorite User]

      public function productFavorite(Request $request)
      {
 
          $products = ProductFavorite::where('user_id', auth('api')->user()->id)->with('user', 'product', 'saller')->get();

          if($products)
          {
              
              $results = [];
               foreach($products as $image){
                   
                   if(File::exists(public_path($image->product['photo'])))
                   {
                    $image->product['photo'] =  url($image->product['photo']);
                   }else{
                    $image->product['photo'] = "Not Found Image In Path";
                   }
                   $image->makeHidden(['created_at', 'updated_at', 'stock_qt']);
                  array_push($results, $image);
               } 
               return response()->json($results);
            //return response()->json(['status' => 'success','data' => $products], 200);
          }else{
            return response()->json(['status' => 'error', 'message' => 'لا يوجد منتجات مفضلة مسجلة']);
          }
      }

      public function addProductFavorite(Request $request)
      {

          $validator = Validator::make($request->all(), [
              'product_id'             => 'required|exists:products,id|integer'
          ],
          [
              'product_id.required'    =>  'يجب ملئ حقل المنتج',
              'product_id.integer'     =>  'يجب ان يكون المنتج رقما',
              'product_id.exists'      =>  'هذا المنتج ليس موجودا في قاعدة البيانات',
          ]);

          if ($validator->fails()) {
              return response()->json(['status' => 'error','message' => 'Invalid Inputs','errors' => $validator->errors()]);
          }

          $product = Product::where('id', $request->product_id)->first();

          if($product)
          {
            $ProductFavorite = ProductFavorite::create([
                'user_id'     => auth('api')->user()->id,
                'product_id'  => $product->id,
                'saller_id'   => $product->gallery_id
            ]);

            return response()->json([
                'status'      => 'success',
                'data'        => $ProductFavorite
            ], 200);
          }else{
            return response()->json(['status' => 'error', 'message' => 'هذا المنتج ليس موجودا في قاعدة البيانات']);
          }
      }

      public function deleteProductFavorite(Request $request)
      {

          $validator = Validator::make($request->all(), [
              'product_id'             => 'required|exists:products,id|integer'
          ],
          [
              'product_id.required'    =>  'يجب ملئ حقل المنتج',
              'product_id.integer'     =>  'يجب ان يكون المنتج رقما',
              'id.exists'      =>  'هذا المنتج ليس موجودا في قاعدة البيانات',
          ]);

          if ($validator->fails()) {
              return response()->json(['status' => 'error','message' => 'Invalid Inputs','errors' => $validator->errors()]);
          }

          $ProductFavorite = ProductFavorite::where('product_id', $request->product_id)->first();

          if($ProductFavorite)
          {
            $ProductFavorite->delete();

            return response()->json(['status' => 'Success', 'message' => 'Deleted Done'], 200);
          }else{
            return response()->json(['status' => 'error', 'message' => 'هذا المنتج ليس موجودا في قاعدة البيانات']);
          }
      }


      public function resetPassword(Request $request)
      {
        
        $user = User::where('email', $request->email)->first();
        $saller = Gallery::where('email', $request->email)->first();
        $saller_services = ServiceSaller::where('email', $request->email)->first();
        $new_password = str_random(8);
        /** Users */
        if($user){
            $data = User::where('id', $user->id)->update(['password' => Hash::make($new_password)]);
            Mail::to($request->email)->send(new \App\Mail\resetPassword($new_password));
            return response()->json(['status' => true, 'message' => 'Successe Send Message']);
        /** Sallers */
        }elseif($saller){
            $data = Gallery::where('id', $saller->id)->update(['password' => Hash::make($new_password)]);
            Mail::to($request->email)->send(new \App\Mail\resetPassword($new_password));
            return response()->json(['status' => true, 'message' => 'Successe Send Message']);
        /** Saller Services */
        }elseif($saller_services){
            $data = ServiceSaller::where('id', $saller_services->id)->update(['password' => Hash::make($new_password)]);
            Mail::to($request->email)->send(new \App\Mail\resetPassword($data));
            return response()->json(['status' => true, 'message' => 'Successe Send Message']);
        }else{
            return response()->json(['status' => false, 'message' => 'This Email Not Found']);
        }
       
    }

}
