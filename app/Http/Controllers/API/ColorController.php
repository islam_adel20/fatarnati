<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Color;
use Validator;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function colors()
    {
        $colors = Color::select('id', 'title', 'color')->get();
        return response()->json(['status' => 'success', 'data' => $colors]);
    }


    public function add_colors(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'color' => 'required'
        ],
        [
            'required'  => 'ادخل اللون'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $color = new Color();
        $color->title = $request->title;
        $color->color = $request->color;
        $color->save();
        return response()->json(['status' => 'success', 'data' => $color]);
    }


    public function edit_colors(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'color' => 'required',
            'id'    => 'required|exists:colors,id'
        ],
        [
            'required'      => 'required',
            'id.exists'     =>  'Id Color Not Found'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $color = Color::findOrFail($request->id);
        $color->title = $request->title;
        $color->color = $request->color;
        $color->save();
        return response()->json(['status' => 'success', 'data' => $color]);
    }


    public function delete_colors(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id'    => 'required|exists:colors,id'
        ],
        [
            'id.required'  =>  'Enter Id Colors',
            'id.exists'    =>  'Id Color Not Found'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $color = Color::findOrFail($request->id);
        $color->delete();
        return response()->json(['status' => 'success', 'message' => 'Deleted Done']);
    }
}
