<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cookie;
use App\Http\Traits\ApiDesignTrait;
use App\Models\User;
use App\Models\ServiceSaller;
use App\Models\City;
use App\Models\SallerCode;
use  Carbon\Carbon;
use Validator;
use App;
use DB;
use Hash;
use File;

class SallersServicesController extends Controller
{

  use ApiDesignTrait;

  public function login(Request $request)
  {

      $validator = Validator::make($request->all(), [
            'phone'           =>'required',
            'password'        =>'required'
        ],
        [
            'phone.required'   => 'من فضلك ادخل رقم الموبايل',
            'password.required' => 'من فضلك ادخل كلمة السر'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

      $signinPhone = $request->phone;
      $signinPassword = $request->password;

      $saller = ServiceSaller::where('phone', '=', $signinPhone)->where('status', '1')->first();

      $date_now = Carbon::now()->toDateString();
      $code = SallerCode::where([['code', $saller->code], ['status', 'expired']])
      ->where('to', '>=', $date_now)->first();

      if(!$code){
        return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => 'الكود غير صحيح']);
      }

      
      if (empty($saller)) {

            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => 'Wrong In Code Or Password']);
      }elseif(!Hash::check($signinPassword, $saller->password))
      {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => 'Wrong In Code Address Or Password']);
      }

      $token = $saller->createToken('ASASK')->accessToken;
      return response()->json(['status' => true, 'message' => 'Login succfully', 'token' => $token]);
  }

  public function all_sellers_services()
  {
      $saller_servcies = ServiceSaller::with('cat')->get();
      $saller_servcies->makeHidden(['code', 'created_at', 'updated_at']);
      
      $results = [];

       foreach($saller_servcies as $image){
          $image->image =  url($image->image);
          array_push($results, $image);
       } 
       return response()->json($results);
       
      //return response()->json(['status' => 'success', 'data' => $saller_servcies]);
  }

  public function show_saller_services(Request $request)
  {
    $id = $request->id;
    if (empty($id)) {
        return response()->json(['status'  => 'fail','message' => 'Invalid Saller ID']);
    }

    $saller = ServiceSaller::where('id', $id)->first();
    
    if($saller) {
        $saller['image'] = asset($saller->image);
        $saller->update(['views' => $saller->views+1]);
        return response()->json(['status' => 'success', 'data' => $saller]);
    }else {
        return response()->json(['status' => 'fail','message' => 'Saller Not Found']);
    }

  }

  public function profile_saller_services(Request $request)
  {
    $id = auth('api_service_sallers')->user()->id;
    if (empty($id)) {
        return response()->json(['status'  => 'error', 'message' => 'Invalid Saller ID']);
    }

    $saller = ServiceSaller::where('id', $id)->first();
    
    if($saller) {
        $saller['image'] = asset($saller->image);
        return response()->json(['status' => 'success', 'data' => $saller]);
    }else {
        return response()->json(['status' => 'fail','message' => 'Saller Not Found']);
    }
  }


  public function add_sallers_services(Request $request)
  {
    try {
      $validator = Validator::make($request->all(), [
          'name'            => 'required',
          'email'           => 'required',
          'phone'           => 'required',
          'password'        => 'required',
          'desc'            => 'required',
          'city_id'         => 'required|exists:cities,id',
          'region_id'       => 'required|exists:regions,id',
          'cat_id'          => 'required|required|exists:category_services,id',
          // 'image'           => 'sometimes|nullable|mimes:jpg,jpeg,png,svg',
          'instgram_link'   => 'nullable',
          'facebook_link'   => 'nullable',
          'twitter_link'    => 'nullable',
          'youtube_link'    => 'nullable',
          'map'             => 'nullable',
          'address'         => 'nullable'
      ],
      [
          'name.required'       => 'Please Enter Name',
          'email.required'      => 'Please Enter Email',
          'phone.required'      =>'Please Enter Phone',
          'desc.required'       =>'Please Enter Description',
          'password.required'   =>'Please Enter Password)',
          'city_id.required'    =>'Please Enter City',
          'region_id.required'  =>'Please Enter Region',
          'cat_id.required'     =>'Please Enter Category',
      ]);


        if($validator->fails())
        {
             return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $code = SallerCode::where([['code', $request->code], ['status', 'unactive']])->first();

        if(!$code){
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => 'الكود غير صحيح']);
        }

        $saller = new ServiceSaller;
        $saller->name  = $request->name;
        $saller->email  = $request->email;
        $saller->phone  = $request->phone;
        $saller->password = bcrypt($request->password);
        $saller->city_id  = $request->city_id;
        $saller->region_id  = $request->region_id;
        $saller->cat_id  = $request->cat_id;
        $saller->desc  = $request->desc;
        $saller->code = $request->code;
        $saller->address = $request->address;

        if($request->has('image') && $request->image != null)
        {
          $image = $request->file('image');
          $imageName = time().'.'.$image->getClientOriginalExtension();
          $destinationPath = public_path('/uploads/sallers_services');
          $image->move($destinationPath, $imageName);
          $saller->image ='/uploads/sallers_services/'.$imageName;
        }

        // $saller->facebook_link = $request->facebook_link;
        // $saller->instgram_link = $request->instgram_link;
        // $saller->twitter_link = $request->twitter_link;
        // $saller->youtube_link = $request->youtube_link;
        // $saller->whatsapp_link = $request->whatsapp_link;
        // $saller->mobile_link = $request->mobile_link;
        $saller->save();
        $sub_from = Carbon::now()->toDateString();
        $sub_to = Carbon::now()->addDays($code->duration)->toDateString();

        SallerCode::where('code', $request->code)->update([
            'status' => 'expired',
            'from'   => $sub_from,
            'to'     => $sub_to
          ]);

          
      $token = $saller->createToken('ASASK')->accessToken;
      return response()->json(['status' => true, 'message' => 'Successfully Added', 'data' => $saller, 'token' => $token]);
    } catch (\Exception $ex) {
      return response()->json(['status' => false, 'message' => 'Exception Error', 'error' => $ex]);
    }

  }


  public function edit_sallers_services(Request $request)
  {
    try {
      $validator = Validator::make($request->all(), [
          'name'            => 'required',
          'email'           => 'required',
          'phone'           => 'required',
          //'password'        => 'sometimes|nullable',
          'desc'            => 'required',
          'city_id'         => 'required|exists:cities,id',
          'region_id'       => 'required|exists:regions,id',
          'cat_id'          => 'required|required|exists:category_services,id',
          'image'           => 'sometimes|nullable|mimes:jpg,jpeg,png,svg',
          'instgram_link'   => 'nullable',
          'facebook_link'   => 'nullable',
          'twitter_link'    => 'nullable',
          'youtube_link'    => 'nullable',
          'map'             => 'nullable',
          'address'         => 'nullable'
      ],
      [
          'name.required'       => 'Please Enter Name',
          'email.required'      => 'Please Enter Email',
          'phone.required'      =>'Please Enter Phone',
          'desc.required'       =>'Please Enter Description',
          'city_id.required'    =>'Please Enter City',
          'region_id.required'  =>'Please Enter Region',
          'cat_id.required'     =>'Please Enter Category',
      ]);

        if($validator->fails())
        {
             return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $saller = ServiceSaller::where('id', $request->id)->first();

        if($saller)
        {
          $saller->name  = $request->name;
          $saller->email  = $request->email;
          $saller->phone  = $request->phone;
        //   if($request->has('password') && $request->password != null)
        //   {
        //     $saller->password = bcrypt($request->password);
        //   }
          $saller->city_id  = $request->city_id;
          $saller->region_id  = $request->region_id;
          $saller->cat_id  = $request->cat_id;
          $saller->desc  = $request->desc;
          $saller->address = $request->address;
          $saller->facebook_link = $request->facebook_link;
          $saller->instgram_link = $request->instgram_link;
          $saller->twitter_link = $request->twitter_link;
          $saller->youtube_link = $request->youtube_link;
          $saller->whatsapp_link = $request->whatsapp_link;
          $saller->mobile_link = $request->mobile_link;
            
          if($request->has('image') && $request->image != null)
          {
            if (File::exists(public_path().$saller->image))
            {
                File::delete(public_path().$saller->image);
            }

            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/sallers_services');
            $image->move($destinationPath, $imageName);
            $saller->image ='/uploads/sallers_services/'.$imageName;
          }

          $saller->instgram_link   = $request->instgram_link;
          $saller->facebook_link   = $request->facebook_link;
          $saller->twitter_link    = $request->twitter_link;
          $saller->youtube_link    = $request->youtube_link;
          $saller->save();
          return response()->json(['status' => 'success', 'data' => $saller]);
          }else{
             return response()->json(['status' => 'error', 'message' => 'This ID Not Found In Database']);
          }
      } catch (\Exception $ex) {
         return response()->json(['status' => 'error', 'message' => 'Exception Erro', 'data' => $ex]);
    }

  }

  public function logout(Request $request)
  {
      $token = auth('api_service_sallers')->user()->token();
      $token->revoke();
      return $this->ApiResponse(200, 'Logged out succfully');

  }
}
