<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RateSaller;
use App\Models\RateSallerServices;
use Validator;

class ReviewsController extends Controller
{       

    //================ Review Saller
    /** Show Review User To Saller */
    public function reviewSaller(Request $request)
    {
        $reivew_saller = RateSaller::with('user')->where('saller_id', $request->saller_id)->get();
        return response()->json(['status' => true, 'data' => $reivew_saller]);
    }

    /** Add Reivew From User To Saller */
    public function addReviewSallers(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'saller_id'     => 'required|exists:galleries,id',
            'review'        => 'required|integer',
            'comment'       => 'required',
        ],
        [
            'required'          => 'This Required',
            'user_id.exists'    => 'This User Not Found',
            'saller_id.exists'  => 'This Saller Not Found',
            'review.integer'    => 'This Review Not Integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $data = RateSaller::create([
            'user_id'       => auth('api')->user()->id,
            'saller_id'     => $request->saller_id,
            'review'        => $request->review,
            'comment'       => $request->comment
        ]);

        return response()->json(['status' => true, 'message' => 'Added successfully', 'data' => $data]);

    }

    /** Edit Reivew From User To Saller */
    public function editReviewSallers(Request $request)
    {   

        $check_review = RateSaller::where('id', $request->review_id)->first();
        if($check_review)
        {
            $validator = Validator::make($request->all(), [
                'saller_id'     => 'required|exists:galleries,id',
                'review'        => 'required|integer',
                'comment'       => 'required',
            ],
            [
                'required'          => 'This Required',
                'user_id.exists'    => 'This User Not Found',
                'saller_id.exists'  => 'This Saller Not Found',
                'review.integer'    => 'This Review Not Integer'
            ]);
    
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
            }
    
            $check_review->update([
                'user_id'       => auth('api')->user()->id,
                'saller_id'     => $request->saller_id,
                'review'        => $request->review,
                'comment'       => $request->comment
            ]);
    
            return response()->json(['status' => true, 'message' => 'Added successfully', 'data' => $check_review]);
        }else{
            return response()->json(['status' => false, 'message' => 'Review_ID Not Found']);
        }
        
    }

    //====================== Review Saller Services
    /** Show Review User To Saller */
    public function reviewSallerServices(Request $request)
    {
        $reivew_saller = RateSallerServices::with('user')->where('saller_id', $request->saller_id)->get();
        return response()->json(['status' => true, 'data' => $reivew_saller]);
    }

    /** Add Reivew From User To Saller */
    public function addReviewSallersServices(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'saller_id'     => 'required|exists:service_sallers,id',
            'review'        => 'required|integer',
            'comment'       => 'required',
        ],
        [
            'required'          => 'This Required',
            'user_id.exists'    => 'This User Not Found',
            'saller_id.exists'  => 'This Saller Not Found',
            'review.integer'    => 'This Review Not Integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }
   
        $data = RateSallerServices::create([
            'user_id'       => auth('api')->user()->id,
            'saller_id'     => $request->saller_id,
            'review'        => $request->review,
            'comment'       => $request->comment
        ]);

        return response()->json(['status' => true, 'message' => 'Added successfully', 'data' => $data]);

    }

    /** Edit Reivew From User To Saller */
    public function editReviewSallersServices(Request $request)
    {   

        $check_review = RateSallerServices::where('id', $request->review_id)->first();
        if($check_review)
        {
            $validator = Validator::make($request->all(), [
                'saller_id'     => 'required|exists:service_sallers,id',
                'review'        => 'required|integer',
                'comment'       => 'required',
            ],
            [
                'required'          => 'This Required',
                'user_id.exists'    => 'This User Not Found',
                'saller_id.exists'  => 'This Saller Not Found',
                'review.integer'    => 'This Review Not Integer'
            ]);
    
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
            }
    
            $check_review->update([
                'user_id'       => auth('api')->user()->id,
                'saller_id'     => $request->saller_id,
                'review'        => $request->review,
                'comment'       => $request->comment
            ]);
    
            return response()->json(['status' => true, 'message' => 'Added successfully', 'data' => $check_review]);
        }else{
            return response()->json(['status' => false, 'message' => 'Review_ID Not Found']);
        }
        
    }

    
}
