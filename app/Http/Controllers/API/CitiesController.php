<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Models\Region;
use App\Models\City;

class CitiesController extends Controller
{
  public function cities_with_reigons()
  {
    $city_with_region = Region::with('city')->get();
    return response()->json(['status' => 'success', 'city_with_region' => $city_with_region]);
  }

  public function city()
  {
    $city = City::select('id', 'name_en', 'name_ar')->get();
    return response()->json(['status' => 'success', 'cities' => $city]);
  }

  public function regions()
  {
    $reigons = Region::select('id', 'name_en', 'name_ar', 'city_id')->get();
    return response()->json(['status' => 'success', 'reigons' => $reigons]);
  }
}
