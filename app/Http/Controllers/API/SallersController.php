<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cookie;
use App\Http\Traits\ApiDesignTrait;
use App\Models\Subscriber;
use App\Models\Slider;
use App\Models\Country;
use App\Models\User;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\ProductColor;
use App\Models\Order;
use App\Models\Category;
use App\Models\Gallery;
use App\Models\City;
use App\Models\SallerCode;
use  Carbon\Carbon;
use Validator;
use Cart;
use App;
use DB;
use Hash;
use File;

class SallersController extends Controller
{

    use ApiDesignTrait;

    public function codes()
    {
    
      $codes = SallerCode::where('status', 'unactive')->get();
      return response()->json(['status' => true, 'codes' => $codes]);
    }
    
  public function login(Request $request)
  {

      $validator = Validator::make($request->all(), [
            'phone'         =>'required|exists:galleries,phone',
            'password'       =>'required'
        ],
        [
            'phone.required'    => 'Please Enter Number Phone',
            'phone.exists'      => 'Please Number Phone Not Exists',
            'password.required' => 'Please Enter Password'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

      $signinPhone = $request->phone;
      $signinPassword = $request->password;
      $saller = Gallery::where('phone', '=', $signinPhone)->where('status', '1')->first();
      
      $date_now = Carbon::now()->toDateString();
    
      $code = SallerCode::where([['code', $saller->code], ['status', 'expired']])
      ->where('to', '>=', $date_now)->first();
      if(!$code){
        return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => 'Code Not Correct']);
      }

      if (empty($saller)) {

            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => 'Wrong In Code Or Password']);
      }elseif(!Hash::check($signinPassword, $saller->password))
      {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => 'Wrong In Password']);
      }

      $token = $saller->createToken('Vatrinty')->accessToken;
      return response()->json(['status' => true, 'message' => 'Login succfully', 'token' => $token]);
  }

  public function all_sellers(Request $request)
  { 

      $cat_id = $request->cat_id;
      $galleries = Gallery::get();

      $galleries->each(function($item){
          $item->image = asset($item->image);
      });

      $galleries->each(function($item2){
          $item2->commercial_register = asset($item2->commercial_register);
      });

      $galleries->each(function($item3){
          $item3->nationalist = asset($item3->nationalist);
      });
        
      $galleries->makeHidden(['password', 'code', 'created_at', 'updated_at', 'activate_token', 'status']);
      return response()->json($galleries, 200);
  }

  public function all_sellers_cats(Request $request)
  { 
      $cat_id = $request->cat_id;

      $galleries = Gallery::whereHas('products', function($query) use($cat_id){
        $query->where('cat', $cat_id)->orWhere('sub_id', $cat_id);
      })->get();

      $galleries->each(function($item){
        $item->image = asset($item->image);
      });

      $galleries->each(function($item2){
          $item2->commercial_register = asset($item2->commercial_register);
      });

      $galleries->each(function($item3){
          $item3->nationalist = asset($item3->nationalist);
      });

      $galleries->makeHidden(['password', 'code', 'created_at', 'updated_at', 'activate_token', 'status']);

      if($galleries->count() == 0)
      {
        return response()->json(['status' => false, 'Message' => 'Not Found Sallers By This Cat_ID']);
      }else{
        return response()->json(['status' => true, 'data' => $galleries]);
      }
     
  }

  public function show_saller(Request $request)
  {
    $id = $request->id;
    if (empty($id)) {
        return response()->json(['status'  => 'fail','message' => 'Invalid Saller ID']);
    }

    $gallery = Gallery::where('id', $id)->first();

    if (!empty($gallery)) {

        $gallery->image = asset($gallery->image);
        $products = Product::where('gallery_id', $gallery->id)->with('images')->get();
        $products->each(function($product){
            $product->images->each(function($item){
                unset($item['id']);
                unset($item['product']);
                unset($item['created_at']);
                unset($item['updated_at']);
                $item->image = asset($item->image);
            });
        });

    }else {

        if (empty($product)) {
            return response()->json(['status' => 'fail','message' => 'Saller Not Found']);
        }
    }

    return response()->json(['saller' => $gallery, 'saller_products' => $products], 200);
  }


  public function profile_saller(Request $request)
  {
    $id = auth('api_saller')->user()->id;
    if (empty($id)) {
        return response()->json(['status'  => 'fail','message' => 'Invalid Saller ID']);
    }

    $gallery = Gallery::where('id', $id)->first();

    if (!empty($gallery)) {

        $gallery->image = asset($gallery->image);
        $products = Product::where('gallery_id', $gallery->id)->with('images')->get();
        $products->each(function($product){
            $product->images->each(function($item){
                unset($item['id']);
                unset($item['product']);
                unset($item['created_at']);
                unset($item['updated_at']);
                $item->image = asset($item->image);
            });
        });
    } else {

        if (empty($product)) {
            return response()->json(['status' => 'fail','message' => 'Saller Not Found']);
        }
    }

    return response()->json(['saller' => $gallery, 'saller_products' => $products], 200);
  }


  public function add_sallers(Request $request)
  {
    try {
        $validator = Validator::make($request->all(), [
            'name_en'                   => 'required',
            'name_ar'                   => 'required',
            'phone'                     => 'required',
            'address_en'                => 'required',
            'address_ar'                => 'required',
            'map'                       => 'required',
            'password'                  => 'required',
            'code'                      => 'required',
            'city_id'                   => 'required|exists:cities,id',
            'region_id'                 => 'required|exists:regions,id',
            'cat_id'                    => 'required|exists:categories,id',
            'sub_id'                    => 'required|exists:categories,id',
            // 'image'                     => 'sometimes|nullable|image|mimes:jpeg,jpg,png',
            // 'commercial_register'       => 'sometimes|nullable|image|mimes:jpeg,jpg,png',
            // 'nationalist'               => 'sometimes|nullable|image|mimes:jpeg,jpg,png'
        ],
        [
            'name_en.required'          =>  'Please Enter Saller Name (EN)',
            'name_ar.required'          =>  'Please Enter Saller Name (AR)',
            'phone.required'            =>  'Please Enter Saller Phone',
            'address_en.required'       =>  'Please Enter Saller Address (EN)',
            'address_ar.required'       =>  'Please Enter Saller Address (AR)',
            'map.required'              =>  'Please Enter Saller Map',
            'password.required'         =>  'Please Enter Saller Password',
            'code.required'             =>  'Please Enter Saller Code',
            'city_id.exists'            =>  'Please ID City Not Exists',
            'region_id.exists'          =>  'Please ID Region Not Exists',
            'cat_id.exists'             =>  'Please Main Category Not Exists',
            'sub_id.exists'             =>  'Please Sub Category Not Exists',
            // 'image.mimes'               =>  'Please Enter the correct encoding for the image',
            // 'commercial_register.mimes' =>  'Please Enter the correct encoding for the image',
            // 'nationalist.mimes'         =>  'Please Enter the correct encoding for the image'
        ]);

        if($validator->fails())
        {
            return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $code = SallerCode::where([['code', $request->code], ['status', 'unactive']])->first();

        if(!$code){
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => 'Code Not Correct']);
        }

        $gallery = new Gallery;
        $gallery->name_en  = $request->name_en;
        $gallery->name_ar  = $request->name_ar;
        $gallery->phone  = $request->phone;
        $gallery->address_en  = $request->address_en;
        $gallery->address_ar  = $request->address_ar;
        $gallery->map  = $request->map;
        $gallery->password = bcrypt($request->password);
        $gallery->city_id  = $request->city_id;
        $gallery->region_id  = $request->region_id;
        $gallery->cat_id  = $request->cat_id;
        $gallery->sub_id  = $request->sub_id;
        $gallery->code = $request->code;
        $gallery->status = 1;
        // $gallery->facebook_link = $request->facebook_link;
        // $gallery->instgram_link = $request->instgram_link;
        // $gallery->twitter_link = $request->twitter_link;
        // $gallery->youtube_link = $request->youtube_link;
        // $gallery->whatsapp_link = $request->whatsapp_link;
        // $gallery->mobile_link = $request->mobile_link;
        

        if($request->has('image') && $request->image != null)
        {
          $image = $request->file('image');
          $imageName = time().'.'.$image->getClientOriginalExtension();
          $destinationPath = public_path('/uploads/galleries');
          $image->move($destinationPath, $imageName);
          $gallery->image ='/uploads/galleries/'.$imageName;
        }

        if($request->has('commercial_register') && $request->commercial_register != null)
        {
          $commercial_register = $request->file('commercial_register');
          $imageNameCommercial = time().'.'.$commercial_register->getClientOriginalExtension();
          $destinationPath = public_path('/uploads/galleries');
          $commercial_register->move($destinationPath, $imageNameCommercial);
          $gallery->commercial_register ='/uploads/galleries/'.$imageNameCommercial;
        }

        if($request->has('nationalist') && $request->nationalist != null)
        {
          $nationalist = $request->file('nationalist');
          $imageNameNationalist = time().'.'.$nationalist->getClientOriginalExtension();
          $destinationPath = public_path('/uploads/galleries');
          $image->move($destinationPath, $imageNameNationalist);
          $gallery->nationalist ='/uploads/galleries/'.$imageNameNationalist;
        }

        $gallery->save();

        $sub_from = Carbon::now()->toDateString();
        $sub_to = Carbon::now()->addDays($code->duration)->toDateString();

        SallerCode::where('code', $request->code)->update([
            'status' => 'expired',
            'from'   => $sub_from,
            'to'     => $sub_to
          ]);
        
        // Login new seller
        $token = $gallery->createToken('Vatrinty')->accessToken;
        
        return response()->json([
            'status' => true, 
            'message' => 'Successfully Added', 
            'data' => $gallery,
            'token' => $token
        ]);
    } catch (\Exception $ex) {
      return response()->json(['status' => false, 'message' => 'Exception Error', 'error' => $ex]);
    }

  }


  public function edit_sallers(Request $request)
  {
    try {
        $validator = Validator::make($request->all(), [
          'name_en'                   => 'required',
          'name_ar'                   => 'required',
          'phone'                     => 'required',
          'address_en'                => 'required',
          'address_ar'                => 'required',
          'map'                       => 'required',
          //'password'                  => 'sometimes|nullable',
          'city_id'                   => 'required|exists:cities,id',
          'region_id'                 => 'required|exists:regions,id',
          'cat_id'                    => 'required|exists:categories,id',
          'sub_id'                    => 'required|exists:categories,id',
          // 'image'                     => 'sometimes|nullable|image|mimes:jpeg,jpg,png',
          // 'commercial_register'       => 'sometimes|nullable|image|mimes:jpeg,jpg,png',
          // 'nationalist'               => 'sometimes|nullable|image|mimes:jpeg,jpg,png'
      ],
      [
          'name_en.required'      =>'Please Enter Saller Name (EN)',
          'name_ar.required'      =>'Please Enter Saller Name (AR)',
          'phone.required'        =>'Please Enter Saller Phone',
          'address_en.required'   =>'Please Enter Saller Address (EN)',
          'address_ar.required'   =>'Please Enter Saller Address (AR)',
          'map.required'          =>'Please Enter Saller Map',
          'password.required'         =>  'Please Enter Saller Password',
          'city_id.exists'            =>  'Please ID City Not Exists',
          'region_id.exists'          =>  'Please ID Region Not Exists',
          // 'image.mimes'               =>  'Please Enter the correct encoding for the image',
          // 'commercial_register.mimes' =>  'Please Enter the correct encoding for the image',
          // 'nationalist.mimes'         =>  'Please Enter the correct encoding for the image'
        ]);

        if($validator->fails())
        {
             return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $gallery = Gallery::where('id', $request->id)->first();

        if($gallery)
        {
                $gallery->name_en  = $request->name_en;
                $gallery->name_ar  = $request->name_ar;
                $gallery->phone  = $request->phone;
                $gallery->address_en  = $request->address_en;
                $gallery->address_ar  = $request->address_ar;
                $gallery->map  = $request->map;
                $gallery->city_id  = $request->city_id;
                $gallery->region_id  = $request->region_id;
                $gallery->cat_id  = $request->cat_id;
                $gallery->sub_id  = $request->sub_id;
                $gallery->facebook_link = $request->facebook_link;
                $gallery->instgram_link = $request->instgram_link;
                $gallery->twitter_link = $request->twitter_link;
                $gallery->youtube_link = $request->youtube_link;
                $gallery->whatsapp_link = $request->whatsapp_link;
                $gallery->mobile_link = $request->mobile_link;
                
                if (File::exists(public_path().$gallery->image))
                {
                    File::delete(public_path().$gallery->image);
                }

                if (File::exists(public_path().$gallery->nationalist))
                {
                    File::delete(public_path().$gallery->nationalist);
                }

                if (File::exists(public_path().$gallery->commercial_register))
                {
                    File::delete(public_path().$gallery->commercial_register);
                }

                if($request->has('image') && $request->image != null)
                {
                  $image = $request->file('image');
                  $imageName = time().'.'.$image->getClientOriginalExtension();
                  $destinationPath = public_path('/uploads/galleries');
                  $image->move($destinationPath, $imageName);
                  $gallery->image ='/uploads/galleries/'.$imageName;
                }

                if($request->has('commercial_register') && $request->commercial_register != null)
                {
                  $commercial_register = $request->file('commercial_register');
                  $imageNameCommercial = time().'.'.$commercial_register->getClientOriginalExtension();
                  $destinationPath = public_path('/uploads/galleries');
                  $commercial_register->move($destinationPath, $imageNameCommercial);
                  $gallery->commercial_register ='/uploads/galleries/'.$imageNameCommercial;
                }

                if($request->has('nationalist') && $request->nationalist != null)
                {
                  $nationalist = $request->file('nationalist');
                  $imageNameNationalist = time().'.'.$nationalist->getClientOriginalExtension();
                  $destinationPath = public_path('/uploads/galleries');
                  $image->move($destinationPath, $imageNameNationalist);
                  $gallery->nationalist ='/uploads/galleries/'.$imageNameNationalist;
                }

                $gallery->save();

                return $this->ApiResponse(200, 'Successfully Updated', Null, $gallery);
          }else{
             return $this->ApiResponse(422, 'This ID Not Found In Database');
          }
      } catch (\Exception $ex) {
         return $this->ApiResponse(422, 'Exception Error', $ex);
    }

  }


  public function productFavorite(Request $request)
  {
    
      $products = \App\Models\ProductFavorite::where('saller_id', auth('api_saller')->user()->id)->with('user', 'product', 'saller')->get();
      if($products)
      {
        return response()->json(['status' => 'success','data' => $products], 200);
      }else{
        return response()->json(['status' => 'error', 'message' => 'لا يوجد منتجات مفضلة مسجلة']);
      }
  }

  public function logout(Request $request)
  {
      $token = auth('api_saller')->user()->token();
      $token->revoke();
      return $this->ApiResponse(200, 'Logged out succfully');
  }
}
