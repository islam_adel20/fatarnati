<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Slider;
use App\Models\Setting;
use Validator;

class SlidersController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sliders()
    {
        $sliders = Slider::select('id', 'image', 'title', 'link')->get();
        
        $results = [];

       foreach($sliders as $slider){
          $slider->image =  url($slider->image);
          array_push($results, $slider);
       } 
       return response()->json($results);
   
    }

  
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg',
            'title' => 'sometimes|nullable',
            'link'  => 'sometimes|nullable',
        ],
        $messages = [
            'image.required' => 'Please Choose Image',
            'image.mimes' => 'Please Choose Valid Image',
        ]);
    
        if($validator->fails())
        {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }
              
        $slider = new Slider;
        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/slider');
        $image->move($destinationPath, $imageName);
        $slider->image = '/uploads/slider/'.$imageName;
        $image = Image::make(public_path().$slider->image)->resize(1280, 500)->save(public_path().$slider->image);
        $slider->title = $request->title;
        $slider->link = $request->link;
        $slider->save();
        return response()->json(['status' => true, 'message' => 'Added successfully', 'data' => $slider]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
 
    public function update(Request $request)
    {
    
        $validator = Validator::make($request->all(), [
            'image' => 'sometimes|nullable|mimes:jpeg,png,jpg,gif,svg',
            'title' => 'sometimes|nullable',
            'link'  => 'sometimes|nullable',
            'id'    => 'required|exists:sliders,id'
        ],
        $messages = [
            'image.required' => 'Please Choose Image',
            'image.mimes' => 'Please Choose Valid Image',
            'id.exists'     => 'This ID Not Found In Database'
        ]);
    
        if($validator->fails())
        {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
        }

        $slider = Slider::findOrFail($request->id);
        
        if($slider)
        {
            if($request->hasFile('image'))
            {

                if (File::exists(public_path().$slider->image))
                {
                    File::delete(public_path().$slider->image);
                }

                $image = $request->file('image');
                $imageName = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/slider');
                $image->move($destinationPath, $imageName);
                $slider->image = '/uploads/slider/'.$imageName;
                $image = Image::make(public_path().$slider->image)->resize(1280, 500)->save(public_path().$slider->image);
            }
        
            $slider->title = $request->title;
            $slider->link = $request->link;
            $slider->save();
            return response()->json(['status' => true, 'message' => 'Updated successfully', 'data' => $slider]);
        }else{
            return response()->json(['status' => false, 'message' => 'This ID Not Found']);
        }
     
    }

 
    public function destroy(Request $request)
    {
        $slider = Slider::findOrFail($request->id);

        if (File::exists(public_path().$slider->image))
        {
            File::delete(public_path().$slider->image);
        }
        $slider->delete();
         return response()->json(['status' => true, 'message' => 'Deleted successfully']);
    }
}
