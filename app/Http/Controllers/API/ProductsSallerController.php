<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cookie;
use App\Http\Traits\ApiDesignTrait;
use App\Http\Traits\TraitHelper;
use App\Models\User;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\ProductColor;
use App\Models\Order;
use App\Models\Category;
use App\Models\Gallery;
use App\Models\City;
use App\Models\ProductSize;
use App\Models\ProductImage;
use App\Models\ProductFavorite;
use Validator;
use App;
use DB;
use Auth;

class ProductsSallerController extends Controller
{

  use ApiDesignTrait, TraitHelper;

  public function allProducts(Request $request)
  {
    //   $id = $request->category_id;

    //   if (empty($id)) {
    //       return response()->json([ 'status' => 'fail',  'message' => 'Invalid Category ID']);
    //   }

    //   $cat = Category::find($id);
    //   if (empty($cat)) {
    //       return response()->json([
    //           'status' => 'fail',
    //           'message' => 'Category Not Found',
    //       ], 404);
    //   }

      $products = Product::orderBy('id', 'desc');
      //$products = $products->where('cat', $id);
      $products = $products->with('images', 'products_colors')->get();
      $products->each(function($product){
          $product->images->each(function($item){
              unset($item['product']);
              unset($item['created_at']);
              unset($item['updated_at']);
              $item->image = asset($item->image);
          });
          $product['photo'] = asset($product->photo);
          unset($product['created_at']);
          unset($product['updated_at']);
          unset($product['deleted_at']);
          if (Auth::guard('api')->check())
            {   
                
                $favs = ProductFavorite::where('user_id', auth('api')->user()->id)->get();
                
                if($favs->count() == 0)
                {
                        $product['product_favorite'] = false;
                }else{
                        $results = [];
                        foreach($favs as $fav){
                            if($fav->product_id == $product->id){
                                 $product['product_favorite'] = true;
                            }else{
                                  $product['product_favorite'] = false;
                            }
                        }
                }
               
            }
      });
    
     
    
    if (Auth::guard('api')->check())
    {   
        return response()->json($products, 200);
    }else{
         return response()->json($products, 200);
    }
     
  }

  public function product(Request $request)
  {
      $id = $request->product_id;

      if (empty($id)) {
          return response()->json([
              'status' => 'fail',
              'message' => 'Invalid Product ID',
          ], 400);
      }

      $product = Product::where('id', $id)->with('images')->first();

      if (!empty($product)) {
          $product->images->each(function($item){
              unset($item['id']);
              unset($item['product']);
              unset($item['created_at']);
              unset($item['updated_at']);
              $item->image = asset($item->image);
          });

          if (!empty($product->cat)) {
              $category = Category::find($product->cat);
              $product['product_category'] = [
                  'id' => $category->id,
                  'title_en' => $category->title_en,
                  'title_ar' => $category->title_ar,
              ];
          }

          if (!empty($product->gallery_id)) {
              $product['partner'] = [
                  'id' => $product->gallery_id,
                  'name_en' => $product->gallery->name_en,
                  'name_ar' => $product->gallery->name_ar,
              ];
          }

          unset($product['cat']);
          unset($product['gallery_id']);
          unset($product['category']);
          unset($product['gallery']);
      } else {
          if (empty($product)) {
              return response()->json([
                  'status' => 'fail',
                  'message' => 'Product Not Found',
              ], 404);
          }
      }

      return response()->json($product, 200);
  }


  public function sallerProducts(Request $request)
  {
      $products = Product::where('gallery_id', auth()->user()->id)->get();

      $products->each(function($product){
        $product->images->each(function($item){
            unset($item['product']);
            unset($item['created_at']);
            unset($item['updated_at']);
            $item->image = asset($item->image);
        });
        $product['photo'] = asset($product->photo);
        unset($product['created_at']);
        unset($product['updated_at']);
        unset($product['deleted_at']);  
              
        $favs = ProductFavorite::where('user_id', auth('api')->user()->id)->get();
        
        if($favs->count() == 0)
        {
                $product['product_favorite'] = false;
        }else{
                $results = [];
                foreach($favs as $fav){
                    if($fav->product_id == $product->id){
                        $product['product_favorite'] = true;
                    }else{
                            $product['product_favorite'] = false;
                    }
                }
        }
      
        });

      $results = [];

       foreach($products as $image){
          $image->photo =  asset($image->photo);
          array_push($results, $image);
       } 
      return response()->json($results);
  }

  public function allSallerProducts(Request $request)
  {
      $products = Product::where('gallery_id', $request->saller_id)->get();

      $saller = $request->saller_id;
      $products->each(function($product) use($saller, $products){
        $product->images->each(function($item){
            unset($item['product']);
            unset($item['created_at']);
            unset($item['updated_at']);
            $item->image = asset($item->image);
        });
        unset($product['created_at']);
        unset($product['updated_at']);
        unset($product['deleted_at']);  
        $product->photo =  asset($product->photo); 
        $product->favorites->each(function($favorite) use($product){
            if($favorite->product_id == $product->id){
                $product['product_favorite'] = true;
            }else{
                $product['product_favorite'] = false;
            }
              
        });
        

        $favs = ProductFavorite::where('saller_id', $saller)->get();
        if($favs->count() == 0)
        {
            $product['product_favorite'] = false;
        }
        
      
    });

    return response()->json($products);
  }

  public function add_product(Request $request)
  {

    if($request->header("Authorization"))
    {
          try {
             
              $validator = Validator::make($request->all(), [
                'title_en'            => 'required',
                'title_ar'            => 'required',
                'text_en'             => 'required',
                'text_ar'             => 'required',
                'price'               => 'required|numeric',
                'cat'                 => 'required|exists:categories,id',
                'sub_id'              => 'required|exists:categories,id', 
                'discount'            => 'nullable',
                'start_discount'      => 'nullable',
                'end_discount'        => 'nullable',
                'price_with_discount' => 'nullable',
                'photo'               => 'required|mimes:jpeg,png,jpg,gif,svg',
                'image.*'             => 'nullable|mimes:jpeg,png,jpg,gif,svg',
            ],
            [
                'title_en.required'   => 'Poroduct Name [en] Is Required',
                'title_ar.required'   => 'Poroduct Name [ar] Is Required',
                'text_en.required'    => 'Poroduct Descripition [en] Is Required',
                'text_ar.required'    => 'Poroduct Descripition [ar] Is Required',
                'price.required'      => 'Poroduct Price Is Required',
                'price.numeric'       => 'Poroduct Price Is Not Numrice',
                'cat.required'        => 'Poroduct Main Category Is Required',
                'sub_id.required'     => 'Poroduct Sub Category Is Required',
                'cat.exists'          => 'Poroduct Main Category Not Exists',
                'sub_id.exists'       => 'Poroduct Sub Category Not Exists',
                'image.required'      => 'Image encoding is not correct',
                'image.*.mimes'       => 'Image encoding is not correct',
                'photo.mimes'         => 'Image encoding is not correct'
              ]);

              if($validator->fails())
              {
                  return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
              }

              /** Create Product **/
              $category = Category::where('id', $request->cat)->first();

              if($category)
              {
                $product = new Product;
                $product->title_en = $request->title_en;
                $product->title_ar = $request->title_ar;
                $product->price = $request->price;
                $product->text_en = $request->text_en;
                $product->text_ar = $request->text_ar;
                $product->cat = $request->cat;
                $product->sub_id = $request->sub_id;
                $product->gallery_id = auth('api_saller')->user()->id;
                $product->discount = $request->discount ?? 0;
                $product->start_discount = $request->start_discount ?? 0;
                $product->end_discount = $request->end_discount ?? 0;
                $product->time_arrive = $request->time_arrive ?? 0;
                if($request->has('discount') && $request->discount != null)
                {
                    if($request->discount >= $request->price)
                    {
                      return response()->json(['status' => true, 'message' => 'Input Invalid', 'error' => 'سعر الخصم لايجب ان يكون أكبر من يساوي السعر الأساسي']);
                    }else{

                      $discount = (int) ceil(($request->price) * (int)($request->discount) * (1/100));
                      $total_price = (int) ceil($request->price - $discount);
                      $product->price_with_discount = $total_price;
                    }
                }

              $photo_path = "";
              if($request->has('photo') && $request->photo != null)
              {
                $photo_path = $this->uploadImage('products', $request->photo);
                $product->photo = $photo_path;
              }
              $product->save();

              }

              /** Add Images **/
              $files = $request->file('image');
              if($files != null)
              {
                $i = 0;
                foreach ($files as $image)
                {
                    $product_img = new ProductImage;
                    $product_img->product = $product->id;
                    $imageName = str_random(20)."_".time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/products/'.$product->id);
                    $image->move($destinationPath, $imageName);
                    $product_img->image = '/uploads/products/'.$product->id.'/'.$imageName;
                    $product_img->save();
                }
              }

              // save product's sizes and there prices
              if (!empty($request->sizes) && !empty($request->prices) &&
                  count($request->sizes) == count($request->prices))
              {

                  for($i = 0;$i < count($request->sizes);$i++)
                  {
                      $productSize = new ProductSize();
                      $productSize->product = $product->id;
                      $productSize->size = $request->sizes[$i];
                      $productSize->price = $request->prices[$i];
                      $productSize->save();
                  }
              }

              // Add Colors
              if (!empty($request->color) && $request->has('color'))
                {
                    for ($i = 0;$i < count($request->color);$i++)
                    {
                        $productColor = new  ProductColor();
                        $productColor->product = $product->id;
                        $productColor->color = $request->color[$i];
                        $productColor->save();
                    }
                }
              return response()->json(['status' => true, 'message' => 'Successfully Added', 'data' => $product]);
          } catch (\Exception $ex) {
               return $ex;
          }
    }else{

      return response()->json(['status' => false, 'message' => 'Invalid', 'error' => 'Token Invalid']);
    }

  }


  public function edit_product(Request $request)
  {

    if ($request->header("Authorization"))
    {

            $validator = Validator::make($request->all(), [
              'title_en'            => 'sometimes|nullable',
              'title_ar'            => 'sometimes|nullable',
              'text_en'             => 'sometimes|nullable',
              'text_ar'             => 'sometimes|nullable',
              'price'               => 'sometimes|nullable|numeric',
              'cat'                 => 'required|exists:categories,id',
              'sub_id'              => 'required|exists:categories,id',
              'discount'            => 'nullable',
              'start_discount'      => 'nullable',
              'end_discount'        => 'nullable',
              'price_with_discount' => 'nullable',
              'photo'               => 'nullable|mimes:jpeg,png,jpg,gif,svg',
              'image.*'             => 'nullable|mimes:jpeg,png,jpg,gif,svg',
          ],
          [
            'title_en.required'   => 'Poroduct Name [en] Is Required',
            'title_ar.required'   => 'Poroduct Name [ar] Is Required',
            'text_en.required'    => 'Poroduct Descripition [en] Is Required',
            'text_ar.required'    => 'Poroduct Descripition [ar] Is Required',
            'price.required'      => 'Poroduct Price Is Required',
            'price.numeric'       => 'Poroduct Price Is Not Numrice',
            'cat.required'        => 'Poroduct Main Category Is Required',
            'sub_id.required'     => 'Poroduct Sub Category Is Required',
            'cat.exists'          => 'Poroduct Main Category Not Exists',
            'sub_id.exists'       => 'Poroduct Sub Category Not Exists',
            'image.required'      => 'Image encoding is not correct',
            'image.*.mimes'       => 'Image encoding is not correct',
            'photo.mimes'         => 'Image encoding is not correct'
            ]);

            if($validator->fails())
            {
                return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
            }

            /** Create Product **/
            $product = Product::where('id', $request->product_id)->first();
            $product->title_en = $request->title_en;
            $product->title_ar = $request->title_ar;
            $product->price = $request->price;
            $product->text_en = $request->text_en;
            $product->text_ar = $request->text_ar;
            $product->cat = $request->cat;
            $product->sub_id = $request->sub_id;
            $product->gallery_id = auth('api_saller')->user()->id;

            if($request->has('discount') && $request->discount != null)
            {
                if($request->discount >= $request->price)
                {
                    return response()->json(['status' => true, 'message' => 'Input Invalid', 'error' => 'سعر الخصم لايجب ان يكون أكبر من يساوي السعر الأساسي']);
                }else{

                    $discount = (int) ceil(($request->price) * (int)($request->discount) * (1/100));
                    $total_price = (int) ceil($request->price - $discount);
                    $product->price_with_discount = $total_price;
                }
            }
            $product->start_discount = $request->start_discount ?? 0;
            $product->end_discount = $request->end_discount ?? 0;
            $product->time_arrive = $request->time_arrive ?? 0;

            if($request->has('price_with_discount') && $request->price_with_discount != null)
            {
                if($request->discount >= $request->price)
                {
                    return response()->json(['status' => true, 'message' => 'Input Invalid', 'error' => 'سعر الخصم لايجب ان يكون أكبر من يساوي السعر الأساسي']);
                }else{
                    $total_price = ceil($request->price - $request->discount);
                    $product->price_with_discount = $total_price;
                }
            }
          
            $file_path = "";
            if($request->has("photo"))
            { 
    
                if(file_exists($product->photo))
                { 
                    File::delete($product->photo);
                }
            
                $file_path = $this->uploadImage('products', $request->photo);
                $product->photo = $file_path;
            }

            $product->save();
           
            // Add Colors
            if (!empty($request->color) && $request->has('color'))
            {
                for ($i = 0;$i < count($request->color);$i++)
                {
                    $productColor = new  ProductColor();
                    $productColor->product = $product->id;
                    $productColor->color = $request->color[$i];
                    $productColor->save();
                }
            }
            return response()->json(['status' => true, 'message' => 'Successfully Added', 'data' => $product]);
   
    }else{

      return response()->json(['status' => false, 'message' => 'Invalid', 'error' => 'Token Invalid']);
    }
  }


  public function productsLatest()
  {
      $products = Product::where('discount', 0)->orderBy('id', 'desc');

      $products = $products->select(
          'id',
          'title_en',
          'title_ar',
          'text_en',
          'text_ar',
          'price',
          'discount')
      ->with('images')
      ->limit(20)->get();

      $products->each(function($product){
          $product->images->each(function($item){
              unset($item['id']);
              unset($item['product']);
              unset($item['created_at']);
              unset($item['updated_at']);
              $item->image = asset($item->image);
          });
      });

      return response()->json($products, 200);
  }

  public function productsSearch(Request $request)
  {
      $keyword = $request->keyword;

      if (empty($keyword)) {
          return response()->json([
              'status' => 'fail',
              'message' => 'Invalid Search Keyword',
          ], 400);
      }

      $products = Product::where(function($query) use ($request) {
          $query->where('title_en', 'like', '%' . $request->keyword . '%')
              ->orWhere('title_ar', 'like', '%' . $request->keyword . '%');
      }) ->orderBy('id', 'desc');

      if (isset($request->first_price) && !empty($request->first_price) &&
          isset($request->last_price) && !empty($request->last_price))
      {
          $products = $products->whereBetween('price', [
              $request->first_price ,
              $request->last_price
          ]);
      }

      $products = $products->select(
          'id',
          'title_en',
          'title_ar',
          'text_en',
          'text_ar',
          'price',
          'discount')
      ->with('images')
      ->paginate(12);

      $products->each(function($product){
          $product->images->each(function($item){
              unset($item['id']);
              unset($item['product']);
              unset($item['created_at']);
              unset($item['updated_at']);
              $item->image = asset($item->image);
          });
      });

      return response()->json($products, 200);
  }

  public function delete_product_color()
  {   
      $ProductColor = ProductColor::findorfail($request->product_color_id);
      $ProductColor->delete();
      return response()->json(['status' => true, 'Deleted Success']);
  }


}
