<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Models\OrderStatus;
use App\Models\Product;
use App\Models\Order;
use Carbon\Carbon;
use Validator;

class OrdersController extends Controller
{


  public function all_orders()
  {
    $all_orders = Order::with('user', 'product')->get();
    return response()->json(['status' => 'success', 'orders' => $all_orders]);
  }


  public function saller_orders()
  {
    $saller_id = auth('api_saller')->user()->id;

    $saller_orders = Order::whereHas('product', function($query) use($saller_id){
        $query->where('gallery_id', $saller_id);
    })->get()->toArray();
    if(empty($saller_orders))
    {
        return response()->json(['status' => 'error', 'messsage' => 'Not Found Products By this Auth Saller']);
    }
    return response()->json(['status' => 'success', 'orders' => $saller_orders]);
  }


  public function add_order(Request $request)
  {

    $validator = Validator::make($request->all(), [
          'product_id'      => 'required|exists:products,id|integer'
      ],
      [
          'product_id.exists'     => 'This Id Not Exists',
          'product_id.integer'    => 'This Id Not Integer',
      ]);

      if($validator->fails())
      {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
      }

      $order = Order::create([
        'user_id'             => auth('api')->user()->id,
        'product_id'          => $request->product_id,
        'order_status_id'     => 1,
        'created_at'          => Carbon::now()
      ]);

      return response()->json(['status' => 'success', 'order' => $order]);
  }

  public function city()
  {
    $city = City::select('name_en', 'name_ar')->get();
    return response()->json(['status' => 'success', 'Done', 'cities' => $city]);
  }

  public function regions()
  {
    $reigons = Region::select('name_en', 'name_ar', 'city_id')->get();
    return response()->json(['status' => 'success', 'Done', ' reigons' => $reigons]);
  }
}
