<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Term;
use Validator;

class TermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function terms_saller()
    {
        $terms_saller = Term::where('type', 'saller')->get();
        return response()->json(['status' => 'success', 'data' => $terms_saller]);
    }

    public function terms_user()
    {
        $terms_user = Term::where('type', 'user')->get();
        return response()->json(['status' => 'success', 'data' => $terms_user]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'content_en' => 'required',
            'content_ar' => 'required',
            'type'       =>'required|in:saller,user'
        ],
        [
            'content_en.required'  => 'من فضلك أدخل المحتوي [EN]',
            'content_ar.required'  => 'من فضلك أدخل المحتوي [AR]',
            'type.required'        => 'من فضلك أدخل نوع الشروط و الأحكام',
            'type.in'              => 'يجب ان تكون نوع الشروط تاجر او عميل',
        ]);

        if ($validator->fails())
        {
            return response()->json(['status' => 'error','message' => 'Invalid Inputs','errors' => $validator->errors()]);
        }

        $term = new Term;
        $term->content_en  = $request->content_en;
        $term->content_ar  = $request->content_ar;
        $term->type  = $request->type;
        $term->save();
        return response()->json(['status' => 'success', 'data' => $term]);
    }

    public function edit(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'content_en' => 'required',
            'content_ar' => 'required',
            'type'       =>'required|in:saller,user'
        ],
        [
            'content_en.required'  => 'من فضلك أدخل المحتوي [EN]',
            'content_ar.required'  => 'من فضلك أدخل المحتوي [AR]',
            'type.required'        => 'من فضلك أدخل نوع الشروط و الأحكام',
            'type.in'              => 'يجب ان تكون نوع الشروط تاجر او عميل',
        ]);

        if ($validator->fails())
        {
            return response()->json(['status' => 'error','message' => 'Invalid Inputs','errors' => $validator->errors()]);
        }

        $term = Term::findorfail($request->id);
        $term->content_en  = $request->content_en;
        $term->content_ar  = $request->content_ar;
        $term->type  = $request->type;
        $term->save();
        return response()->json(['status' => 'success', 'data' => $term]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $term = Term::where('id', $request->id)->first();
        if($term)
        {
          $term->delete();
          return response()->json(['status' => 'success', 'messaage' => 'Deleted Done']);
        }else{
          return response()->json(['status' => 'success', 'message' => 'This Id Not Found In Database']);
        }

    }
}
