<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cookie;

use Validator;
use Cart;
use App;
use DB;

use App\Models\PromoCode;
use App\Models\Subscriber;
use App\Models\Slider;
use App\Models\Country;
use App\Models\User;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\ProductColor;
use App\Models\Order;
use App\Models\Category;
use App\Models\Gallery;
use App\Models\Social;
use App\Models\OrderStatus;
use App\Models\About;
use App\Models\Contact;
use App\Models\City;

class MainController extends Controller
{
    public function slider()
    {
        $sliders = Slider::select('image')->get();
        $sliders->each(function($item){
            $item->image = asset($item->image);
        });

        return response()->json($sliders->toArray(), 200);
    }

    public function social()
    {
        $socials = Social::select('name', 'link')->get()->toArray();
        return response()->json($socials, 200);
    }

    public function returnPolicy()
    {
        $section = Section::where('position', 'return_policy')
            ->select('title_en', 'title_ar', 'text_en', 'text_ar')->first();

        return response()->json($section, 200);
    }

    public function terms()
    {
        $section = Section::where('position', 'terms_conditions')
            ->select('title_en', 'title_ar', 'text_en', 'text_ar')->first();

        return response()->json($section, 200);
    }

    public function privacy()
    {
        $section = Section::where('position', 'privacy')
            ->select('title_en', 'title_ar', 'text_en', 'text_ar')->first();

        return response()->json($section, 200);
    }

    public function helpCenter()
    {
        $section = Section::where('position', 'help_center')
            ->select('title_en', 'title_ar', 'text_en', 'text_ar')->first();

        return response()->json($section, 200);
    }

    public function tips()
    {
        $tips = Article::select('title', 'text as iframe')->get()->toArray();
        return response()->json($tips, 200);
    }

    public function about()
    {
        $section = Section::where('position', 'about_home')
            ->select('title_en', 'title_ar', 'text_en', 'text_ar')->first();

        return response()->json($section, 200);
    }

    public function tax()
    {
        $section = Section::where('position', 'tax')
            ->select('title_en as tax')->first();

        return response()->json($section, 200);
    }

    public function categories()
    {
        $categories = Category::select(
            'id',
            'title_en',
            'title_ar',
            'image',
            'cat as main_category_id'
        )->get();

        $categories->each(function($item){
            $item->image = asset($item->image);
        });

        return response()->json($categories, 200);
    }

    public function parent_categories()
    {
        $categories = Category::select(
            'id',
            'title_en',
            'title_ar',
            'image',
            'cat as main_category_id'
        )->get();

        $categories->each(function($item){
            $item->image = asset($item->image);
        });

        return response()->json($categories, 200);
    }


    public function gallery(Request $request)
    {
        $id = $request->id;

        if (empty($id)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Invalid Partner ID',
            ], 400);
        }

        $gallery = Gallery::where('id', $id)->select(
            'id',
            'name_en',
            'name_ar',
            'image'
        )->first();

        if (!empty($gallery)) {
            $gallery->image = asset($gallery->image);

            $products = Product::select(
                'id',
                'title_en',
                'title_ar',
                'text_en',
                'text_ar',
                'price',
                'discount',
                'stock_qty')
            ->where('gallery_id', $gallery->id)
            ->with('images')
            ->get();

            $products->each(function($product){
                $product->images->each(function($item){
                    unset($item['id']);
                    unset($item['product']);
                    unset($item['created_at']);
                    unset($item['updated_at']);
                    $item->image = asset($item->image);
                });
            });
        } else {
            if (empty($product)) {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Partner Not Found',
                ], 404);
            }
        }

        return response()->json([
            'partner' => $gallery,
            'partner_products' => $products
        ], 200);
    }

    public function contact()
    {
        $contactInfo = Contact::select(
            'address_en',
            'address_ar',
            'phone',
            'email',
            'map')
        ->first();

        return response()->json($contactInfo, 200);
    }

    public function sendContact(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'subject' => 'required',
            'message' => 'required'
        ],
        [
            'name.required' => 'Please Enter Your Name',
            'email.required' => 'Please Enter Your E-mail Address',
            'email.email' => 'Please Enter Valid E-mail Address',
            'phone.required' => 'Please Enter Phone Number',
            'subject.required' => 'Please Enter Your Message Subject',
            'message.required' => 'Please Enter Your Message'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Invalid Inputs',
                'errors' => $validator->errors()
            ], 400);
        }

        $contact = Contact::first();

        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $smsg = $request->input('message');

        $to = $contact->email;

        $subject = $request->input('subject');

        $message = '<html><body>';
        $message .= "<p><b>Name : </b>".$name."</p><p><b>E-mail Address : </b>".$email."</p>
        <p><b>Phone : </b>".$phone."</p>".$smsg;
        $message .= '</body></html>';

        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=utf-8';
        $headers[] = 'To: Fitoverfat <'.$to.'>';
        $headers[] = 'From: '.$name.' <'.$email.'>';


        // $msg = new ContactMessage;
        // $msg->name = $name;
        // $msg->email = $email;
        // $msg->phone = $phone;
        // $msg->subject = $subject;
        // $msg->msg = $smsg;
        // $msg->save();
        // $message = view('mail.contact', compact('subject', 'name', 'email', 'phone', 'smsg'));

        // @mail($to, $subject, $message, implode("\r\n", $headers));

        return response()->json([
            'status' => 'success',
            'message' => 'Your Message Sent Successfully',
        ], 200);
    }

    public function productsSearch(Request $request)
    {
        $keyword = $request->keyword;

        if (empty($keyword)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Invalid Search Keyword',
            ], 400);
        }

        $products = Product::where(function($query) use ($request) {
            $query->where('title_en', 'like', '%' . $request->keyword . '%')
                ->orWhere('title_ar', 'like', '%' . $request->keyword . '%');
        }) ->orderBy('id', 'desc');

        if (isset($request->first_price) && !empty($request->first_price) &&
            isset($request->last_price) && !empty($request->last_price))
        {
            $products = $products->whereBetween('price', [
                $request->first_price ,
                $request->last_price
            ]);
        }

        $products = $products->select(
            'id',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'price',
            'discount',
            'stock_qty')
        ->with('images')
        ->paginate(12);

        $products->each(function($product){
            $product->images->each(function($item){
                unset($item['id']);
                unset($item['product']);
                unset($item['created_at']);
                unset($item['updated_at']);
                $item->image = asset($item->image);
            });
        });

        return response()->json($products, 200);
    }

    public function productsLatest()
    {
        $products = Product::where('discount', 0)->orderBy('id', 'desc');

        $products = $products->select(
            'id',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'price',
            'discount',
            'stock_qty')
        ->with('images')
        ->limit(20)->get();

        $products->each(function($product){
            $product->images->each(function($item){
                unset($item['id']);
                unset($item['product']);
                unset($item['created_at']);
                unset($item['updated_at']);
                $item->image = asset($item->image);
            });
        });

        return response()->json($products, 200);
    }

    public function productsWeeklyOffer()
    {
        $products = Product::where('discount', '!=', 0)->orderBy('id', 'desc');

        $products = $products->select(
            'id',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'price',
            'discount',
            'stock_qty')
        ->with('images')
        ->limit(20)->get();

        $products->each(function($product){
            $product->images->each(function($item){
                unset($item['id']);
                unset($item['product']);
                unset($item['created_at']);
                unset($item['updated_at']);
                $item->image = asset($item->image);
            });
        });

        return response()->json($products, 200);
    }

    public function productsBestSelling()
    {
        $bestSellingItemsIDs = [];
        $bestSellingItemsIDs = OrderItem::select('product', DB::raw('count(*) as total'))
            ->groupBy('product')->get()->pluck('product')->toArray();

        $products = Product::whereIn('id', $bestSellingItemsIDs);

        $products = $products->select(
            'id',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'price',
            'discount',
            'stock_qty')
        ->with('images')
        ->limit(20)->get();

        $products->each(function($product){
            $product->images->each(function($item){
                unset($item['id']);
                unset($item['product']);
                unset($item['created_at']);
                unset($item['updated_at']);
                $item->image = asset($item->image);
            });
        });

        return response()->json($products, 200);
    }

    public function products(Request $request)
    {
        $id = $request->category_id;

        if (empty($id)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Invalid Category ID',
            ], 400);
        }

        $cat = Category::find($id);
        if (empty($cat)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Category Not Found',
            ], 404);
        }

        $products = Product::orderBy('id', 'desc');

        if ($cat->cat == 0) {
            $products = $products->where('main_cat', '=', $id);
        } else {
            $products = $products->where('sub_cat', '=', $id);
        }

        if (isset($request->first_price) && !empty($request->first_price) &&
            isset($request->last_price) && !empty($request->last_price))
        {
            $products = $products->whereBetween('price', [
                $request->first_price ,
                $request->last_price
            ]);
        }

        $products = $products->select(
            'id',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'price',
            'discount',
            'stock_qty')
        ->with('images')
        ->paginate(12);

        $products->each(function($product){
            $product->images->each(function($item){
                unset($item['id']);
                unset($item['product']);
                unset($item['created_at']);
                unset($item['updated_at']);
                $item->image = asset($item->image);
            });
        });

        return response()->json($products, 200);
    }

    public function product(Request $request)
    {
        $id = $request->product_id;

        if (empty($id)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Invalid Product ID',
            ], 400);
        }

        $product = Product::where('id', $id)->select(
            'id',
            'cat',
            'gallery_id',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'price',
            'discount',
            'stock_qty')
        ->with('images')
        ->first();

        if (!empty($product)) {
            $product->images->each(function($item){
                unset($item['id']);
                unset($item['product']);
                unset($item['created_at']);
                unset($item['updated_at']);
                $item->image = asset($item->image);
            });

            if (!empty($product->cat)) {
                $category = Category::find($product->cat);
                $product['product_category'] = [
                    'id' => $category->id,
                    'title_en' => $category->title_en,
                    'title_ar' => $category->title_ar,
                ];
            }

            if (!empty($product->gallery_id)) {
                $product['partner'] = [
                    'id' => $product->gallery_id,
                    'name_en' => $product->gallery->name_en,
                    'name_ar' => $product->gallery->name_ar,
                ];
            }

            unset($product['cat']);
            unset($product['gallery_id']);
            unset($product['category']);
            unset($product['gallery']);
        } else {
            if (empty($product)) {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Product Not Found',
                ], 404);
            }
        }

        return response()->json($product, 200);
    }

    public function addToCart(Request $request)
    {
        if (empty($request->product_id)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Invalid Product ID',
            ], 400);
        }

        if (empty($request->qty)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Missing Quantity',
            ], 400);
        }

        if ((int)$request->qty <= 0) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Invalid Quantity',
            ], 400);
        }

        $product = Product::find($request->product_id);
        if (empty($product)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Product Not Found',
            ], 404);
        }

        $cart = ShoppingCart::where('user_id', auth('api')->user()->id)
            ->where('product_id', $product->id)->first();

        if (!empty($cart)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Product Already In The Cart',
            ], 404);
        }

        if($product->stock_qty == 0)
        {
            return response()->json([
                'status' => 'fail',
                'message' => 'Out of stock',
            ], 200);
        }
        else if($request->qty > $product->stock_qty)
        {
            return response()->json([
                'status' => 'fail',
                'message' => 'No Available Quantity',
            ], 200);
        }
        else
        {
            $newCart = new ShoppingCart();
            $newCart->user_id = auth('api')->user()->id;
            $newCart->product_id = $product->id;
            $newCart->price = $product->price;
            $newCart->qty = $request->qty;
            $newCart->total = ($product->price * $request->qty);
            $newCart->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Added Successfully',
            ], 200);
        }
    }

    function updateCart(Request $request)
    {
        if (empty($request->product_id)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Invalid Product ID',
            ], 400);
        }

        if (empty($request->qty)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Missing Quantity',
            ], 400);
        }

        if ((int)$request->qty <= 0) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Invalid Quantity',
            ], 400);
        }

        $product = Product::find($request->product_id);
        if (empty($product)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Product Not Found',
            ], 404);
        }

        $cart = ShoppingCart::where('user_id', auth('api')->user()->id)
            ->where('product_id', $product->id)->first();

        if (empty($cart)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Product Not Found In The Cart',
            ], 404);
        }

        if ($product->stock_qty == 0) {
            $cart->delete();
        } else {
            $cart->total = ($product->price * $request->qty);
            $cart->qty = $request->qty;
            $cart->save();
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Updated Successfully',
            'new_qty' => $cart->qty
        ], 200);
    }

    function deleteItem(Request $request)
    {
        if (empty($request->product_id)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Invalid Product ID',
            ], 400);
        }

        $product = Product::find($request->product_id);
        if (empty($product)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Product Not Found',
            ], 404);
        }

        $cart = ShoppingCart::where('user_id', auth('api')->user()->id)
            ->where('product_id', $product->id)->first();

        if (empty($cart)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Product Not Found In The Cart',
            ], 404);
        }

        $cart->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Removed Successfully',
        ], 200);
    }

    function cart()
    {
        $cart = ShoppingCart::where('user_id', auth('api')->user()->id)->get();

        $total = 0;
        $items = [];
        if ($cart->count() > 0) {
            foreach ($cart as $item) {
                if (empty($item->product)) {
                $item->delete();
                continue;
            }

                $total += $item->total;
                $items[] = [
                    'product_id' => $item->product->id,
                    'name_en' => $item->product->title_en,
                    'name_ar' => $item->product->title_ar,
                    'price' => $item->price,
                    'quantity' => $item->qty,
                    'total' => $item->total,
                    'url' => url('api/product?product_id='.$item->product->id),
                    'image' => asset($item->product->images[0]->image)
                ];
            }
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Your Cart Loaded Successfully',
            'total' => $total,
            'items' => $items
        ], 200);
    }

    public function paymentMethods()
    {
        $paymentMethods = PayMethod::select('id', 'title_en')->get()->toArray();
        return response()->json($paymentMethods, 200);
    }

    public function cities()
    {
        $cities = City::select('id', 'name_en', 'name_ar', 'shipment')->get()->toArray();
        return response()->json($cities, 200);
    }


    public function checkDiscount(Request $request)
    {
        $code = '';
        if (!empty($request->discount_code)) {
            $code = PromoCode::where('name', $request->discount_code)->first();
            if (empty($code) || !$code->isValid()) {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Discount Code Not Valid',
                ], 404);
            }
        }

        return response()->json($code->cost, 200);
    }
}
