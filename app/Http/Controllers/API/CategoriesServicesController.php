<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Models\CategoryServices;
use Validator;
use File;

class CategoriesServicesController extends Controller
{
  public function categories_serv()
  {
    $categories = CategoryServices::get();
    $results = [];

    foreach($categories as $image){
        if($image['image'] != null && File::exists(public_path().$image['image']))
        {
            $image->image = asset($image->image);
        }else{
            $image->image = 'Image Not Exists In Path';
        }
        array_push($results, $image);
    } 
    return response()->json($results);
  }

  public function show_categories_serv(request $request)
  {

    $validator = Validator::make($request->all(), [
          'id'      => 'required|exists:categories,id|integer'
      ],
      [
          'id.exists'     => 'This Id Not Exists',
          'id.integer'    => 'This Id Not Integer',
      ]);

      if($validator->fails())
      {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
      }

      $category = CategoryServices::where('id', $request->id)->first();

      if($category->cat != 0)
      {
          return response()->json(['status' => 'error', 'message' => 'This Not Category Main Be Category Sub']);
      }
      return response()->json(['status' => 'success', 'category' => $category]);
  }
  public function add_categorys_serv(Request $request)
  {
    $validator = Validator::make($request->all(), [
          'name_en' => 'required',
          'name_ar' => 'required',
          'image' => 'sometimes|nullable|mimes:jpeg,png,jpg,gif,svg,webp'
      ],
      [
          'name_en.required'=>'Please Enter Category Name (EN)',
          'name_ar.required'=>'Please Enter Category Name (AR)',
          'image.mimes' => 'Please Choose Valid Image File'
      ]);

      if($validator->fails())
      {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
      }

      $category = new CategoryServices;
      $category->name_en  = $request->name_en;
      $category->name_ar  = $request->name_ar;

      if($request->has('image') && $request->image != null)
        {
            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/sallers_services');
            $image->move($destinationPath, $imageName);
            $category->image ='/uploads/sallers_services/'.$imageName;
        }

      $category->save();

      return response()->json(['status' => 'success', 'category' => $category]);
  }


  public function edit_categorys_serv(Request $request)
  {
    $validator = Validator::make($request->all(), [
          'name_en' => 'required',
          'name_ar' => 'required',
          'image' => 'sometimes|nullable|mimes:jpeg,png,jpg,gif,svg,webp'
      ],
      [
          'name_en.required'=>'Please Enter Category Name (EN)',
          'name_ar.required'=>'Please Enter Category Name (AR)',
          'image.mimes' => 'Please Choose Valid Image File'
      ]);

      if($validator->fails())
      {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
      }

      $category = CategoryServices::where('id', $request->id)->first();
      $category->name_en  = $request->name_en;
      $category->name_ar  = $request->name_ar;
      if($request->has('image') && $request->image != null)
          {
            if (File::exists(public_path().$category->image))
            {
                File::delete(public_path().$category->image);
            }

            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/sallers_services');
            $image->move($destinationPath, $imageName);
            $category->image ='/uploads/sallers_services/'.$imageName;
          }
      $category->save();
      return response()->json(['status' => 'success', 'category' => $category]);
  }

  public function delete_categorys_serv(Request $request)
  {

      $validator = Validator::make($request->all(), ['id' => 'required:exists:categories,id']);
      if($validator->fails())
      {
          return response()->json(['status' => false, 'message' => 'Invalid Inputs', 'errors' => $validator->errors()]);
      }

      $category = CategoryServices::where([['id', $request->id], ['deleted_at', null]])->first();
      if($category)
      {
          $category->delete();
          return response()->json(['status' => 'success', 'message' =>'Delete Done']);
      }else{
          return response()->json(['status' => 'error', 'message' =>'This ID Deleted']);
      }

  }

}
