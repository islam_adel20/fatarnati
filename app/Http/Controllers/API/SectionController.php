<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Section;
use App\Models\Contact;

class SectionController extends Controller
{

    public function about_app()
    {
      $about_app = Section::where('position', 'about_home')->get();
      return response()->json(['status' => 'success', 'data' => $about_app]);
    }

    public function privacy_app()
    {
      $return_policy = Section::where('position', 'return_policy')->get();
      return response()->json(['status' => 'success', 'data' => $return_policy]);
    }

    public function contact()
    {
      $contact = Contact::get();
      return response()->json(['status' => 'success', 'data' => $contact]);
    }


}
