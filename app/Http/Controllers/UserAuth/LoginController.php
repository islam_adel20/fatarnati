<?php

namespace App\Http\Controllers\UserAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Hesto\MultiAuth\Traits\LogsoutGuard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\User;
use App\Models\Social;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, LogsoutGuard {
        LogsoutGuard::logout insteadof AuthenticatesUsers;
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    public $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user.guest', ['except' => 'logout']);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $socails = Social::all();
        return view('user.auth.login')->with(['socials'=>$socails]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('user');
    }

    protected function loginWithCredentials(Request $request)
    {
        $validatedData = $request->validate([
            'email'=>'required|email',
            'password'=>'required'
        ]);
        $signinEmail = $request->email;
        $signinPassword = $request->password;

        $user = User::where('email', '=', $signinEmail)->first();

        if($user === NULL)
        {
            return redirect()->back()->withErrors(['errors'=>'Wrong In Email Address Or Password']);
        }
        else if(!Hash::check($signinPassword, $user->password))
        {
            return redirect()->back()->withErrors(['errors'=>'Wrong In Email Address Or Password']);
        }
        else if($user->active == 0)
        {
            return redirect()->back()->withErrors(['errors'=>'Please Activate Your Account First']);
        }
        else
        {
            if($request->remember) {Auth::guard('user')->login($user, true);}
            else {Auth::guard('user')->login($user);}
            return redirect('Profile');
        }
    }
    public function logout(Request $request)
    {
        Auth::guard('user')->logout();
        // $request->session()->flush();
        // $request->session()->regenerate();
        return redirect('/');
    }
}
