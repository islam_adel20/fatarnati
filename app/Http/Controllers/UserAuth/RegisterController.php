<?php

namespace App\Http\Controllers\UserAuth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Validation\Rule;
use App\Models\Social;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user.guest');
    }

    public function showRegistrationForm()
    {
        $socails = Social::all();
        return view('user.auth.register')->with(['socials'=>$socails]);
    }


    public function register_user (Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
            'password' => 'required|min:6|confirmed',
        ]);

        $token = str_random(10).time();
        $user = new User;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->phone = $request->phone;
        $user->name = $request->first_name." ".$request->last_name;
        if($request->country) {$user->country = $request->country;}
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->activate_token = $token;
        $user->active = 1;
        $user->save();
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=utf-8';

        // Additional headers
        $headers[] = 'From: Skai <info@skai.com>';
        $user = User::where('email', '=', $request->email)->first();
        $subject = "Account Activation";
        $link = url("verify-account/".$user->token);
        $message = "<p style='direction: rtl; text-align:right;'><b>Welcome ".$user->name.",</b></p style='direction: rtl; text-align:right;'><p>Thanks For Creating New Account With Us.</p>";
        $message .= "<p><a href=".$link.">".$link."</p>";
        $to = $user->email;
        // Mail it
        //mail($to, $subject, $message, implode("\r\n", $headers));

        return redirect('/login')->with('success', __('messages.register_success'));
    }
}
