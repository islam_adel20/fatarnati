<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Term;

class TermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $terms_saller = Term::where('type', 'saller')->get();
        return view('admin.pages.terms.terms_saller')->with(['terms_saller' => $terms_saller]);
    }

    public function terms_user()
    {
        $terms_user = Term::where('type', 'user')->get();
        return view('admin.pages.terms.terms_user')->with(['terms_user' => $terms_user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.terms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'content_en' => 'required',
            'content_ar' => 'required',
            'type'       =>'required|in:saller,user'
        ],
        [
            'content_en.required'  => 'من فضلك أدخل المحتوي [EN]',
            'content_ar.required'  => 'من فضلك أدخل المحتوي [AR]',
            'type.required'        => 'من فضلك أدخل نوع الشروط و الأحكام',
            'type.in'              => 'يجب ان تكون نوع الشروط تاجر او عميل',
        ]);

        $term = new Term;
        $term->content_en  = $request->content_en;
        $term->content_ar  = $request->content_ar;
        $term->type  = $request->type;
        $term->save();

        if($request->type == 'saller')
        {
          return redirect()->route('terms.index');
        }else{
          return redirect('admin/terms/user');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $term = Term::findorfail($id);
        return view('admin.pages.terms.edit')->with(['term' => $term]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([
            'content_en' => 'required',
            'content_ar' => 'required',
            'type'       =>'required|in:saller,user'
        ],
        [
            'content_en.required'  => 'من فضلك أدخل المحتوي [EN]',
            'content_ar.required'  => 'من فضلك أدخل المحتوي [AR]',
            'type.required'        => 'من فضلك أدخل نوع الشروط و الأحكام',
            'type.in'              => 'يجب ان تكون نوع الشروط تاجر او عميل',
        ]);

        $term = Term::findorfail($id);
        $term->content_en  = $request->content_en;
        $term->content_ar  = $request->content_ar;
        $term->type  = $request->type;
        $term->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $term = Term::findorfail($id);
        $term->delete();
        return back();
    }
}
