<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Subscriber;
use App\Models\SallerCode;
use Carbon\Carbon;

class SubscriberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sub_pending()
    {
        $sub_now = Carbon::now()->toDateString();
        $subs = SallerCode::where([['to', '>=', $sub_now], ['to', '!=', null]])->get();
        return view('admin.pages.subscribers.subs_pending')->with(['subs'=>$subs]);
    }

    public function sub_finish()
    {
        $sub_now = Carbon::now()->toDateString();
        $subs = SallerCode::where([['to', '<', $sub_now], ['to', '!=', null]])->get();
        return view('admin.pages.subscribers.subs_finish')->with(['subs'  =>  $subs]);
    }


}
