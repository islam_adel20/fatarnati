<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::where('cat', '=', 0)->get();
        return view('admin.pages.categories.index')->with(['categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.pages.categories.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validatedData = $request->validate([
            'title_en' => 'required',
            'title_ar' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg,webp'
        ],
        [
            'title_en.required'=>'Please Enter Category Name (EN)',
            'title_ar.required'=>'Please Enter Category Name (AR)',
            'image.required' => 'Please Choose Image',
            'image.mimes' => 'Please Choose Valid Image File'
        ]);

        $category = new Category;
        $category->title_en  = $request->title_en;
        $category->title_ar  = $request->title_ar;
        $category->cat  = $request->cat;

        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/categories');
        $image->move($destinationPath, $imageName);
        $category->image ='/uploads/categories/'.$imageName;

        $category->save();

        alert()->success('تم الأضافة بنجاح');
        if($category->cat == 0)
        {
            return redirect()->route('categories.index');
        }
        else
        {
            return redirect()->route('categories.show', $category->cat);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $main = Category::findorfail($id);
        $categories = Category::where('cat', '=', $id)->where('cat', '!=', 0)->get();
        return view('admin.pages.categories.subs')->with(['categories'  =>  $categories, 'main' =>  $main]);
    }

    public function create_sub($id)
    {
        $main = Category::findorfail($id);
        if ($main->cat > 0) {abort(404);}
        return view('admin.pages.categories.create_sub')->with(['main'=>$main]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findorfail($id);
        $categories = Category::all();
        return view('admin.pages.categories.edit', compact('category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findorfail($id);
        $validatedData = $request->validate([
            'title_en' => 'required',
            'title_ar' => 'required',
        ],
        [
            'title_en.required'=>'Please Enter Category Name (EN)',
            'title_ar.required'=>'Please Enter Category Name (AR)',
        ]);

        $category->title_en  = $request->title_en;
        $category->title_ar  = $request->title_ar;
        $category->cat  = $request->cat;

        if($request->hasFile('image'))
        {
            $validatedData = $request->validate([
                'image' => 'required|mimes:jpeg,png,jpg,gif,svg,webp'
            ],
            $messages = [
                'image.required' => 'Please Choose Image',
            'image.mimes' => 'Please Choose Valid Image File',
            ]);

            if (File::exists(public_path().$category->image))
            {
                File::delete(public_path().$category->image);
            }

            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/categories');
            $image->move($destinationPath, $imageName);
            $category->image = '/uploads/categories/'.$imageName;
        }

        $category->save();

        alert()->success('تم التحديث بنجاح');
        if($category->cat == 0)
        {
            return redirect()->route('categories.index');
        }
        else
        {
            return redirect()->route('categories.show', $category->cat);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findorfail($id);

        if (File::exists(public_path().$category->image))
        {
            File::delete(public_path().$category->image);
        }

        $category->delete();
        alert()->success('تم الحذف بنجاح');
        return redirect()->back();
    }


    public function get_subs(Request $request)
    {
        $id = $request->cat;
        $main = Category::findorfail($id);
        if ($main->cat == 0)
        {
            $categories = Category::where('cat', '=', $id)->get();
            echo '<option value="" disabled selected>Choose Category</option>';
            foreach ($categories as $cc)
            {
                ?>
                <option value="<?php echo $cc->id; ?>"><?php echo $cc->title; ?></option>
                <?php
            }
        }
    }
}
