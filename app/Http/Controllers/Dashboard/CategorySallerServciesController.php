<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Models\CategoryServices;

class CategorySallerServciesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = CategoryServices::get();
        return view('admin.pages.categories_saller_servcies.index')->with(['categories' =>  $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = CategoryServices::all();
        return view('admin.pages.categories_saller_servcies.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validatedData = $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required',
            'image' => 'sometimes|nullable|mimes:jpeg,png,jpg,gif,svg,webp'
        ],
        [
            'name_en.required'  =>  'Please Enter Category Name (EN)',
            'name_ar.required'  =>  'Please Enter Category Name (AR)',
            'image.mimes'       => 'This Image Not jpeg,png,jpg,gif,svg,webp'
        ]);


        $category = new CategoryServices;
        $category->name_en  = $request->name_en;
        $category->name_ar  = $request->name_ar;
        if($request->has('image') && $request->image != null)
        {
            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/sallers_services');
            $image->move($destinationPath, $imageName);
            $category->image ='/uploads/sallers_services/'.$imageName;
        }
        $category->save();

        alert()->success('تم الأضافة بنجاح');
        return redirect('admin/saller/servcies/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit($id)
    {
        $category = CategoryServices::findorfail($id);
        $categories = CategoryServices::all();
        return view('admin.pages.categories_saller_servcies.edit', compact('category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = CategoryServices::findorfail($id);
        $validatedData = $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required',
            'image'   => 'sometimes|nullable|mimes:jpeg,png,jpg,gif,svg,webp'
        ],
        [
            'name_en.required'  =>  'Please Enter Category Name (EN)',
            'name_ar.required'  =>  'Please Enter Category Name (AR)',
            'image.mimes'       => 'This Image Not jpeg,png,jpg,gif,svg,webp'
        ]);

        $category->name_en  = $request->name_en;
        $category->name_ar  = $request->name_ar;
        if($request->has('image') && $request->image != null)
        {
            if (File::exists(public_path().$category->image))
            {
                File::delete(public_path().$category->image);
            }

            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/sallers_services');
            $image->move($destinationPath, $imageName);
            $category->image ='/uploads/sallers_services/'.$imageName;
        }
        $category->save();

        alert()->success('تم التحديث بنجاح');
        return redirect('admin/saller/servcies/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = CategoryServices::findorfail($id);
        $category->delete();
        alert()->success('تم الحذف بنجاح');
        return redirect()->back();
    }


    public function get_subs(Request $request)
    {
        $id = $request->cat;
        $main = CategoryServices::findorfail($id);
        if ($main->cat == 0)
        {
            $categories = Category::where('cat', '=', $id)->get();
            echo '<option value="" disabled selected>Choose Category</option>';
            foreach ($categories as $cc)
            {
                ?>
                <option value="<?php echo $cc->id; ?>"><?php echo $cc->title; ?></option>
                <?php
            }
        }
    }
}
