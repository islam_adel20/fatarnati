<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use App\Models\About;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections = About::get();
        return view('admin.pages.about.index')->with(['sections'=>$sections]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.about.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
			'title' => 'required',
            'text' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg,webp'
        ],
        $messages = [
            'title.required' => 'Please Enter Title',
            'text.required' => 'Please Enter Description',
            'image.required' => 'Please Choose Image',
            'image.mimes' => 'Please Choose Valid Image File'
        ]);

        $section = new About;
        $section->title = $request->title;
        $section->text  = $request->text;

        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/about');
        $image->move($destinationPath, $imageName);
        $section->image ='/uploads/about/'.$imageName;
        $section->save();
        return redirect()->route('about.index');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about = About::findOrFail($id);
        return view('admin.pages.about.edit')->with(['about'=>$about]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
			'title' => 'required',
            'text' => 'required'
        ],
        $messages = [
            'title.required' => 'Please Enter Title',
            'text.required' => 'Please Enter Description'
        ]);

        $section = About::findOrFail($id);
        $section->title = $request->title;
        $section->text  = $request->text;

        if($request->hasFile('image'))
        {
            $validatedData = $request->validate([
                'image' => 'required|mimes:jpeg,png,jpg,gif,svg,webp'
            ],
            $messages = [
                'image.required' => 'Please Choose Image',
            'image.mimes' => 'Please Choose Valid Image File',
            ]);

            if (File::exists(public_path().$section->image))
            {
                File::delete(public_path().$section->image);
            }

            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/about');
            $image->move($destinationPath, $imageName);
            $section->image = '/uploads/about/'.$imageName;
        }
        $section->save();
        return redirect()->route('about.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section = About::findOrFail($id);

        if (File::exists(public_path().$section->image))
        {
            File::delete(public_path().$section->image);
        }
        $section->delete();
        return redirect()->route('about.index');
    }
}
