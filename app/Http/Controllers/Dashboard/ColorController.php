<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Size;
use App\Models\Color;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colors = Color::all();
        return view('admin.pages.colors.index')->with(['colors' =>  $colors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.colors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $validatedData = $request->validate([
            'title' => 'required',
            'color' => 'required'
        ],
        [
            'required'  => 'ادخل اللون'
        ]);

        $color = new Color();
        $color->title = $request->title;
        $color->color = $request->color;
        $color->save();

        alert()->success('تم الأضافة بنجاح');
        return redirect()->route('colors.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $color = Color::findOrFail($id);
        return view('admin.pages.colors.edit')->with(['color'  =>  $color]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'color' => 'required'
        ],
        [
            'required'  =>  'ادخل اللون',
        ]);

        $color = Color::findOrFail($id);
        $color->title = $request->title;
        $color->color = $request->color;
        $color->save();

        alert()->success('تم التحديث بنجاح');
        return redirect()->route('colors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $color = Color::findOrFail($id);
        $color->delete();

        alert()->success('تم الحذف بنجاح');
        return redirect()->route('colors.index');
    }
}
