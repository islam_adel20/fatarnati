<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Section;


class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sections = [
            'about_home',
            'home_footer',
            'return_policy',
            'terms_conditions',
            'privacy',
            'help_center',
            'header_image',
            'tax'
        ];

        if(!in_array($id, $sections)) {return abort('404');}

        if($id == 'about_home') {$data = array('title_en', 'title_ar', 'editor_en', 'editor_ar', 'link', 'image');}
        else if($id == 'home_footer') {$data = array('text_en', 'text_ar');}
        else if($id == 'return_policy') {$data = array('title_en', 'title_ar', 'editor_en', 'editor_ar');}
        else if($id == 'terms_conditions') {$data = array('title_en', 'title_ar', 'editor_en', 'editor_ar');}
        else if($id == 'privacy') {$data = array('title_en', 'title_ar', 'editor_en', 'editor_ar');}
        else if($id == 'help_center') {$data = array('title_en', 'title_ar', 'editor_en', 'editor_ar');}
        else if($id == 'header_image') {$data = array('image');}
        else if($id == 'tax') {$data = array('number');}

        if($id != 'our_features')
        {
            $section = Section::where('position', $id)->first();
            if($section === NULL)
            {
                $section = new Section;
                $section->position = $id;
                $section->save();
            }
            return view('admin.pages.section.edit', compact('section', 'data'));
        }
        elseif($id == 'our_features')
        {
            $section = Section::where('position', $id)->first();
            if($section === NULL)
            {
                $section = new Section;
                $section->position = $id;
                $section->save();
                $section = new Section;
                $section->position = $id;
                $section->save();
                $section = new Section;
                $section->position = $id;
                $section->save();
            }
            $sections = Section::where('position', $id)->get();
            return view('admin.pages.section.index', compact('sections'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $section = Section::findorfail($id);
        $sections = array('our_features');
        if(!in_array($section->position, $sections)) {abort('404');}
        $data = array('title_en', 'title_ar', 'text_en', 'text_ar', 'image', 'link');
        return view('admin.pages.section.edit', compact('section', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sections = [
            'about_home',
            'home_footer',
            'return_policy',
            'terms_conditions',
            'privacy',
            'help_center',
            'header_image',
            'tax'
        ];

        $section = Section::findorfail($id);
        if(!in_array($section->position, $sections)) {abort('404');}

        if($section->position == 'about_home')
        {
            $validatedData = $request->validate([
                'title_en' => 'required',
                'title_ar' => 'required',
                'text_en' => 'required',
                'text_ar' => 'required',
                'link' => 'url|nullable'
            ],
            $messages = [
                'title_en.required' => 'Please Enter Title (EN)',
                'title_ar.required' => 'Please Enter Title (AR)',
                'text_en.required' => 'Please Enter Text (EN)',
                'text_ar.required' => 'Please Enter Text (AR)',
                'link.url' => 'Please Enter Valid link',
            ]);
            $section->title_en = $request->title_en;
            $section->title_ar = $request->title_ar;
            $section->text_en = $request->text_en;
            $section->text_ar = $request->text_ar;
            $section->link = $request->link;

            if($request->hasFile('image'))
            {
                $validatedData = $request->validate([
                    'image' => 'required|mimes:jpeg,png,jpg,gif,svg'
                ],
                $messages = [
                    'image.required' => 'Please Choose Image',
                    'image.mimes' => 'Please Choose Valid Image'
                ]);

                if (File::exists(public_path().$section->image))
                {
                    File::delete(public_path().$section->image);
                }

                $image = $request->file('image');
                $imageName = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/sections');
                $image->move($destinationPath, $imageName);
                $section->image = '/uploads/sections/'.$imageName;
            }
            $section->save();
            return redirect()->back();
        }
        else if($section->position == 'return_policy' ||
            $section->position == 'terms_conditions' ||
            $section->position == 'help_center' ||
            $section->position == 'privacy')
        {
            $validatedData = $request->validate([
                'title_en' => 'required',
                'title_ar' => 'required',
                'text_en' => 'required',
                'text_ar' => 'required',
            ],
            $messages = [
                'title_en.required' => 'Please Enter Title (EN)',
                'title_ar.required' => 'Please Enter Title (AR)',
                'text_en.required' => 'Please Enter Text (EN)',
                'text_ar.required' => 'Please Enter Text (AR)',
            ]);
            $section->title_en = $request->title_en;
            $section->title_ar = $request->title_ar;
            $section->text_en = $request->text_en;
            $section->text_ar = $request->text_ar;
            $section->save();
            return redirect()->back();
        }
        elseif($section->position == 'our_features')
        {
            $validatedData = $request->validate([
                'title' => 'required',
                'text' => 'required',
            ],
            $messages = [
                'title.required' => 'Please Enter Title',
                'text.required' => 'Please Enter Text',
            ]);
            $section->title = $request->title;
            $section->text = $request->text;
            $section->link = $request->link;
            if($request->hasFile('image'))
            {
                $validatedData = $request->validate([
                    'image' => 'required|mimes:jpeg,png,jpg,gif,svg'
                ],
                $messages = [
                    'image.required' => 'Please Choose Image',
                    'image.mimes' => 'Please Choose Valid Image'
                ]);

                if (File::exists(public_path().$section->image))
                {
                    File::delete(public_path().$section->image);
                }

                $image = $request->file('image');
                $imageName = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/sections');
                $image->move($destinationPath, $imageName);
                $section->image = '/uploads/sections/'.$imageName;
                $image = Image::make(public_path().$section->image)->resize(360, 360)->save(public_path().$section->image);
            }
            $section->save();
            return redirect('admin/sections/our_features');

        }
        else if($section->position == 'home_footer')
        {
            $section->text_en = $request->text_en;
            $section->text_ar = $request->text_ar;
            $section->save();
            return redirect()->back();
        }
        else if($section->position == 'header_image')
        {

            if($request->hasFile('image'))
            {
                $validatedData = $request->validate([
                    'image' => 'required|mimes:jpeg,png,jpg,gif,svg'
                ],
                $messages = [
                    'image.required' => 'Please Choose Image',
                    'image.mimes' => 'Please Choose Valid Image'
                ]);

                if (File::exists(public_path().$section->image))
                {
                    File::delete(public_path().$section->image);
                }

                $image = $request->file('image');
                $imageName = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/sections');
                $image->move($destinationPath, $imageName);
                $section->image = '/uploads/sections/'.$imageName;
            }
            $section->save();
            return redirect('admin/sections/header_image');
        }
        else if($section->position == 'tax')
        {
            $section->title_en = $request->title_en;
            $section->save();
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
