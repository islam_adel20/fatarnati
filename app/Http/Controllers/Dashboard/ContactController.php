<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Models\Contact;

class ContactController extends Controller
{
    public function index()
    {
		$contact = Contact::first();
		if($contact === NULL)
		{
			$contact = new Contact;
			$contact->address_en = '-';
			$contact->address_ar = '-';
			$contact->phone = '-';
			$contact->email = '-';
			$contact->map = '-';
			$contact->working_hours = '-';
			$contact->save();
		}
		return view('admin.pages.contact.index')->with(['contact'=>$contact]);
    }

    public function contact_save(Request $request)
    {
		$validatedData = $request->validate([
			'address_en' => 'required',
			'address_ar' => 'required',
			'phone' => 'required',
			'email' => 'required'
        ],
        $messages = [
            'address_en.required' => 'Please Enter Address (EN)',
            'address_ar.required' => 'Please Enter Address (AR)',
            'phone.required' => 'Please Enter Phone Number',
			'email.required' => 'Please Enter Email Address'
		]);

        $contact = Contact::first();
		$contact->address_en = $request->address_en;
		$contact->address_ar = $request->address_ar;
		$contact->phone = $request->phone;
		$contact->email = $request->email;
		$contact->map = $request->map;
		$contact->save();
    	return redirect()->back();
	}
}
