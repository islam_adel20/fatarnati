<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Traits\TraitHelper;
use App\Models\Product;
use App\Models\Category;
use App\Models\Gallery;
use App\Models\ProductImage;
use App\Models\ProductSize;
use App\Models\Size;
use App\Models\Color;
use App\Models\ProductColor;
use COM;

class ProductController extends Controller{
    
    use TraitHelper;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::get();
        return view('admin.pages.products.index')->with(['products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = Category::where('cat', '=', 0)->get();
        $galleries = Gallery::all();
        $sizes = Size::all();
        $colors = Color::all();

        return view('admin.pages.products.create')->with([
            'cats'         => $cats,
            'galleries'    => $galleries,
            'sizes'        => $sizes,
            'colors'       => $colors
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'title_en'                  => 'required',
            'title_ar'                  => 'required',
            'text_en'                   => 'nullable',
            'text_ar'                   => 'nullable',
            'price'                     => 'nullable|numeric',
            'cat'                       => 'required',
            'sub_id'                    => 'required',
            'gallery_id'                => 'nullable',
            'discount'                  => 'numeric|nullable',
            'image'                     => 'nullable',
            'image.*'                   => 'mimes:jpeg,png,jpg,gif,svg'
        ],
        [
            'title_en.required'         => 'Please Enter Product Name (EN)',
            'title_ar.required'         => 'Please Enter Product Name (AR)',
            'text_en.required'          => 'Please Enter Product Description (EN)',
            'text_ar.required'          => 'Please Enter Product Description (AR)',
            'price.required'            => 'Please Enter Product Price',
            'price.numeric'             => 'Product Price Must Be Number',
            'discount.numeric'          => 'Product discount Must Be Number',
            'discount.numeric'          => 'Product stock Must Be Number',
            'cat.required'              => 'Please Choose Product Category',
            'image.required'            => 'Please Choose Product Images',
            'image.*.mimes'             => 'Please Choose Product Images',
        ]);

        $category = Category::find($request->cat);
        $product = new Product;
        $product->title_en = $request->title_en;
        $product->title_ar = $request->title_ar;
        $product->price = $request->price;
        $product->text_en = $request->text_en;
        $product->text_ar = $request->text_ar;
        $product->cat = $request->cat;
        $product->sub_id = $request->sub_id;
        $product->gallery_id = $request->gallery_id ?? 0;
        $product->discount = $request->discount ?? 0;
        $photo_path = "";
        if($request->has("photo"))
        { 
            $photo_path = $this->uploadImage('products', $request->photo);
        }
        $product->photo = $photo_path;
        $product->save();

        $files = $request->file('image');
        $i = 0;
        foreach ($files as $image)
        {
            $product_img = new ProductImage;
            $product_img->product = $product->id;
            $imageName = str_random(20)."_".time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/products/'.$product->id);
            $image->move($destinationPath, $imageName);
            $product_img->image = '/uploads/products/'.$product->id.'/'.$imageName;
            $product_img->save();
        }

        // save product's sizes and there prices
        if (!empty($request->sizes) && !empty($request->prices) &&
            count($request->sizes) == count($request->prices))
        {
            for ($i = 0;$i < count($request->sizes);$i++) {
                $productSize = new ProductSize();
                $productSize->product = $product->id;
                $productSize->size = $request->sizes[$i];
                $productSize->price = $request->prices[$i];
                $productSize->save();
            }
        }

        if (!empty($request->color) && $request->has('color'))
        {
            for ($i = 0;$i < count($request->color);$i++)
            {
                $productColor = new  ProductColor();
                $productColor->product = $product->id;
                $productColor->color = $request->color[$i];
                $productColor->save();
            }
        }

        alert()->success('تم الأضافة بنجاح');
        return redirect()->route('products.index');
    }

    public function show($id)
    {
        $product = Product::findorfail($id);
        return view('admin.pages.products.images')->with(['product'=>$product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findorfail($id);
        $cats = Category::where('cat', '=', 0)->get();
        $galleries = Gallery::all();
        $sizes = Size::get();
        $colors = Color::all();
        $product_colors = ProductColor::where('product', $id)->get();
       
        return view('admin.pages.products.edit')->with([
            'product'         => $product,
            'cats'            => $cats,
            'galleries'       => $galleries,
            'sizes'           => $sizes,
            'colors'          => $colors,
            'product_colors'  => $product_colors
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findorfail($id);
        $validatedData = $request->validate([
            'title_en'            => 'nullable',
            'title_ar'            => 'nullable',
            'text_en'             => 'nullable',
            'text_ar'             => 'nullable',
            'price'               => 'nullable|numeric',
            'discount'            => 'numeric|nullable',
            'cat'                 => 'required',
            'sub_id'              => 'required',
            'gallery_id'          => 'nullable',
            'photo'               => 'sometimes|nullable'
        ],
        [
            'title_en.required'       =>'Please Enter Product Name (EN)',
            'title_ar.required'       =>'Please Enter Product Name (AR)',
            'text_en.required'        =>'Please Enter Product Description (EN)',
            'text_ar.required'        =>'Please Enter Product Description (AR)',
            'price.required'          =>'Please Enter Product Price',
            'price.numeric'           =>'Product Price Must Be Number',
            'price.numeric'           =>'Product Stock Must Be Number',
            'discount.numeric'        =>'Product discount Must Be Number',
            'cat.required'            => 'Please Choose Product Price'
        ]);

        $category = Category::find($request->cat);

        $product->title_en = $request->title_en;
        $product->title_ar = $request->title_ar;
        $product->price = $request->price;
        $product->text_en = $request->text_en;
        $product->text_ar = $request->text_ar;
        $product->cat = $request->cat;
        $product->sub_id = $request->sub_id;
        $product->gallery_id = $request->gallery_id ?? 0;
        $product->discount = $request->discount ?? 0;
        $file_path = "";
        if($request->has("photo"))
        { 

            if(file_exists($product->photo))
            { 
                File::delete($product->photo);
            }
        
            $file_path = $this->uploadImage('products', $request->photo);
        }

        $product->photo = $file_path;
        $product->save();

        // save product's sizes and there prices
        if (!empty($request->deleted_sizes)) {
            $sizes = ProductSize::whereIn('id', explode(',', trim($request->deleted_sizes, ',')))->get();

            foreach ($sizes as $size) {
                $size->delete();
            }
        }

        // save product's sizes and there prices
        if (!empty($request->sizes) && !empty($request->prices) &&
            count($request->sizes) == count($request->prices))
        {
            for ($i = 0;$i < count($request->sizes);$i++) {
                $sizeFound = ProductSize::where('size', $request->sizes[$i])
                    ->where('price', $request->prices[$i])
                    ->where('product', $product->id)
                    ->first();

                if (!empty($sizeFound)) {
                    continue;
                }

                $productSize = new ProductSize();
                $productSize->product = $product->id;
                $productSize->size = $request->sizes[$i];
                $productSize->price = $request->prices[$i];
                $productSize->save();
            }
        }

        if (!empty($request->color) && $request->has('color'))
        {
            for ($i = 0;$i < count($request->color);$i++)
            {
                $productColor = new  ProductColor();
                $productColor->product = $product->id;
                $productColor->color = $request->color[$i];
                $productColor->save();
            }
        }

        alert()->success('تم التحديث بنجاح');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findorfail($id);
        if(file_exists($product->photo))
        { 
            File::delete($product->photo);
        }
        $product->delete();

        alert()->success('تم الحذف بنجاح');
        return redirect()->back();
    }

    public function delete_product_color($id)
    {   
        $ProductColor = ProductColor::findorfail($id);
        $ProductColor->delete();
        alert()->success('تم حذف اللون بنجاح');
        return redirect()->back();
    }


    public function filter_subs_category(Request $request)
    {   
        $cat_id = $request->cat_id;
        $cat_subs = Category::where('cat', $cat_id)->get();
        $html = view('admin.pages.products.cat_subs', ['cat_subs' => $cat_subs])->render();
        return response()->json(['status' => 'success', 'result' => $html]);
    
    }

   
}
