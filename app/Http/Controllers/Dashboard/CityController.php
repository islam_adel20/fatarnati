<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\City;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::where('hide', '=', 0)->get();
        return view('admin.pages.cities.index')->with(['cities'=>$cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.cities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required'
        ],
        [
            'name_en.required'=>'Please Enter City Name (EN)',
            'name_ar.required'=>'Please Enter City Name (AR)'
        ]);

        $city = new City;
        $city->name_en  = $request->name_en;
        $city->name_ar  = $request->name_ar;
        alert()->success('تم الأضافة بنجاح');
        $city->save();

        return redirect()->route('cities.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = City::findorfail($id);
        if($city->hide == 0)
        {
            return view('admin.pages.cities.edit')->with(['city'=>$city]);
        }
        else
        {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $city = City::findorfail($id);
        if($city->hide == 0)
        {
            $validatedData = $request->validate([
                'name_en' => 'required',
                'name_ar' => 'required'
            ],
            [
                'name_en.required'=>'Please Enter City Name (EN)',
                'name_ar.required'=>'Please Enter City Name (AR)'
            ]);
            $city->name_en  = $request->name_en;
            $city->name_ar  = $request->name_ar;
            $city->save();

            alert()->success('تم التحديث بنجاح');
            return redirect()->route('cities.index');
        }
        else
        {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city = City::findorfail($id);
        $city->delete();
        alert()->success('تم الحذف بنجاح');
        return redirect()->route('cities.index');
    }
}
