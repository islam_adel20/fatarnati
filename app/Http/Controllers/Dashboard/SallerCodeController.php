<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SallerCode;
use App\Models\Gallery;
use Carbon\Carbon;

class SallerCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $codes = SallerCode::get();
        return view('admin.pages.saller_codes.index')->with(['codes'  =>  $codes]);
    }

    public function create()
    {
        return view('admin.pages.saller_codes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $validatedData = $request->validate(
      [
        'num'       => 'required',
        'duration'  => 'required',
        'cost'      => 'required'
      ],
      [
        'num.required'      =>  'من فضلك ادخل عدد الأكواد',
        'duration.required' => 'من فضلك ادخل المدة الزمنية',
        'cost.required'     => 'من فضلك أدخل تكلفة الأشتراك'
      ]);


        $count_codes = $request->num;

        for($i = 0; $i <= $count_codes-1; $i++){

          $code =  str_random(4).time();
           SallerCode::create([
             'code'      => $code,
             'duration'  => $request->duration,
             'cost'      => $request->cost
           ]);
        }

        return redirect('admin/saller/codes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit($id)
    {
        $code = SallerCode::findorfail($id);
        return view('admin.pages.saller_codes.edit', compact('code'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $code = SallerCode::findorfail($id);
        $validatedData = $request->validate(
        [
          'code'      => 'required',
          'duration'  => 'required',
          'cost'      => 'required'
        ],
        [
          'code.required'       =>  'من فضلك ادخل الكود',
          'duration.required'   => 'من فضلك ادخل المدة الزمنية',
          'cost.required'       => 'من فضلك أدخل تكلفة الأشتراك'
        ]);

        $code->update([
          'code'      => $request->code,
          'duration'  => $request->duration,
          'cost'      => $request->cost
        ]);

        return redirect('admin/saller/codes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $code = SallerCode::findorfail($id);
        $code->delete();
        return redirect()->back();
    }


}
