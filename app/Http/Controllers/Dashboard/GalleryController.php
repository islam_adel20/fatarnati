<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Models\ProductFavorite;
use App\Models\Gallery;
use App\Models\Order;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = Gallery::all();
        return view('admin.pages.galleries.index')->with(['galleries'=>$galleries]);
    }

    public function saller_orders($id)
    {
      $saller_orders = Order::whereHas('product', function($query) use($id){
            $query->where('gallery_id', $id);
        })->get();
        return view('admin.pages.galleries.orders')->with(['saller_orders' => $saller_orders]);
    }

    public function productsFavorite($id)
    {
        $products = ProductFavorite::where('saller_id', $id)->get();
        return view('admin.pages.galleries.products_favorite')->with(['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.galleries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name_en'                     => 'required',
            'name_ar'                     => 'required',
            'phone'                       => 'required',
            'address_en'                  => 'required',
            'address_ar'                  => 'required',
            'map'                         => 'required',
            'image'                       => 'sometimes|nullable|image|mimes:jpeg,jpg,png',
            'commercial_register'         => 'sometimes|nullable|image|mimes:jpeg,jpg,png',
            'nationalist'                 => 'sometimes|nullable|image|mimes:jpeg,jpg,png',
            'instgram_link'               => 'nullable',
            'facebook_link'               => 'nullable',
            'twitter_link'                => 'nullable',
            'youtube_link'                => 'nullable'
        ],
        [
            'name_en.required'            => 'Please Enter Gallery Name (EN)',
            'name_ar.required'            => 'Please Enter Gallery Name (AR)',
            'phone.required'              => 'Please Enter Gallery Phone',
            'address_en.required'         => 'Please Enter Gallery Address (EN)',
            'address_ar.required'         => 'Please Enter Gallery Address (AR)',
            'map.required'                => 'Please Enter Gallery Map',
        ]);

        $gallery = new Gallery;
        $gallery->name_en  = $request->name_en;
        $gallery->name_ar  = $request->name_ar;
        $gallery->phone  = $request->phone;
        $gallery->address_en  = $request->address_en;
        $gallery->address_ar  = $request->address_ar;
        $gallery->map  = $request->map;


        if($request->has('image') && $request->image != null)
        {
          $image = $request->file('image');
          $imageName = time().'.'.$image->getClientOriginalExtension();
          $destinationPath = public_path('/uploads/galleries');
          $image->move($destinationPath, $imageName);
          $gallery->image ='/uploads/galleries/'.$imageName;
        }

        if($request->has('commercial_register') && $request->commercial_register != null)
        {
          $commercial_register = $request->file('commercial_register');
          $imageNameCommercial = time().'.'.$commercial_register->getClientOriginalExtension();
          $destinationPath = public_path('/uploads/galleries');
          $commercial_register->move($destinationPath, $imageNameCommercial);
          $gallery->commercial_register ='/uploads/galleries/'.$imageNameCommercial;
        }

        if($request->has('nationalist') && $request->nationalist != null)
        {
          $nationalist = $request->file('nationalist');
          $imageNameNationalist = time().'.'.$nationalist->getClientOriginalExtension();
          $destinationPath = public_path('/uploads/galleries');
          $image->move($destinationPath, $imageNameNationalist);
          $gallery->nationalist ='/uploads/galleries/'.$imageNameNationalist;
        }

        $gallery->instgram_link   = $request->instgram_link;
        $gallery->facebook_link   = $request->facebook_link;
        $gallery->twitter_link    = $request->twitter_link;
        $gallery->youtube_link    = $request->youtube_link;
        $gallery->save();

        alert()->success('تم الأضافة بنجاح');
        return redirect()->route('galleries.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = Gallery::findorfail($id);
        return view('admin.pages.galleries.edit', compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gallery = Gallery::findorfail($id);

        $validatedData = $request->validate([
            'name_en'                     => 'required',
            'name_ar'                     => 'required',
            'phone'                       => 'required',
            'address_en'                  => 'required',
            'address_ar'                  => 'required',
            'map'                         => 'required',
            'image'                       => 'sometimes|nullable|image|mimes:jpeg,jpg,png',
            'commercial_register'         => 'sometimes|nullable|image|mimes:jpeg,jpg,png',
            'nationalist'                 => 'sometimes|nullable|image|mimes:jpeg,jpg,png',
            'instgram_link'               => 'nullable',
            'facebook_link'               => 'nullable',
            'twitter_link'                => 'nullable',
            'youtube_link'                => 'nullable'
        ],
        [
            'name_en.required'            => 'Please Enter Gallery Name (EN)',
            'name_ar.required'            => 'Please Enter Gallery Name (AR)',
            'phone.required'              => 'Please Enter Gallery Phone',
            'address_en.required'         => 'Please Enter Gallery Address (EN)',
            'address_ar.required'         => 'Please Enter Gallery Address (AR)',
            'map.required'                => 'Please Enter Gallery Map',
        ]);
        $gallery->name_en  = $request->name_en;
        $gallery->name_ar  = $request->name_ar;
        $gallery->phone  = $request->phone;
        $gallery->address_en  = $request->address_en;
        $gallery->address_ar  = $request->address_ar;
        $gallery->map  = $request->map;

        if($request->hasFile('image'))
        {
            $validatedData = $request->validate([
                'image' => 'required|mimes:jpeg,png,jpg,gif,svg,webp'
            ],
            $messages = [
                'image.required' => 'Please Choose Image',
                'image.mimes' => 'Please Choose Valid Image File',
            ]);

            if (File::exists(public_path().$gallery->image))
            {
                File::delete(public_path().$gallery->image);
            }

            if (File::exists(public_path().$gallery->nationalist))
            {
                File::delete(public_path().$gallery->nationalist);
            }

            if (File::exists(public_path().$gallery->commercial_register))
            {
                File::delete(public_path().$gallery->commercial_register);
            }

            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/galleries');
            $image->move($destinationPath, $imageName);
            $gallery->image = '/uploads/galleries/'.$imageName;
        }

        if($request->has('commercial_register') && $request->commercial_register != null)
        {
          $commercial_register = $request->file('commercial_register');
          $imageNameCommercial = time().'.'.$commercial_register->getClientOriginalExtension();
          $destinationPath = public_path('/uploads/galleries');
          $commercial_register->move($destinationPath, $imageNameCommercial);
          $gallery->commercial_register ='/uploads/galleries/'.$imageNameCommercial;
        }

        if($request->has('nationalist') && $request->nationalist != null)
        {
          $nationalist = $request->file('nationalist');
          $imageNameNationalist = time().'.'.$nationalist->getClientOriginalExtension();
          $destinationPath = public_path('/uploads/galleries');
          $image->move($destinationPath, $imageNameNationalist);
          $gallery->nationalist ='/uploads/galleries/'.$imageNameNationalist;
        }

        $gallery->instgram_link   = $request->instgram_link;
        $gallery->facebook_link   = $request->facebook_link;
        $gallery->twitter_link    = $request->twitter_link;
        $gallery->youtube_link    = $request->youtube_link;
        $gallery->save();

        alert()->success('تم التحديث بنجاح');
        return redirect()->route('galleries.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = Gallery::findorfail($id);

        if (File::exists(public_path().$gallery->image))
        {
            File::delete(public_path().$gallery->image);
        }

        if (File::exists(public_path().$gallery->nationalist))
        {
            File::delete(public_path().$gallery->nationalist);
        }

        if (File::exists(public_path().$gallery->commercial_register))
        {
            File::delete(public_path().$gallery->commercial_register);
        }

        $gallery->delete();
        alert()->success('تم الحذف بنجاح');
        return redirect()->back();
    }

    /**
     * Show gallery's products.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function products($id)
    {
        return view('admin.pages.galleries.products', [
            'gallery' => Gallery::findOrFail($id)
        ]);
    }

    /**
     * Show gallery's orders.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function orders($id)
    {
        $orders = Order::whereHas('items', function($query) use($id) {
            $query->whereHas('product_info', function($query2) use($id) {
                $query2->where('gallery_id', $id);
            });
        })->get();

        return view('admin.pages.galleries.orders', [
            'gallery' => Gallery::findOrFail($id),
            'statuss' => OrderStatus::get(),
            'orders' => $orders,
        ]);
    }
}
