<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Models\City;
use App\Models\ProductFavorite;


class ClientController extends Controller
{
    public function index()
    {
        $clients = User::get();
        return view('admin.pages.clients.index')->with(['clients' =>  $clients]);
    }

    public function show($id)
    {
        return view('admin.pages.clients.show')->with(['client' => User::findorfail($id)]);
    }

    public function products_favorite($id)
    {
        $products = ProductFavorite::where('user_id', $id)->get();
        return view('admin.pages.clients.products_favorite')->with(['products' =>  $products]);
    }

}
