<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\User;
use App\Models\Product;
use App\Models\Order;
use App\Models\Gallery;
use App\Models\Category;

class DashboardController extends Controller
{
    public function index()
    {
        $admins = Admin::get();
        $clients = User::get();
        //=================================
        $products = Product::get();
        $products_show = Product::where('deleted_at', null)->get();
        $products_soft_delete = Product::onlyTrashed()->get();
        //=================================
        $orders = Order::get();
        $orders_users = Order::select('user_id')->groupBy('user_id')->get();
        $orders_products = Order::select('product_id')->groupBy('product_id')->get();
        //=================================
        $allGalleries = Gallery::all();
        $gallers_active = Gallery::where('status', '1')->get();
        $gallers_unactive = Gallery::where('status', '0')->get();
        //=================================
        $category = Category::all();
        $category_main = Category::where('cat', 0)->get();
        $category_sub = Category::where('cat', '!=', 0)->get();
        //=================================

        return view('admin.home')->with([
            'admins'                      =>  $admins,
            'clients'                     =>  $clients,
            'orders'                      =>  $orders,
            'orders_users'                =>  $orders_users,
            'orders_products'             =>  $orders_products,
            'products'                    =>  $products,
            'products_show'               =>  $products_show,
            'products_soft_delete'        =>  $products_soft_delete,
            'allGalleries'                =>  $allGalleries,
            'gallers_active'              =>  $gallers_active,
            'gallers_unactive'            =>  $gallers_unactive,
            'category'                    =>  $category,
            'category_main'               =>  $category_main,
            'category_sub'                =>  $category_sub
        ]);
    }

    public function profile()
    {
        return view('admin.pages.admins.profile');
    }

    public function save_profile(Request $request)
    {
        $admin = Admin::findorfail(Auth::guard('admin')->user()->id);
        $validatedData = $request->validate([
            'name' => 'required',
            'phone' => 'required'
        ],
        [
            'name.required'=>'Please Enter Name',
            'phone.required'=>'Please Enter Phone Number'
        ]);

        if($admin->user_name != $request->user_name)
        {
            $validatedData = $request->validate([
                'user_name' => ['required', Rule::unique('admins')->where(function($query) {$query->where('hide', '=', 0);})]
            ],
            [
                'user_name.required'=>'Please Enter User Name',
                'user_name.unique'=>'This User Name Is Registered To Another User'
            ]);
        }
        if($admin->email != $request->email)
        {
            $validatedData = $request->validate([
                'email' => ['required', 'email', Rule::unique('admins')->where(function($query) {$query->where('hide', '=', 0);})]
            ],
            [
                'email.required'=>'Please Enter Email Address',
                'email.unique'=>'This Email Address Is Registered To Another User',
                'email.email'=>'Please Enter Correct Email Address'
            ]);
        }

        $admin->name  = $request->name;
        $admin->user_name = $request->user_name;
        $admin->phone = $request->phone;
        $admin->email = $request->email;

        if($request->hasFile('image'))
        {
            $validatedData = $request->validate([
                'image' => 'mimes:jpeg,png,jpg,gif,svg,gif,webp'
            ],
            $messages = [
                'image.mimes' => 'Please Choose Image File'
            ]);

            if (File::exists(public_path().$admin->image))
            {
                File::delete(public_path().$admin->image);
            }

            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/admins');
            $image->move($destinationPath, $imageName);
            $admin->image = '/uploads/admins/'.$imageName;
        }

        $admin->save();
        Session::flash('alert_message','Account Information Changed Successfully');
        return redirect('admin/profile');
    }

    public function change_password()
    {
        return view('admin.pages.admins.change_password');
    }

    public function password_save(Request $request)
    {
        $admin = Admin::findorfail(Auth::guard('admin')->user()->id);
        $validatedData = $request->validate([
            'password' => 'required|min:6|confirmed',
        ],
        [
            'password.required'=>'Please Enter Password',
            'password.min'=>'Password Must Be At Least 6 Charachters',
            'password.confirmed'=>'Password & Its Confirmation Not Matching',
        ]);
        $admin->password = bcrypt($request->password);
        $admin->save();
        Session::flash('alert_message','Password Changed Successfully');
        return redirect('admin/change_password');
    }
}
