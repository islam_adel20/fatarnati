<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Slider;
use App\Models\Setting;

class SliderController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::get();
        return view('admin.pages.slider.index')->with(['sliders'    =>  $sliders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg',
            'title' => 'sometimes|nullable',
            'link'  => 'sometimes|nullable',
        ],
        $messages = [
            'image.required' => 'Please Choose Image',
            'image.mimes' => 'Please Choose Valid Image',
        ]);

        $slider = new Slider;
        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/slider');
        $image->move($destinationPath, $imageName);
        $slider->image = '/uploads/slider/'.$imageName;
        $image = Image::make(public_path().$slider->image)->resize(1280, 500)->save(public_path().$slider->image);
        $slider->title = $request->title;
        $slider->link = $request->link;
        $slider->save();
        return redirect()->route('slider.index');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::findOrFail($id);
        return view('admin.pages.slider.edit')->with(['slider'=>$slider]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
        $validatedData = $request->validate([
            'image' => 'sometimes|nullable|mimes:jpeg,png,jpg,gif,svg',
            'title' => 'sometimes|nullable',
            'link'  => 'sometimes|nullable',
        ],
        $messages = [
            'image.required' => 'Please Choose Image',
            'image.mimes' => 'Please Choose Valid Image'
        ]);

        $slider = Slider::findOrFail($id);

        if($request->hasFile('image'))
        {

            if (File::exists(public_path().$slider->image))
            {
                File::delete(public_path().$slider->image);
            }

            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/slider');
            $image->move($destinationPath, $imageName);
            $slider->image = '/uploads/slider/'.$imageName;
            $image = Image::make(public_path().$slider->image)->resize(1280, 500)->save(public_path().$slider->image);
        }
        
        $slider->title = $request->title;
        $slider->link = $request->link;
        $slider->save();
        return redirect()->route('slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::findOrFail($id);

        if (File::exists(public_path().$slider->image))
        {
            File::delete(public_path().$slider->image);
        }
        $slider->delete();
        return redirect()->route('slider.index');
    }
}
