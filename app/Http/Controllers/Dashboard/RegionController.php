<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Region;
use App\Models\City;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regions = Region::where('hide', '=', 0)->get();
        return view('admin.pages.regions.index')->with(['regions' => $regions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::get();
        return view('admin.pages.regions.create', compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required',
            'city_id' => 'required|exists:cities,id'
        ],
        [
            'name_en.required'  =>  'ادخل الأسم (EN)',
            'name_ar.required'  =>  'ادخل الأسم (AR)',
            'city_id.required'  =>  'ادخل المدينة',
            'city_id.exists'  =>  'المدينة ليست مسجلة',
        ]);

        $region = new Region;
        $region->name_en  = $request->name_en;
        $region->name_ar  = $request->name_ar;
        $region->city_id = $request->city_id;
        $region->save();

        alert()->success('تم الأضافة بنجاح');
        return redirect()->route('regions.index');
    }


    public function edit($id)
    {
        $region = Region::findorfail($id);

        if($region->hide == 0)
        {
            $cities = City::get();
            return view('admin.pages.regions.edit')->with(['region' => $region, 'cities' => $cities]);
        }
        else
        {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $region = Region::findorfail($id);
        if($region->hide == 0)
        {
            $validatedData = $request->validate([
                'name_en' => 'required',
                'name_ar' => 'required',
            ],
            [
                'name_en.required'=>'Please Enter Region Name (EN)',
                'name_ar.required'=>'Please Enter Region Name (AR)',
            ]);
            $region->name_en  = $request->name_en;
            $region->name_ar  = $request->name_ar;
            $region->city_id = $request->city_id;
            $region->save();
            alert()->success('تم التحديث بنجاح');
            return back();
        }
        else
        {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $region = Region::findorfail($id);
        $region->delete();
        alert()->success('تم الحذف بنجاح');
        return redirect()->route('regions.index');
    }
}
