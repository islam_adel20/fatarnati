<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Models\ServiceSaller;
use App\Models\CategoryServices;
use App\Models\City;

class SallerServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $saller_servcies = ServiceSaller::all();
        return view('admin.pages.sallers_services.index')->with(['saller_servcies'  =>  $saller_servcies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = CategoryServices::get();
        $cities = City::get();
        return view('admin.pages.sallers_services.create', compact('cats', 'cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name'            => 'required',
            'email'           => 'required',
            'phone'           => 'required',
            'password'        => 'required',
            'desc'            => 'required',
            'city_id'         => 'required',
            'cat_id'          => 'required',
            'image'           => 'sometimes|nullable|mimes:jpg,jpeg,png,svg',
            'instgram_link'   => 'nullable',
            'facebook_link'   => 'nullable',
            'twitter_link'    => 'nullable',
            'youtube_link'    => 'nullable',
            'map'             => 'nullable',
        ],
        [
            'name.required'       => 'Please Enter Name',
            'email.required'      => 'Please Enter Email',
            'phone.required'      =>'Please Enter Phone',
            'desc.required'       =>'Please Enter Description',
            'password.required'   =>'Please Enter Password)',
            'city_id.required'    =>'Please Enter City',
            'cat_id.required'     =>'Please Enter Category',
        ]);

        $saller = new ServiceSaller;
        $saller->name  = $request->name;
        $saller->email  = $request->email;
        $saller->phone  = $request->phone;
        $saller->password = bcrypt($request->password);
        $saller->city_id  = $request->city_id;
        $saller->cat_id  = $request->cat_id;

        if($request->has('image') && $request->image != null)
        {
          $image = $request->file('image');
          $imageName = time().'.'.$image->getClientOriginalExtension();
          $destinationPath = public_path('/uploads/sallers_services');
          $image->move($destinationPath, $imageName);
          $saller->image ='/uploads/sallers_services/'.$imageName;
        }

        $saller->instgram_link   = $request->instgram_link;
        $saller->facebook_link   = $request->facebook_link;
        $saller->twitter_link    = $request->twitter_link;
        $saller->youtube_link    = $request->youtube_link;
        $saller->save();
        alert()->success('تم الأضافة بنجاح');
        return redirect()->route('services.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = ServiceSaller::findorfail($id);
        $cats = CategoryServices::get();
        $cities = City::get();
        return view('admin.pages.sallers_services.edit', compact('row', 'cats', 'cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $saller = ServiceSaller::findorfail($id);

        $validatedData = $request->validate([
            'name'            => 'required',
            'email'           => 'required',
            'phone'           => 'required',
            'password'        => 'sometimes|nullable',
            'desc'            => 'required',
            'city_id'         => 'required',
            'cat_id'          => 'required',
            'image'           => 'sometimes|nullable|mimes:jpg,jpeg,png,svg',
            'instgram_link'   => 'nullable',
            'facebook_link'   => 'nullable',
            'twitter_link'    => 'nullable',
            'youtube_link'    => 'nullable',
            'map'             => 'nullable'
        ],
        [
            'name.required'       => 'Please Enter Name',
            'email.required'      => 'Please Enter Email',
            'phone.required'      =>'Please Enter Phone',
            'desc.required'       =>'Please Enter Description',
            'password.required'   =>'Please Enter Password)',
            'city_id.required'    =>'Please Enter City',
            'cat_id.required'     =>'Please Enter Category',
        ]);


        $saller->name  = $request->name;
        $saller->desc  = $request->desc;
        $saller->email  = $request->email;
        $saller->phone  = $request->phone;
        if($request->password)
        {
            $saller->password = bcrypt($request->password);
        }
        $saller->city_id  = $request->city_id;
        $saller->cat_id  = $request->cat_id;

        if($request->has('image') && $request->image != null)
        {

          if (File::exists(public_path().$saller->image))
          {
              File::delete(public_path().$saller->image);
          }

          $image = $request->file('image');
          $imageName = time().'.'.$image->getClientOriginalExtension();
          $destinationPath = public_path('/uploads/sallers_services');
          $image->move($destinationPath, $imageName);
          $saller->image ='/uploads/sallers_services/'.$imageName;
        }
        $saller->instgram_link   = $request->instgram_link;
        $saller->facebook_link   = $request->facebook_link;
        $saller->twitter_link    = $request->twitter_link;
        $saller->youtube_link    = $request->youtube_link;
        $saller->save();

        alert()->success('تم التحديث بنجاح');
        return redirect()->route('services.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $saller = ServiceSaller::findorfail($id);

        if (File::exists(public_path().$saller->image))
        {
            File::delete(public_path().$saller->image);
        }
        
        $saller->delete();

        alert()->success('تم الحذف بنجاح');
        return redirect()->back();
    }


}
