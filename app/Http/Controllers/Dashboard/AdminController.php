<?php
namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

use App\Models\Admin;
use App\Models\Permission;

class AdminController extends Controller
{
    public function index()
    {
        if (!Auth::guard('admin')->user()->is_super) {abort(404);}
        $admins = Admin::get();
        return view('admin.pages.admins.index')->with(['admins'=>$admins]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::guard('admin')->user()->is_super) {abort(404);}
        return view('admin.pages.admins.create', ['permissions' => Permission::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'user_name' => 'required|unique:admins,user_name,NULL,id,deleted_at,NULL',
            'email' => 'required|unique:admins,email,NULL,id,deleted_at,NULL',
            'password' => 'required|min:6|confirmed',
            'phone' => 'required',
            'image' => 'mimes:jpeg,png,jpg,gif,svg,gif,webp'
        ],
        [
            'name.required'=>'Please Enter Name',
            'user_name.required'=>'Please Enter User Name',
            'user_name.unique'=>'This User Name Is Registered To Another User',
            'email.required'=>'Please Enter Email Address',
            'email.unique'=>'This Email Address Is Registered To Another User',
            'email.email'=>'Please Enter Correct Email Address',
            'password.required'=>'Please Enter Password',
            'password.min'=>'Password Must Be At Least 6 Charachters',
            'password.confirmed'=>'Password & Its Confirmation Not Matching',
            'phone.required'=>'Please Enter Phone Number',
            'image.mimes' => 'Please Choose Image File'
        ]);
        $admin = new Admin;
        $admin->name  = $request->name;
        $admin->user_name = $request->user_name;
        $admin->phone = $request->phone;
        $admin->email = $request->email;
        $admin->password = bcrypt($request->password);
        if($request->hasFile('image'))
        {
            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/admins');
            $image->move($destinationPath, $imageName);
            $admin->image ='/uploads/admins/'.$imageName;
        }

        if($request->is_super == 1)
        {
          $admin->is_super = $request->is_super;
        }

        $admin->save();

        // save permissions
        if (!empty($request->permissions)) {
            $admin->permissions()->attach($request->permissions);
        }

        alert()->success('تم الأضافة بنجاح');
        return redirect()->route('admins.index');
    }

    public function edit($id)
    {
        if (!Auth::guard('admin')->user()->is_super &&
            !Auth::guard('admin')->user()->id != $id)
        {
            abort(404);
        }

        $admin = Admin::findorfail($id);
        return view('admin.pages.admins.edit')->with(['admin'=>$admin, 'permissions' => Permission::all()]);
    }

    public function change_password($id)
    {
        if (!Auth::guard('admin')->user()->is_super &&
            !Auth::guard('admin')->user()->id != $id)
        {
            abort(404);
        }

        $admin = Admin::findorfail($id);
        alert()->success('تم التحديث بنجاح');
        return view('admin.pages.admins.edit_password')->with(['admin'=>$admin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = Admin::findorfail($id);
        $validatedData = $request->validate([
            'name' => 'required',
            'phone' => 'required'
        ],
        [
            'name.required'=>'Please Enter Name',
            'phone.required'=>'Please Enter Phone Number',
        ]);

        $admin = Admin::findorfail($id);
        if($admin->user_name != $request->user_name)
        {
            $validatedData = $request->validate([
                'user_name' => 'required|unique:admins,user_name,'.$id.',id,deleted_at,NULL'
            ],
            [
                'user_name.required'=>'Please Enter User Name',
                'user_name.unique'=>'This User Name Is Registered To Another User'
            ]);
        }
        if($admin->email != $request->email)
        {
            $validatedData = $request->validate([
                'email' => 'required|unique:admins,email,'.$id.',id,deleted_at,NULL'
            ],
            [
                'email.required'=>'Please Enter Email Address',
                'email.unique'=>'This Email Address Is Registered To Another User',
                'email.email'=>'Please Enter Correct Email Address',
            ]);
        }

        $admin->name  = $request->name;
        $admin->user_name = $request->user_name;
        $admin->phone = $request->phone;
        $admin->email = $request->email;

        if($request->hasFile('image'))
        {
            $validatedData = $request->validate([
                'image' => 'mimes:jpeg,png,jpg,gif,svg,gif,webp'
            ],
            $messages = [
                'image.mimes' => 'Please Choose Image File'
            ]);

            if (File::exists(public_path().$admin->image))
            {
                File::delete(public_path().$admin->image);
            }

            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/admins');
            $image->move($destinationPath, $imageName);
            $admin->image = '/uploads/admins/'.$imageName;
        }

        if($request->is_super == 1)
        {
          $admin->is_super = $request->is_super;
        }
        $admin->save();

        // update permissions
        if (!empty($request->permissions)) {
            $admin->permissions()->sync($request->permissions);
        }

        alert()->success('تم التحديث بنجاح');
        return redirect()->route('admins.index');
    }

    public function password_save (Request $request, $id)
    {
        $admin = Admin::findorfail($id);
        $validatedData = $request->validate([
            'password' => 'required|min:6|confirmed',
        ],
        [
            'password.required'=>'Please Enter Password',
            'password.min'=>'Password Must Be At Least 6 Charachters',
            'password.confirmed'=>'Password & Its Confirmation Not Matching',
        ]);
        $admin->password = bcrypt($request->password);
        $admin->save();
        return redirect()->route('admins.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::guard('admin')->user()->is_super) {abort(404);}
        $admin = Admin::findorfail($id);
        $admin->delete();

        alert()->success('تم الحذف بنجاح');
        return redirect()->route('admins.index');
    }
}
