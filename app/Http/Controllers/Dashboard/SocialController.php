<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Social;

class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $socail = Social::all();
        return view('admin.pages.social.index')->with(['socail'=>$socail]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.social.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'network' => 'required',
            'link' => 'required|url'
        ],
        $messages = [
            'network.required' => 'Please Choose Website',
            'link.required' => 'Please Enter URL',
            'link.url' => 'Please Enter Valid URL'
        ]);
        $social = new Social;
        $social->name = $request->network;
        $social->link = $request->link;
        $social->save();
        return redirect()->route('social.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $social = Social::findOrFail($id);
        return view('admin.pages.social.edit')->with(['social'=>$social]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'network' => 'required',
            'link' => 'required|url'
        ],
        $messages = [
            'network.required' => 'Please Choose Website',
            'link.required' => 'Please Enter URL',
            'link.url' => 'Please Enter Valid URL'
        ]);
        $social = Social::findOrFail($id);
        $social->name = $request->network;
        $social->link = $request->link;
        $social->save();
        return redirect()->route('social.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $social = Social::findOrFail($id);
        $social->delete();
        return redirect()->route('social.index');
    }
}
