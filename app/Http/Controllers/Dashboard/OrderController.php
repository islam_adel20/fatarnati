<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductSize;
use App\Models\Order;

class OrderController extends Controller
{
  public function all_orders()
  {
    $all_orders = Order::get();
    return view('admin.pages.orders.all_orders', compact('all_orders'));
  }

  public function delete_order($id)
  {
    $order = Order::find($id);
    $order->delete();
    alert()->success('تم الحذف بنجاح');
    return back();
  }


}
