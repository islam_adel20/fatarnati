<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cookie;

use Validator;
use Cart;
use App;
use DB;

use App\Models\PromoCode;
use App\Models\Subscriber;
use App\Models\Slider;
use App\Models\News;
use App\Models\Block;
use App\Models\Country;
use App\Models\City;
use App\Models\User;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\ProductColor;
use App\Models\ProductSize;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Category;
use App\Models\Gallery;
use App\Models\Section;
use App\Models\Social;
use App\Models\OrderStatus;
use App\Models\About;
use App\Models\Contact;
use App\Models\Article;
use App\Models\PayMethod;
use App\Models\Address;

class HomeController extends Controller
{
    /****
     * Home Page
     */
    public function home()
    {
        $bestSellingItemsIDs = [];
        $bestSellingItemsIDs = OrderItem::select('product', DB::raw('count(*) as total'))
            ->groupBy('product')->get()->pluck('product')->toArray();

        $sliders = Slider::all();
        $categories = Category::all();
        $homeGalleries = Gallery::all();
        $bestSellingProducts = Product::whereIn('id', $bestSellingItemsIDs)->orderBy('id', 'desc')->limit(10)->get();
        $weeklyOfferProducts = Product::where('discount', '!=', 0)->orderBy('id', 'desc')->limit(10)->get();
        $newProducts = Product::where('discount', 0)->orderBy('id', 'desc')->limit(10)->get();
        $socials = Social::all();

        return view('home')->with([
            'sliders'=>$sliders,
            'categories'=>$categories,
            'homeGalleries'=>$homeGalleries,
            'bestSellingProducts'=>$bestSellingProducts,
            'weeklyOfferProducts'=>$weeklyOfferProducts,
            'newProducts'=>$newProducts,
            'socials'=>$socials
        ]);
    }

    public function return_policy ()
    {
        $section = Section::where('position', 'return_policy')->first();
        $socials = Social::all();
        return view('return-policy')->with(['section'=>$section, 'socials'=>$socials]);
    }

    public function terms ()
    {
        $section = Section::where('position', 'terms_conditions')->first();
        $socials = Social::all();
        return view('terms')->with(['section'=>$section, 'socials'=>$socials]);
    }

    public function privacy ()
    {
        $section = Section::where('position', 'privacy')->first();
        $socials = Social::all();
        return view('privacy')->with(['section'=>$section, 'socials'=>$socials]);
    }

    public function helpCenter ()
    {
        $section = Section::where('position', 'help_center')->first();
        $socials = Social::all();
        return view('privacy')->with(['section'=>$section, 'socials'=>$socials]);
    }

    public function tips ()
    {
        $links = Article::all();
        $socials = Social::all();
        return view('tips')->with(['links'=>$links, 'socials'=>$socials]);
    }

    public function about ()
    {
        $section = Section::where('position', 'about_home')->first();
        $socials = Social::all();
        return view ('about', compact('section', 'socials'));
    }

    public function categories ()
    {
        $categories = Category::all();
        $socials = Social::all();
        return view ('categories', compact('categories', 'socials'));
    }

    public function galleries ()
    {
        $galleries = Gallery::all();
        $socials = Social::all();
        return view ('galleries', compact('galleries', 'socials'));
    }

    public function gallery ($id)
    {
        $gallery = Gallery::findorfail($id);
        $socials = Social::all();
        return view ('gallery', compact('gallery', 'socials'));
    }
    public function contact ()
    {
        $socials = Social::all();
        $contact_info = Contact::first();
        return view ('contact', compact('socials', 'contact_info'));
    }

    public function send_contact (Request $request)
    {
        $request->validate([
			'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'subject' => 'required',
            'message' => 'required'
        ]);

        $contact = Contact::first();

        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $smsg = $request->input('message');

        $to = $contact->email;

        $subject = $request->input('subject');

        $message = '<html><body>';
        $message .= "<p><b>Name : </b>".$name."</p><p><b>E-mail Address : </b>".$email."</p>
        <p><b>Phone : </b>".$phone."</p>".$smsg;
        $message .= '</body></html>';

        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=utf-8';
        $headers[] = 'To: Fitoverfat <'.$to.'>';
        $headers[] = 'From: '.$name.' <'.$email.'>';


        // $msg = new ContactMessage;
        // $msg->name = $name;
        // $msg->email = $email;
        // $msg->phone = $phone;
        // $msg->subject = $subject;
        // $msg->msg = $smsg;
        // $msg->save();
        // $message = view('mail.contact', compact('subject', 'name', 'email', 'phone', 'smsg'));

        // @mail($to, $subject, $message, implode("\r\n", $headers));

        return redirect()->back()->with('success', __('messages.message_sent'));
    }


    /*****
     * Products Search
     */
    public function productsSearch(Request $request)
    {
        $products = Product::where(function($query) use ($request) {
            $query->where('title_en', 'like', '%' . $request->keyword . '%')
                ->orWhere('title_ar', 'like', '%' . $request->keyword . '%');
        }) ->orderBy('id', 'desc');

        if (isset($request->first_price) && !empty($request->first_price) &&
            isset($request->last_price) && !empty($request->last_price))
        {
            $products = $products->whereBetween('price', [
                $request->first_price ,
                $request->last_price
            ]);
        }

        if (isset($request->gallery_id) && !empty($request->gallery_id))
        {
            $products = $products->where('gallery_id', $request->gallery_id);
        }

        return view('search')->with([
            'keyword' => $request->keyword,
            'products' => $products->paginate(12),
            'socials' => Social::all()
        ]);
    }

    /*****
     * Products
     */
    public function products(Request $request, $id)
    {
        $cat = Category::findorfail($id);
        $products = Product::orderBy('id', 'desc');

        if ($cat->cat == 0) {
            $products = $products->where('main_cat', '=', $id);
        } else {
            $products = $products->where('sub_cat', '=', $id);
        }

        if (isset($request->first_price) && !empty($request->first_price) &&
            isset($request->last_price) && !empty($request->last_price))
        {
            $products = $products->whereBetween('price', [
                $request->first_price ,
                $request->last_price
            ]);
        }

        if (isset($request->gallery_id) && !empty($request->gallery_id))
        {
            $products = $products->where('gallery_id', $request->gallery_id);
        }

        return view('products')->with([
            'cat' => $cat,
            'products' => $products->paginate(12),
            'socials' => Social::all()
        ]);
    }

    /****
     * Product Details
     */
    public function product($id)
    {
        $socails = Social::all();
        $product = Product::findorfail($id);
        $similarProducts = Product::where('sub_cat', $product->sub_cat)->latest()->take(15)->get();

        return view('product')->with([
            'product'=>$product,
            'socials'=>$socails,
            'similarProducts' => $similarProducts,
        ]);
    }

    public function add_to_cart (Request $request)
    {
        $product = Product::findorfail($request->product);
        $productPrice = $product->price;

        if (!empty($request->size) && $request->size != 0) {
            $productSize = ProductSize::find($request->size);
            $productPrice = $productSize->price;
        }

        if($product->stock_qty == 0)
        {
            return  response()->json(['success' => false, 'msg' => 'Out of stock'], 200);
        }
        else if($request->qty > $product->stock_qty)
        {
            return  response()->json(['success' => false, 'msg' => 'No Available Quantity'], 200);
        }
        else
        {
            $cart_id = "Asask".$request->product;
            $price = ($product->discount != 0)? ($productPrice - $product->discount) : $productPrice;
            Cart::add(['id' => $cart_id, 'name' => $product->title_en, 'qty' => $request->qty, 'price' => $price,
            'options' => [
                'image' =>$product->images[0]->image,
                'product' => $request->product,
                'name_ar' => $product->title_ar,
                'size' => $request->size
            ]]);

            $details = [];
            foreach (Cart::content() as $item) {
                $details[] = [
                    'url' => url('Product/'.$item->options->product),
                    'img' => asset($item->options->image),
                    'name' => (App::getLocale() == 'en')? $item->name : $item->options->name_ar,
                    'qty' => $item->qty,
                    'price' => $item->price
                ];
            }

            return  response()->json([
                'success' => true,
                'msg' => __('messages.add_success'),
                'count' => Cart::content()->count(),
                'total' => Cart::subtotal(),
                'details' => $details
            ], 200);
        }
    }

    function cart()
    {
        $socails = Social::all();
        return view('cart')->with(['socials'=>$socails]);
    }

    function checkout()
    {
        $socails = Social::all();
        $methods = PayMethod::all();
        return view('checkout')->with(['socials'=>$socails, 'methods'=>$methods]);
    }

    function checkoutReview(Request $request)
    {
        $code = '';
        if($request->code != '')
        {
            $code = PromoCode::where('name', $request->code)->first();

            if (empty($code) || !$code->isValid()) {
                return redirect()->back()->withErrors(['code' => __('messages.invalid_discount')]);
            }
        }

        $validatedData = $request->validate([
            'address' => 'required',
            'pay_method' => 'required',
        ]);

        $address = Address::find($request->address);
        $payMethod = PayMethod::find($request->pay_method);

        $total = doubleval(str_replace(',', '', Cart::subtotal()));
        $tax = intval(Section::select('title_en')->where('position', 'tax')->first()->title_en);
        $socails = Social::all();
        return view('checkout_review')->with([
            'socials'=>$socails,
            'data'=>$request->all(),
            'code' => $code,
            'address' => $address,
            'payMethod' => $payMethod,
            'total' => $total,
            'tax' => $tax
        ]);
    }

    function checkout_process(Request $request)
    {
        $total = 0;

        $code = PromoCode::where('name', $request->code_id)->first();
        $st = OrderStatus::first();
        $order = new Order;
        $order->user = Auth::guard('user')->user()->id;
        $order->address_id = $request->address;
        $order->code_id = (!empty($code) != '')? $code->id : 0;
        $order->status = $st->id;
        $order->currency = 1;
        $order->pay_method = $request->pay_method;
        $order->total = 0;
        $order->save();

        foreach (Cart::content() as $item)
        {
            $itm = new OrderItem;
            $itm->order = $order->id;
            $itm->product = $item->options->product;
            $itm->size = $item->options->size;
            $itm->color = 0;
            $itm->qty = $item->qty;
            $itm->price = $item->price;
            $total = $total + ($item->qty * $item->price);
            $itm->save();

            $product = Product::findorfail($itm->product);
            $product->stock_qty = $product->stock_qty - $item->qty;
            if($product->stock_qty < 0) {$product->stock_qty = 0;}
            $product->save();
        }

        Cart::destroy();
        $order->total = $total;
        $order->save();

        if($order->pay_method == 1) {
            return redirect('/my-orders');
        } else {
            return redirect('/Order-Transaction/'.$order->id);
        }
    }

    public function order_transaction ($id)
    {
        $order = Order::where('id', $id)->where('pay_method', 2)->where('user', Auth::guard('user')->user()->id)->first();
        if($order === NULL) {abort('404');}
        $egp_price = $order->total * 100;
        // Accept API KEY
        $api_key = 'ZXlKaGJHY2lPaUpJVXpVeE1pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnVZVzFsSWpvaWFXNXBkR2xoYkNJc0luQnliMlpwYkdWZmNHc2lPak13TVRZMkxDSmpiR0Z6Y3lJNklrMWxjbU5vWVc1MEluMC5wc3JXeHI2TmFSVDdjczRJTEJxeW5ndHFTOEhLYjhlNlR0dXJzeWtzTGlUcmp1a25LR2Mya2doa05uSzVPY1pLNC1ZanBPZktQUExjZVQxNFVUTnZLZw==';
        $integration_id = 89803;
        $iframe_id = 89046;
        $currency = "EGP";

        $post = ['api_key' => $api_key];
        $payload = json_encode( array( "api_key"=> $api_key) );

        $ch = curl_init('https://accept.paymobsolutions.com/api/auth/tokens');
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $result = curl_exec($ch);
        // execute!
        $response = curl_exec($ch);
        // close the connection, release resources used
        curl_close($ch);
        $response=json_decode($response);
        // dd($response->profile);
        // do anything you want with your response
        $token=$response->token;
        $id=$response->profile->id;
        // dd($id);
        $post = [
            'auth_token' =>$token ,
            'merchant_id'=>$id,
            'delivery_needed'=>"false",
            'amount_cents'=>$egp_price,
            'currency'=>$currency,
            'items'=>array(),

        ];
        $payload = json_encode( $post );

        $ch = curl_init('https://accept.paymobsolutions.com/api/ecommerce/orders');
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $result = curl_exec($ch);
        // execute!
        $response = curl_exec($ch);
        // close the connection, release resources used
        curl_close($ch);
        $response=json_decode($response);

        $order_id=$response->id;

        $order->transaction_id = $order_id;
        $order->save();
        // dd($order_id);
        $post = [
            'auth_token' =>$token ,
            'order_id'=>$order_id,
            'expiration'=>3600,
            'amount_cents'=>$egp_price,
            'currency'=>$currency,
            "integration_id" => $integration_id,
            "billing_data" => array(
                "apartment"=> "NA",
                "email"=> Auth::guard('user')->user()->email,
                "floor"=> "NA",
                "first_name"=> Auth::guard('user')->user()->name,
                "street"=> $order->address,
                "building"=> "NA",
                "phone_number"=> Auth::guard('user')->user()->phone,
                "shipping_method"=> "NA",
                "postal_code"=> "NA",
                "city"=> "NA",
                "country"=> "Egypt",
                "last_name"=> "NA",
                "state"=> "NA"
            ),
            "lock_order_when_paid"=> "false"
        ];

        $payload = json_encode( $post );

        $ch = curl_init('https://accept.paymobsolutions.com/api/acceptance/payment_keys');
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $result = curl_exec($ch);
        // execute!
        $response = curl_exec($ch);
        // close the connection, release resources used
        curl_close($ch);
        $response=json_decode($response);
        // dd($response);
        $iframe_token = $response->token;

        $iframe = '<iframe style="width:100%; height:100% !important;border: 0px;" src="https://accept.paymobsolutions.com/api/acceptance/iframes/'.$iframe_id.'?payment_token='.$iframe_token.'"></iframe>';
        return $iframe;
    }

    function delete_item ($item)
    {
        Cart::remove($item);
        return redirect()->back();
    }

    function update_cart (Request $request)
    {
        for ($i = 0; $i < Cart::content()->count(); $i++)
        {
            $qty = ceil($request->qty[$i]);
            if($qty < 1) {$qty = 1;}

            $product = Product::findorfail($request->product_id[$i]);
            if($product->stock_qty == 0)
            {
                Cart::remove($request->product[$i]);
            }
            else if($qty > $product->stock_qty)
            {
                Cart::update($request->product[$i], $product->stock_qty);
            }
            else
            {
                Cart::update($request->product[$i], $qty);
            }
        }
        return redirect()->back();
    }
    /***
     * Subscribe User
     */
    public function subscribe(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:subscribers,email,NULL,id,deleted_at,NULL'
        ]);

        $ss = new Subscriber;
        $ss->email = $request->email;
        $ss->save();

        $message = '';
        if (app()->getLocale() == 'en') {
            $message = 'Subscribed successfully';
        } else {
            $message = 'تم الإشتراك في القائمة البريدية بنجاح';
        }

        return redirect()->back()->with('subscribe_success', $message);
    }

    /*****
     * Change Client Country Site View
     */
    public function my_default_country(Request $request)
    {
        $country = $request->num;
        $countries = Country::where('id', '=', $country)->orderBy('id')->first();
        if($countries !== NULL)
        {
            $minutes = 10 * 365 * 60;
            Cookie::queue('mags_country', $countries->id, $minutes);
        }
        Cart::destroy();
    }

    /******
     * Verify Account
     */
    public function verify_account ($token)
    {
        $user = User::where('activate_token', '=', $token)->first();
        if($user !== NULL)
        {
            $user->active = 1;
            $user->activate_token = '';
            $user->save();
        }
        return redirect('/');
    }

    public function profile()
    {
        $orders = Order::where('user', '=', Auth::guard('user')->user()->id)->orderBy('id', 'desc')->get();
        $socails = Social::all();
        $profile_image = Section::where('position', 'profile_image')->first();
        return view('profile')->with(['orders'=>$orders, 'socials'=>$socails, 'profile_image'=>$profile_image]);
    }

    public function my_orders()
    {
        $orders = Order::where('user', '=', Auth::guard('user')->user()->id)->orderBy('id', 'desc')->get();
        $socails = Social::all();
        return view('orders')->with(['orders'=>$orders, 'socials'=>$socails]);
    }

    public function order_details($id)
    {
        $order = Order::findorfail($id);
        if(Auth::guard('user')->user()->id != $order->user) {return abort('404');}
        $tax = intval(Section::select('title_en')->where('position', 'tax')->first()->title_en);
        $socails = Social::all();
        return view('order_details')->with(['order'=>$order, 'tax' => $tax, 'socials'=>$socails]);
    }


    public function change_information ()
    {
        $socails = Social::all();
        return view('change_information')->with(['socials'=>$socails]);
    }

    public function change_password ()
    {
        $socails = Social::all();
        return view('change_password')->with(['socials'=>$socails]);
    }

    public function save_information (Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            // 'address' => 'required',
            'phone' => 'required'
        ]);

        if($request->email != Auth::guard('user')->user()->email)
        {
            $cc = User::where('email', '=', $request->email)->first();
            if($cc !== NULL)
            {
                return response()->json(['success' => false, 'errors'=>'This Email Address Is Already Registered']);
            }
        }

        $user = User::findorfail(Auth::guard('user')->user()->id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->name = $request->first_name." ".$request->last_name;
        $user->email = $request->email;
        // $user->address = $request->address;
        $user->phone = $request->phone;
        $user->save();

        return redirect()->back()->with('success', __('messages.data_changed'));
    }

    public function save_password (Request $request)
    {
        $request->validate([
            'password' => 'required|min:6|confirmed'
        ]);

        $user = User::findorfail(Auth::guard('user')->user()->id);
        $user->password = bcrypt($request->password);
        $user->save();

        return redirect()->back()->with('success', __('messages.pass_changed'));
    }

    public function myAddresses()
    {
        $socails = Social::all();
        return view('addresses')->with(['socials'=>$socails]);
    }

    public function myAddress($id)
    {
        $address = Address::find($id);

        if (Auth::guard('user')->user()->id != $address->user_id) {
            return redirect()->back();
        }

        $cities = City::all();
        $socails = Social::all();
        return view('address')->with(['address'=>$address, 'cities' => $cities, 'socials'=>$socails]);
    }

    public function showAddAddress()
    {
        $cities = City::all();
        $socails = Social::all();
        return view('add_address')->with(['cities' => $cities, 'socials'=>$socails]);
    }

    public function addAddress(Request $request)
    {
        $request->validate([
            'city_id' => 'required',
            'name' => 'required',
            'area' => 'required',
            'street' => 'required',
            'building' => 'required',
            'apartment' => 'required'
        ]);

        $newAddress = new Address();
        $newAddress->user_id = Auth::guard('user')->user()->id;
        $newAddress->city_id = $request->city_id;
        $newAddress->name = $request->name;
        $newAddress->area = $request->area;
        $newAddress->street = $request->street;
        $newAddress->building = $request->building;
        $newAddress->apartment = $request->apartment;
        $newAddress->save();

        return redirect()->back()->with('success', __('messages.address_created'));
    }

    public function updateAddress(Request $request, $id)
    {
        $request->validate([
            'city_id' => 'required',
            'name' => 'required',
            'area' => 'required',
            'street' => 'required',
            'building' => 'required',
            'apartment' => 'required'
        ]);

        $address = Address::find($id);

        if (Auth::guard('user')->user()->id != $address->user_id) {
            return redirect()->back();
        }

        $address->user_id = Auth::guard('user')->user()->id;
        $address->city_id = $request->city_id;
        $address->name = $request->name;
        $address->area = $request->area;
        $address->street = $request->street;
        $address->building = $request->building;
        $address->apartment = $request->apartment;
        $address->save();

        return redirect()->back()->with('success', __('messages.address_updated'));
    }

    public function deleteAddress($id)
    {
        $address = Address::find($id);

        if (Auth::guard('user')->user()->id != $address->user_id) {
            return redirect()->back();
        }

        $address->delete();

        return redirect()->back()->with('success', __('messages.address_deleted'));
    }

    public function switchLang($lang)
    {
        if (in_array($lang, ['en', 'ar'])) {
            session()->put('lang', $lang);
            app()->setLocale($lang);
        }

        return redirect()->back();
    }
}
