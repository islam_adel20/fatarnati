<?php

namespace App\Http\Middleware;

use Closure;

use App\Models\Admin;
use App\Models\Permission;

class CheckAdminPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $controller = class_basename(\Route::current()->controller);

        if ($controller == 'DashboardController' || $controller == 'SliderController' ||
            ($controller == 'AdminController' && \Auth::guard('admin')->user()->is_super) ||
            $controller == 'ArticleController' || $controller == 'OrderStatusController' ||
            $controller == 'ProductImageController')
        {
            return $next($request);
        }

        $permission = Permission::where('class', $controller)->first();

        if (checkPermission($permission->name)) {
            return $next($request);
        }

        return redirect('admin/');
    }
}
