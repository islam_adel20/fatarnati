<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Foundation\Application;

use App;

class APISettings
{
    public $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $api_key = $request->header('x-api-key');
        if($api_key == 'fatarnati')
        {
            $locale = $request->header('Content-Language');

            // if the header is missed
            // // take the default local language
            // if(!$locale)
            // {
            //     $locale = Language::where('main_language','true')->first()->abbreviation;
            // }
            //
            // // check the languages defined is supported
            //
            // if (!in_array($locale, config('app.supported_languages')))
            // {
            //     // respond with error
            //     return response()->json(['success'=>false, 'code'=>200, 'message' => 'اللغة غير مدعومة']);
            // }
            // // set the local language
            // $this->app->setLocale($locale);

            // get the response after the request is done
            $response = $next($request);

            // set Content Languages header in the response
            //$response->headers->set('Content-Language', $locale);

            // return the response
            return $response;
        }
        else
        {
            return response()->json(['success'=>false, 'code'=>200, 'message'=> 'لا يوجد صلاحية للدخول']);
        }
        return $next($request);
    }
}
