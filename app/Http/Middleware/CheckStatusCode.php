<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\SallerCode;
use App\Models\Gallery;
use App\Models\ServiceSaller;
use Carbon\Carbon;

class CheckStatusCode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $codes = SallerCode::where('status', 'expired')->get();
        foreach($codes as $code){
            if(Carbon::now()->toDateString() > $code->to)
            {
              SallerCode::where('id', $code->id)->update([
                'status' => 'finish'
              ]);

              Gallery::where('code', $code->code)->update(['status' => 0]);
              ServiceSaller::where('code', $code->code)->update(['status' => 0]);
            }
        }
        return $next($request);
    }
}
