<?php
namespace App\Http\Traits;
use App\Models\User;
use Validator;
use Hash;

Trait ApiDesignTrait{
    public function ApiResponse($code = 200, $message = null, $errors = null, $data = null)
    {
        $array = [
            'status'    => $code,
            'message'   => $message
        ];

        if(is_null($data) && !is_null($errors))
        {
            $array['errors'] = $errors;
        }elseif(is_null($errors) && !is_null($data)){
            $array['data'] = $data;
        }

        return $array;
    }


    public function validator($validator)
    {
        if($validator->fails())
        {
            return $this->ApiResponse(422, 'Error', $validator->errors());
        }
    }
}
