<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \View::share('contact_info', \App\Models\Contact::first());
        \View::share('galleries', \App\Models\Gallery::take(5)->get());
        \View::share('allGalleries', \App\Models\Gallery::all());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
