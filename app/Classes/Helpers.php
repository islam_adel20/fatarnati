<?php
use Illuminate\Support\Facades\Cookie;

use App\Models\News;
use App\Models\Category;
use App\Models\Country;
use App\Models\ProductPrice;
use App\Models\ProductImage;
use App\Models\ProductSize;
use App\Models\Color;
use App\Models\Size;
use App\Models\Currency;
use App\Models\Order;
use App\Models\Admin;
use App\Models\Permission;

function order_transactions ()
{
    $orders = Order::where('transaction_id', '!=', '')->where('is_paid', 0)->get();
    foreach ($orders as $order)
    {
        $api_key = 'ZXlKaGJHY2lPaUpJVXpVeE1pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnVZVzFsSWpvaWFXNXBkR2xoYkNJc0luQnliMlpwYkdWZmNHc2lPak13TVRZMkxDSmpiR0Z6Y3lJNklrMWxjbU5vWVc1MEluMC5wc3JXeHI2TmFSVDdjczRJTEJxeW5ndHFTOEhLYjhlNlR0dXJzeWtzTGlUcmp1a25LR2Mya2doa05uSzVPY1pLNC1ZanBPZktQUExjZVQxNFVUTnZLZw==';
        $post = ['api_key' => $api_key];
        $payload = json_encode( array( "api_key"=> $api_key) );

        $ch = curl_init('https://accept.paymobsolutions.com/api/auth/tokens');
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $result = curl_exec($ch);
        // execute!
        $response = curl_exec($ch);
        // close the connection, release resources used
        curl_close($ch);
        $response=json_decode($response);
        // dd($response->profile);
        // do anything you want with your response
        $token=$response->token;

        $payload = json_encode( array( "auth_token"=> $token, "merchant_order_id"=>$response->profile->id,
        "order_id"=>$order->transaction_id) );

        $ch = curl_init('https://accept.paymobsolutions.com/api/ecommerce/orders/transaction_inquiry');
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $result = curl_exec($ch);
        // execute!
        $response = curl_exec($ch);
        // close the connection, release resources used
        curl_close($ch);
        $response=json_decode($response, true);
        if(array_key_exists('id', $response))
        {
            $transaction = $response['id'];
            $ch = curl_init('https://accept.paymobsolutions.com/api/acceptance/transactions/'.$transaction);
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer '.$token));
            # Return response instead of printing.
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            # Send request.
            $result = curl_exec($ch);
            // execute!
            $response = curl_exec($ch);
            // close the connection, release resources used
            curl_close($ch);
            $response=json_decode($response, true);
            if($response['is_capture'])
            {
                $order->is_paid = 1;
                $order->save();
            }
            else
            {
                $order->is_paid = -1;
                $order->save();
            }
        }
        else
        {
            $order->is_paid = -1;
            $order->save();
        }
    }
}

function currency_info($color)
{
    $c = Currency::where('id', $color)->first();
    return $c;
}

function color_details($color)
{
    $c = Color::where('id', $color)->first();
    return $c;
}

function size_details($sz)
{
    $c = Size::where('id', $sz)->first();
    return $c;
}

function get_news()
{
    $news = News::orderBy('id', 'desc')->get();
    return $news;
}

function get_maincats()
{
    $main_cats = Category::where('cat', '=', 0)->orderBy('id')->get();
    return $main_cats;
}

function get_countries()
{
    $countries = Country::orderBy('id')->get();
    return $countries;
}

function get_default_country()
{
    if(Cookie::get('mags_country') === NULL)
    {
        $countries = Country::where('active', '=', 1)->orderBy('id')->first();
        $minutes = 10 * 365 * 60;
        Cookie::queue('mags_country', $countries->id, $minutes);
        return $countries;
    }
    else
    {
        $id = Cookie::get('mags_country');
        $countries = Country::where('id', '=', $id)->orderBy('id')->first();
        return $countries;
    }
}

function country_products()
{
    if(Cookie::get('mags_country') === NULL)
    {
        $countries = Country::where('active', '=', 1)->orderBy('id')->first();
    }
    else
    {
        $id = Cookie::get('mags_country');
        $countries = Country::where('id', '=', $id)->orderBy('id')->first();
    }

    $default = $countries->id;
    $products = ProductPrice::where('country', '=', $default)->get();
    $pr = array();
    foreach ($products as $product)
    {
        $pr[] = $product->product;
    }
    return $pr;
}

function product_price ($product)
{
    if(Cookie::get('mags_country') === NULL)
    {
        $countries = Country::where('active', '=', 1)->orderBy('id')->first();
    }
    else
    {
        $id = Cookie::get('mags_country');
        $countries = Country::where('id', '=', $id)->orderBy('id')->first();
    }

    $default = $countries->id;
    $products = ProductPrice::where('country', '=', $default)->where('product', '=', $product)->first();
    return $products;
}

function first_product_image($product, $color)
{
    $image = ProductImage::where('product', $product)->where('color', $color)->first();
    return $image->image;
}

function product_images($product, $color)
{
    $images = ProductImage::where('product', $product)->where('color', $color)->get();
    return $images;
}

function checkPermission($permission) {
    if (Auth::guard('admin')->check()) {
        if (in_array($permission, Auth::guard('admin')->user()->permissions->pluck('name')->toArray())) {
            return true;
        }
    }

    return false;
}

function getSize($id){
    return ProductSize::find($id)->size_info;
}
