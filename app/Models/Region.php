<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
  public function getNameAttribute()
  {
      return (app()->isLocale('en'))? $this->name_en : $this->name_ar;
  }

  public function city()
  {
      return $this->belongsTo('App\Models\City', 'city_id', 'id');
  }

  public function galleries()
  {
      return $this->hasMany('App\Models\Gallery', 'city_id', 'id');
  }

  public function sallers()
  {
    return $this->hasMany('App\Models\ServiceSaller', 'city_id', 'id');
  }

  public function users()
  {
    return $this->hasMany('App\Models\User', 'city', 'id');
  }


}
