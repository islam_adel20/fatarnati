<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RateSaller extends Model
{
    protected $fillable = ['user_id', 'saller_id', 'comment', 'review'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function saller()
    {
        return $this->belongsTo('App\Models\Gallery', 'saller_id', 'id');
    }
}
