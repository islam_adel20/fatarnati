<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class ServiceSaller extends Authenticatable
{

  use HasApiTokens, Notifiable;
  protected $fillable = [
    'name', 'email', 'phone', 'password', 'desc', 'city_id', 'cat_id', 'code', 'image', 'whatsapp_link', 'mobile_link',
    'instgram_link', 'facebook_link', 'twitter_link', 'youtube_link', 'map', 'address', 'region_id', 'views'
  ];

  protected $hidden = ['password'];
  public function city()
  {
    return $this->belongsTo('App\Models\Region', 'city_id', 'id');
  }

  public function cat()
  {
    return $this->belongsTo('App\Models\CategoryServices', 'cat_id', 'id');
  }

  public function socials_saller()
  {
    return $this->hasMany('App\Models\Social', 'saller_id', 'id');
  }

  public function socials_saller_serv()
  {
    return $this->hasMany('App\Models\Social', 'saller_serv_id', 'id');
  }

  public function review_saller_services()
  {
      return $this->hasMany('App\Models\RateSaller', 'saller_id', 'id');
  }

}
