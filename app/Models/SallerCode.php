<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SallerCode extends Model
{
    protected $fillable = ['code', 'status', 'from', 'to', 'duration', 'cost'];

}
