<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['room_id', 'sender_id', 'message_from', 'message'];

    public function room()
    {
        return $this->belongsTo('App\Models\Room', 'room_id', 'id');
    }

    public function users()
    {
        return $this->hasMany('App\Models\User', 'room_id', 'id');
    }

    public function sallers()
    {
        return $this->hasMany('App\Models\Gallery', 'saller_id', 'id');
    }

    public function services()
    {
        return $this->hasMany('App\Models\ServiceSaller', 'saller_services_id', 'id');
    }
}
