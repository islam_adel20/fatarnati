<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Gallery extends Authenticatable
{

    use HasApiTokens, Notifiable;
    protected $fillable = [
      'activate_token', 'commercial_register', 'nationalist', 'password',
      'name_en', 'name_ar', 'phone', 'address_en', 'address_ar', 'map',
      'image', 'city_id', 'code', 'status', 'created_at', 'region_id', 'cat_id', 'sub_id',
      'instgram_link', 'facebook_link', 'twitter_link', 'youtube_link', 'whatsapp_link', 'mobile_link'
    ];
    
    protected $hidden = ['code', 'status', 'activate_token', 'password'];
    public function products()
    {
        return $this->hasMany('App\Models\Product', 'gallery_id', 'id');
    }

    public function sub()
    {
      return $this->hasOne('App\Models\Subscriber', 'saller_id', 'id');
    }

    public function city()
    {
      return $this->hasOne('App\Models\Region', 'city_id', 'id');
    }

    public function favorites()
    {
        return $this->hasMany('App\Models\ProductFavorite', 'saller_id', 'id');
    }

    public function review_saller()
    {
        return $this->hasMany('App\Models\RateSaller', 'saller_id', 'id');
    }
    
}
