<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{

    protected $fillable = ['title'];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
}
