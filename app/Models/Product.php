<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $fillable = ['title_en', 'title_ar'];
    function images()
    {
        return $this->hasMany('App\Models\ProductImage', 'product')->orderBy('id');
    }

    function sizes()
    {
        return $this->hasMany('App\Models\ProductSize', 'product');
    }

    function category()
    {
        return $this->belongsTo('App\Models\Category', 'cat', 'id');
    }

    function category_sub()
    {
        return $this->belongsTo('App\Models\Category', 'sub_id', 'id');
    }

    function gallery()
    {
        return $this->belongsTo('App\Models\Gallery', 'gallery_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'product_id', 'id');
    }

    public function favorites()
    {
        return $this->hasMany('App\Models\ProductFavorite', 'product_id', 'id');
    }

    public function products_colors()
    {
        return $this->hasMany('App\Models\ProductColor', 'product', 'id');
    }


}
