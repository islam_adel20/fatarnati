<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscriber extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    protected $fillable = ['saller_id', 'sub_from', 'sub_to', 'sub_type', 'sub_status', 'saller_status', 'cost'];
    protected $dates = ['deleted_at'];

    public function saller()
    {
      return $this->belongsTo('App\Models\Gallery', 'saller_id', 'id');
    }


}
