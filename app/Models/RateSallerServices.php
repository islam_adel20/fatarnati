<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RateSallerServices extends Model
{
    protected $fillable = ['user_id', 'saller_id', 'comment', 'review'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function saller_services()
    {
        return $this->belongsTo('App\Models\ServiceSaller', 'saller_id', 'id');
    }
}
