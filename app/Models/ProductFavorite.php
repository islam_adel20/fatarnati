<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductFavorite extends Model
{
    protected $fillable = ['user_id', 'product_id', 'saller_id'];

    public function user()
    {
      return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function product()
    {
      return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }

    public function saller()
    {
      return $this->belongsTo('App\Models\Gallery', 'saller_id', 'id');
    }
}
