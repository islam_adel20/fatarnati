<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function getNameAttribute()
    {
        return (app()->isLocale('en'))? $this->name_en : $this->name_ar;
    }

    public function regions()
   {
       return $this->hasMany('App\Models\Region', 'city_id', 'id');
   }
}
