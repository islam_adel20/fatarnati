<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryServices extends Model
{
    protected $fillable = ['name_ar', 'name_en', 'image'];

    public function sallers()
    {
      return $this->hasMany('App\Models\ServiceSaller', 'cat_id', 'id');
    }
}
