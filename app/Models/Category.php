<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function main()
    {
        return $this->belongsTo('App\Models\Category', 'cat', 'id');
    }

    public function subs()
    {
        return $this->hasMany('App\Models\Category', 'cat');
    }

    public function ads()
    {
        return $this->hasMany('App\Models\CategoryAd', 'cat')->inRandomOrder()->limit(2);
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product', 'cat');
    }
}
