<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['user_id', 'saller_id', 'saller_services_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function saller()
    {
        return $this->belongsTo('App\Models\Gallery', 'saller_id', 'id');
    }

    public function saller_services()
    {
        return $this->belongsTo('App\Models\ServiceSaller', 'saller_services_id', 'id');
    }

    public function messages()
    {
        return $this->hasMany('App\Models\Message', 'room_id', 'id');
    }
}
