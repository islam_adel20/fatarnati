<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductColor extends Model
{

    protected $fillable = ['color', 'product'];
    
    public function color_product()
    {
        return $this->belongsTo('App\Models\Color', 'color', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product', 'id');
    }
}
