$(document).ready(function() {
    $('#kt_table_1').DataTable({"iDisplayLength": 25});
    $('#kt_table_2').DataTable({"iDisplayLength": 25,
        columnDefs: [ {
            orderable: false,
            className: 'checkbox',
            targets:   0
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
    });
} );