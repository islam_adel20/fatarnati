$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});




function uniqId() 
{
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < 10; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    result = Math.round(new Date().getTime() + (Math.random() * 100))+'_'+result;
    return result;
}


$('body').on('click', '.collapse_details_box', function(e) {
    var box = $(this).attr('box');
    $('#single_order_item_box_'+box+' .order_item_details').addClass('height_zero');
    $('#single_order_item_box_'+box+' .order_item_collapse').removeClass('height_zero');
    return false;
});
$('body').on('click', '.uncollapse_details_box', function(e) {
    var box = $(this).attr('box');
    $('#single_order_item_box_'+box+' .order_item_details').removeClass('height_zero');
    $('#single_order_item_box_'+box+' .order_item_collapse').addClass('height_zero');
    return false;
});
$('body').on('click', '.delete_order_item', function(e) {
    var box = $(this).attr('box');
    $('#single_order_item_box_'+box).remove();
    return false;
});



$('body').on('click', '#add_order_item', function(e) {
    var action = $(this).attr('button-url');
    $.ajax({
        type: 'POST',
        data: {'order_item': uniqId()},
        url: action,
        success: function(data) 
        {
           $('#order_products').append(data);
        }
    }); 
    return false;
});

$('body').on('keyup', '#client_search', function(e) {
    var action = $(this).attr('data-url');
    var search = $(this).val();
    $.ajax({
        type: 'POST',
        data: {search: search},
        url: action,
        success: function(data) 
        {
           $('#order_client_info').html(data);
        }
    }); 
    return false;
});


$('body').on('input change paste', '#main_cat_selector', function(e) {
    var action = $(this).attr('data-url');
    var cat = $(this).val();
    $.ajax({
        type: 'POST',
        data: {cat: cat},
        url: action,
        success: function(data) 
        {
           $('#cat_selector').html(data);
        }
    }); 
    return false;
});




//============= Edit My 
//===== Select Type Media Lecture And Lesson
$('#filter-sub').change(function(){
    $id = $("#filter-sub option:selected").val();
    $url = $(this).attr('data-url');
    $.ajax({
        url:$url,
        type:"GET",
        dataType:"JSON",
        data:{cat_id:$id},
        success:function(data)
        {
          $('#cat_subs').html(data.result);
        }
    });
});




