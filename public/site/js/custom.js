$.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
  

$(document).ready(function () { 
    $('.add-success').hide();

    $('body').on('click', '.search_box button', function (e) {
        e.preventDefault();

        if ($('.search_box input[name="keyword"]:visible').val()) {
            $('.search_box form').submit();
        }
    });

    $('body').on('click', '.add_to_cart_btn', function () {
        $.ajax({
            url: 'https://asaskstore.com/test/add_to_cart',
            method: 'GET',
            data: {
                product: $(this).data('num'),
                qty: 1,
                size: 0,
            }
        })
        .done(function(res){
            $(window).scrollTop(0);
            $('.add-success').show();
            $('#add-success-message').text(res.msg);
            $('.item_count').text(res.count);

            //update the cart
            $('.cart_gallery .cart_item').remove();
            
            let newContent = '';
            $.each(res.details, function(key, item){
                newContent += `
                    <div class="cart_item">
                        <div class="cart_img">
                            <a href="${item.url}">
                                <img src="${item.img}" alt="">
                            </a>
                        </div>
                        <div class="cart_info">
                            <a href="${item.url}">
                                ${item.name}
                            </a>
                            <p>${item.qty} x <span> ${item.price} EGP </span></p>
                        </div>
                    </div>
                `;
            });

            $('.cart_gallery').append(newContent);
            $('.price').text(res.total + 'EGP');
        });
    });
    
    $('body').on('click', '#add_to_cart', function () {
        // if the product has sizes , and nothing was selected
        if ($('.product-sizes').length > 0) {
            if ($('#size').val() == 0) {
                alert('Please choose size !');
            }
        }

        $.ajax({
            url: 'https://asaskstore.com/test/add_to_cart',
            method: 'GET',
            data: {
                product: $('#pro').val(),
                qty: $('#qty').val(),
                size: $('#size').val(),
            }
        })
        .done(function(res){
            $(window).scrollTop(0);
            $('.add-success').show();
            $('#add-success-message').text(res.msg);
            $('.item_count').text(res.count);

            //update the cart
            $('.cart_gallery .cart_item').remove();
            
            let newContent = '';
            $.each(res.details, function(key, item){
                newContent += `
                    <div class="cart_item">
                        <div class="cart_img">
                            <a href="${item.url}">
                                <img src="${item.img}" alt="">
                            </a>
                        </div>
                        <div class="cart_info">
                            <a href="${item.url}">
                                ${item.name}
                            </a>
                            <p>${item.qty} x <span> ${item.price} EGP </span></p>
                        </div>
                    </div>
                `;
            });

            $('.cart_gallery').append(newContent);
            $('.price').text(res.total + 'EGP');
        });
    });

    $('body').on('click', '.select-size', function (e) {
        e.preventDefault();

        $('.select-size').removeClass('btn-success').addClass('btn-primary');
        $(this).addClass('btn-success');

        // update price
        $('.current_price').text($(this).data('size-id') + ' EGP');

        $('#size').val($(this).data('size-id'));
    });
}); // end document ready
