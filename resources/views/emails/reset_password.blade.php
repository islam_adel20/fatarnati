@component('mail::message')
<h3> New Password: <span style="color:blue"> {{$password}} </span></h3>
Thanks,<br>
{{ config('app.name') }}
@endcomponent
