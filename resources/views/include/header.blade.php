<body>

    <!--header area start-->

    <header>
        <div class="header_area container_width color_six p-0">
            <div class="main_header7 border-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="main_header7_inner d-flex align-items-center justify-content-between">
                                <div class="header_account_area">
                                    <div class="header_account_list header_wishlist">
                                        @if (App::getLocale() == 'en')
                                            <a href="{{ url('switch_lang/ar') }}">
                                                <img src="{{ asset('site/img/icon/egypt.svg') }}" alt="">
                                            </a>
                                        @else
                                            <a href="{{ url('switch_lang/en') }}">
                                                <img src="{{ asset('site/img/icon/uk.png') }}" alt="">
                                            </a>
                                        @endif
                                    </div>
                                    <div class="header_account_list register">
                                        @if (Auth::guard('user')->user())
                                            <ul>
                                                <li><a href="{{ url('logout') }}">{{ __('global.logout') }}</a></li>
                                                <li><span>/</span></li>
                                                <li><a href="{{ url('Profile') }}">{{ __('global.my_account') }}</a></li>
                                            </ul>
                                        @else
                                            <ul>
                                                <li><a href="{{ url('register') }}">{{ __('global.register') }}</a></li>
                                                <li><span>/</span></li>
                                                <li><a href="{{ url('login') }}">{{ __('global.login') }}</a></li>
                                            </ul>
                                        @endif
                                    </div>
                                    <!--<div class="header_account_list header_wishlist">-->
                                    <!--    <a href="#"><span class="lnr lnr-heart"></span> <span-->
                                    <!--            class="item_count">3</span> </a>-->
                                    <!--</div>-->
                                    <div class="header_account_list  mini_cart_wrapper">
                                        <a href="javascript:void(0)"><span class="lnr lnr-cart"></span><span
                                                class="item_count">{{ Cart::content()->count() }}</span></a>
                                        @include('include.cart')
                                    </div>
                                </div>

                                <div class="col-lg-5 col_search5">
                                    <div class="search_box search_five mobail_s_none">
                                        <form action="{{ route('search') }}">
                                            <input placeholder="{{ __('global.enter_keyword') }}" type="text" name="keyword">
                                            <button type="submit"><span class="lnr lnr-magnifier"></span></button>
                                        </form>
                                    </div>
                                </div>

                                <div class="logo">
                                    <a href="{{ url('/') }}"><img src="{{ asset('site/img/logo/logo.jpg') }}" alt=""></a>
                                </div>

                                <div class="canvas_open canvas_menu_icon">
                                    <a href="javascript:void(0)"><i class="icon-menu"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header_bottom sticky-header">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-10 offset-lg-1">
                            <!--main menu start-->
                            <div class="main_menu menu_position">
                                <nav>
                                    <ul class="justify-content-center">
                                        <li><a class="active" href="{{ url('/') }}">{{ __('global.home') }}</a></li>
                                        <li><a href="{{ url('/About') }}">{{ __('global.about_asask') }}</a></li>
                                        
										@foreach (get_maincats() as $mcat)
                                            <li>
                                                <a href="{{ url('Products/' . $mcat->id) }}" style="transition: none;">
                                                    @if (App::getLocale() == 'en')
                                                        {{ $mcat->title_en }}
                                                    @else
                                                        {{ $mcat->title_ar }}
                                                    @endif
                                                </a>
                                                @if ($mcat->subs->count())
                                                    <ul class="sub_menu">
                                                        @foreach ($mcat->subs as $category)
                                                            @if ($category->subs->count() == 0)
                                                                <li>
                                                                    <a href="{{ url('Products/' . $category->id) }}" style="transition:none;">
                                                                        @if (App::getLocale() == 'en')
                                                                            {{ $category->title_en }}
                                                                        @else
                                                                            {{ $category->title_ar }}
                                                                        @endif
                                                                    </a>
                                                                </li>
                                                            @else
                                                                <li class="dropdown-submenu">
                                                                    <a href="{{ url('Products/' . $category->id) }}">
                                                                        <span class="nav-label">
                                                                            @if (App::getLocale() == 'en')
                                                                                {{ $category->title_en }}
                                                                            @else
                                                                                {{ $category->title_ar }}
                                                                            @endif
                                                                        </span>
                                                                    </a>
                                                                    <ul class="dropdown-menu">
                                                                        @foreach ($category->subs as $sub)
                                                                            <li>
                                                                                <a href="{{ url('Products/' . $sub->id) }}">
                                                                                    @if (App::getLocale() == 'en')
                                                                                        {{ $sub->title_en }}
                                                                                    @else
                                                                                        {{ $sub->title_ar }}
                                                                                    @endif
                                                                                </a>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                        
										<li><a href="{{ url('/Galleries') }}">{{ __('global.parteners') }}</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <!--main menu end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!--header area end-->
