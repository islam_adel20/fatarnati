<!--mini cart-->
<div class="mini_cart" style="overflow-y: scroll;">
    <div class="cart_gallery">
        <div class="cart_close">
            <div class="cart_text">
                <h3>{{ __('global.cart') }}</h3>
            </div>
            {{-- <div class="mini_cart_close">
                <a href="javascript:void(0)"><i class="icon-x"></i></a>
            </div> --}}
        </div>
        @if (Cart::content()->count() > 0)
            @foreach(Cart::content() as $ff)
                <div class="cart_item">
                    <div class="cart_img">
                        <a href="{{ url('Product/'.$ff->options->product) }}">
                            <img src="{{ asset($ff->options->image) }}" alt="">
                        </a>
                    </div>
                    <div class="cart_info">
                        <a href="{{ url('Product/'.$ff->options->product) }}">
                            @if (App::getLocale() == 'en')
                                {{$ff->name}}
                            @else
                                {{ $ff->options->name_ar }}
                            @endif
                        </a>
                        <p>{{$ff->qty}} x <span> {{$ff->price}} EGP </span></p>
                    </div>
                </div>
            @endforeach
        @else 
            <p class="text-center"><b>{{ __('global.sorry_no_items') }}</b></p>
        @endif
    </div>
    <div class="mini_cart_table">
        <div class="cart_table_border">
            <div class="cart_total mt-10">
                <span>{{ __('global.total') }}:</span>
                <span class="price">{{ Cart::subtotal() }} EGP</span>
            </div>
        </div>
    </div>
    <div class="mini_cart_footer">
        <div class="cart_button">
            <a href="{{ url('Shopping-Cart') }}"><i class="fa fa-shopping-cart"></i> {{ __('global.cart') }}</a>
        </div>
        <div class="cart_button">
            <a href="{{url('Checkout')}}"><i class="fa fa-sign-in"></i> {{ __('global.checkout') }}</a>
        </div>
    </div>
</div>
<!--mini cart end-->
