	<!--footer area start-->
	<footer class="footer_widgets color_six">
		<div class="container_width download-app-banner">
			<div class="row">
				<div class="col-md-6 col-12 left">
					<h3 class="mt-3">{{ __('global.download_app') }}</h3>
				</div>
				<div class="col-md-6 col-12 right">
					<a href="#">
						<img src="{{ asset('site/img/icon/appstore.png') }}" alt="AppStore">
					</a>
					<a href="#">
						<img src="{{ asset('site/img/icon/playstore.png') }}" alt="AppStore">
					</a>
				</div>
			</div>
		</div>
		<div class="footer_top container_width pt-3">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-12 col-sm-7">
						<div class="widgets_container contact_us">
							<div class="footer_logo text-center">
								<a href="{{ url('/') }}"><img src="{{ asset('site/img/logo/logo.jpg') }}" alt=""></a>
							</div>
							<p class="footer_desc text-center">
								@if (App::getLocale() == 'en')
									{{ $footer->text_en }}
								@else
									{{ $footer->text_ar }}
								@endif
							</p>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>{{ __('global.info') }}</h3>
							<div class="footer_menu">
								<ul>
									<li><a href="{{ url('/About') }}">{{ __('global.about_asask') }}</a></li>
									<li><a href="{{ url('/Return-Policy') }}">{{ __('global.return_policy') }}</a></li>
									<li><a href="{{ url('/Contact') }}">{{ __('global.call_us') }}</a></li>
									<li><a href="{{ url('/Terms-and-Conditions') }}"> {{ __('global.terms') }}</a></li>
									<li><a href="{{ url('/Privacy') }}">{{ __('global.privacy') }}</a></li>
									<li><a href="{{ url('/Tips-and-Guides') }}">{{ __('global.tips') }}</a></li>
									<li><a href="{{ url('/Help-Center') }}">{{ __('global.help_center') }}</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-4">
						<div class="widgets_container widget_menu">
							<h3>{{ __('global.cats') }}</h3>
							<div class="footer_menu">
								<ul>
									@foreach (get_maincats() as $mcat)
										<li>
											<a href="{{ url('Products/' . $mcat->id) }}">
												@if (App::getLocale() == 'en')
													{{ $mcat->title_en }}
												@else
													{{ $mcat->title_ar }}
												@endif
											</a>
										</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-8">
						<div class="widgets_container widget_newsletter text-center">
							<h3>{{ __('global.subscribe_title') }}</h3>
							<p class="footer_desc">{{ __('global.subscribe_now') }}</p>
							<div class="subscribe_form">
								<form action="{{ url('/subscribe') }}" method="POST" id="mc-form" class="mc-form footer-newsletter">
									{{ csrf_field() }}
									<input id="mc-email" type="email" name="email" autocomplete="off"
										placeholder="{{ __('global.email') }}" />
									<button type="submit">{{ __('global.subscribe') }}</</button>
								</form>
								<!-- mailchimp-alerts Start -->
								<div class="mailchimp-alerts text-centre">
									<div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
									<div class="mailchimp-success"></div><!-- mailchimp-success end -->
									<div class="mailchimp-error"></div><!-- mailchimp-error end -->
								</div><!-- mailchimp-alerts end -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer_bottom footer_bottom_bg container_width">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 col-md-7">
						<div class="copyright_area">
							@if (App::getLocale() == 'en')
								<p>All rights reserved to <b>ASASK</b> &copy; {{ date('Y') }} , Design & develop by 
								<a href="https://matrixclouds.com/ar/" target="_blank"><b>Matrix Clouds</b></a></p>
							@else
								<p>جميع الحقوق محفوظة <b>ASASK</b> &copy; {{ date('Y') }} . تصميم و تطوير <a
										href="https://matrixclouds.com/ar/" target="_blank"><b>Matrix Clouds</b></a></p>
							@endif		
						</div>
					</div>
					<div class="col-lg-6 col-md-5">
						<div class="footer_payment footer-social">
							<ul>
								@foreach ($socials as $social)
									<li><a href="{{ $social->link }}" class="fa fa-{{ $social->name }}" target="_blank"></a></li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--footer area end-->

	<!-- JS
	============================================ -->
	<!--jquery min js-->
	<script src="{{ asset('site/js/vendor/jquery-3.4.1.min.js') }}"></script>
	<!--popper min js-->
	<script src="{{ asset('site/js/popper.js') }}"></script>
	<!--bootstrap min js-->
	<script src="{{ asset('site/js/bootstrap.min.js') }}"></script>
	<!--owl carousel min js-->
	<script src="{{ asset('site/js/owl.carousel.min.js') }}"></script>
	<!--slick min js-->
	<script src="{{ asset('site/js/slick.min.js') }}"></script>
	<!--magnific popup min js-->
	<script src="{{ asset('site/js/jquery.magnific-popup.min.js') }}"></script>
	<!--counterup min js-->
	<script src="{{ asset('site/js/jquery.counterup.min.js') }}"></script>
	<!--jquery countdown min js-->
	<script src="{{ asset('site/js/jquery.countdown.js') }}"></script>
	<!--jquery ui min js-->
	<script src="{{ asset('site/js/jquery.ui.js') }}"></script>
	<!--jquery elevatezoom min js-->
	<script src="{{ asset('site/js/jquery.elevatezoom.js') }}"></script>
	<!--isotope packaged min js-->
	<script src="{{ asset('site/js/isotope.pkgd.min.js') }}"></script>
	<!--slinky menu js-->
	<script src="{{ asset('site/js/slinky.menu.js') }}"></script>
	<!--instagramfeed menu js-->
	<script src="{{ asset('site/js/jquery.instagramFeed.min.js') }}"></script>
	<!-- tippy bundle umd js -->
	<script src="{{ asset('site/js/tippy-bundle.umd.js') }}"></script>
	<!-- Plugins JS -->
	<script src="{{ asset('site/js/plugins.js') }}"></script>

	<!-- Main JS -->
	<script src="{{ asset('site/js/main.js') }}"></script>
	<script src="{{ asset('site/js/custom.js') }}"></script>

	{{-- Newsletter success --}}
	@if (session()->has('subscribe_success'))
		<script>
			alert("{{ session('subscribe_success') }}");
		</script>
	@endif
</body>

</html>
