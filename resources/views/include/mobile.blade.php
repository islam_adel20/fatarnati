<!--offcanvas menu area start-->
<div class="off_canvars_overlay">
    {{-- Empty --}}
</div>
<div class="offcanvas_menu offcanvas6">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="offcanvas_menu_wrapper">
                    <div class="canvas_close">
                        <a href="javascript:void(0)"><i class="icon-x"></i></a>
                    </div>
                    <div class="search_container offcanvas__search">
                        <div class="search_box">
                            <form action="{{ route('search') }}">
                                <input placeholder="{{ __('global.enter_keyword') }}" type="text" name="keyword">
                                <button type="submit"><span class="lnr lnr-magnifier"></span></button>
                            </form>
                        </div>
                    </div>
                    <div id="menu" class="text-left ">
                        <ul class="offcanvas_main_menu">
                            <li class="menu-item-has-children active">
                                <a href="{{ url('/') }}">{{ __('global.home') }}</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="{{ url('/About') }}">{{ __('global.about_asask') }}</a>
                            </li>

                            @foreach (get_maincats() as $mcat)
                                <li class="menu-item-has-children">
                                    <a href="{{ url('Products/' . $mcat->id) }}">
                                        @if (App::getLocale() == 'en')
                                            {{ $mcat->title_en }}
                                        @else
                                            {{ $mcat->title_ar }}
                                        @endif
                                    </a>
                                    @if ($mcat->subs->count())
                                        <ul class="sub-menu">
                                            @foreach ($mcat->subs as $category)
                                                <li>
                                                    <a href="{{ url('Products/' . $category->id) }}">
                                                        @if (App::getLocale() == 'en')
                                                            {{ $category->title_en }}
                                                        @else
                                                            {{ $category->title_ar }}
                                                        @endif
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach

                            <li class="menu-item-has-children active">
                                <a href="{{ url('/Galleries') }}">{{ __('global.parteners') }}</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="{{ url('/Contact') }}">{{ __('global.call_us') }}</a>
                            </li>

                            @if (Auth::guard('user')->user())
                                <li class="menu-item-has-children active">
                                    <a href="{{ url('/Profile') }}">{{ __('global.my_account') }}</a>
                                </li>
                                <li class="menu-item-has-children active">
                                    <a href="{{ url('/logout') }}">{{ __('global.logout') }}</a>
                                </li>
                            @else
                                <li class="menu-item-has-children active">
                                    <a href="{{ url('/register') }}">{{ __('global.register') }}</a>
                                </li>
                                <li class="menu-item-has-children active">
                                    <a href="{{ url('/login') }}">{{ __('global.login') }}</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                    <div class="offcanvas_footer">
                        <span><a href="#"><i class="fa fa-envelope-o"></i>{{ $contact_info->email }}</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--offcanvas menu area end-->
