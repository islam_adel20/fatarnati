@extends('admin.layout.main')
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fa fa-users"></i></span>
                <h3 class="kt-portlet__head-title">Clients</h3>
            </div>
        </div>
        <div class="kt-portlet__body text-center">
			<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
				<thead>
					<tr>
						<th>#</th>
						<th>الأسم</th>
						<th>الموبايل</th>
						<th>أنشئ في</th>
						<th>الأعدادات</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($clients as $client)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td>{{$client->name}}</td>
							<td>{{$client->phone}}</td>
							<td>{{date('Y-m-d', strtotime($client->created_at))}}</td>
							<td>
								<a href="{{ route('clients.show', $client->id) }}" class="btn btn-success">
									<i class="fa fa-eye"></i> عرض
								</a>

                <a href="{{ url('admin/products_favorite/clients/'.$client->id) }}" class="btn btn-primary">
                  <i class="fa fa-list"></i> المنتجات المفضلة
                </a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
        </div>
	</div>
</div>
@endsection
