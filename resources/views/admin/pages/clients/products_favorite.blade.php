@extends('admin.layout.main')
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-boxes"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; المنتجات المفضلة [{{$products->count()}}] منتج</h3>
            </div>

        </div>
        <div class="kt-portlet__body text-center">
			<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
				<thead>
					<tr>
						<th>#</th>
						<th>الأسم (EN)</th>
						<th>الأسم (AR)</th>
						<th>القسم</th>
						<th>التاجر</th>
						<th>الأعدادات</th>
					</tr>
				</thead>
				<tbody>
					@foreach($products as $product)
             @if(App\Models\Product::where('id', $product->id)->count() > 0)
              @foreach(App\Models\Product::where('id', $product->product_id)->get() as $row)
  						<tr>
  							<td>{{$row->id}}</td>
  							<td>{{$row->title_en}}</td>
  							<td>{{$row->title_ar}}</td>
  							<td>{{(!empty($row->category)? $row->category->title_ar : '')}}</td>
  							<td>{{$row->gallery->name_en ?? ''}}</td>
  							<td>
  								<div class="dropdown">
  									<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
  									aria-expanded="false">
  										الأعدادات
  									</button>
  									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  										<a class="dropdown-item" href="{{route('products.show', $row->id)}}">الصور</a>
  									</div>
  								</div>
  							</td>
  						</tr>
              @endforeach
             @endif
					@endforeach
				</tbody>
			</table>
        </div>
	</div>
</div>
@endsection
