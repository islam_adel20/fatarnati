@extends('admin.layout.main')
@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fa fa-star"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; تعديل التاجر</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="{{url('admin/galleries/'.$gallery->id)}}" enctype="multipart/form-data">
				{{csrf_field()}}
				<input type="hidden" name="_method" value="PUT" />

				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الأسم (EN)</label>
							<input class="form-control" type="text" placeholder="Name (EN)" name="name_en" value="{{$gallery->name_en}}" id="name_en" />
							@if ($errors->has('name_en'))<span class="form-text text-danger"><strong>{{ $errors->first('name_en') }}</strong></span>@endif
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الأسم (AR)</label>
							<input class="form-control" type="text" placeholder="Name (AR)" name="name_ar" value="{{$gallery->name_ar}}" id="name_ar" />
							@if ($errors->has('name_ar'))<span class="form-text text-danger"><strong>{{ $errors->first('name_ar') }}</strong></span>@endif
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الموبايل</label>
							<input class="form-control" type="text" placeholder="Phone" name="phone" value="{{$gallery->phone}}" id="phone" />
							@if ($errors->has('phone'))<span class="form-text text-danger"><strong>{{ $errors->first('phone') }}</strong></span>@endif
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>العنوان (EN)</label>
							<input class="form-control" type="text" placeholder="Address (EN)" name="address_en" value="{{$gallery->address_en}}" id="address_en" />
							@if ($errors->has('address_en'))<span class="form-text text-danger"><strong>{{ $errors->first('address_en') }}</strong></span>@endif
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>العنوان (AR)</label>
							<input class="form-control" type="text" name="address_ar" value="{{$gallery->address_ar}}" id="address_ar" />
							@if ($errors->has('address_ar'))<span class="form-text text-danger"><strong>{{ $errors->first('address_ar') }}</strong></span>@endif
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الخريطة</label>
							<input class="form-control" type="text" placeholder="Map" name="map" value="{{$gallery->map}}" id="map" />
							@if ($errors->has('map'))<span class="form-text text-danger"><strong>{{ $errors->first('map') }}</strong></span>@endif
						</div>
					</div>
				</div>

				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">الصورة</label>
					<div class="col-lg-7">
						<div class="custom-file">
							<input type="file" class="custom-file-input" name="image" id="image" />
							<label class="custom-file-label" for="image">اختار الصورة</label>
						</div>
					</div>
					<div class="col-lg-3">
						<img src="{{asset($gallery->image)}}">
					</div>
				</div>

        <div class="form-group m-form__group row">
          <label class="col-lg-2 col-form-label">السجل التجاري</label>
          <div class="col-lg-7">
            <div class="custom-file">
              <input type="file" class="custom-file-input" name="commercial_register" id="image" />
              <label class="custom-file-label" for="image">اختار الصورة</label>
            </div>
          </div>

          <div class="col-lg-3">
            <img src="{{asset($gallery->commercial_register)}}">
          </div>
        </div>

        <div class="form-group m-form__group row">
          <label class="col-lg-2 col-form-label">البطاقة القومية</label>
          <div class="col-lg-7">
            <div class="custom-file">
              <input type="file" class="custom-file-input" name="nationalist" id="image" />
              <label class="custom-file-label" for="image">اختار الصورة</label>
            </div>
          </div>

          <div class="col-lg-3">
            <img src="{{asset($gallery->nationalist)}}">
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              <label>رابط الفيس بوك</label>
              <input class="form-control" type="text" placeholder="الفيسبوك" name="facebook_link" value="{{$gallery->facebook_link}}" id="phone" />
              @if ($errors->has('facebook_link'))<span class="form-text text-danger"><strong>{{ $errors->first('facebook_link') }}</strong></span>@endif
            </div>


            <div class="col-md-6">
              <label>رابط تويتر</label>
              <input class="form-control" type="text" placeholder="تويتر" name="twitter_link" value="{{$gallery->twitter_link}}" id="phone" />
              @if ($errors->has('twitter_link'))<span class="form-text text-danger"><strong>{{ $errors->first('twitter_link') }}</strong></span>@endif
            </div>

            <div class="col-md-6">
              <label>رابط الأنستجرام</label>
              <input class="form-control" type="text" placeholder="انتسجرام" name="instgram_link" value="{{$gallery->instgram_link}}" id="phone" />
              @if ($errors->has('instgram_link'))<span class="form-text text-danger"><strong>{{ $errors->first('instgram_link') }}</strong></span>@endif
            </div>


            <div class="col-md-6">
              <label>رابط اليوتيوب</label>
              <input class="form-control" type="text" placeholder="اليوتيوب" name="youtube_link" value="{{$gallery->youtube_link}}" id="phone" />
              @if ($errors->has('youtube_link'))<span class="form-text text-danger"><strong>{{ $errors->first('youtube_link') }}</strong></span>@endif
            </div>
          </div>
        </div>

				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
