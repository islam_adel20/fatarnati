@extends('admin.layout.main')
@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fa fa-star"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; أضافة تاجر </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="{{url('admin/galleries')}}" enctype="multipart/form-data">
				{{csrf_field()}}

				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الأسم (EN)</label>
							<input class="form-control" type="text" placeholder="الأسم (EN)" name="name_en" value="{{old('name_en')}}" id="name_en" />
							@if ($errors->has('name_en'))<span class="form-text text-danger"><strong>{{ $errors->first('name_en') }}</strong></span>@endif
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الأسم (AR)</label>
							<input class="form-control" type="text" placeholder="الأسم (AR)" name="name_ar" value="{{old('name_ar')}}" id="name_ar" />
							@if ($errors->has('name_ar'))<span class="form-text text-danger"><strong>{{ $errors->first('name_ar') }}</strong></span>@endif
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الموبايل</label>
							<input class="form-control" type="text" placeholder="الموبايل" name="phone" value="{{old('phone')}}" id="phone" />
							@if ($errors->has('phone'))<span class="form-text text-danger"><strong>{{ $errors->first('phone') }}</strong></span>@endif
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>العنوان (EN)</label>
							<input class="form-control" type="text" placeholder="العنوان (EN)" name="address_en" value="{{old('address_en')}}" id="address_en" />
							@if ($errors->has('address_en'))<span class="form-text text-danger"><strong>{{ $errors->first('address_en') }}</strong></span>@endif
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>العنوان (AR)</label>
							<input class="form-control" type="text" placeholder="العنوان (AR)" name="address_ar" value="{{old('address_ar')}}" id="address_ar" />
							@if ($errors->has('address_ar'))<span class="form-text text-danger"><strong>{{ $errors->first('address_ar') }}</strong></span>@endif
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الخريطة</label>
							<input class="form-control" type="text" placeholder="الخريطة" name="map" value="{{old('map')}}" id="map" />
							@if ($errors->has('map'))<span class="form-text text-danger"><strong>{{ $errors->first('map') }}</strong></span>@endif
						</div>
					</div>
				</div>

        <div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>كلمة السر</label>
							<input class="form-control" type="password" placeholder="كلمة السر" name="password" value="{{old('address_ar')}}" id="address_ar" />
							@if ($errors->has('password'))<span class="form-text text-danger"><strong>{{ $errors->first('password') }}</strong></span>@endif
						</div>
					</div>
				</div>

				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">الصورة</label>
					<div class="col-lg-10">
						<div class="custom-file">
							<input type="file" class="custom-file-input" name="image" id="image" />
							<label class="custom-file-label" for="image">اختار الصورة</label>
						</div>
					</div>
				</div>

        <div class="form-group m-form__group row">
          <label class="col-lg-2 col-form-label">السجل التجاري</label>
          <div class="col-lg-10">
            <div class="custom-file">
              <input type="file" class="custom-file-input" name="commercial_register" id="image" />
              <label class="custom-file-label" for="image">اختار الصورة</label>
            </div>
          </div>
        </div>

        <div class="form-group m-form__group row">
          <label class="col-lg-2 col-form-label">البطاقة القومية</label>
          <div class="col-lg-10">
            <div class="custom-file">
              <input type="file" class="custom-file-input" name="nationalist" id="image" />
              <label class="custom-file-label" for="image">اختار الصورة</label>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              <label>رابط الفيس بوك</label>
              <input class="form-control" type="text" placeholder="الفيسبوك" name="facebook_link" value="{{old('facebook_link')}}" id="phone" />
              @if ($errors->has('facebook_link'))<span class="form-text text-danger"><strong>{{ $errors->first('facebook_link') }}</strong></span>@endif
            </div>


            <div class="col-md-6">
              <label>رابط تويتر</label>
              <input class="form-control" type="text" placeholder="تويتر" name="twitter_link" value="{{old('twitter_link')}}" id="phone" />
              @if ($errors->has('twitter_link'))<span class="form-text text-danger"><strong>{{ $errors->first('twitter_link') }}</strong></span>@endif
            </div>

            <div class="col-md-6">
              <label>رابط الأنستجرام</label>
              <input class="form-control" type="text" placeholder="انتسجرام" name="instgram_link" value="{{old('instgram_link')}}" id="phone" />
              @if ($errors->has('instgram_link'))<span class="form-text text-danger"><strong>{{ $errors->first('instgram_link') }}</strong></span>@endif
            </div>


            <div class="col-md-6">
              <label>رابط اليوتيوب</label>
              <input class="form-control" type="text" placeholder="اليوتيوب" name="youtube_link" value="{{old('youtube_link')}}" id="phone" />
              @if ($errors->has('youtube_link'))<span class="form-text text-danger"><strong>{{ $errors->first('youtube_link') }}</strong></span>@endif
            </div>
          </div>
        </div>

				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
