@extends('admin.layout.main')
@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-image"></i></span>
                <h3 class="kt-portlet__head-title">تعديل الصورة</h3>
            </div>
        </div>
        <div class="kt-portlet__body">

<!--begin::Form-->
<form class="m-form m-form--label-align-left" method="post" action="{{route('slider.update', $slider->id)}}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="hidden" name="_method" value="PUT" />

	<div class="kt-portlet__body">
		@if($errors->any())
			<div class="alert alert-danger">{{$errors->first()}}</div>
		@endif

			<div class="form-group m-form__group row">
					<div class="col-lg-4">
					     <label>صورة الأعلان</label>
						<div class="custom-file">
							<input type="file" class="custom-file-input" id="image" name="image">
							<label class="custom-file-label" for="image">أختار صورة</label>
						</div>
					</div>
					
					<div class="col-lg-4">
						<div class="form-group">
						    <label>عنوان الأعلان</label>
							<input type="text" class="form-control" name="title" value="{{$slider->title}}">
						</div>
					</div>
					
						<div class="col-lg-4">
						<div class="form-group">
						    <label>رابط الأعلان</label>
							<input type="text" class="form-control" name="link" value="{{$slider->link}}">
						</div>
					</div>
					
				</div>
		</div>

		<div class="kt-portlet__foot">
				<div class="kt-form__actions">
					<div class="row">
						<div class="col-lg-12 text-right">
							<button type="submit" class="btn btn-success">حفظ</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!--end::Form--><!--end::Form-->
		</div>
	</div>
</div>

@endsection
