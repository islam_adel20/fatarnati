@extends('admin.layout.main')

@section('content')
<!--begin::Portlet-->
<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">Contact Us Messages</h3>
		</div>

		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
					<div class="dropdown">
						<a href="#" class="btn btn-danger d-block d-md-inline-block mb-1" data-toggle="modal" data-target="#myModalDALL">
						<i class="fas fa-trash"></i> Delete Selected</a>	
					</div>
					<div class="modal fade" id="myModalDALL" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Delete Selected Messages</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
						<button type="button" class="btn btn-danger" id="delete_selected_message" task="Delete" 
						url="{{url('admin/delete_messages_all')}}">Delete</button>
						<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
						</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="kt-portlet__body">

		<!--begin::Section-->
		<div class="kt-section">
			
			<div class="kt-section__content">
				<div class="table-responsive">
					<table class="table data_table2" id="inbox_table">
						<thead>
							<tr>
								<th class="disable_sort">
									<label class="kt-checkbox kt-checkbox--bold  kt-checkbox--primary">
										<input type="checkbox" id="checkAll" >
										<span></span>
									</label>
								</th>
								<th>#</th>
								<th>Name</th>
								<th>Email</th>
								<th>Phone</th>
								<th>Subject</th>
								<th>Sent At</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($msgs as $msg)
								<tr @if($msg->viewed == 0) class="bg-light" @endif>
									<td>
										<label class="kt-checkbox kt-checkbox--bold  kt-checkbox--primary">
											<input type="checkbox" class="check_single" name="item[]" value="{{$msg->id}}"  />
											<span></span>
										</label>
									</td>
									<td>{{$loop->iteration}}</td>
									<td>{{$msg->name}}</td>
									<td>{{$msg->email}}</td>
									<td>{{$msg->phone}}</td>
									<td>{{$msg->subject}}</td>
									<td>{{date('d/m/Y h:i A', strtotime($msg->created_at))}}</td>
									<td>
									<button class="btn btn-brand dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" 
									aria-haspopup="true" aria-expanded="true">Action</button>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="{{url('admin/contact_messages/'.$msg->id)}}"><i class="fa fa-edit"></i> Details</a>
										<a class="dropdown-item" data-toggle="modal" href="#myModal-{{ $msg->id }}"><i class="fa fa-trash"></i> Delete</a>
									</div>
									<div class="modal fade" id="myModal-{{ $msg->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
										<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Delete Message</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
										<form role="form" action="{{ url('/admin/contact_messages_delete/'.$msg->id) }}" class="" method="POST">
										{{ csrf_field() }}
										<p>are you sure</p>
										<button type="submit" class="btn btn-danger" name='delete_modal'><i class="fa fa-trash" aria-hidden="true"></i> delete</button>
										</form>
										</div>
										</div>
										</div>
									</div>
									</td>
								</tr>
							@endforeach
							
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<!--end::Section-->
	</div>

	<!--end::Form-->
</div>
<!--end::Portlet-->

@endsection
