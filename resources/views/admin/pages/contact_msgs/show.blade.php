@extends('admin.layout.main')

@section('content')
	<!--begin::Portlet-->
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
				<i class="fas fa-envelope"></i> Contact Us Message
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body">
			<div class="form-group m-form__group row">
				<label class="col-form-label col-lg-4"><b>Date : </b>{{date('d/m/Y h:i A', strtotime($msg->created_at))}}</label>
				<label class="col-form-label col-lg-4"><b>Name : </b>{{$msg->name}}</label>
				<label class="col-form-label col-lg-4"><b>Phone : </b>{{$msg->phone}}</label>
				<label class="col-form-label col-lg-4"><b>Email : </b>{{$msg->email}}</label>
			</div>	
			<h4>{{$msg->subject}}</h4>
			<p>{{$msg->msg}}</p>
			<hr class="d-block" style="border:solid 2px whitesmoke; width:100%;" />
			<h3 style="color: #3F51B5;"><b>Reply</b></h4>
			@if ($msg->replaied_by == 0)
				<!--begin::Form-->
				<form class="kt-form kt-form--label-left" id="kt_form_1" method="post"  action="{{ url('admin/contact_messages_send') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="kt-portlet__body">
						<input type="hidden" name="msg" value="{{$msg->id}}" /> 
						@if($errors->any())
							<div class="alert alert-danger">{{$errors->first()}}</div>
						@endif
						<div class="form-group m-form__group row">
							<label class="col-lg-2 col-form-label">Subject </label>
							<div class="col-lg-10">
								<input type="text" class="form-control m-input" name="title" value="{{old('title')}}" />
							</div>
						</div>
						
						<div class="form-group m-form__group row">
							<label class="col-lg-2 col-form-label">Message</label>
							<div class="col-lg-10">
									<textarea class="form-control m-input ckeditor" name="text">{{old('text')}}</textarea>
							</div>
						</div>								
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-12 text-right">
									<button type="submit" class="btn btn-success">Send</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			@else
			<br /><br />
			<h4>{{$msg->replay_subject}}</h4>
			<p>{!!$msg->replay_msg!!}</p>
			@endif

		</div>			
	</div>
	<!--end::Portlet-->
@endsection