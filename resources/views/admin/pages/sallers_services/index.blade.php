@extends('admin.layout.main')
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fa fa-star"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; تجار الخدمات</h3>
            </div>
        </div>
        <div class="kt-portlet__body text-center">
			<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
				<thead>
					<tr>
						<th>#</th>
						<th>الأسم</th>
						<th>الأيميل</th>
						<th>الموبايل</th>
            <th>القسم</th>
            <th>تفاصيل أخري</th>
						<th>الاعدادات</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($saller_servcies as $saller)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td>{{$saller->name}}</td>
							<td>{{$saller->email}}</td>
							<td>{{$saller->phone}}</td>
              <td>
                    @if(App\Models\CategoryServices::where('id', $saller->cat_id)->count() > 0)
                        {{$saller->cat['name_ar']}}
                    @else
                        <span style="color:red"> يرجي تحديد قسم</span>
                    @endif
              </td>
              <td>

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal{{$saller->id}}">
                  عرض
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal{{$saller->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-body">
                        @if($saller->image != null)
                        <img src="{{asset($saller->image)}}" width="50%" style="display:block;margin:auto">
                        @else
                        <h4 class="alert alert-danger"> لا يوجد صورة مرفقة </h4>
                        @endif
                        <p style="margin-top:20px;font-size:16px">
                           <span style="float:right"> الوصف <i class="fas fa-hand-point-left"></i> </span>
                           <span style="color:blue;"> {{$saller->desc}} </span>
                         </p>
                        <hr>

                        @if(App\Models\Region::where('city_id', $saller->city_id)->count() > 0)
                        <p style="margin-top:20px;font-size:16px">
                           <span style="float:right"> المحافظة <i class="fas fa-hand-point-left"></i> </span>
                           <span style="color:blue;"> {{$saller->city->city['name_ar']}} </span>
                         </p>
                        <hr>
                        <p style="margin-top:20px;font-size:16px">
                           <span style="float:right"> المدينة <i class="fas fa-hand-point-left"></i> </span>
                           <span style="color:blue;"> {{$saller->city['name_ar']}} </span>
                         </p>
                        <hr>
                        @else
                        <p style="margin-top:20px;font-size:16px">
                           <span style="float:right"> المدينة <i class="fas fa-hand-point-left"></i> </span>
                           <span style="color:red;"> لم يحدد المدينة </span>
                         </p>
                        <hr>
                        @endif

                        @if($saller->facebook_link != null)
                        <a href="{{$saller->facebook_link}}" class="btn btn-primary"> الفيس بوك </a>
                        @endif

                        @if($saller->instgram_link != null)
                        <a href="{{$saller->instgram_link}}" class="btn btn-info">الأنستجرام </a>
                        @endif

                        @if($saller->twitter_link != null)
                        <a href="{{$saller->twitter_link}}" class="btn btn-success"> تويتر </a>
                        @endif

                        @if($saller->youtube_link != null)
                        <a href="{{$saller->youtube_link}}" class="btn btn-danger"> اليوتيوب </a>
                        @endif

                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
                      </div>
                    </div>
                  </div>
                </div>

              </td>
							<td>
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">
										الأعدادات
									</button>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="{{route('services.edit', $saller->id)}}"><i class="fas fa-edit"></i> تعديل</a>
										<a class="dropdown-item" data-toggle="modal" href="#myModal-{{ $saller->id }}"><i class="fas fa-trash"></i> حذف</a>
									</div>

                  <!-- Button trigger modal -->
                  <button class="btn btn-success" data-toggle="modal" data-target="#exampleModalsub{{$saller->id}}">
                    تفاصيل الأشتراك
                  </button>

                  <!-- Modal -->
                  <div class="modal fade" id="exampleModalsub{{$saller->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-body">
                          @if($saller->code != null)
                          <h4 class="alert alert-success"> الكود :  {{$saller->code}}</h4>
                          <h4 class="alert alert-success"> حالة الأشتراك :
                            @foreach(App\Models\SallerCode::where('code', $saller->code)->get() as $code)
                              @if($code->status == 'expired')
                                جاري -

                                  [ {{$code->from}} / {{$code->to}} ]
                              @elseif($code->status == 'finish')
                                منتهي
                              @endif
                            @endforeach
                          </h4>
                          @else
                          <h4 class="alert alert-danger text-center"> لا يوجد أشتراك حاليا</h4>
                          @endif
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">الغاء</button>
                        </div>
                      </div>
                    </div>
								</div>

								<div class="modal fade" id="myModal-{{ $saller->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
								<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">حذف التاجر</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
								<form role="form" action="{{ url('admin/sallers/services/'.$saller->id) }}" class="" method="POST">
								<input name="_method" type="hidden" value="DELETE">
								{{ csrf_field() }}
								<p>هل أنت متأكد من الحذف ؟ </p>
								<button type="submit" class="btn btn-danger" name='delete_modal'>حذف</button>
								<button type="button" class="btn btn-success" data-dismiss="modal">الغاء</button>
								</form>
								</div>
								</div>
								</div>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
    </div>
	</div>
</div>
@endsection
