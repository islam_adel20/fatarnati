@extends('admin.layout.main')
@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fa fa-star"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; تعديل</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="{{url('admin/sallers/services/'.$row->id)}}" enctype="multipart/form-data">
				{{csrf_field()}}
        {{method_field('PUT')}}
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الأسم</label>
							<input class="form-control" type="text" name="name" value="{{$row->name}}" id="name" />
							@if ($errors->has('name'))<span class="form-text text-danger"><strong>{{ $errors->first('name') }}</strong></span>@endif
						</div>

            <div class="col-md-6">
              <label>الأيميل</label>
              <input class="form-control" type="email" name="email" value="{{$row->email}}" id="email" />
              @if ($errors->has('email'))<span class="form-text text-danger"><strong>{{ $errors->first('email') }}</strong></span>@endif
            </div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">

            <div class="col-md-6">
              <label>الأقسام</label>
              <select class="form-control" name="cat_id">
                @foreach($cats as $cat)
                  <option value="{{$cat->id}}" @if($row->cat_id == $cat->id) selected @endif>{{$cat->name_ar}}</option>
                @endforeach
              </select>
            </div>


            <div class="col-md-6">
              <label>المدينة</label>

              <select class="form-control" name="city_id" id="gallery">
                @foreach($cities as $city)
                    <optgroup label="{{$city['name_ar']}}">
                      @foreach($city->regions as $region)
                        <option value="{{$region->id}}" @if($row->city_id == $region->id) selected @endif>{{$region->name_ar}}</option>
                      @endforeach
                    </optgroup>
                @endforeach
              </select>
            </div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الموبايل</label>
							<input class="form-control" type="text" name="phone" value="{{$row->phone}}" id="phone" />
							@if ($errors->has('phone'))<span class="form-text text-danger"><strong>{{ $errors->first('phone') }}</strong></span>@endif
						</div>

            <div class="col-md-6">
              <label>كلمة السر</label>
              <input class="form-control" type="password" name="password">
              @if ($errors->has('password'))<span class="form-text text-danger"><strong>{{ $errors->first('password') }}</strong></span>@endif
            </div>
					</div>
				</div>

				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">الصورة</label>
					<div class="col-lg-10">
						<div class="custom-file">
							<input type="file" class="custom-file-input" name="image" id="image" />
							<label class="custom-file-label" for="image">اختار الصورة</label>
						</div>
					</div>
				</div>

        <div class="form-group">
          <div class="row">
            <div class="col-md-12">
              <label> وصف التاجر</label>
              <textarea class="form-control" rows="7" placeholder="وصف التاجر" name="desc">
                  {{$row->desc}}
               </textarea>
              @if ($errors->has('desc'))<span class="form-text text-danger"><strong>{{ $errors->first('desc') }}</strong></span>@endif
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              <label>رابط الفيس بوك</label>
              <input class="form-control" type="text" placeholder="الفيسبوك" name="facebook_link" value="{{$row->facebook_link}}" id="phone" />
              @if ($errors->has('facebook_link'))<span class="form-text text-danger"><strong>{{ $errors->first('facebook_link') }}</strong></span>@endif
            </div>


            <div class="col-md-6">
              <label>رابط تويتر</label>
              <input class="form-control" type="text" placeholder="تويتر" name="twitter_link" value="{{$row->twitter_link}}" id="phone" />
              @if ($errors->has('twitter_link'))<span class="form-text text-danger"><strong>{{ $errors->first('twitter_link') }}</strong></span>@endif
            </div>

            <div class="col-md-6">
              <label>رابط الأنستجرام</label>
              <input class="form-control" type="text" placeholder="انتسجرام" name="instgram_link" value="{{$row->instgram_link}}" id="phone" />
              @if ($errors->has('instgram_link'))<span class="form-text text-danger"><strong>{{ $errors->first('instgram_link') }}</strong></span>@endif
            </div>


            <div class="col-md-6">
              <label>رابط اليوتيوب</label>
              <input class="form-control" type="text" placeholder="اليوتيوب" name="youtube_link" value="{{$row->youtube_link}}" id="phone" />
              @if ($errors->has('youtube_link'))<span class="form-text text-danger"><strong>{{ $errors->first('youtube_link') }}</strong></span>@endif
            </div>
          </div>
        </div>

				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
