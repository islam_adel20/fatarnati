@extends('admin.layout.main')
@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fa fa-phone"></i></span>
                <h3 class="kt-portlet__head-title">Contact Information</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form kt-form--label-left" id="kt_form_1" method="post"  action="{{ url('/admin/contact') }}" enctype="multipart/form-data">
				{{ csrf_field() }}
				
				@if($errors->any())
					<div class="alert alert-danger">{{$errors->first()}}</div>
				@endif
	
				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">Address (EN)</label>
					<div class="col-lg-10">
						<input type="text" class="form-control m-input" name="address_en" value="{{$contact->address_en}}" />
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">Address (AR)</label>
					<div class="col-lg-10">
						<input type="text" class="form-control m-input" name="address_ar" value="{{$contact->address_ar}}" />
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">Phone</label>
					<div class="col-lg-10">
						<input type="text" class="form-control m-input" name="phone" value="{{$contact->phone}}" />
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">Email</label>
					<div class="col-lg-10">
						<input type="text" class="form-control m-input" name="email" value="{{$contact->email}}" />
						@if ($errors->has('email'))
							<span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
						@endif 
					</div>
				</div>
				
				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">Map</label>
					<div class="col-lg-10">
						<textarea name="map" class="form-control map_textarea" dir="ltr">{{$contact->map}}</textarea>
					</div>										
				</div>
				<div class="map_frame">{!!$contact->map!!}</div>
					
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-12 text-right">
								<button type="submit" class="btn btn-success">Save</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>					
@endsection

