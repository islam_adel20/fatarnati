@extends('admin.layout.main')
@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-box"></i></span>
                <h3 class="kt-portlet__head-title">تعديل المنتج</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="{{url('admin/products/'.$product->id)}}" enctype="multipart/form-data">
				{{csrf_field()}}
				<input type="hidden" name="_method" value="PUT" />
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الأسم (EN)</label>
							<input class="form-control" type="text" placeholder="Name (EN)" name="title_en" value="{{$product->title_en}}" id="title_en" required />
							@if ($errors->has('title_en'))<span class="form-text text-danger"><strong>{{ $errors->first('title_en') }}</strong></span>@endif
						</div>
						<div class="col-md-6">
							<label>الأسم (AR)</label>
							<input class="form-control" type="text" placeholder="Name (AR)" name="title_ar" value="{{$product->title_ar}}" id="title_ar" required />
							@if ($errors->has('title_ar'))<span class="form-text text-danger"><strong>{{ $errors->first('title_ar') }}</strong></span>@endif
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>القسم الرئيسي</label>
							<select class="form-control" name="cat" id="filter-sub" data-url="{{route('filter.subs.category')}}" required>
								<option value="" disabled selected>اختار القسم الرئيسي</option>
								@foreach($cats as $cat)
								<option value="{{$cat->id}}" @if($cat->id == $product->cat) selected @endif>{{$cat->title_ar}}</option>
								@endforeach
							</select>
						</div>

						<div class="col-md-6">
							<label>القسم الفرعي</label>
							<select class="form-control" name="sub_id" id="cat_subs" required>
								<option value="{{$product->sub_id}}"> {{$product->category_sub['title_ar']}}</option>
							</select>
						</div>
					
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>السعر (EGP)</label>
							<input class="form-control" type="text" placeholder="Price" name="price" value="{{$product->price}}" id="title">
							@if ($errors->has('price'))<span class="form-text text-danger"><strong>{{ $errors->first('price') }}</strong></span>@endif
						</div>
						<div class="col-md-6">
							<label> الخصم (EGP)</label>
							<input class="form-control" type="text" placeholder="Discount" name="discount" value="{{$product->discount}}" id="title" />
							@if ($errors->has('discount'))<span class="form-text text-danger"><strong>{{ $errors->first('discount') }}</strong></span>@endif
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>التاجر</label>
							<select class="form-control" name="gallery_id" id="gallery">
								<option value="" disabled selected>Choose Partener</option>
								@foreach($galleries as $gallery)
									<option value="{{$gallery->id}}"
										@if($gallery->id == $product->gallery_id) selected @endif
										>{{$gallery->name_en}}</option>
								@endforeach
							</select>
						</div>

						<div class="col-md-6">
							<label>الصورة الرئيسية</label>
							<div class="custom-file">
								<input type="file" class="custom-file-input" id="image" name="photo" required>
								<label class="custom-file-label" for="image">Choose file</label>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الوصف (EN)</label>
							<textarea class="ckeditor" name="text_en" id="text_en">{{$product->text_en}}</textarea>
							@if ($errors->has('text_en'))<span class="form-text text-danger"><strong>{{ $errors->first('text_en') }}</strong></span>@endif
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الوصف (AR)</label>
							<textarea class="ckeditor" name="text_ar" id="text_ar" required>{{$product->text_ar}}</textarea>
							@if ($errors->has('text_ar'))<span class="form-text text-danger"><strong>{{ $errors->first('text_ar') }}</strong></span>@endif
						</div>
					</div>
				</div>

				<hr>

				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label><b>المقاسات</b></label>
							<button class="btn btn-success float-right" id="add-size">
								<i class="fa fa-plus"></i> أضافة مقاس
							</button>

							<table class="table table-bordered mt-5" id="sizes">
								<thead>
									<tr>
										<th>المقاس</th>
										<th>السعر</th>
										<th>حذف</th>
									</tr>
								</thead>
								<tbody>
									@forelse($product->sizes as $productSize)
										<tr>
											<td>
												<select class="form-control" name="sizes[]" required>
												   	@foreach($sizes as $size)
														<option value="{{$size->id}}"
															@if ($size->id == $productSize->size) selected @endif
															>{{$size->id}}</option>
													@endforeach
												</select>
											</td>
											<td>
												<input type="number" class="form-control" name="prices[]" min="0" value="{{$productSize->price}}" required>
											</td>
											<td>
												<button class="btn btn-danger remove-size" data-size-id="{{$productSize->id}}">
													<i class="fa fa-times"></i> حذف
												</button>
											</td>
										</tr>
									@empty
										<tr class="sizes-placeholder">
											<td	td colspan="3">لا يوجد مقاسات مضافة حتي الأن</td>
										</tr>
									@endforelse
								</tbody>
							</table>

							<input type="hidden" name="deleted_sizes">
						</div>
					</div>
				</div>

				<div class="form-group">
					
				</div>

				<div class="form-group">
				<hr>
				<h4 class="alert alert-success" style="width:100px"><b>الألوان</b></h4>
					<div class="row">

					<div class="col-md-6">
						<label>أختار الألوان</label>
						<select class="form-control" name="color[]" multiple>
							<option value="" disabled selected>أختار الالوان</option>
							@foreach($colors as $color)
								<option value="{{$color->id}}" style="background:{{$color->color}};color:#fff">{{$color->title}}</option>
							@endforeach
						</select>
					</div>

					<div class="col-md-12">
					
						<table class="table table-bordered mt-5 text-center" id="colors">
							<thead>
								<tr>
									<th>اسم اللون</th>
									<th>اللون</th>
									<th>حذف</th>
								</tr>
							</thead>
							<tbody>
								@forelse($product_colors as $product_color)
									<tr>
										<td>{{$product_color->color_product['title']}}</td>
										<td>
										<div style="width:40px;height:40px;background:{{$product_color->color_product['color']}}"> </div>
										</td>
										<td>
											<a class="btn btn-danger delete-color" href="{{url('admin/delete/product/color/'.$product_color->id)}}">
												<i class="fa fa-times"></i> حذف
											</a>
										</td>
									</tr>
								@empty
									<tr class="sizes-placeholder">
										<td	td colspan="3">لا يوجد الوان متاحة الأن</td>
									</tr>
								@endforelse
							</tbody>
						</table>
					</div>
					</div>
				</div>


				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>
		$(document).ready(function(){
			$('#add-size').click(function(e){
				e.preventDefault();

				let sizeBox = `
					<tr class="size-box">
						<td>
							<select class="form-control" name="sizes[]" required>
								<option value="" disabled selected>Choose Size</option>
								@foreach ($sizes as $size)
									<option value="{{$size->id}}">{{$size->title}}</option>
								@endforeach
							</select>
						</td>
						<td>
							<input type="number" class="form-control" name="prices[]" min="0" required>
						</td>
						<td>
							<button class="btn btn-danger remove-size">
								<i class="fa fa-times"></i> Delete
							</button>
						</td>
					</tr>
				`;

				// first row !
				if ($('.sizes-placeholder').length > 0) {
					$('#sizes tbody').html('');
				}

				// add the box
				$('#sizes tbody').append(sizeBox);

				// scroll to bottom of the page (glitch)
				$("html, body").animate({ scrollTop: $(document).height() }, 1000);
			});

			$(document).on('click', '.remove-size', function(e){
				e.preventDefault();

				// add size id to deleted array
				if ($(this).data('size-id')) {
					let val = $('input[name="deleted_sizes"]').val();
					$('input[name="deleted_sizes"]').val(val + ',' + $(this).data('size-id'));
				}

				// add the box
				$(this).closest('td').closest('tr').remove();

				// if no rows !
				if ($('.size-box').length == 0) {
					$('#sizes tbody').append(`
						<tr class="sizes-placeholder">
							<td	td colspan="3">No sizes added yet</td>
						</tr>
					`);
				}
			});


			$(document).on('click', '.delete-color', function(e){
				e.preventDefault();
				$url = $(this).attr('data-url');
				alert($url);
				$.ajax({
					url:$ur;
					type:"POST",
					dataType:"JSON",
					success:function(data){
						alert("success delete");
					}
				});
			});
		});
	</script>
@endsection
