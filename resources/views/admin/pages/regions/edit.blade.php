@extends('admin.layout.main')
@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-map-marker-alt"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; تعديل</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="{{url('admin/regions/'.$region->id)}}" enctype="multipart/form-data">
				{{csrf_field()}}
				<input type="hidden" name="_method" value="PUT" />
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الأسم (EN)</label>
							<input class="form-control" type="text" placeholder="الأسم" name="name_en" value="{{$region->name_en}}" id="name_en" />
							@if ($errors->has('name_en'))<span class="form-text text-danger"><strong>{{ $errors->first('name_en') }}</strong></span>@endif
						</div>

            <div class="col-md-6">
              <label>الأسم (AR)</label>
              <input class="form-control" type="text" placeholder="الأسم" name="name_ar" value="{{$region->name_ar}}" id="name_ar" />
              @if ($errors->has('name_ar'))<span class="form-text text-danger"><strong>{{ $errors->first('name_ar') }}</strong></span>@endif
            </div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">

					<div class="col-md-6">
					<label>المدينة</label>
					<select class="form-control" name="city_id" id="gallery">
						<option value="" disabled selected>اختار المدينة</option>
						@foreach($cities as $city)
						<option value="{{$city->id}}" @if($region->city_id == $city->id) selected @endif>{{$city->name_ar}}</option>
						@endforeach
					</select>
					</div>
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
