@extends('admin.layout.main')
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fa fa-balance-scale"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; المقاسات</h3>
            </div>
        </div>
        <div class="kt-portlet__body text-center">
			       <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
				<thead>
					<tr>
						<th>#</th>
						<th>المقاس</th>
						<th>الأعدادات</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($sizes as $size)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td>{{$size->title}}</td>
							<td>
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">
										الأعدادات
									</button>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="{{route('sizes.edit', $size->id)}}">تعديل</a>
										<a class="dropdown-item" data-toggle="modal" href="#myModal-{{ $size->id }}">حذف</a>
									</div>
								</div>

								<div class="modal fade" id="myModal-{{ $size->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
								<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">حذف المقاس</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
								<form role="form" action="{{ url('admin/sizes/'.$size->id) }}" class="" method="POST">
								<input name="_method" type="hidden" value="DELETE">
								{{ csrf_field() }}
								<p>هل انت متاكد من الحذف</p>
								<button type="submit" class="btn btn-danger" name='delete_modal'>حذف</button>
								<button type="button" class="btn btn-success" data-dismiss="modal">الغاء</button>
								</form>
								</div>
								</div>
								</div>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
        </div>
	</div>
</div>
@endsection
