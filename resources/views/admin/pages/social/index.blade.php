@extends('admin.layout.main')

@section('content')
	<!--begin::Portlet-->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon"><i class="fab fa-facebook"></i></span>
                <h3 class="kt-portlet__head-title">السوشيال ميديا</h3>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin::Section-->
            <div class="kt-section">

                <div class="kt-section__content text-center">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                <th>#</th>
                                <th>موقع السوشيال ميديا</th>
                                <th>الأعدادات</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($socail as $section)

                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                         <td class="social_media_icon">
                                         @if ($section->name == 'facebook')
                                            <i class="fab fa-{{$section->name}}-f"></i>
                                        @elseif ($section->name == 'linkedin')
                                            <i class="fab fa-{{$section->name}}-in"></i>
                                        @elseif ($section->name == 'rss')
                                            <i class="fas fa-{{$section->name}}"></i>
                                        @elseif ($section->name == 'google-plus')
                                            <i class="fab fa-{{$section->name}}-g"></i>
                                        @else
                                            <i class="fab fa-{{$section->name}}"></i>
                                         @endif
                                         </td>
                                         <td>
                                            <button class="btn btn-brand dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="true">الأعدادات</button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="{{route('social.edit', $section->id)}}"><i class="fa fa-edit"></i> تعديل</a>
                                                <a class="dropdown-item" data-toggle="modal" href="#myModal-{{ $section->id }}"><i class="fa fa-trash"></i> حذف</a>
                                            </div>
                                                <div class="modal fade" id="myModal-{{ $section->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                <div class="modal-content">
                                                <div class="modal-body">
                                                <form role="form" action="{{ url('/admin/social/'.$section->id) }}" class="" method="POST">
                                                <input name="_method" type="hidden" value="DELETE">
                                                {{ csrf_field() }}
                                                <p>هل أنت متاكد من الحذف ؟</p>
                                                <button type="submit" class="btn btn-danger" name='delete_modal'><i class="fa fa-trash" aria-hidden="true"></i> حذف</button>
                                                </form>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!--end::Section-->
        </div>

        <!--end::Form-->
    </div>

        <!--end::Portlet-->
@endsection
