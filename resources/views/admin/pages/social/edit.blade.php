@extends('admin.layout.main')

@section('content')
    <!--begin::Portlet-->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon"><i class="fab fa-facebook"></i></span>
                    <h3 class="kt-portlet__head-title">
                        تعديل السوشيال ميديا
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
            <form class="kt-form kt-form--label-left" id="kt_form_1" method="post"
                action="{{ url('/admin/social/' . $social->id) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT" />

                <div class="kt-portlet__body">
                    @if ($errors->any())
                        <div class="alert alert-danger">{{ $errors->first() }}</div>
                    @endif
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">مواقع السوشيال ميديا</label>
                        <div class="col-lg-10">
                            <select name="network" id="network" class="form-control">
                                <option value="" disabled="" selected=""> أختار السوشيال ميديا</option>
                                <option value="facebook" @if ($social->name == 'facebook') selected @endif>اللفيس بوك</option>
                                <option value="twitter" @if ($social->name == 'twitter') selected @endif>تويتر</option>
                                <option value="linkedin" @if ($social->name == 'linkedin') selected @endif>لينكد ان</option>
                                <option value="instagram" @if ($social->name == 'instagram') selected @endif>الأنستجرام</option>
                                <option value="youtube" @if ($social->name == 'youtube') selected @endif>اليوتيوب</option>
                                <option value="google" @if ($social->name == 'google') selected @endif>جوجل</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">الرابط</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control m-input" name="link" value="{{ $social->link }}" />
                            @if ($errors->has('link'))
                                <span class="help-block"><strong>{{ $errors->first('link') }}</strong></span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <button type="submit" class="btn btn-success">حفظ</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
