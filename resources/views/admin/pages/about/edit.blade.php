@extends('admin.layout.main')

@section('content')

	<!--begin::Portlet-->
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">تعديل</h3>
			</div>
		</div>

		<!--begin::Form-->
		<form class="kt-form kt-form--label-left" id="kt_form_1" method="post"  action="{{route('about.update',['id'=>$about->id])}}" enctype="multipart/form-data">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="PUT" />

			<div class="kt-portlet__body">
				@if($errors->any())
					<div class="alert alert-danger">{{$errors->first()}}</div>
				@endif
				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">عنوان </label>
					<div class="col-lg-10">
						<input type="text" class="form-control m-input" name="title" value="{{$about->title}}" />
					</div>
				</div>

				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">النص</label>
					<div class="col-lg-10">
							<textarea class="form-control m-input ckeditor" name="text">{{$about->text}}</textarea>
					</div>
				</div>

				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">صورة</label>
					<div class="col-lg-7">
						<div class="custom-file">
							<input type="file" class="custom-file-input" name="image" id="image" />
							<label class="custom-file-label" for="image">Choose file</label>
						</div>
					</div>
					<div class="col-lg-3">
						<img src="{{asset($about->image)}}">
					</div>
				</div>

			</div>
			<div class="kt-portlet__foot">
				<div class="kt-form__actions">
					<div class="row">
						<div class="col-lg-12 text-right">
							<button type="submit" class="btn btn-success">حفظ</button>
						</div>
					</div>
				</div>
			</div>
		</form>

		<!--end::Form-->
	</div>

@endsection
