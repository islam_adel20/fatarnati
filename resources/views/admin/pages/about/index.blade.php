@extends('admin.layout.main')

@section('content')
	<!--begin::Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    عن الشركة
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin::Section-->
            <div class="kt-section">

                <div class="kt-section__content">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                <th>#</th>
                                <th>العنوان</th>
                                <th>الأعدادات</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($sections as $section)

                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td>{{$section->title}}</td>
                                        <td>
                                            <button class="btn btn-brand dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="true">الأعدادات</button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="{{route('about.edit', $section->id)}}"><i class="fa fa-edit"></i> تعديل</a>
                                                <a class="dropdown-item" data-toggle="modal" href="#myModal-{{ $section->id }}"><i class="fa fa-trash"></i> حذف</a>
                                            </div>
                                                <div class="modal fade" id="myModal-{{ $section->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">حذف</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                <form role="form" action="{{ url('/admin/about/'.$section->id) }}" class="" method="POST">
                                                <input name="_method" type="hidden" value="DELETE">
                                                {{ csrf_field() }}
                                                <p>are you sure</p>
                                                <button type="submit" class="btn btn-danger" name='delete_modal'><i class="fa fa-trash" aria-hidden="true"></i> حذف</button>
                                                </form>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                                            </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!--end::Section-->
        </div>

        <!--end::Form-->
    </div>

        <!--end::Portlet-->
@endsection
