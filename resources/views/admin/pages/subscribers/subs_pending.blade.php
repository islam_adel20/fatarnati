@extends('admin.layout.main')
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-paper-plane"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; الأشتراكات الجارية</h3>
            </div>
        </div>
        <div class="kt-portlet__body text-center">
			<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
				<thead>
					<tr>
						<th>#</th>
						<th>اسم التاجر</th>
						<th>الموبايل</th>
            <th> الكود</th>
						<th>بداية الأشتراك</th>
            <th>نهاية الأشتراك</th>
            <th>التكلفة</th>
					</tr>
				</thead>
				<tbody>
					@foreach($subs as $sub)
            @foreach(App\Models\Gallery::where('code', $sub->code)->get() as $saller)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td>{{$saller['name_ar']}}</td>
							<td>{{$saller['phone']}}</td>
              <td>{{$sub->code}}</td>
              <td> {{$sub->from}}</td>
              <td> {{$sub->to}}</td>
              <td> {{$sub->cost}}</td>
						</tr>
            @endforeach
					@endforeach
				</tbody>
			</table>
        </div>
	</div>
</div>
@endsection
