@extends('admin.layout.main')
@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-list"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; تعديل</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="{{url('admin/categories/'.$category->id)}}" enctype="multipart/form-data">
				{{csrf_field()}}
				<input type="hidden" name="_method" value="PUT" />
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الأسم (EN)</label>
							<input class="form-control" type="text" placeholder="الأسم (EN)" name="title_en" value="{{$category->title_en}}" id="title_en" />
							@if ($errors->has('title_en'))<span class="form-text text-danger"><strong>{{ $errors->first('title_en') }}</strong></span>@endif
						</div>
						<div class="col-md-6">
							<label>الأسم (AR)</label>
							<input class="form-control" type="text" placeholder="الأسم (AR)" name="title_ar" value="{{$category->title_ar}}" id="title_ar" />
							@if ($errors->has('title_ar'))<span class="form-text text-danger"><strong>{{ $errors->first('title_ar') }}</strong></span>@endif
						</div>
					</div>
				</div>
				@if ($category->cat !== 0)
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<label>الأقسام</label>
								<select name="cat" class="form-control" id="cat">
									@foreach ($categories as $cat)
										<option value="{{ $cat->id }}"
											@if ($category->cat == $cat->id)
												selected
											@endif
										>
											{{ $cat->title_en }}
										</option>
									@endforeach
								</select>
								@if ($errors->has('cat'))<span class="form-text text-danger"><strong>{{ $errors->first('cat') }}</strong></span>@endif
							</div>
						</div>
					</div>
				@else
					<input type="hidden" name="cat" value="0">
				@endif
				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">الصورة</label>
					<div class="col-lg-7">
						<div class="custom-file">
							<input type="file" class="custom-file-input" name="image" id="image" />
							<label class="custom-file-label" for="image">اختار الصورة</label>
						</div>
					</div>
					<div class="col-lg-3">
						<img src="{{asset($category->image)}}">
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
