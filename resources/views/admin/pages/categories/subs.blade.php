@extends('admin.layout.main')
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-list"></i></span>
                <h3 class="kt-portlet__head-title">&nbsp; القسم الرئيسي - {{$main->title_ar}}</h3>
            </div>
            {{--  <div class="kt-portlet__head-toolbar">
            	<div class="kt-portlet__head-wrapper">
                	<div class="kt-portlet__head-actions">
                    	<a href="{{url('categories/create/'.$main->id)}}" class="btn btn-brand btn-elevate btn-icon-sm"><i class="la la-plus"></i> New Category</a>

                    </div>
                </div>
            </div>  --}}
        </div>
        <div class="kt-portlet__body text-center">
			<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
				<thead>
					<tr>
						<th>#</th>
						<th>اسم القسم (EN)</th>
						<th>اسم القسم (AR)</th>
						<th>الصورة</th>
						<th>الأعدادات</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($categories as $categ)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td>{{$categ->title_en}}</td>
							<td>{{$categ->title_ar}}</td>
							<td>
								@if(file_exists(public_path($categ->image)))
								<img src="{{asset($categ->image)}}" width="100px" height="100px">
								@else
								<h5 class="alert alert-danger text-center"> لا يوجد صورة </h5>
								@endif
							</td>
							<td>
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">
										الأعدادات
									</button>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="{{route('categories.edit', $categ->id)}}">تعديل</a>
										<a class="dropdown-item" data-toggle="modal" href="#myModal-{{ $categ->id }}">حذف</a>
									</div>
								</div>

								<div class="modal fade" id="myModal-{{ $categ->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
								<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">حذف القسم</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
								<form role="form" action="{{url('admin/categories/'.$categ->id) }}" class="" method="POST">
								<input name="_method" type="hidden" value="DELETE">
								{{ csrf_field() }}
								<p>هل انت متأكد من الحذف ؟</p>
								<button type="submit" class="btn btn-danger" name='delete_modal'>حذف</button>
								<button type="button" class="btn btn-success" data-dismiss="modal">اغلاق</button>
								</form>
								</div>
								</div>
								</div>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
        </div>
	</div>
</div>
@endsection
