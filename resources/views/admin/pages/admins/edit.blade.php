@extends('admin.layout.main')
@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-user-edit"></i></span>
                <h3 class="kt-portlet__head-title">تعديل المشرف</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="{{url('admin/admins/'.$admin->id)}}" enctype="multipart/form-data">
				{{csrf_field()}}
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{$error}}</li>
							@endforeach
						</ul>
					</div>
				@endif
				<input type="hidden" name="_method" value="PUT" />
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الأسم</label>
							<input class="form-control" type="text" placeholder="الأسم" name="name" value="{{$admin->name}}" id="name" />
						</div>
						<div class="col-md-6">
							<label>اسم المستخدم</label>
							<input class="form-control" type="text" placeholder="اسم المستخدم" name="user_name" value="{{$admin->user_name}}" id="user_name" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>البريد الألكتروني</label>
							<input class="form-control" type="email"  placeholder="البريد الألكتروني" value="{{$admin->email}}" id="email" name="email" />
						</div>
						<div class="col-md-6">
							<label>الموبايل</label>
							<input class="form-control" type="text"  placeholder="الموبايل" name="phone" value="{{$admin->phone}}" id="phone" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الصورة</label>
							<div class="custom-file">
								<input type="file" class="custom-file-input" id="image" name="image">
								<label class="custom-file-label" for="image">اختار الصورة</label>
							</div>
						</div>
						<div class="col-md-2">@if($admin->image != '')<img src="{{asset($admin->image)}}" />@endif</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الصلاحيات</label>
							<div class="row text-left">
								@foreach ($permissions as $permission)
									<div class="col-md-4 p-3">
										<p><input type="checkbox" name="permissions[]" value="{{ $permission->id }}" class="permissions"
											@if(in_array($permission->id, $admin->permissions->pluck('id')->toArray())) checked @endif/> {{ $permission->label }}</p>
									</div>
								@endforeach
							</div>
						</div>

            @if(Auth::guard('admin')->user()->is_super)
            <div class="col-md-4">
              <label>صلاحية الأدمن</label>
                <select name="is_super" class="form-control">
                    <option value="0" @if($admin->is_super == 0) selected @endif> مشرف</option>
                    <option value="1" @if($admin->is_super == 1) selected @endif> مدير </option>
                </select>
            </div>
            @endif
					</div>
				</div>

				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
