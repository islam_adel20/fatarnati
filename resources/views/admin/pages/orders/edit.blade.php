@extends('admin.layout.main')
@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-list"></i></span>
                <h3 class="kt-portlet__head-title">تعديل طلب بيع</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="{{url('selling_order/'.$order->id)}}" enctype="multipart/form-data" id="ajsuform">
				{{csrf_field()}}
				<div id="ajsuform_yu"></div>
				<input type="hidden" name="_method" value="PUT" />
				<div class="form-group" id="client_finder">
					<div class="row">
						<div class="col-md-12">
							<label>البحث عن العميل</label>
							<input class="form-control" type="text" placeholder="Find Client" name="client_search" value="{{$order->client_info->phone}}" id="client_search"
							data-url="{{url('find_client')}}" />
						</div>
					</div>
				</div>
				<div class="form-group" id="order_client_info">
					<div id='cient_finder_data'>
						<input type="hidden" name="client" value="{{$order->client_info->id}}" />
						<h3>تفاصيل العميل</h3>
						<div class='row'>
							<div class='col-md-4'><p><b>الأسم : </b>{{$order->client_info->name}}</p></div>
							<div class='col-md-4'><p><b>الموبايل : </b>{{$order->client_info->phone}}</p></div>
							<div class='col-md-4'><p><b>البريد الألكتروني : </b>{{$order->client_info->email}}</p></div>
						</div>
					</div>
				</div>
				<hr />
				<h3 class="selling_order_products_title">
					<i class="fas fa-list"></i> طلب بضائع
					<button type="button" class="btn btn-brand btn-sm pull-right" id="add_order_item"
					button-url="{{url('add_order_item')}}"><i class="fas fa-plus-square"></i> أضافة طلب بضاعة</button>
				</h3>
				<ol id="order_products">
					@foreach ($order->items as $item)
						<li class="single_order_item" id="single_order_item_box_ABCD{{$item->id}}">
							<input type="hidden" name="order_item[]" value="ABCD{{$item->id}}" />
							<div class="order_item_details">
								<div class="row form-group">
									<div class="col-md-5">
										<label>المنتج</label>
										<select class="order_product_item form-control" name="product[]" id="order_item_ABCD{{$item->id}}"
										options-url="{{url('product_options')}}" item-id="ABCD{{$item->id}}">
											<option value="" disabled selected>أضافة طلب بضاعة</option>
											@foreach ($products as $product)
												<option value="{{$product->id}}" @if($product->id == $item->product) selected @endif>{{$product->title}}</option>
											@endforeach
											<!-- <option value="0">New Product</option> -->
										</select>
									</div>
									<div class="col-md-5">
										<label>العدد</label>
										<input class="form-control" type="number" placeholder="QTY" name="qty[]" value="{{$item->qty}}" />
									</div>
									<div class="col-md-1">
										<label><br /></label>
										<button type="button" class="btn btn-danger btn-sm btn-block delete_order_item" box="ABCD{{$item->id}}"><i class="fas fa-trash-alt"></i></button>
									</div>
									<div class="col-md-1">
										<label><br /></label>
										<button type="button" class="btn btn-warning btn-sm btn-block collapse_details_box" box="ABCD{{$item->id}}"><i class="fas fa-minus"></i></button>
									</div>
								</div>
								<div id="order_item_options_ABCD{{$item->id}}">
									@if(count($item->product_info->colors) > 0 || count($item->product_info->sizes) > 0)
										<div class="row form-group">
											@if(count($item->product_info->colors) > 0)
												<div class="col-md-5">
													<label>اللون</label>
													<select class="form-control" name="color[]">
														<option value="" disabled selected>أختار البضاعة</option>
														@foreach ($item->product_info->colors as $color)
															<option value="{{$color->color_info->id}}" @if($item->color == $color->color_info->id) selected @endif>{{$color->color_info->title}}</option>
														@endforeach
														</select>
												</div>
											@else
												<input type="hidden" name="color[]" value="0" />
											@endif
											@if(count($item->product_info->sizes) > 0)
												<div class="col-md-5">
													<label>المقاس</label>
													<select class="form-control" name="size[]">
														<option value="" disabled selected>Choose Item Size</option>
														@foreach ($item->product_info->sizes as $size)
															<option value="{{$size->size_info->id}}" @if($item->size == $size->size_info->id) selected @endif>{{$size->size_info->title}}</option>
															<!-- <option value="0">New Product</option> -->
														@endforeach
													</select>
												</div>
											@else
												<input type="hidden" name="size[]" value="0" />
											@endif
										</div>
									@else
										<input type="hidden" name="color[]" value="0" />
										<input type="hidden" name="size[]" value="0" />
									@endif
								</div>
								<div class="row form-group">
									<div class="col-md-12">
										<label>الملاحظة</label>
										<textarea class="form-control" placeholder="Note" name="note[]">{{$item->note}}</textarea>
									</div>
								</div>
							</div>
							<div class="order_item_collapse height_zero">
								<div class="row">
									<div class="col-md-11">
										<p>طلب بضاعة</p>
									</div>
									<div class="col-md-1">
										<button type="button" class="btn btn-warning btn-sm btn-block uncollapse_details_box" box="ABCD{{$item->id}}"><i class="fas fa-plus"></i></button>
									</div>
								</div>
							</div>
						</li>
					@endforeach
				</ol>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
