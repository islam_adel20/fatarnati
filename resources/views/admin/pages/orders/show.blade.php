@extends('admin.layout.main')
@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			       <div class="kt-portlet__head-label">
				        <span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-shopping-bag"></i></span>
                <h3 class="kt-portlet__head-title">تفاصيل الطلب</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
				<div class="form-group">
					<div class='row'>
						<div class='col-md-4'><p><b>الأسم : </b>{{$order->client_info->name}}</p></div>
						<div class='col-md-4'><p><b>الموبايل : </b>{{$order->client_info->phone}}</p></div>
						<div class='col-md-4'><p><b>البريد الألكتروني : </b>{{$order->client_info->email}}</p></div>
						<div class='col-md-4'><p><b>المدينة : </b>{{$order->address->city->name_en}}</p></div>
						<div class='col-md-4'><p><b>المنطقة : </b>{{$order->address->area}}</p></div>
						<div class='col-md-4'><p><b>الشارع : </b>{{$order->address->street}}</p></div>
						<div class='col-md-4'><p><b>المبني : </b>{{$order->address->building}}</p></div>
						<div class='col-md-4'><p><b>الشقة : </b>{{$order->address->apartment}}</p></div>
						<div class='col-md-4'><p><b>الشحن : </b>{{$order->address->city->shipment}} EGP</p></div>
						<div class='col-md-4'><p><b>الخصم : </b>{{$order->discount}} EGP</p></div>
						<div class='col-md-4'><p><b>ضريبة : </b>{{$order->tax_value}} EGP</p></div>
						<div class='col-md-4'><p><b>طريقة الدفع : </b>{{$order->pay_info->title_en}}</p></div>
						<div class='col-md-4'>
							<select name="status" class="form-control selling_order_status" num="{{$order->id}}" url="{{url('admin/order_status')}}">
								@foreach ($statuss as $status)
									<option value="{{$status->id}}" @if($status->id == $order->status) selected @endif>{{$status->title}}</option>
								@endforeach
							</select>
					</div>
				</div>
				<hr />

				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>المنتج</th>
							<th>المقاس</th>
							<th>العدد</th>
							<th>السعر</th>
							{{-- <th>اللون</th> --}}
							<th>الأجمالي</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($order->items as $item)
							<tr>
								<td>{{$loop->iteration}}</td>
								<td>{{$item->product_info->title_en}}</td>
								<td>@if($item->size) {{$item->sizeInfo->size_info->title_en}} @endif</td>
								<td>{{$item->qty}}</td>
								<td>{{$item->price}} EGP</td>
								{{--  <td>@if($item->color > 0) {{$item->color_info->title}} @endif</td> --}}
								<td>{{$item->qty * $item->price}} EGP</td>
							</tr>
					@endforeach
					<tr>
						<td colspan="5"></td>
						<td>{{$order->sub_total}} EGP</td>
					</tr>
				</table>

		</div>
	</div>
</div>
@endsection
