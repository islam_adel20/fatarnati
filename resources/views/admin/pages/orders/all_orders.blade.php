@extends('admin.layout.main')
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-shopping-bag"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; الطلبات</h3>
            </div>

        </div>
        <div class="kt-portlet__body text-center">
			<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
				<thead>
					<tr>
						<th>#</th>
						<th>العميل</th>
            <th>المنتج</th>
						<th>التاريخ</th>
						<th>الأعدادات</th>
					</tr>
				</thead>
				<tbody>
					@foreach($all_orders as $order)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td>

                @if(App\Models\User::where('id', $order->user_id)->count() > 0)
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal{{$order->id}}">
                  {{$order->user['name']}}
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">بيانات العميل</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body text-right">
                      <h4> الأسم<span style="color:green">: {{$order->user['name']}}</span></h4>
                      <hr>
                      <h4>الأيميل<span style="color:green">: {{$order->user['email']}}</span></h4>
                      <hr>
                      <h4>الموبايل<span style="color:green">: {{$order->user['phone']}}</span></h4>
                      <hr>
                      <h4>العنوان<span style="color:green">: {{$order->user['address']}}</span></h4>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
                    </div>
                  </div>
                </div>
                </div>
                @else
                    <span style="color:red"> لا يوجد عميل مسجل</span>
                @endif
              </td>

              <td>

                @if(App\Models\Product::where('id', $order->product_id)->count() > 0)
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModalp{{$order->id}}">
                    بيانات المنتج
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModalp{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">بيانات العميل</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body text-right">
                      <h4>اسم المنتج<span style="color:green">: {{$order->product['title_ar']}}</span></h4>
                      <hr>
                      <h4>القسم<span style="color:green">: {{$order->product->category['title_ar']}}</span></h4>
                      <hr>
                      <h4>التاجر<span style="color:green">: {{$order->product->gallery['name_ar']}}</span></h4>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
                    </div>
                  </div>
                </div>
                </div>
                @else
                    <span style="color:red"> لا يوجد منتج مسجل</span>
                @endif
              </td>
							<td>{{date('Y-m-d', strtotime($order->created_at))}}</td>
							<td>
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">
									الأعدادات
									</button>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										{{-- <a class="dropdown-item" href="{{url('orders/'.$order->id.'/invoice')}}" target="_blank">Invoice</a> --}}
										<a class="dropdown-item" data-toggle="modal" href="#myModal-{{ $order->id }}">حذف</a>
									</div>
								</div>

								<div class="modal fade" id="myModal-{{ $order->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
								<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">حذف الطلب</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
								<form role="form" action="{{ url('admin/delete/order/'.$order->id) }}" class="" method="POST">
								<input name="_method" type="hidden" value="DELETE">
								{{ csrf_field() }}
								<p>هل انت متأكد من الحذف ؟</p>
								<button type="submit" class="btn btn-danger" name='delete_modal'>حذف</button>
								<button type="button" class="btn btn-success" data-dismiss="modal">أغلاق</button>
								</form>
								</div>
								</div>
								</div>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
        </div>
	</div>
</div>
@endsection
