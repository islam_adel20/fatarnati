<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Three Stores Dashboard</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
				active: function() {sessionStorage.fonts = true;}
			});
		</script>
		<link rel="stylesheet" href="{{asset('ckeditor/css/samples.css')}}">
		<link rel="stylesheet" href="{{asset('ckeditor/toolbarconfigurator/lib/codemirror/neo.css')}}">

		<link rel="stylesheet" href="{{asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/tether/dist/css/tether.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/select2/dist/css/select2.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/nouislider/distribute/nouislider.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/dropzone/dist/dropzone.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/summernote/dist/summernote.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/animate.css/animate.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/toastr/build/toastr.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/morris.js/morris.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/sweetalert2/dist/sweetalert2.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/general/socicon/css/socicon.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/custom/vendors/line-awesome/css/line-awesome.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/custom/vendors/flaticon/flaticon.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/custom/vendors/flaticon2/flaticon.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/custom/vendors/fontawesome5/css/all.min.css')}}">
		<link rel="stylesheet" href="{{asset('assets/vendors/custom/datatables/datatables.bundle.css')}}">

		<!--begin::Global Theme Styles(used by all pages) -->
        <link rel="stylesheet" href="{{asset('assets/demo/default/base/style.bundle.css')}}">
        <link rel="stylesheet" href="{{asset('assets/demo/default/base/custom.css')}}">
        <!--end::Global Theme Styles -->
        <!--begin::Layout Skins(used by all pages) -->
		<link rel="stylesheet" href="{{asset('assets/demo/default/skins/header/base/light.css')}}">
		<link rel="stylesheet" href="{{asset('assets/demo/default/skins/header/menu/light.css')}}">
		<link rel="stylesheet" href="{{asset('assets/demo/default/skins/brand/dark.css')}}">
		<link rel="stylesheet" href="{{asset('assets/demo/default/skins/aside/dark.css')}}">
		<!--end::Layout Skins -->
		<link rel="stylesheet" href="{{ asset('css/custom.css')}}">
		<link rel="stylesheet" href="{{ asset('css/chat.css')}}">
		<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
		<link href="{{asset('favicon.ico')}}" rel="shortcut icon" type="image/vnd.microsoft.icon">
		<link rel="shortcut icon" type="image/ico" href="{{asset('session1.png')}}" />

	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled
	kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading" onload="print()">

		<!-- begin:: Page -->

	<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-list"></i></span>
					<h3 class="kt-portlet__head-title">فاتورة طلب بيع</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
					<div class="form-group">
						<div class='row'>
							<div class='col-md-6'><p><b>الأسم : </b>{{$order->client_info->name}}</p></div>
							<div class='col-md-6'><p><b>الموبايل : </b>{{$order->client_info->phone}}</p></div>
							<div class='col-md-6'><p><b>البريد الألكتروني : </b>{{$order->client_info->email}}</p></div>
							<div class='col-md-6'><p><b>تاريخ الطلب : </b>{{date('Y-m-d', strtotime($order->created_at))}}</p></div>
							<div class='col-md-12'><p><b>العنوان : </b>{{$order->client_info->address}} - {{$order->client_info->city_info->title}}</p></div>
					</div>
					<hr />

					<table class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>المنتج</th>
								<th>العدد</th>
								<th>السعر</th>
								<th>اللون</th>
								<th>المقاس</th>
								<th>الأجمالي</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($order->items as $item)
								<tr>
									<td>{{$loop->iteration}}</td>
									<td>{{$item->product_info->title}}</td>
									<td>{{$item->qty}}</td>
									<td>{{$item->price}} EGP</td>
									<td>@if($item->color > 0) {{$item->color_info->title}} @endif</td>
									<td>@if($item->size > 0) {{$item->size_info->title}} @endif</td>
									<td>{{$item->qty * $item->price}} EGP</td>
								</tr>
						@endforeach
						<tr>
							<td colspan="6"></td>
							<td>{{$totals}} EGP</td>
						</tr>
					</table>

				</div>
			</div>
		</div>
	</body>
</html>
