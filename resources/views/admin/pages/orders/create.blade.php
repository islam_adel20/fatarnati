@extends('admin.layout.main')
@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-plus-square"></i></span>
                <h3 class="kt-portlet__head-title">أضافة طلب بيع</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="{{url('selling_order')}}" enctype="multipart/form-data" id="ajsuform">
				{{csrf_field()}}
				<div id="ajsuform_yu"></div>
				<div class="form-group" id="client_finder">
					<div class="row">
						<div class="col-md-12">
							<label>البحث عن العميل</label>
							<input class="form-control" type="text" placeholder="Find Client" name="client_search" value="{{old('client_search')}}" id="client_search"
							data-url="{{url('find_client')}}" />
						</div>
					</div>
				</div>
				<div class="form-group" id="order_client_info">

				</div>
				<hr />
				<h3 class="selling_order_products_title">
					<i class="fas fa-list"></i> طلب بضائع
					<button type="button" class="btn btn-brand btn-sm pull-right" id="add_order_item"
					button-url="{{url('add_order_item')}}"><i class="fas fa-plus-square"></i> أضافة بضاعة جديدة</button>
				</h3>
				<ol id="order_products">
					<li class="single_order_item" id="single_order_item_box_{{$first_box_id}}">
						<input type="hidden" name="order_item[]" value="{{$first_box_id}}" />
						<div class="order_item_details">
							<div class="row form-group">
								<div class="col-md-5">
									<label>المنتج</label>
									<select class="order_product_item form-control" name="product[]" id="order_item_{{$first_box_id}}"
									options-url="{{url('product_options')}}" item-id="{{$first_box_id}}">
										<option value="" disabled selected>أختار البضاعة</option>
										@foreach ($products as $product)
											<option value="{{$product->id}}">{{$product->title}}</option>
										@endforeach
										<!-- <option value="0">New Product</option> -->
									</select>
								</div>
								<div class="col-md-5">
									<label>العدد</label>
									<input class="form-control" type="number" placeholder="QTY" name="qty[]" />
								</div>
								<div class="col-md-1"></div>
								<div class="col-md-1">
									<label><br /></label>
									<button type="button" class="btn btn-warning btn-sm btn-block collapse_details_box" box="{{$first_box_id}}"><i class="fas fa-minus"></i></button>
								</div>
							</div>
							<div id="order_item_options_{{$first_box_id}}">

							</div>
							<div class="row form-group">
								<div class="col-md-12">
									<label>الملاحظة</label>
									<textarea class="form-control" placeholder="Note" name="note[]"></textarea>
								</div>
							</div>
						</div>
						<div class="order_item_collapse height_zero">
							<div class="row">
								<div class="col-md-11">
									<p>طلب بضاعة</p>
								</div>
								<div class="col-md-1">
									<button type="button" class="btn btn-warning btn-sm btn-block uncollapse_details_box" box="{{$first_box_id}}"><i class="fas fa-plus"></i></button>
								</div>
							</div>
						</div>
					</li>

				</ol>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
