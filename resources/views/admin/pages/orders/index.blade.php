@extends('admin.layout.main')
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-shopping-bag"></i></span>
                <h3 class="kt-portlet__head-title">الطلبات</h3>
            </div>

        </div>
        <div class="kt-portlet__body">
			<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
				<thead>
					<tr>
						<th>#</th>
						<th>العميل</th>
						<th>الحالة</th>
						<th>طريقة الدفع</th>
						<th>التاريخ</th>
						<th>الأعدادات</th>
					</tr>
				</thead>
				<tbody>
					@foreach($orders as $order)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td>{{$order->client_info->name}}</td>
							<td>
								<select name="status" class="form-control selling_order_status" num="{{$order->id}}" url="{{url('admin/order_status')}}">
									@foreach ($statuss as $status)
										<option value="{{$status->id}}" @if($status->id == $order->status) selected @endif>{{$status->title}}</option>
									@endforeach
								</select>
							</td>
							<td>{{$order->pay_info->title_en}} @if($order->pay_method > 1 && $order->is_paid == 1) <span class="text-success">Paid</div> @endif</td>
							<td>{{date('Y-m-d', strtotime($order->created_at))}}</td>
							<td>
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">
									الأعدادات
									</button>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="{{route('orders.show', $order->id)}}">التفاصيل</a>
										{{-- <a class="dropdown-item" href="{{url('orders/'.$order->id.'/invoice')}}" target="_blank">Invoice</a> --}}
										<a class="dropdown-item" data-toggle="modal" href="#myModal-{{ $order->id }}">حذف</a>
									</div>
								</div>

								<div class="modal fade" id="myModal-{{ $order->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
								<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">حذف الطلب</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
								<form role="form" action="{{ url('admin/orders/'.$order->id) }}" class="" method="POST">
								<input name="_method" type="hidden" value="DELETE">
								{{ csrf_field() }}
								<p>هل انت متأكد من الحذف ؟</p>
								<button type="submit" class="btn btn-danger" name='delete_modal'>حذف</button>
								<button type="button" class="btn btn-success" data-dismiss="modal">أغلاق</button>
								</form>
								</div>
								</div>
								</div>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
        </div>
	</div>
</div>
@endsection
