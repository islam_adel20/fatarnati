@extends('admin.layout.main')

@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-tags"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; الأكواد</h3>
            </div>
        </div>
        <div class="kt-portlet__body text-center">

            <!--begin::Section-->
            <div class="kt-section">

                <div class="kt-section__content">
                    <div class="table-responsive">
                      <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>الكود</th>
                                    <th>حالة الكود</th>
                                    <th>المدة الزمنية</th>
                                    <th>بداية التفعيل</th>
                                    <th>انتهاء التفعيل</th>
                                    <th>التكلفة</th>
                                    <th>الأعدادات</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($codes as $code)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td>{{$code->code}}</td>
                                        <td>
                                          @if($code->status == 'expired')
                                            <span style="color:green">مفعل </span>
                                          @elseif($code->status == 'unactive')
                                            <span style="color:red">غير مفعل </span>
                                          @elseif($code->status == 'finish')
                                            <span style="color:red"> انتهاء التفعيل </span>
                                          @endif
                                        </td>
                                        <td>
                                            @if($code->status == 'unactive')
                                                {{$code->duration}} يوم
                                            @else
                                                @if($code->duration - date_diff(new \DateTime($code->from), new \DateTime())->format("%d") <= 0)
                                                    0 يوم
                                                @else
                                                    {{$code->duration - date_diff(new \DateTime($code->from), new \DateTime())->format("%d")}} يوم
                                                @endif
                                            @endif

                                          </td>

                                        <td>
                                          @if($code->from == null && $code->status == 'unactive')
                                            <span style="color:red"> لم يبدأ</span>
                                          @else
                                            {{$code->from}}
                                          @endif
                                        </td>
                                        <td>
                                          @if($code->to == null && $code->status == 'unactive')
                                            <span style="color:red"> لم يبدأ</span>
                                          @else
                                            {{$code->to}}
                                          @endif
                                        </td>

                                        <td> {{$code->cost}}</td>
                                        <td>
                                            <button class="btn btn-brand dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="true">الأعدادات</button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="{{route('codes.edit', $code->id)}}"><i class="fa fa-edit"></i> تعديل</a>
                                                <a class="dropdown-item" data-toggle="modal" href="#myModal-{{ $code->id }}"><i class="fa fa-trash"></i> حذف</a>
                                            </div>
                                                <div class="modal fade" id="myModal-{{ $code->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">حذف الكود</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                <form role="form" action="{{ url('/admin/saller/codes/'.$code->id) }}" class="" method="POST">
                                                <input name="_method" type="hidden" value="DELETE">
                                                {{ csrf_field() }}
                                                <p>هل أنت متأكد من الحذف ؟</p>
                                                <button type="submit" class="btn btn-danger" name='delete_modal'><i class="fa fa-trash" aria-hidden="true"></i>حذف</button>
                                                </form>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                                            </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!--end::Section-->
        </div>
    </div>
</div>
@endsection
