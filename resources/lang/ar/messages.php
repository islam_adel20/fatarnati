<?php

return [
    'message_sent' => 'تم إرسال رسالتك بنجاح',
    'data_changed' => 'تم تعديل بياناتك بنجاح',
    'pass_changed' => 'تم تحديث كلمة المرور بنجاح',
    'address_created' => 'تم إضافة العنوان بنجاح',
    'address_updated' => 'تم تعديل العنوان بنجاح',
    'address_deleted' => 'تم حذف العنوان بنجاح',
    'register_success' => 'تم إنشاء الحساب بنجاح',
    'add_success' => 'تم إضافة المنتج بنجاح',
    'invalid_discount' => 'كوبون الخصم غير سليم أو منتهي'
];
