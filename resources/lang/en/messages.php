<?php

return [
    'message_sent' => 'Your Message Sent Successfully',
    'data_changed' => 'Your Data Changed Successfully',
    'pass_changed' => 'Your Password Changed Successfully',
    'address_created' => 'Your Address Created Successfully',
    'address_updated' => 'Your Address Updated Successfully',
    'address_deleted' => 'Your Address Deleted Successfully',
    'register_success' => 'Account Created Successfully.',
    'add_success' => 'Added Successfully',
    'invalid_discount' => 'Discount Code Is Not Valid'
];
