<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


  Route::group(['namespace' => 'Dashboard', 'middleware'=>['admin', 'admin.permissions'], 'prefix' => 'admin'], function() {
    Route::get('/', 'DashboardController@index');

    /************** Admin Account Settings */
    Route::get('/profile', 'DashboardController@profile');
    Route::post('/profile', 'DashboardController@save_profile');
    Route::get('/change_password', 'DashboardController@change_password');
    Route::post('/change_password', 'DashboardController@password_save');

    /************* Admins */
    Route::resource('admins', 'AdminController');
    Route::get('admins/edit_password/{id}', 'AdminController@change_password');
    Route::post('admins/save_password/{id}', 'AdminController@password_save');

    /************* Slider */
    Route::resource('slider', 'SliderController');

    /***** Web Pages Sections */
    Route::resource('sections', 'SectionController');

    /***** Categories */
    Route::resource('categories', 'CategoryController');

    /***** Categories Saller Services */
    Route::resource('saller/servcies/category', 'CategorySallerServciesController');

    /***** Galleries */
    Route::resource('galleries', 'GalleryController');
    Route::get('gallery_products/{id}', 'GalleryController@products');
    Route::get('gallery_orders/{id}', 'GalleryController@saller_orders');
    Route::get('gallery_products_favorite/{id}', 'GalleryController@productsFavorite');


    /*****  Saller Srvices */
    Route::resource('sallers/services', 'SallerServicesController');

    /***** Saller Codes */
    Route::resource('saller/codes', 'SallerCodeController');

    /***** Products */
    Route::resource('products', 'ProductController');
    Route::resource('/products_images', 'ProductImageController')->except(['create']);
    Route::get('/products_images/{id}/create', 'ProductImageController@create');
    Route::any('delete/product/color/{id}', 'ProductController@delete_product_color');
    Route::get('filter/subs/category', 'ProductController@filter_subs_category')->name('filter.subs.category');

    /***** cities & regions */
    Route::resource('cities', 'CityController');
    Route::resource('regions', 'RegionController');


    /***** Clients */
    Route::resource('clients', 'ClientController');
    Route::get('products_favorite/clients/{id}', 'ClientController@products_favorite');


    /***** Orders */
    Route::get('all/orders', 'OrderController@all_orders');
    Route::any('delete/order/{id}', 'OrderController@delete_order');

    // /******* Subscribers */
    // Route::get('sub/pending', 'SubscriberController@sub_pending');
    // Route::get('sub/finish', 'SubscriberController@sub_finish');
    // Route::get('sub/free/{id}', 'SubscriberController@sub_free');
    // Route::get('sub/payment/{id}', 'SubscriberController@sub_payment');
    // Route::get('sub/open/{id}', 'SubscriberController@sub_open');
    // Route::get('sub/block/{id}', 'SubscriberController@sub_block');

    /******* Social Media */
    Route::resource('social', 'SocialController');

    /*********** About Us */
    Route::resource('/about', 'AboutController');

    /*********** Sizes */
    Route::resource('/sizes', 'SizeController');

    /*********** colors */
    Route::resource('colors', 'ColorController');

    /****************** Contact Us */
    Route::get('/contact', 'ContactController@index');
    Route::post('/contact', 'ContactController@contact_save');

    /****************** Terms */
    Route::resource('terms', 'TermController');
    Route::get('user/terms', 'TermController@terms_user');

    /****************** Subscribers */
    Route::get('sub/pending', 'SubscriberController@sub_pending');
    Route::get('sub/finish', 'SubscriberController@sub_finish');

});


Route::get('/', 'AdminAuth\LoginController@showLoginForm')->name('login');
Route::group(['prefix' => 'admin'], function () {
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::get('/logout', 'AdminAuth\LoginController@logout')->name('logout');
});
