<?php

use Illuminate\Http\Request;
use Illuminate\Routing\RouteGroup;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// General stuff


Route::group(['namespace' => 'API', 'middleware' => ['auth:api', 'api.settings', 'api.check.status.code']], function () {

    /***** Orders */
    Route::post('add/order', 'OrdersController@add_order');

    /** Logout From Users & Saller & Saller Services **/
    Route::post('logout/user','UserController@logout');
    Route::post('/my-info','UserController@myInfo');
    Route::post('/edit-info','UserController@editInfo');
    Route::post('/change-password','UserController@changePassword');
    Route::post('profile/user','UserController@profileUser');
    Route::post('edit/user','UserController@editUser');
    Route::get('product/favorite/user','UserController@productFavorite');
    Route::post('add/product/favorite/user','UserController@addProductFavorite');
    Route::post('delete/product/favorite/user','UserController@deleteProductFavorite');
    
    /** Review From User To Saller */
    Route::post('add/review/user-saller', 'ReviewsController@addReviewSallers');
    Route::post('edit/review/user-saller', 'ReviewsController@editReviewSallers');
    /** Review From User To Saller Services*/
    Route::post('add/review/user-saller-services', 'ReviewsController@addReviewSallersServices');
    Route::post('edit/review/user-saller-services', 'ReviewsController@editReviewSallersServices');
    /** Send Message To Saller And Saller Services */
    Route::post('send/message', 'MessagesController@sendMessage');
    Route::post('show/messages', 'MessagesController@showMessages');
});

/*********** Auth Saller **/
Route::group(['namespace' => 'API', 'middleware' => ['auth:api_saller', 'api.settings', 'api.check.status.code']], function () {
  /** Sallers **/
  Route::get('show/saller', 'SallersController@show_saller');
  Route::get('profile/saller', 'SallersController@profile_saller');
  Route::post('edit/sallers', 'SallersController@edit_sallers');
  Route::get('product/favorite/saller', 'SallersController@productFavorite');

  /** Proudcts With Sallers **/
  Route::get('saller/products', 'ProductsSallerController@sallerProducts');
  Route::post('add/products/saller', 'ProductsSallerController@add_product');
  Route::post('edit/products/saller', 'ProductsSallerController@edit_product');
  Route::post('delete/product/color','ProductsSallerController@delete_product_color');

  /** Orders **/
  Route::get('saller/orders', 'OrdersController@saller_orders');

  /** Logout **/
  Route::post('logout/saller','SallersController@logout');

  /** Send Message To User */
  Route::post('send/message', 'MessagesController@sendMessage');
  Route::post('show/saller/messages', 'MessagesController@showSallerMessages');
  Route::post('show/rooms/saller', 'MessagesController@sallerRooms');
});

/*********** Auth Saller Services **/
Route::group(['namespace' => 'API', 'middleware' => ['auth:api_service_sallers', 'api.settings', 'api.check.status.code']], function () {

  /** Sallers Services **/
  Route::get('show/saller/services', 'SallersServicesController@show_saller_services');
  Route::get('profile/saller/services', 'SallersServicesController@profile_saller_services');
  Route::post('edit/sallers/services', 'SallersServicesController@edit_sallers_services');

  /** Logout **/
  Route::post('logout/saller/servcies','SallersServicesController@logout');

  /** Send Message To User */
  Route::post('send/message', 'MessagesController@sendMessage');
  Route::post('show/messages', 'MessagesController@showMessages');
  Route::post('show/rooms/saller-services', 'MessagesController@sallerServicesRooms');
});

Route::namespace('API')->group(function () {

  /***** Sallers */
  Route::get('all/sallers', 'SallersController@all_sellers');
  Route::get('all/codes', 'SallersController@codes');
  Route::get('all/sallers/cats', 'SallersController@all_sellers_cats');
  Route::get('review/user-saller', 'ReviewsController@reviewSaller');

  /***** Cateogries Main */
  Route::get('categories', 'CategoriesController@categories_main');
  Route::get('show/category', 'CategoriesController@show_categories_main');
  Route::post('add/category', 'CategoriesController@add_categorys_main');
  Route::post('edit/category', 'CategoriesController@edit_categorys_main');
  Route::post('delete/category', 'CategoriesController@delete_categorys_main');
  /***** Cateogries Sub */
  Route::get('categories/sub', 'CategoriesController@categories_sub');
  Route::get('show/category/sub', 'CategoriesController@show_categories_sub');
  Route::post('add/category/sub', 'CategoriesController@add_categorys_sub');
  Route::post('edit/category/sub', 'CategoriesController@edit_categorys_sub');
  Route::post('delete/category/sub', 'CategoriesController@delete_categorys_sub');

  /** Register And Login Saller **/
  Route::post('login/sallers', 'SallersController@login');
  Route::post('add/sallers', 'SallersController@add_sallers');
  Route::post('login/sallers/services', 'SallersServicesController@login');
  Route::post('add/sallers/services', 'SallersServicesController@add_sallers_services');

  /**** Cities */
  Route::get('cities/with/reigons','CitiesController@cities_with_reigons');
  Route::get('cities','CitiesController@city');
  Route::get('regions','CitiesController@regions');

  /**** Products **/
  Route::get('all/products', 'ProductsSallerController@allProducts');
  Route::get('detailes/product', 'ProductsSallerController@product');
  Route::get('products/latest', 'ProductsSallerController@productsLatest');
  Route::get('products/search', 'ProductsSallerController@productsSearch');
  Route::get('all/sallers/products', 'ProductsSallerController@allSallerProducts');
 
  
   

  /**** Categories Services */
  Route::get('categories/services', 'CategoriesServicesController@categories_serv');
  Route::get('show/category/services', 'CategoriesServicesController@show_categories_serv');
  Route::post('add/category/services', 'CategoriesServicesController@add_categorys_serv');
  Route::post('edit/category/services', 'CategoriesServicesController@edit_categorys_serv');
  Route::post('delete/category/services', 'CategoriesServicesController@delete_categorys_serv');

  /*** Saller Services */
  Route::get('all/sallers/services', 'SallersServicesController@all_sellers_services');
  Route::get('review/user-saller-services', 'ReviewsController@reviewSallerServices');

  /**** Orders */
  Route::get('all/orders', 'OrdersController@all_orders');

  /**** Terms and Conditions **/
  Route::get('terms/sallers', 'TermController@terms_saller');
  Route::get('terms/users', 'TermController@terms_user');
  Route::post('add/terms', 'TermController@store');
  Route::post('edit/terms', 'TermController@edit');
  Route::post('delete/terms', 'TermController@destroy');


  /** Sliders **/
  Route::get('sliders', 'SlidersController@sliders');
  Route::post('add/slider', 'SlidersController@store');
  Route::post('edit/slider', 'SlidersController@update');
  Route::post('delete/slider', 'SlidersController@destroy');
  
  /** Soical Media **/
  Route::get('social', 'SocialController@soical');
  Route::post('add/social', 'SocialController@add_soical');
  Route::post('edit/social', 'SocialController@edit_soical');
  Route::post('delete/social', 'SocialController@delete_soical');

  /** Contact Us **/
  Route::get('contact','SectionController@contact');

  /** Privacy App **/
  Route::get('privacy/app','SectionController@privacy_app');

  /** About App **/
  Route::get('about/app','SectionController@about_app');

  /** Sizes **/
  Route::get('sizes', 'SizeController@sizes');
  Route::post('add/sizes','SizeController@add_sizes');
  Route::post('edit/sizes','SizeController@edit_sizes');
  Route::post('delete/sizes','SizeController@delete_sizes');

    /** Colors **/
    Route::get('colors', 'ColorController@colors');
    Route::post('add/colors','ColorController@add_colors');
    Route::post('edit/colors','ColorController@edit_colors');
    Route::post('delete/colors','ColorController@delete_colors');

  /** Register And Login Users **/
  Route::post('register/user','UserController@register');
  Route::post('login/user','UserController@login');
  Route::post('reset/password/user','UserController@resetPassword');

});
