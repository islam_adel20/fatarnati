<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceSallersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_sallers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longtext('desc')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('password')->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->integer('cat_id')->unsigned()->nullable();
            $table->string('code')->nullable();
            $table->string('image')->nullable();
            $table->longtext('map')->nullable();
            $table->string('facebook_link')->nullable();
            $table->string('instgram_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('youtube_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_sallers');
    }
}
