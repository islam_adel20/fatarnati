<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_en');
            $table->string('title_ar');
            $table->text('text_en');
            $table->text('text_ar');
            $table->integer('main_cat');
            $table->integer('sub_cat');
            $table->integer('cat')->default(0);
            $table->integer('gallery_id')->default(0);
            $table->integer('sale')->default(0);
            $table->integer('new')->default(0);
            $table->integer('active')->default(1);
            $table->double('price');
            $table->integer('discount')->default(0);
            $table->integer('stock_qty')->default(0);
            $table->string('start_discount')->nullable();
            $table->string('end_discount')->nullable();
            $table->string('time_arrive')->nullable();
            $table->double('price_with_discount');
            $table->string('photo')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
