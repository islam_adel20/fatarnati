<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscribers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('saller_id')->unsigned();
            $table->string('sub_from')->nullable();
            $table->string('sub_to')->nullable();
            $table->boolean('sub_type')->nullable();
            $table->boolean('sub_status')->nullable();
            $table->enum('saller_status', ['0', '1'])->default(0);
            $table->integer('cost')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscribers');
    }
}
