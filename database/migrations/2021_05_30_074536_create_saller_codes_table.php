<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSallerCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saller_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->enum('status', ['expired', 'unactive', 'finish'])->default('unactive');
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->string('duration')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saller_codes');
    }
}
