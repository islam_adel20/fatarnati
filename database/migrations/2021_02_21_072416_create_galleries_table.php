<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galleries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_en');
            $table->string('name_ar');
            $table->string('phone');
            $table->string('address_en');
            $table->string('address_ar');
            $table->text('map');
            $table->string('image')->nullable();
            $table->string('commercial_register')->nullable();
            $table->string('nationalist')->nullable();
            $table->string('password')->nullable();
            $table->string('activate_token')->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->string('code')->nullable();
            $table->string('facebook_link')->nullable();
            $table->string('instgram_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('youtube_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galleries');
    }
}
