<?php

use Illuminate\Database\Seeder;

class MainCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'title' => 'Women',
            'cat' => 0,
            'image'=>'/site/img/banner/products-cat2.jpg'
        ]);

        DB::table('categories')->insert([
            'title' => 'Men',
            'cat' => 0,
            'image'=>'/site/img/banner/products-cat1.jpg'
        ]);

        DB::table('categories')->insert([
            'title' => 'Footware',
            'cat' => 0,
            'image'=>'/site/img/banner/products-cat3.jpg'
        ]);

        DB::table('categories')->insert([
            'title' => 'Jewellery',
            'cat' => 0,
            'image'=>'/site/img/banner/products-cat1.jpg'
        ]);
        
    }
}
