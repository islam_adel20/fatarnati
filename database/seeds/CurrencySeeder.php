<?php

use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->insert([
            'title' => 'EGP'
        ]);

        DB::table('currencies')->insert([
            'title' => 'EUR'
        ]);

        DB::table('currencies')->insert([
            'title' => 'USD'
        ]);

        DB::table('currencies')->insert([
            'title' => 'KSA'
        ]);

        DB::table('currencies')->insert([
            'title' => 'AED'
        ]);
    }
}
