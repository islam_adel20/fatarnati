<?php

use Illuminate\Database\Seeder;

class ProductImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 20; $i++)
        {
            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 2,
                'image' => '/site/img/products/7.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 2,
                'image' => '/site/img/products/1.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 2,
                'image' => '/site/img/products/2.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 2,
                'image' => '/site/img/products/3.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 3,
                'image' => '/site/img/products/6.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 3,
                'image' => '/site/img/products/1.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 3,
                'image' => '/site/img/products/2.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 3,
                'image' => '/site/img/products/3.jpg'
            ]);            

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 4,
                'image' => '/site/img/products/5.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 4,
                'image' => '/site/img/products/1.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 4,
                'image' => '/site/img/products/2.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 4,
                'image' => '/site/img/products/3.jpg'
            ]);            

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 5,
                'image' => '/site/img/products/4.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 5,
                'image' => '/site/img/products/1.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 5,
                'image' => '/site/img/products/2.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 5,
                'image' => '/site/img/products/3.jpg'
            ]);            

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 6,
                'image' => '/site/img/products/10.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 6,
                'image' => '/site/img/products/1.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 6,
                'image' => '/site/img/products/2.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 6,
                'image' => '/site/img/products/3.jpg'
            ]);
        } 

    }
}
