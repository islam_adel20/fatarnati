<?php

use Illuminate\Database\Seeder;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pay_methods')->insert([
            [
                'title_en' => 'Cash On Delivery',
                'title_ar' => 'الدفع عند الإستلام',
            ],
            [
                'title_en' => 'Online',
                'title_ar' => 'عن طريق الفيزا',
            ],
        ]);
    }
}
