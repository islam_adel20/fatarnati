<?php

use Illuminate\Database\Seeder;

class SubOfMainCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 4; $i++)
        {
            DB::table('categories')->insert([
                'title' => 'Dresses',
                'cat' => $i
            ]);

            DB::table('categories')->insert([
                'title' => 'Shoes',
                'cat' => $i
            ]);

            DB::table('categories')->insert([
                'title' => 'Handbags',
                'cat' => $i
            ]);

            DB::table('categories')->insert([
                'title' => 'Clothing',
                'cat' => $i
            ]);

            DB::table('categories')->insert([
                'title' => 'Day',
                'cat' => $i
            ]);

            DB::table('categories')->insert([
                'title' => 'Evening',
                'cat' => $i
            ]);

            DB::table('categories')->insert([
                'title' => 'Sports',
                'cat' => $i
            ]);

            DB::table('categories')->insert([
                'title' => 'Clothing',
                'cat' => $i
            ]);

            DB::table('categories')->insert([
                'title' => 'Day',
                'cat' => $i
            ]);

            DB::table('categories')->insert([
                'title' => 'Evening',
                'cat' => $i
            ]);

            DB::table('categories')->insert([
                'title' => 'Sports',
                'cat' => $i
            ]);

            DB::table('categories')->insert([
                'title' => 'Adults',
                'cat' => $i
            ]);

            DB::table('categories')->insert([
                'title' => 'Kids',
                'cat' => $i
            ]);

            DB::table('categories')->insert([
                'title' => 'Accessories',
                'cat' => $i
            ]);
        }
    }
}
