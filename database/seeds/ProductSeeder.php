<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 20; $i++)
        {
            DB::table('products')->insert([
                'title' => 'Product '.$i,
                'text' => 'Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum. Quisque in arcu id dui vulputate mollis eget non arcu. Aenean et nulla purus. Mauris vel tellus non nunc mattis lobortis. Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum. Quisque in arcu id dui vulputate mollis eget non arcu. Aenean et nulla purus. Mauris vel tellus non nunc mattis lobortis.',
                'main_cat' => 1,
                'sub_cat'=> 5,
            ]);

            DB::table('product_sizes')->insert([
                'product' => $i,
                'size'=> 1,
            ]);

            DB::table('product_sizes')->insert([
                'product' => $i,
                'size'=> 2,
            ]);

            DB::table('product_sizes')->insert([
                'product' => $i,
                'size'=> 3,
            ]);

            DB::table('product_sizes')->insert([
                'product' => $i,
                'size'=> 4,
            ]);

            DB::table('product_colors')->insert([
                'product' => $i,
                'color'=> 2,
            ]);

            DB::table('product_colors')->insert([
                'product' => $i,
                'color'=> 3,
            ]);

            DB::table('product_colors')->insert([
                'product' => $i,
                'color'=> 4,
            ]);

            DB::table('product_colors')->insert([
                'product' => $i,
                'color'=> 5,
            ]);

            DB::table('product_colors')->insert([
                'product' => $i,
                'color'=> 6,
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 2,
                'image' => '/site/img/products/7.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 2,
                'image' => '/site/img/products/1.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 2,
                'image' => '/site/img/products/2.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 2,
                'image' => '/site/img/products/3.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 3,
                'image' => '/site/img/products/6.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 3,
                'image' => '/site/img/products/1.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 3,
                'image' => '/site/img/products/2.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 3,
                'image' => '/site/img/products/3.jpg'
            ]);            

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 4,
                'image' => '/site/img/products/5.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 4,
                'image' => '/site/img/products/1.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 4,
                'image' => '/site/img/products/2.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 4,
                'image' => '/site/img/products/3.jpg'
            ]);            

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 5,
                'image' => '/site/img/products/4.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 5,
                'image' => '/site/img/products/1.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 5,
                'image' => '/site/img/products/2.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 5,
                'image' => '/site/img/products/3.jpg'
            ]);            

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 6,
                'image' => '/site/img/products/10.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 6,
                'image' => '/site/img/products/1.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 6,
                'image' => '/site/img/products/2.jpg'
            ]);

            DB::table('product_images')->insert([
                'product' => $i,
                'color'=> 6,
                'image' => '/site/img/products/3.jpg'
            ]); 
            
            DB::table('product_prices')->insert([
                'product' => $i,
                'country'=> 1,
                'price' => 100.00
            ]);
            
            DB::table('product_prices')->insert([
                'product' => $i,
                'country'=> 2,
                'price' => 100.00
            ]);

            DB::table('product_prices')->insert([
                'product' => $i,
                'country'=> 3,
                'price' => 100.00
            ]);
        }
    }
}
