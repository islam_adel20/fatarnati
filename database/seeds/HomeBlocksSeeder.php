<?php

use Illuminate\Database\Seeder;

class HomeBlocksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blocks')->insert([
            'position'=>'best_sellers',
            'title' => 'Best Sellers',
            'text' => "Our Customer's Favourite Cuts",
            'image' => '/site/img/banner/products-pack1.jpg'
        ]);

        DB::table('blocks')->insert([
            'position'=>'all_products',
            'title' => 'All Products',
            'text' => "See All Products",
            'image' => '/site/img/banner/products-pack2.jpg'
        ]);

        DB::table('blocks')->insert([
            'position'=>'about_mags',
            'text' => "<p>We make leisurewear for everywhere.</p><br><p>Effortless essentials – perfected with custom-made performance fabrics.</p><p>A tailored ﬁt. And design details that elevate ‘good’ to unquestionably better.</p>",
            'image' => '/site/img/logo.png'
        ]);
    }
}
