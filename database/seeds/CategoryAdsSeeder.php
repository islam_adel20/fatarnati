<?php

use Illuminate\Database\Seeder;

class CategoryAdsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_ads')->insert([
            'image' => '/site/img/banner/banner-menu1.jpg',
            'url' => '#',
            'cat' => 1
        ]);

        DB::table('category_ads')->insert([
            'image' => '/site/img/banner/banner-menu2.jpg',
            'url' => '#',
            'cat' => 1
        ]);

        DB::table('category_ads')->insert([
            'image' => '/site/img/banner/banner-menu1.jpg',
            'url' => '#',
            'cat' => 3
        ]);
    }
}
