<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminSeeder::class);
        $this->call(ContactSeeder::class);
        $this->call(OrderStatusesSeeder::class);
        $this->call(PaymentMethodSeeder::class);
        $this->call(PermissionSeeder::class);
    }
}
