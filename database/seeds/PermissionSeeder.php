<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name' => 'home',
                'label' => 'Control Home',
                'class' => 'SectionController',
            ],
            [
                'name' => 'cities',
                'label' => 'Control Cities',
                'class' => 'CityController',
            ],
            [
                'name' => 'clients',
                'label' => 'Control Clients',
                'class' => 'ClientController',
            ],
            [
                'name' => 'categories',
                'label' => 'Control Categories',
                'class' => 'CategoryController',
            ],
            [
                'name' => 'partners',
                'label' => 'Control Partners',
                'class' => 'GalleryController',
            ],
            [
                'name' => 'products',
                'label' => 'Control Products',
                'class' => 'ProductController',
            ],
            [
                'name' => 'orders',
                'label' => 'Control Orders',
                'class' => 'SellingOrderController',
            ],
            [
                'name' => 'sizes',
                'label' => 'Control Sizes',
                'class' => 'SizeController',
            ],
            [
                'name' => 'codes',
                'label' => 'Control Discount Codes',
                'class' => 'DiscountController',
            ],
            [
                'name' => 'subscribers',
                'label' => 'Control Subscribers',
                'class' => 'SubscriberController',
            ],
            [
                'name' => 'social',
                'label' => 'Control Social Media',
                'class' => 'SocialController',
            ],
            [
                'name' => 'contact',
                'label' => 'Control Contact Us',
                'class' => 'ContactController',
            ],
        ]);
    }
}
