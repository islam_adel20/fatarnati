<?php

use Illuminate\Database\Seeder;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sizes')->insert([
            'title' => 'XL'
        ]);

        DB::table('sizes')->insert([
            'title' => 'L'
        ]);

        DB::table('sizes')->insert([
            'title' => 'M'
        ]);

        DB::table('sizes')->insert([
            'title' => 'S'
        ]);
    }
}
