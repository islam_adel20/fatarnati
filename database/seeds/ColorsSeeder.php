<?php

use Illuminate\Database\Seeder;

class ColorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colors')->insert([
            'title' => 'White',
            'color' => '#FFFFFF'
        ]);

        DB::table('colors')->insert([
            'title' => 'Black',
            'color' => '#000000'
        ]);

        DB::table('colors')->insert([
            'title' => 'Grey',
            'color' => '#808080'
        ]);

        DB::table('colors')->insert([
            'title' => 'Red',
            'color' => '#FF0000'
        ]);

        DB::table('colors')->insert([
            'title' => 'Blue',
            'color' => '#092ddd'
        ]);

        DB::table('colors')->insert([
            'title' => 'SkyBlue',
            'color' => '#9fd2fa'
        ]);

        DB::table('colors')->insert([
            'title' => 'DarkBlue',
            'color' => '#003762'
        ]);

        DB::table('colors')->insert([
            'title' => 'Green',
            'color' => '#05ac0c'
        ]);

        DB::table('colors')->insert([
            'title' => 'Brown',
            'color' => '#795548'
        ]);

        DB::table('colors')->insert([
            'title' => 'Orange',
            'color' => '#ff9800'
        ]);

        DB::table('colors')->insert([
            'title' => 'Gold',
            'color' => '#ffc107'
        ]);
    }
}
