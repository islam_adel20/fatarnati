<?php

use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contacts')->insert([
            'address_en' => '-',
            'address_ar' => '-',
            'working_hours' => '-',
            'phone' => '-',
            'email' => '-',
            'map' => '-',
        ]);
    }
}
