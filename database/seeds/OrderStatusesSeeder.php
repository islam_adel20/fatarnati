<?php

use Illuminate\Database\Seeder;

class OrderStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_statuses')->insert([
            ['title' => 'Pending', 'title_ar' => 'في إنتظار التأكيد'],
            ['title' => 'In Processing', 'title_ar' => 'يراجع'],
            ['title' => 'Shipping', 'title_ar' => 'في التوصيل'],
            ['title' => 'Shipped', 'title_ar' => 'تم توصيله'],
            ['title' => 'Completed', 'title_ar' => 'تم تسليمه'],
        ]);
    }
}
