<?php

use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->insert([
            'title' => 'Etiam imperdiet volutpat libero eu tristique.',
            'url' => '#'
        ]);

        DB::table('news')->insert([
            'title' => 'Curabitur porttitor ante eget hendrerit adipiscing.',
            'url' => '#'
        ]);

        DB::table('news')->insert([
            'title' => 'Praesent ornare nisl lorem, ut condimentum lectus gravida ut.',
            'url' => '#'
        ]);

        DB::table('news')->insert([
            'title' => 'Nunc ultrices tortor eu massa placerat posuere.',
            'url' => '#'
        ]);
        
    }
}
