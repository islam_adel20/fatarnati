<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            'title' => 'Egypt',
            'flag' => '/site/img/countries/eg.png',
            'currency' => 1,
            'active' => 1
        ]);

        DB::table('countries')->insert([
            'title' => 'KSA',
            'flag' => '/site/img/countries/sa.png',
            'currency' => 4,
            'active' => 0
        ]);

        DB::table('countries')->insert([
            'title' => 'UAE',
            'flag' => '/site/img/countries/ae.png',
            'currency' => 5,
            'active' => 0
        ]);

        DB::table('countries')->insert([
            'title' => 'USA',
            'flag' => '/site/img/countries/us.png',
            'currency' => 3,
            'active' => 0
        ]);

        DB::table('countries')->insert([
            'title' => 'France',
            'flag' => '/site/img/countries/fr.png',
            'currency' => 2,
            'active' => 0
        ]);

        DB::table('countries')->insert([
            'title' => 'Germany',
            'flag' => '/site/img/countries/de.png',
            'currency' => 2,
            'active' => 0
        ]);

        DB::table('countries')->insert([
            'title' => 'England',
            'flag' => '/site/img/countries/gb.png',
            'currency' => 2,
            'active' => 0
        ]);
    }
}
