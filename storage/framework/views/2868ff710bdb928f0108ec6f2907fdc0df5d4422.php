<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-box"></i></span>
                <h3 class="kt-portlet__head-title">Edit Product</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(url('admin/products/'.$product->id)); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				<input type="hidden" name="_method" value="PUT" />
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">	
							<label>Name (EN)</label>												
							<input class="form-control" type="text" placeholder="Name (EN)" name="title_en" value="<?php echo e($product->title_en); ?>" id="title_en" required />
							<?php if($errors->has('title_en')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('title_en')); ?></strong></span><?php endif; ?>
						</div>
						<div class="col-md-6">	
							<label>Name (AR)</label>												
							<input class="form-control" type="text" placeholder="Name (AR)" name="title_ar" value="<?php echo e($product->title_ar); ?>" id="title_ar" required />
							<?php if($errors->has('title_ar')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('title_ar')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">	
							<label>Category</label>												
							<select class="form-control" name="cat" id="cat" required>
								<?php $__currentLoopData = $cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<optgroup label="<?php echo e($cat->title_en); ?>">
										<?php $__currentLoopData = $cat->subs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subCat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>										
											<option value="<?php echo e($subCat->id); ?>" <?php if($subCat->id == $product->cat): ?> selected <?php endif; ?>><?php echo e($subCat->title_en); ?></option>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</optgroup>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
						</div>
						<div class="col-md-6">	
							<label>Stock Qty</label>												
							<input class="form-control" type="number" placeholder="Stock Qty" name="stock_qty" 
							value="<?php echo e($product->stock_qty); ?>" id="stock_qty" required />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">	
							<label>Price (EGP)</label>												
							<input class="form-control" type="text" placeholder="Price" name="price" value="<?php echo e($product->price); ?>" id="title" required />
							<?php if($errors->has('price')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('price')); ?></strong></span><?php endif; ?>
						</div>
						<div class="col-md-6">	
							<label>Discount (EGP)</label>												
							<input class="form-control" type="text" placeholder="Discount" name="discount" value="<?php echo e($product->discount); ?>" id="title" />
							<?php if($errors->has('discount')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('discount')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">	
							<label>Partener</label>												
							<select class="form-control" name="gallery_id" id="gallery">
								<option value="" disabled selected>Choose Partener</option>
								<?php $__currentLoopData = $galleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($gallery->id); ?>"
										<?php if($gallery->id == $product->gallery_id): ?> selected <?php endif; ?>
										><?php echo e($gallery->name_en); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">	
							<label>Description (EN)</label>												
							<textarea class="ckeditor" placeholder="Description (EN)" name="text_en" id="text_en" required><?php echo e($product->text_en); ?></textarea>
							<?php if($errors->has('text_en')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('text_en')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">	
							<label>Description (AR)</label>												
							<textarea class="ckeditor" placeholder="Description (AR)" name="text_ar" id="text_ar" required><?php echo e($product->text_ar); ?></textarea>
							<?php if($errors->has('text_ar')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('text_ar')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>

				<hr>

				<div class="form-group">
					<div class="row">
						<div class="col-md-12">	
							<label><b>Sizes</b></label>												
							<button class="btn btn-success float-right" id="add-size">
								<i class="fa fa-plus"></i> Add Size
							</button>

							<table class="table table-bordered mt-5" id="sizes">
								<thead>
									<tr>
										<th>Size</th>
										<th>Price</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									<?php $__empty_1 = true; $__currentLoopData = $product->sizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productSize): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
										<tr class="size-box">
											<td>
												<select class="form-control" name="sizes[]" required>
													<?php $__currentLoopData = $sizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
														<option value="<?php echo e($size->id); ?>"
															<?php if($size->id == $productSize->size): ?> selected <?php endif; ?>
															><?php echo e($size->title_en); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</select>
											</td>
											<td>
												<input type="number" class="form-control" name="prices[]" min="0" value="<?php echo e($productSize->price); ?>" required>
											</td>
											<td>
												<button class="btn btn-danger remove-size" data-size-id="<?php echo e($productSize->id); ?>">
													<i class="fa fa-times"></i> Delete
												</button>
											</td>
										</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
										<tr class="sizes-placeholder">
											<td	td colspan="3">No sizes added yet</td>
										</tr>
									<?php endif; ?>
								</tbody>
							</table>

							<input type="hidden" name="deleted_sizes">
						</div>
					</div>
				</div>
				
				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>					
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
	<script>
		$(document).ready(function(){
			$('#add-size').click(function(e){
				e.preventDefault();

				let sizeBox = `
					<tr class="size-box">
						<td>
							<select class="form-control" name="sizes[]" required>
								<option value="" disabled selected>Choose Size</option>
								<?php $__currentLoopData = $sizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($size->id); ?>"><?php echo e($size->title_en); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
						</td>
						<td>
							<input type="number" class="form-control" name="prices[]" min="0" required>
						</td>
						<td>
							<button class="btn btn-danger remove-size">
								<i class="fa fa-times"></i> Delete
							</button>
						</td>
					</tr>
				`;

				// first row !
				if ($('.sizes-placeholder').length > 0) {
					$('#sizes tbody').html('');	
				}

				// add the box
				$('#sizes tbody').append(sizeBox);

				// scroll to bottom of the page (glitch)
				$("html, body").animate({ scrollTop: $(document).height() }, 1000);
			});
			
			$(document).on('click', '.remove-size', function(e){
				e.preventDefault();

				// add size id to deleted array
				if ($(this).data('size-id')) {
					let val = $('input[name="deleted_sizes"]').val();
					$('input[name="deleted_sizes"]').val(val + ',' + $(this).data('size-id'));
				}

				// add the box
				$(this).closest('td').closest('tr').remove();

				// if no rows !
				if ($('.size-box').length == 0) {
					$('#sizes tbody').append(`
						<tr class="sizes-placeholder">
							<td	td colspan="3">No sizes added yet</td>
						</tr>
					`);
				}
			});
		});
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>