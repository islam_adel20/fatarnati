<?php $__env->startSection('content'); ?>
<!-- Category Products Area
============================================ -->
<section class="product-page-area ptb-50" style="direction: ltr">
    <div class="container text-center" style="padding: 100px 0px;">
        <h1 id="page_title" style="padding: 50px 0px;"><?php echo e(__('global.my_orders')); ?></h1>

        <div class="row" <?php if(!App::isLocale('en')): ?> style="direction: rtl;" <?php endif; ?>>
            <div class="col-md-3">
                <?php echo $__env->make('profile_side', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <div class="col-md-9">
                <div class="table-responsive">
                    <?php if(count($orders)): ?>
                        <table class="table table-bordered table-striped table-responsive" id="shopping_cart_table">
                            <thead>
                                <tr class="table-warning">
                                    <td>#</td>
                                    <td><?php echo e(__('global.status')); ?></td>
                                    <td><?php echo e(__('global.pay_method')); ?></td>
                                    <td><?php echo e(__('global.ordered_at')); ?></td>
                                    <td><?php echo e(__('global.details')); ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e(__('global.order')); ?> #<?php echo e($order->id); ?></td>
                                        <td>
                                            <?php if(App::getLocale() == 'en'): ?>
                                                <?php echo e($order->status_info->title); ?>

                                            <?php else: ?>
                                                <?php echo e($order->status_info->title_ar); ?>

                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if(App::getLocale() == 'en'): ?>
                                                <?php echo e($order->pay_info->title_en); ?>

                                            <?php else: ?>
                                                <?php echo e($order->pay_info->title_ar); ?>

                                            <?php endif; ?>
                                        </td>
                                        <td><?php echo e(date('Y-m-d', strtotime($order->created_at))); ?></td>
                                        <td><a href="<?php echo e(url('order_details/'.$order->id)); ?>" class="btn btn-warning"><?php echo e(__('global.details')); ?></a></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    <?php else: ?>
                        <p><?php echo e(__('global.no_orders')); ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>