<?php $__env->startSection('content'); ?>
<!-- Category Products Area
============================================ -->
<!-- Category Products Area
============================================ -->
<div class="product-page-area page-area">
	<div class="container text-center" style="padding: 150px 0px;">
		<h1 style="padding: 50px; 0px;"><?php echo e(__('global.shopping_cart')); ?></h1>
			<?php if(Cart::content()->count() > 0): ?>
				<form action="<?php echo e(url('update_cart')); ?>" method="post" class="table-responsive" id="cart-form">
					<?php echo e(csrf_field()); ?>

					<table class="table" id="shopping_cart_table">
						<thead>
							<tr>
								<th class="text-center"><?php echo e(__('global.product')); ?></th>
								<th class="text-center"><?php echo e(__('global.sizes')); ?></th>
								<th class="text-center"><?php echo e(__('global.price')); ?></th>
								<th class="text-center"><?php echo e(__('global.qty')); ?></th>
								<th class="text-center"><?php echo e(__('global.total')); ?></th>
								<th class="text-center"><?php echo e(__('global.delete')); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php $__currentLoopData = Cart::content(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td class="td-product">
									<a href="<?php echo e(url('Product/'.$item->id)); ?>">
										<img alt="<?php echo e($item->name); ?>" src="<?php echo e(asset($item->options->image)); ?>" style="width:150px;height:150px;">
										<span>
											<?php if(App::getLocale() == 'en'): ?>
												<?php echo e($item->name); ?>

											<?php else: ?>
												<?php echo e($item->options->name_ar); ?>

											<?php endif; ?>
										</span>
									</a>
								</td>
								<td>
									<?php if($item->options->size != 0): ?>
										<?php if(App::getLocale() == 'en'): ?>
											<?php echo e(getSize($item->options->size)->title_en); ?>

										<?php else: ?>
											<?php echo e(getSize($item->options->size)->title_ar); ?>

										<?php endif; ?>
									<?php endif; ?>
								</td>
								<td><?php echo e($item->price); ?>  EGP</td>
								<td>
									<input type="number" value="<?php echo e($item->qty); ?>" name="qty[]" class="form-control" />
									<input type="hidden" value="<?php echo e($item->rowId); ?>" name="product[]" />
									<input type="hidden" value="<?php echo e($item->options->product); ?>" name="product_id[]" />
								</td>
								<td><?php echo e($item->price * $item->qty); ?>  EGP</td>
								<td><a href="<?php echo e(url('delete_item/'.$item->rowId)); ?>" class="btn btn-danger" title="Remove"><i class="fa fa-trash"></i></a></td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<tr>
							<td colspan="3"></td>
							<td colspan="2" class="text-right"> <?php echo e(__('global.total')); ?>: <?php echo e(Cart::subtotal()); ?> EGP</td>
						</tr>
						</tbody>
					</table>
				</form>
				<div class="row">
					<div class="col-12 text-right cart-mobile-button">
						<button class="button" type="submit" onclick="$('#cart-form').submit()"><?php echo e(__('global.update')); ?></button>
						<button class="button" type="button" 
							onclick="window.location.href = '<?php echo e(url("Checkout")); ?>';">
							<span class="fa fa-check"></span> <?php echo e(__('global.checkout')); ?>

						</button>
					</div>
				</div>
			<?php else: ?>
				<section class="ptb-50">
					<h3 class="text-center"><?php echo e(__('global.sorry_no_items')); ?></h3>
				</section>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>