<?php $__env->startSection('content'); ?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fa fa-star"></i></span>
                <h3 class="kt-portlet__head-title">التجار</h3>
            </div>
        </div>
        <div class="kt-portlet__body text-center">
			<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
				<thead>
					<tr>
						<th>#</th>
						<th>الأسم (EN)</th>
						<th>الأسم (AR)</th>
						<th>الموبايل</th>
						<th>الاعدادات</th>
					</tr>
				</thead>
				<tbody>
					<?php $__currentLoopData = $galleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr>
							<td><?php echo e($loop->iteration); ?></td>
							<td><?php echo e($gallery->name_en); ?></td>
							<td><?php echo e($gallery->name_ar); ?></td>
							<td><?php echo e($gallery->phone); ?></td>
							<td>
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">
										الأعدادات
									</button>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="<?php echo e(route('galleries.edit', $gallery->id)); ?>"><i class="fas fa-edit"></i> تعديل</a>
										<a class="dropdown-item" data-toggle="modal" href="#myModal-<?php echo e($gallery->id); ?>"><i class="fas fa-trash"></i> حذف</a>
									</div>
									<button class="btn btn-warning" type="button">
										<a href="<?php echo e(url('admin/gallery_products/' . $gallery->id)); ?>" style="color: black;">
											<i class="fas fa-boxes"></i> المنتجات
										</a>
									</button>
									<button class="btn btn-success" type="button">
										<a href="<?php echo e(url('admin/gallery_orders/' . $gallery->id)); ?>" style="color: white;">
											<i class="fas fa-shopping-bag"></i> الطلبات
										</a>
									</button>

                  <button class="btn btn-success" type="button">
										<a href="<?php echo e(url('admin/gallery_products_favorite/' . $gallery->id)); ?>" style="color: white;">
											<i class="fas fa-shopping-bag"></i> المفضلة
										</a>
									</button>

                  <!-- Button trigger modal -->
                  <button class="btn btn-success" data-toggle="modal" data-target="#exampleModalsub<?php echo e($gallery->id); ?>">
                    تفاصيل الأشتراك
                  </button>

                  <!-- Modal -->
                  <div class="modal fade" id="exampleModalsub<?php echo e($gallery->id); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-body">
                            <?php if($gallery->code != null): ?>
                            <h4 class="alert alert-success"> الكود :  <?php echo e($gallery->code); ?></h4>
                            <h4 class="alert alert-success"> حالة الأشتراك :
                              <?php $__currentLoopData = App\Models\SallerCode::where('code', $gallery->code)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $code): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($code->status == 'expired'): ?>
                                  جاري -

                                    [ <?php echo e($code->from); ?> / <?php echo e($code->to); ?> ]
                                <?php elseif($code->status == 'finish'): ?>
                                  منتهي
                                <?php endif; ?>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </h4>
                            <?php else: ?>
                            <h4 class="alert alert-danger text-center"> لا يوجد أشتراك حاليا</h4>
                            <?php endif; ?>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">الغاء</button>
                        </div>
                      </div>
                    </div>
								</div>

								<div class="modal fade" id="myModal-<?php echo e($gallery->id); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
								<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">حذف التاجر</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
								<form role="form" action="<?php echo e(url('admin/galleries/'.$gallery->id)); ?>" class="" method="POST">
								<input name="_method" type="hidden" value="DELETE">
								<?php echo e(csrf_field()); ?>

								<p>هل انت متأكد من الحذف ؟ </p>
								<button type="submit" class="btn btn-danger" name='delete_modal'>الحذف</button>
								<button type="button" class="btn btn-success" data-dismiss="modal">الغاء</button>
								</form>
								</div>
								</div>
								</div>
								</div>
							</td>
						</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</tbody>
			</table>
    </div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>