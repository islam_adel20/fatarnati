<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-user-edit"></i></span>
                <h3 class="kt-portlet__head-title">Edit Admin</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(url('admin/admins/'.$admin->id)); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				<?php if($errors->any()): ?>
					<div class="alert alert-danger">
						<ul>
							<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<li><?php echo e($error); ?></li>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</ul>
					</div>
				<?php endif; ?>
				<input type="hidden" name="_method" value="PUT" />
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">	
							<label>Name</label>												
							<input class="form-control" type="text" placeholder="Name" name="name" value="<?php echo e($admin->name); ?>" id="name" />
						</div>
						<div class="col-md-6">	
							<label>User Name</label>												
							<input class="form-control" type="text" placeholder="User Name" name="user_name" value="<?php echo e($admin->user_name); ?>" id="user_name" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">	
							<label>E-mail</label>	
							<input class="form-control" type="email"  placeholder="E-mail" value="<?php echo e($admin->email); ?>" id="email" name="email" />
						</div>				
						<div class="col-md-6">	
							<label>Phone</label>										
							<input class="form-control" type="text"  placeholder="Phone" name="phone" value="<?php echo e($admin->phone); ?>" id="phone" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">	
							<label>Image</label>	
							<div class="custom-file">
								<input type="file" class="custom-file-input" id="image" name="image">
								<label class="custom-file-label" for="image">Choose file</label>
							</div>
						</div>
						<div class="col-md-2"><?php if($admin->image != ''): ?><img src="<?php echo e(asset($admin->image)); ?>" /><?php endif; ?></div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">	
							<label>Permissions</label>
							<div class="row text-left">
								<?php $__currentLoopData = $permissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $permission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<div class="col-md-4 p-3">
										<p><input type="checkbox" name="permissions[]" value="<?php echo e($permission->id); ?>" class="permissions" 
											<?php if(in_array($permission->id, $admin->permissions->pluck('id')->toArray())): ?> checked <?php endif; ?>/> <?php echo e($permission->label); ?></p>
									</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</div>											
						</div>
					</div>
				</div>

				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>					
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>