<?php $__env->startSection('content'); ?>
    <!--success area start-->
    <div class="row add-success" onclick="$(this).hide();">
        <div class="col-md-6 offset-md-3 col-12">
            <div class="alert alert-success text-center">
                <b id="add-success-message"></b>
            </div>
        </div>
    </div> 
    <!--success area end-->   

    <!--slider area start-->
    <section class="slider_section slider_height container_width color_six">
        <div class="slider_area owl-carousel">
            <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="single_slider d-flex align-items-center">
                    <img src="<?php echo e(asset($slider->image)); ?>" />
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </section>
    <!--slider area end-->

    <!--categories area start-->
    <div class="custom_product_area banner_area home3_bg_area">
        <div class="container">
            <div class="categories_container owl-carousel">
                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="single_brand">
                        <div class="product_thumb">
                            <a class="primary_img" href="<?php echo e(url('/Products/' . $category->id)); ?>"><img
                                style="height: 200px;width: 300px;" src="<?php echo e(asset($category->image)); ?>" alt=""></a>
                            <h4 class="product_name text-center mt-5">
                                <a href="<?php echo e(url('/Products/' . $category->id)); ?>">
                                    <?php if(App::getLocale() == 'en'): ?>
                                        <?php echo e($category->title_en); ?>

                                    <?php else: ?>
                                        <?php echo e($category->title_ar); ?>

                                    <?php endif; ?>
                                </a>
                            </h4>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <div class="row">
                <div class="col-12 categories-button-container">
                    <a href="<?php echo e(url('/Categories')); ?>">
                        <button class="categories-button text-center"><?php echo e(__('global.all_cats')); ?></button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--categories area end-->

    <!--new products area start-->
    <div class="product_area  container_width color_six mb-65">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title section_title6">
                        <h2><?php echo e(__('global.latest_products')); ?></h2>
                    </div>
                </div>
            </div>
            <div class="product_container">
                <div class="row">
                    <div class="col-12">
                        <div class="product_carousel product_column5 owl-carousel">
                            <?php $__currentLoopData = $newProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="product_items">
                                    <article class="single_product">
                                        <figure>
                                            <div class="product_thumb">
                                                <a class="primary_img" href="<?php echo e(url('Product/' . $product->id)); ?>">
                                                    <img src="<?php echo e(asset($product->images[0]->image)); ?>"
                                                        alt="<?php echo e($product->title_en); ?>">
                                                </a>
                                                <div class="label_product">
                                                    <?php if($product->discount > 0): ?>
                                                        <span class="label_sale">Sale</span>
                                                    <?php endif; ?>
                                                    <span class="label_new">New</span>
                                                </div>
                                                <div class="action_links">
                                                    <ul>
                                                        <li class="add_to_cart add_to_cart_btn" data-num="<?php echo e($product->id); ?>"><a href="javascript:;"
                                                                data-tippy="<?php echo e(__('global.add_to_cart')); ?>" data-tippy-placement="top"
                                                                data-tippy-arrow="true" data-tippy-inertia="true"> <span
                                                                    class="lnr lnr-cart"></span></a></li>
                                                        <li class="quick_button"><a
                                                                href="<?php echo e(url('Product/' . $product->id)); ?>"
                                                                data-tippy="<?php echo e(__('global.show')); ?>" data-tippy-placement="top"
                                                                data-tippy-arrow="true" data-tippy-inertia="true"> <span
                                                                    class="lnr lnr-magnifier"></span></a></li>
                                                        <!--<li class="wishlist"><a href="#" data-tippy="أضف لقائمة المفضلة"-->
                                                        <!--        data-tippy-placement="top" data-tippy-arrow="true"-->
                                                        <!--        data-tippy-inertia="true"><span-->
                                                        <!--            class="lnr lnr-heart"></span></a></li>-->
                                                    </ul>
                                                </div>
                                            </div>
                                            <figcaption class="product_content">
                                                <h4 class="product_name">
                                                    <a href="<?php echo e(url('Product/' . $product->id)); ?>">
                                                        <?php if(App::getLocale() == 'en'): ?>
                                                            <?php echo e($product->title_en); ?>

                                                        <?php else: ?>
                                                            <?php echo e($product->title_ar); ?>

                                                        <?php endif; ?>
                                                    </a>
                                                </h4>
                                                <?php if(!empty($product->category)): ?>
                                                    <p>
                                                        <a href="<?php echo e(url('Products/' . $product->category->id)); ?>">
                                                            <?php if(App::getLocale() == 'en'): ?>
                                                                <?php echo e($product->category->title_en); ?>

                                                            <?php else: ?>
                                                                <?php echo e($product->category->title_ar); ?>

                                                            <?php endif; ?>
                                                        </a>
                                                    </p>
                                                <?php else: ?>
                                                    <p style="height: 12px;"></p>
                                                <?php endif; ?>
                                                <div class="price_box">
                                                    <?php if($product->discount > 0): ?>
                                                        <span
                                                            class="current_price"><?php echo e($product->price - $product->discount); ?>

                                                            EGP</span>
                                                        <span class="old_price"><?php echo e($product->price); ?> EGP</span>
                                                    <?php else: ?>
                                                        <span class="current_price"><?php echo e($product->price); ?> EGP</span>
                                                    <?php endif; ?>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </article>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--new products area end-->

    <!--weekly offer products area start-->
    <div class="product_area  container_width color_six mb-65 home3_bg_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title section_title6">
                        <h2><?php echo e(__('global.weekly_offer')); ?></h2>
                    </div>
                </div>
            </div>
            <div class="product_container">
                <div class="row">
                    <div class="col-12">
                        <div class="product_carousel product_column5 owl-carousel">
                            <?php $__currentLoopData = $weeklyOfferProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="product_items">
                                    <article class="single_product">
                                        <figure>
                                            <div class="product_thumb">
                                                <a class="primary_img" href="<?php echo e(url('Product/' . $product->id)); ?>">
                                                    <img src="<?php echo e(asset($product->images[0]->image)); ?>"
                                                        alt="<?php echo e($product->title_en); ?>">
                                                </a>
                                                <div class="label_product">
                                                    <?php if($product->discount > 0): ?>
                                                        <span class="label_sale">Sale</span>
                                                    <?php endif; ?>
                                                    
                                                </div>
                                                <div class="action_links">
                                                    <ul>
                                                        <li class="add_to_cart add_to_cart_btn" data-num="<?php echo e($product->id); ?>"><a href="javascript:;"
                                                                data-tippy="<?php echo e(__('global.add_to_cart')); ?>" data-tippy-placement="top"
                                                                data-tippy-arrow="true" data-tippy-inertia="true"> <span
                                                                    class="lnr lnr-cart"></span></a></li>
                                                        <li class="quick_button"><a
                                                                href="<?php echo e(url('Product/' . $product->id)); ?>"
                                                                data-tippy="<?php echo e(__('global.show')); ?>" data-tippy-placement="top"
                                                                data-tippy-arrow="true" data-tippy-inertia="true"> <span
                                                                    class="lnr lnr-magnifier"></span></a></li>
                                                        <!--<li class="wishlist"><a href="#" data-tippy="أضف لقائمة المفضلة"-->
                                                        <!--        data-tippy-placement="top" data-tippy-arrow="true"-->
                                                        <!--        data-tippy-inertia="true"><span-->
                                                        <!--            class="lnr lnr-heart"></span></a></li>-->
                                                    </ul>
                                                </div>
                                            </div>
                                            <figcaption class="product_content">
                                                <h4 class="product_name">
                                                    <a href="<?php echo e(url('Product/' . $product->id)); ?>">
                                                        <?php if(App::getLocale() == 'en'): ?>
                                                            <?php echo e($product->title_en); ?>

                                                        <?php else: ?>
                                                            <?php echo e($product->title_ar); ?>

                                                        <?php endif; ?>
                                                    </a>
                                                </h4>
                                                <?php if(!empty($product->category)): ?>
                                                    <p>
                                                        <a href="<?php echo e(url('Products/' . $product->category->id)); ?>">
                                                            <?php if(App::getLocale() == 'en'): ?>
                                                                <?php echo e($product->category->title_en); ?>

                                                            <?php else: ?>
                                                                <?php echo e($product->category->title_ar); ?>

                                                            <?php endif; ?>
                                                        </a>
                                                    </p>
                                                <?php else: ?>
                                                    <p style="height: 12px;"></p>
                                                <?php endif; ?>
                                                <div class="price_box">
                                                    <?php if($product->discount > 0): ?>
                                                        <span
                                                            class="current_price"><?php echo e($product->price - $product->discount); ?>

                                                            EGP</span>
                                                        <span class="old_price"><?php echo e($product->price); ?> EGP</span>
                                                    <?php else: ?>
                                                        <span class="current_price"><?php echo e($product->price); ?> EGP</span>
                                                    <?php endif; ?>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </article>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--weekly offer products area end-->

    <!--best selling products area start-->
    <div class="product_area  container_width color_six mb-65">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title section_title6">
                        <h2><?php echo e(__('global.best_selling')); ?></h2>
                    </div>
                </div>
            </div>
            <div class="product_container">
                <div class="row">
                    <div class="col-12">
                        <div class="product_carousel product_column5 owl-carousel">
                            <?php $__currentLoopData = $bestSellingProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="product_items">
                                    <article class="single_product">
                                        <figure>
                                            <div class="product_thumb">
                                                <a class="primary_img" href="<?php echo e(url('Product/' . $product->id)); ?>">
                                                    <img src="<?php echo e(asset($product->images[0]->image)); ?>"
                                                        alt="<?php echo e($product->title_en); ?>">
                                                </a>
                                                <div class="label_product">
                                                    <?php if($product->discount > 0): ?>
                                                        <span class="label_sale">Sale</span>
                                                    <?php endif; ?>
                                                    
                                                </div>
                                                <div class="action_links">
                                                    <ul>
                                                        <li class="add_to_cart add_to_cart_btn" data-num="<?php echo e($product->id); ?>"><a href="javascript:;"
                                                                data-tippy="<?php echo e(__('global.add_to_cart')); ?>" data-tippy-placement="top"
                                                                data-tippy-arrow="true" data-tippy-inertia="true"> <span
                                                                    class="lnr lnr-cart"></span></a></li>
                                                        <li class="quick_button"><a
                                                                href="<?php echo e(url('Product/' . $product->id)); ?>"
                                                                data-tippy="<?php echo e(__('global.show')); ?>" data-tippy-placement="top"
                                                                data-tippy-arrow="true" data-tippy-inertia="true"> <span
                                                                    class="lnr lnr-magnifier"></span></a></li>
                                                        <!--<li class="wishlist"><a href="#" data-tippy="أضف لقائمة المفضلة"-->
                                                        <!--        data-tippy-placement="top" data-tippy-arrow="true"-->
                                                        <!--        data-tippy-inertia="true"><span-->
                                                        <!--            class="lnr lnr-heart"></span></a></li>-->
                                                    </ul>
                                                </div>
                                            </div>
                                            <figcaption class="product_content">
                                                <h4 class="product_name">
                                                    <a href="<?php echo e(url('Product/' . $product->id)); ?>">
                                                        <?php if(App::getLocale() == 'en'): ?>
                                                            <?php echo e($product->title_en); ?>

                                                        <?php else: ?>
                                                            <?php echo e($product->title_ar); ?>

                                                        <?php endif; ?>
                                                    </a>
                                                </h4>
                                                <?php if(!empty($product->category)): ?>
                                                    <p>
                                                        <a href="<?php echo e(url('Products/' . $product->category->id)); ?>">
                                                            <?php if(App::getLocale() == 'en'): ?>
                                                                <?php echo e($product->category->title_en); ?>

                                                            <?php else: ?>
                                                                <?php echo e($product->category->title_ar); ?>

                                                            <?php endif; ?>
                                                        </a>
                                                    </p>
                                                <?php else: ?>
                                                    <p style="height: 12px;"></p>
                                                <?php endif; ?>
                                                <div class="price_box">
                                                    <?php if($product->discount > 0): ?>
                                                        <span
                                                            class="current_price"><?php echo e($product->price - $product->discount); ?>

                                                            EGP</span>
                                                        <span class="old_price"><?php echo e($product->price); ?> EGP</span>
                                                    <?php else: ?>
                                                        <span class="current_price"><?php echo e($product->price); ?> EGP</span>
                                                    <?php endif; ?>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </article>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--best selling products area end-->

    <!--brand area start-->
    <div class="brand_area home3_bg_area mb-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title section_title6">
                        <h2><?php echo e(__('global.parteners')); ?></h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="brand_container owl-carousel">
                        <?php $__currentLoopData = $homeGalleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="single_brand">
                                <a href="<?php echo e(url('/Gallery/' . $gallery->id)); ?>"><img src="<?php echo e(asset($gallery->image)); ?>" alt=""></a>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--brand area end-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>