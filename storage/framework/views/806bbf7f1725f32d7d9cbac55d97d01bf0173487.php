<?php $__env->startSection('content'); ?>
<!-- Category Products Area
============================================ -->
<!-- Category Products Area
============================================ -->
<section class="ptb-50" style="padding-bottom: 50px;">
    <div class="container text-center">
        <div id="change_information">
            <h1  id="page_title" style="padding: 100px 0px;"><?php echo e($address->name); ?></h1>
            <div class="row">
                <div class="row">
                    <div class="col-md-9">
                        <form class="form-horizontal box_shadowed_form"  role="form" method="POST" action="<?php echo e(url('/update-address/'.$address->id)); ?>" id="ajform">
                            <?php if(session()->has('success')): ?>
                                <div class="alert alert-success"><?php echo e(session('success')); ?></div>
                            <?php endif; ?>
                            
                            <?php if($errors->any()): ?>
                                <div class="alert alert-danger"><?php echo e($errors->first()); ?></div>
                            <?php endif; ?>
                            
                            <div id="ajform_res"></div>
                            <?php echo e(csrf_field()); ?>

    
                            <div class="form-group">
                                <label for="name" class="control-label"><?php echo e(__('global.name')); ?></label>
                                <input id="name" type="text" class="form-control" name="name" value="<?php echo e($address->name); ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="address" class="control-label"><?php echo e(__('global.address')); ?></label>
                                <input id="address" type="text" class="form-control" name="address" value="<?php echo e($address->address); ?>" required>
                            </div>
                            <div class="form-group my-3">
                                <button type="submit" class="button"><?php echo e(__('global.save')); ?></button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3" <?php if(!App::isLocale('en')): ?> style="direction: rtl;" <?php endif; ?>>
                        <?php echo $__env->make('profile_side', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>