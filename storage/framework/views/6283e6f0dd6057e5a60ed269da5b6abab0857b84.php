<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-list"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; تعديل</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(url('admin/saller/servcies/category/'.$category->id)); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				<input type="hidden" name="_method" value="PUT" />
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الأسم (EN)</label>
							<input class="form-control" type="text" placeholder="الأسم (EN)" name="name_en" value="<?php echo e($category->name_en); ?>" id="name_en" />
							<?php if($errors->has('name_en')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('name_en')); ?></strong></span><?php endif; ?>
						</div>
						<div class="col-md-6">
							<label>الأسم (AR)</label>
							<input class="form-control" type="text" placeholder="الأسم (AR)" name="name_ar" value="<?php echo e($category->name_ar); ?>" id="name_ar" />
							<?php if($errors->has('name_ar')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('name_ar')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>

				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>