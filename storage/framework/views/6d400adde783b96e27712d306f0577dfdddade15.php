<?php $__env->startSection('content'); ?>
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area" style="background-image: url('<?php echo e(asset($headerImage->image)); ?>');">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <h3><?php echo e(__('global.privacy')); ?></h3>
                        <ul>
                            <li><a href="<?php echo e(url('/')); ?>"><?php echo e(__('global.home')); ?></a></li>
                            <li><?php echo e(__('global.privacy')); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!--about section area -->
    <section class="about_section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <figure>
                        <figcaption class="about_content">
                            <article>
                                <?php if(App::getLocale() == 'en'): ?>
                                    <?php echo $section->text_en; ?>

                                <?php else: ?>
                                    <?php echo $section->text_ar; ?>

                                <?php endif; ?>
                            </article>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>
    </section>
    <!--about section end-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>