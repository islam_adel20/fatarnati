<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-tags"></i></span>
                <h3 class="kt-portlet__head-title">Add Discount Code</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(url('admin/discount_code')); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				<div class="form-group">
					<div class="row">
						<div class="col-md-12">	
							<label>Name</label>												
							<input class="form-control" type="text" placeholder="Name" name="name" value="<?php echo e(old('name')); ?>" id="name" />
							<?php if($errors->has('name')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('name')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">	
							<label>Cost</label>												
							<input class="form-control" type="text" placeholder="Cost" name="cost" value="<?php echo e(old('cost')); ?>" id="cost" />
							<?php if($errors->has('cost')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('cost')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">	
							<label>Start At</label>												
							<input class="form-control" type="date" placeholder="Start At" name="start_at" value="<?php echo e(old('start_at')); ?>" id="start_at" />
							<?php if($errors->has('start_at')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('start_at')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">	
							<label>Expire At</label>												
							<input class="form-control" type="date" placeholder="Expire At" name="expire_at" value="<?php echo e(old('expire_at')); ?>" id="expire_at" />
							<?php if($errors->has('expire_at')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('expire_at')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>					
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>