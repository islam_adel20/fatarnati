<!--offcanvas menu area start-->
<div class="off_canvars_overlay">
    
</div>
<div class="offcanvas_menu offcanvas6">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="offcanvas_menu_wrapper">
                    <div class="canvas_close">
                        <a href="javascript:void(0)"><i class="icon-x"></i></a>
                    </div>
                    <div class="search_container offcanvas__search">
                        <div class="search_box">
                            <form action="<?php echo e(route('search')); ?>">
                                <input placeholder="<?php echo e(__('global.enter_keyword')); ?>" type="text" name="keyword">
                                <button type="submit"><span class="lnr lnr-magnifier"></span></button>
                            </form>
                        </div>
                    </div>
                    <div id="menu" class="text-left ">
                        <ul class="offcanvas_main_menu">
                            <li class="menu-item-has-children active">
                                <a href="<?php echo e(url('/')); ?>"><?php echo e(__('global.home')); ?></a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="<?php echo e(url('/About')); ?>"><?php echo e(__('global.about_asask')); ?></a>
                            </li>

                            <?php $__currentLoopData = get_maincats(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li class="menu-item-has-children">
                                    <a href="<?php echo e(url('Products/' . $mcat->id)); ?>">
                                        <?php if(App::getLocale() == 'en'): ?>
                                            <?php echo e($mcat->title_en); ?>

                                        <?php else: ?>
                                            <?php echo e($mcat->title_ar); ?>

                                        <?php endif; ?>
                                    </a>
                                    <?php if($mcat->subs->count()): ?>
                                        <ul class="sub-menu">
                                            <?php $__currentLoopData = $mcat->subs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li>
                                                    <a href="<?php echo e(url('Products/' . $category->id)); ?>">
                                                        <?php if(App::getLocale() == 'en'): ?>
                                                            <?php echo e($category->title_en); ?>

                                                        <?php else: ?>
                                                            <?php echo e($category->title_ar); ?>

                                                        <?php endif; ?>
                                                    </a>
                                                </li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    <?php endif; ?>
                                </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <li class="menu-item-has-children active">
                                <a href="<?php echo e(url('/Galleries')); ?>"><?php echo e(__('global.parteners')); ?></a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="<?php echo e(url('/Contact')); ?>"><?php echo e(__('global.call_us')); ?></a>
                            </li>

                            <?php if(Auth::guard('user')->user()): ?>
                                <li class="menu-item-has-children active">
                                    <a href="<?php echo e(url('/Profile')); ?>"><?php echo e(__('global.my_account')); ?></a>
                                </li>
                                <li class="menu-item-has-children active">
                                    <a href="<?php echo e(url('/logout')); ?>"><?php echo e(__('global.logout')); ?></a>
                                </li>
                            <?php else: ?>
                                <li class="menu-item-has-children active">
                                    <a href="<?php echo e(url('/register')); ?>"><?php echo e(__('global.register')); ?></a>
                                </li>
                                <li class="menu-item-has-children active">
                                    <a href="<?php echo e(url('/login')); ?>"><?php echo e(__('global.login')); ?></a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <div class="offcanvas_footer">
                        <span><a href="#"><i class="fa fa-envelope-o"></i><?php echo e($contact_info->email); ?></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--offcanvas menu area end-->
