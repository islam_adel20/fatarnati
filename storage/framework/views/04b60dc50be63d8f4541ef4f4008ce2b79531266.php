<?php $__env->startSection('content'); ?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-tags"></i></span>
                <h3 class="kt-portlet__head-title">الأكواد</h3>
            </div>
        </div>
        <div class="kt-portlet__body text-center">

            <!--begin::Section-->
            <div class="kt-section">

                <div class="kt-section__content">
                    <div class="table-responsive">
                      <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>الكود</th>
                                    <th>حالة الكود</th>
                                    <th>المدة الزمنية</th>
                                    <th>بداية التفعيل</th>
                                    <th>انتهاء التفعيل</th>
                                    <th>التكلفة</th>
                                    <th>الأعدادات</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $codes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $code): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <th scope="row"><?php echo e($loop->iteration); ?></th>
                                        <td><?php echo e($code->code); ?></td>
                                        <td>
                                          <?php if($code->status == 'expired'): ?>
                                            <span style="color:green">مفعل </span>
                                          <?php elseif($code->status == 'unactive'): ?>
                                            <span style="color:red">غير مفعل </span>
                                          <?php elseif($code->status == 'finish'): ?>
                                            <span style="color:red"> انتهاء التفعيل </span>
                                          <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if($code->status == 'unactive'): ?>
                                                <?php echo e($code->duration); ?> يوم
                                            <?php else: ?>
                                                <?php if($code->duration - date_diff(new \DateTime($code->from), new \DateTime())->format("%d") <= 0): ?>
                                                    0 يوم
                                                <?php else: ?>
                                                    <?php echo e($code->duration - date_diff(new \DateTime($code->from), new \DateTime())->format("%d")); ?> يوم
                                                <?php endif; ?>
                                            <?php endif; ?>

                                          </td>

                                        <td>
                                          <?php if($code->from == null && $code->status == 'unactive'): ?>
                                            <span style="color:red"> لم يبدأ</span>
                                          <?php else: ?>
                                            <?php echo e($code->from); ?>

                                          <?php endif; ?>
                                        </td>
                                        <td>
                                          <?php if($code->to == null && $code->status == 'unactive'): ?>
                                            <span style="color:red"> لم يبدأ</span>
                                          <?php else: ?>
                                            <?php echo e($code->to); ?>

                                          <?php endif; ?>
                                        </td>

                                        <td> <?php echo e($code->cost); ?></td>
                                        <td>
                                            <button class="btn btn-brand dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="true">الأعدادات</button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="<?php echo e(route('codes.edit', $code->id)); ?>"><i class="fa fa-edit"></i> تعديل</a>
                                                <a class="dropdown-item" data-toggle="modal" href="#myModal-<?php echo e($code->id); ?>"><i class="fa fa-trash"></i> حذف</a>
                                            </div>
                                                <div class="modal fade" id="myModal-<?php echo e($code->id); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">حذف الكود</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                <form role="form" action="<?php echo e(url('/admin/saller/codes/'.$code->id)); ?>" class="" method="POST">
                                                <input name="_method" type="hidden" value="DELETE">
                                                <?php echo e(csrf_field()); ?>

                                                <p>هل أنت متأكد من الحذف ؟</p>
                                                <button type="submit" class="btn btn-danger" name='delete_modal'><i class="fa fa-trash" aria-hidden="true"></i>حذف</button>
                                                </form>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                                            </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!--end::Section-->
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>