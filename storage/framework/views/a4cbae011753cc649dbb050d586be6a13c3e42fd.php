<?php $__env->startSection('content'); ?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <div id="home_reports">
            <h4 class="alert alert-success"> <i class="fas fa-stream"></i> &nbsp; أحصائيات المستخدمين  </h4>
            <!-- Start Row -->
            <div class="row">
                <?php if(Auth::guard('admin')->user()->is_super): ?>
                    <div class="col-md-6">
                        <div class="report_box">
                            <p class="report_title"> <i class="fas fa-user"></i> المشرفين</p>
                            <h3 class="report_number text-left"><?php echo e($admins->count()); ?></h3>

                        </div>
                    </div>
                <?php endif; ?>

                <?php if(checkPermission('clients')): ?>
                    <div class="col-md-6">
                        <div class="report_box">
                            <p class="report_title"> <i class="fas fa-users"></i> العملاء</p>
                            <h3 class="report_number text-left"><?php echo e($clients->count()); ?></h3>
                        </div>
                    </div>
                <?php endif; ?>
              </div>
              <!-- End Row -->

              <!-- Start Row -->
              <?php if(checkPermission('products')): ?>
              <h4 class="alert alert-success"> <i class="fas fa-stream"></i> &nbsp; أحصائيات المنتجات  </h4>
              <div class="row">
                    <div class="col-md-4">
                        <div class="report_box">
                            <p class="report_title"> <i class="fas fa-boxes"></i> جميع المنتجات</p>
                            <h3 class="report_number text-left"><?php echo e($products->count()); ?></h3>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="report_box">
                            <p class="report_title"> <i class="fas fa-boxes"></i> المنتجات المعروضة</p>
                            <h3 class="report_number text-left"><?php echo e($products_show->count()); ?></h3>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="report_box">
                            <p class="report_title"> <i class="fas fa-boxes"></i> المنتجات المحذوفة </p>
                            <h3 class="report_number text-left"><?php echo e($products_soft_delete->count()); ?></h3>
                        </div>
                    </div>
              </div>
              <?php endif; ?>
              <!-- End Row -->

                <!-- Start Row -->
                <?php if(checkPermission('orders')): ?>
                <h4 class="alert alert-success"> <i class="fas fa-stream"></i> &nbsp; الطلبات  </h4>
                <div class="row">
                    <div class="col-md-4">
                        <div class="report_box">
                            <p class="report_title"> <i class="fas fa-shopping-bag"></i> اجمالي الطلبات</p>
                            <h3 class="report_number text-left"><?php echo e($orders->count()); ?></h3>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="report_box">
                            <p class="report_title"> <i class="fas fa-shopping-bag"></i> عملاء الطلبات</p>
                            <h3 class="report_number text-left"><?php echo e($orders_users->count()); ?></h3>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="report_box">
                            <p class="report_title"> <i class="fas fa-shopping-bag"></i> منتجات الطلبات</p>
                            <h3 class="report_number text-left"><?php echo e($orders_products->count()); ?></h3>
                        </div>
                    </div>
              </div>
              <?php endif; ?>
              <!-- End Row -->

              <!-- Start Row -->
              <?php if(checkPermission('categories')): ?>
              <h4 class="alert alert-success"> <i class="fas fa-stream"></i> &nbsp;  أقسام التجار  </h4>
              <div class="row">
                  <div class="col-md-4">
                      <div class="report_box">
                          <p class="report_title"> <i class="fas fa-shopping-bag"></i> أجمالي الأقسام</p>
                          <h3 class="report_number text-left"><?php echo e($category->count()); ?></h3>
                      </div>
                  </div>

                  <div class="col-md-4">
                      <div class="report_box">
                          <p class="report_title"> <i class="fas fa-shopping-bag"></i> الأقسام الرئيسية</p>
                          <h3 class="report_number text-left"><?php echo e($category_main->count()); ?></h3>
                      </div>
                  </div>


                  <div class="col-md-4">
                      <div class="report_box">
                          <p class="report_title"> <i class="fas fa-shopping-bag"></i> الأقسام الفرعية</p>
                          <h3 class="report_number text-left"><?php echo e($category_sub->count()); ?></h3>
                      </div>
                  </div>
              </div>
              <?php endif; ?>
              <!-- End Row -->

              <!-- Start Row -->
              <?php if(checkPermission('partners')): ?>
              <h4 class="alert alert-success"> <i class="fas fa-stream"></i> &nbsp; التجار  </h4>
              <div class="row">
                    <div class="col-md-4">
                        <div class="report_box">
                            <p class="report_title"><i class="fa fa-star"></i> اجمالي التجار</p>
                            <h3 class="report_number text-left"><?php echo e($allGalleries->count()); ?></h3>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="report_box">
                            <p class="report_title"><i class="fa fa-star"></i> التجار النشطاء</p>
                            <h3 class="report_number text-left"><?php echo e($gallers_active->count()); ?></h3>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="report_box">
                            <p class="report_title"><i class="fa fa-star"></i> التجار الغير نشطاء</p>
                            <h3 class="report_number text-left"><?php echo e($gallers_unactive->count()); ?></h3>
                        </div>
                    </div>

            </div>
            <?php endif; ?>
            <!-- End Row -->

        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>