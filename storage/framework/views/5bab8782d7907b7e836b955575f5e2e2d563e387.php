<?php $__env->startSection('content'); ?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-list"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp;  الأقسام الرئيسية</h3>
            </div>
        </div>
        <div class="kt-portlet__body text-center">
			<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
				<thead>
					<tr>
						<th>#</th>
						<th>اسم القسم (EN)</th>
						<th>اسم القسم (AR)</th>
						<th>الأعدادات</th>
					</tr>
				</thead>
				<tbody>
					<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categ): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr>
							<td><?php echo e($loop->iteration); ?></td>
							<td><?php echo e($categ->title_en); ?></td>
							<td><?php echo e($categ->title_ar); ?></td>
							<td>
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">
										الأعدادات
									</button>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="<?php echo e(route('categories.edit', $categ->id)); ?>"><i class="fas fa-edit"></i> تعديل</a>
										<a class="dropdown-item" data-toggle="modal" href="#myModal-<?php echo e($categ->id); ?>"><i class="fas fa-trash"></i> حذف</a>
									</div>
									<a href="<?php echo e(route('categories.show', $categ->id)); ?>" class="btn btn-success" type="button">
										الأقسام الفرعية
									</a>
								</div>

								<div class="modal fade" id="myModal-<?php echo e($categ->id); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
								<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">حذف القسم</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
								<form role="form" action="<?php echo e(url('admin/categories/'.$categ->id)); ?>" class="" method="POST">
								<input name="_method" type="hidden" value="DELETE">
								<?php echo e(csrf_field()); ?>

								<p>هل انت متأكد من الحذف ؟ </p>
								<button type="submit" class="btn btn-danger" name='delete_modal'>حذف</button>
								<button type="button" class="btn btn-success" data-dismiss="modal">اغلاق</button>
								</form>
								</div>
								</div>
								</div>
								</div>
							</td>
						</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</tbody>
			</table>
        </div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>