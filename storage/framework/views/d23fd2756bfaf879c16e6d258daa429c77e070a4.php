<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fa fa-star"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; تعديل</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(url('admin/sallers/services/'.$row->id)); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

        <?php echo e(method_field('PUT')); ?>

				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الأسم</label>
							<input class="form-control" type="text" name="name" value="<?php echo e($row->name); ?>" id="name" />
							<?php if($errors->has('name')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('name')); ?></strong></span><?php endif; ?>
						</div>

            <div class="col-md-6">
              <label>الأيميل</label>
              <input class="form-control" type="email" name="email" value="<?php echo e($row->email); ?>" id="email" />
              <?php if($errors->has('email')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('email')); ?></strong></span><?php endif; ?>
            </div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">

            <div class="col-md-6">
              <label>الأقسام</label>
              <select class="form-control" name="cat_id">
                <?php $__currentLoopData = $cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($cat->id); ?>" <?php if($row->cat_id == $cat->id): ?> selected <?php endif; ?>><?php echo e($cat->name_ar); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </select>
            </div>


            <div class="col-md-6">
              <label>المدينة</label>

              <select class="form-control" name="city_id" id="gallery">
                <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <optgroup label="<?php echo e($city['name_ar']); ?>">
                      <?php $__currentLoopData = $city->regions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $region): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($region->id); ?>" <?php if($row->city_id == $region->id): ?> selected <?php endif; ?>><?php echo e($region->name_ar); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </optgroup>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </select>
            </div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الموبايل</label>
							<input class="form-control" type="text" name="phone" value="<?php echo e($row->phone); ?>" id="phone" />
							<?php if($errors->has('phone')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('phone')); ?></strong></span><?php endif; ?>
						</div>

            <div class="col-md-6">
              <label>كلمة السر</label>
              <input class="form-control" type="password" name="password">
              <?php if($errors->has('password')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('password')); ?></strong></span><?php endif; ?>
            </div>
					</div>
				</div>

				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">الصورة</label>
					<div class="col-lg-10">
						<div class="custom-file">
							<input type="file" class="custom-file-input" name="image" id="image" />
							<label class="custom-file-label" for="image">اختار الصورة</label>
						</div>
					</div>
				</div>

        <div class="form-group">
          <div class="row">
            <div class="col-md-12">
              <label> وصف التاجر</label>
              <textarea class="form-control" rows="7" placeholder="وصف التاجر" name="desc">
                  <?php echo e($row->desc); ?>

               </textarea>
              <?php if($errors->has('desc')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('desc')); ?></strong></span><?php endif; ?>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              <label>رابط الفيس بوك</label>
              <input class="form-control" type="text" placeholder="الفيسبوك" name="facebook_link" value="<?php echo e($row->facebook_link); ?>" id="phone" />
              <?php if($errors->has('facebook_link')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('facebook_link')); ?></strong></span><?php endif; ?>
            </div>


            <div class="col-md-6">
              <label>رابط تويتر</label>
              <input class="form-control" type="text" placeholder="تويتر" name="twitter_link" value="<?php echo e($row->twitter_link); ?>" id="phone" />
              <?php if($errors->has('twitter_link')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('twitter_link')); ?></strong></span><?php endif; ?>
            </div>

            <div class="col-md-6">
              <label>رابط الأنستجرام</label>
              <input class="form-control" type="text" placeholder="انتسجرام" name="instgram_link" value="<?php echo e($row->instgram_link); ?>" id="phone" />
              <?php if($errors->has('instgram_link')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('instgram_link')); ?></strong></span><?php endif; ?>
            </div>


            <div class="col-md-6">
              <label>رابط اليوتيوب</label>
              <input class="form-control" type="text" placeholder="اليوتيوب" name="youtube_link" value="<?php echo e($row->youtube_link); ?>" id="phone" />
              <?php if($errors->has('youtube_link')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('youtube_link')); ?></strong></span><?php endif; ?>
            </div>
          </div>
        </div>

				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>