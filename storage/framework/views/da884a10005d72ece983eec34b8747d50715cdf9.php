<?php $__env->startSection('content'); ?>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="fas fa-images"></i></span>
				<h3 class="kt-portlet__head-title">سلايدر</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
            	<div class="kt-portlet__head-wrapper">
                	<div class="kt-portlet__head-actions">
                    	<a href="<?php echo e(route('slider.create')); ?>" class="btn btn-brand btn-elevate btn-icon-sm"><i class="la la-plus"></i> New Image</a>
                    </div>
                </div>
            </div>
        </div>
	<div class="m-portlet__body">
		<div class="container-fluid">
			<div class="row">
				<?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="col-md-6" style="margin-bottom:20px">
					<div class="image_gallery">
					<img src="<?php echo e(asset($slider->image)); ?>" class="img-responsive" alt="Image">
					<hr>
					<div class="gallery_overlay text-center">
					<a href="<?php echo e(route('slider.edit',$slider->id)); ?>" class="btn btn-info"><i class="fas fa-edit"></i> تعديل</a>
					<a data-toggle="modal" href="#myModal-<?php echo e($slider->id); ?>" class="btn btn-danger"> <i class="fas fa-trash-alt"></i> حذف</a>
					</div>

					<div class="modal fade" id="myModal-<?php echo e($slider->id); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">حذف سلايدر</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form role="form" action="<?php echo e(url('admin/slider/'.$slider->id)); ?>" class="" method="POST">
									<input name="_method" type="hidden" value="DELETE">
									<?php echo e(csrf_field()); ?>

									<p>هل انت متأكد من الحذف ؟</p>
									<button type="submit" class="btn btn-danger" name='delete_modal'>حذف</button>
									<button type="button" class="btn btn-success" data-dismiss="modal">الغاء</button>
									</form>
								</div>
							</div>
						</div>
					</div>

					</div>
					</div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>