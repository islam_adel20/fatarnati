<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-tags"></i></span>
                <h3 class="kt-portlet__head-title">أضافة أكواد جديدة</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(url('admin/saller/codes')); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				<div class="form-group">
					<div class="row">
						<div class="col-md-4">
							<label>عدد الأكواد المنشاءة</label>
							<input class="form-control" type="number" placeholder="عدد الأكواد" name="num" required="required">
						</div>

            <div class="col-md-4">
							<label>المدة الزمنية [الأيام]</label>
							<input class="form-control" type="number" placeholder="المدة الزمنية" name="duration" required="required">
						</div>

            <div class="col-md-4">
							<label>تكلفة الأشتراك</label>
							<input class="form-control" type="number" placeholder="تكلفة الأشتراك" name="cost" required="required">
						</div>
					</div>
				</div>



				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>