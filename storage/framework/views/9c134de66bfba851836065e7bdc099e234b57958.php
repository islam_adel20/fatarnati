<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-tags"></i></span>
                <h3 class="kt-portlet__head-title">Edit Discount Code</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(route('codes.update', $code->id)); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				<input type="hidden" name="_method" value="PUT" />

        <div class="form-group">
					<div class="row">
						<div class="col-md-4">
							<label>الكود</label>
							<input class="form-control" type="text" value="<?php echo e($code->code); ?>" name="code" required="required">
						</div>

            <div class="col-md-4">
							<label>المدة الزمنية [الأيام]</label>
							<input class="form-control" type="number" value="<?php echo e($code->duration); ?>" name="duration" required="required">
						</div>

            <div class="col-md-4">
							<label>تكلفة الأشتراك</label>
							<input class="form-control" type="number" value="<?php echo e($code->cost); ?>" name="cost" required="required">
						</div>
					</div>
				</div>

				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>