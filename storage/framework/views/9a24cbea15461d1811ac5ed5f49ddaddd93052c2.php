<?php $__env->startSection('content'); ?>
<!-- Category Products Area
============================================ -->
<!-- Category Products Area
============================================ -->
<section class="ptb-50" style="padding-bottom: 50px;">
    <div class="container text-center">
        <div id="change_information">
            <h1  id="page_title" style="padding: 100px 0px;"><?php echo e(__('global.add_address')); ?></h1>
            <div class="row">
                <div class="row">
                    <div class="col-md-9">
                        <form class="form-horizontal box_shadowed_form"  role="form" method="POST" action="<?php echo e(url('/add-address')); ?>" id="ajform">
                            <?php if(session()->has('success')): ?>
                                <div class="alert alert-success"><?php echo e(session('success')); ?></div>
                            <?php endif; ?>
                            
                            <?php if($errors->any()): ?>
                                <div class="alert alert-danger"><?php echo e($errors->first()); ?></div>
                            <?php endif; ?>
                            
                            <div id="ajform_res"></div>
                            <?php echo e(csrf_field()); ?>

    
                            <div class="form-group">
                                <label for="name" class="control-label"><?php echo e(__('global.name')); ?></label>
                                <input id="name" type="text" class="form-control" name="name" required>
                            </div>
                            <div class="form-group">
								<label><?php echo e(__('global.city')); ?></label>
								<select name="city_id" class="form-control">
									<?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<option value="<?php echo e($city->id); ?>">
											<?php echo e($city->name); ?>

										</option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
							</div>
							<div class="form-group">
                                <label for="area" class="control-label"><?php echo e(__('global.area')); ?></label>
                                <input id="area" type="text" class="form-control" name="area" required>
                            </div>
							<div class="form-group">
                                <label for="street" class="control-label"><?php echo e(__('global.street')); ?></label>
                                <input id="street" type="text" class="form-control" name="street" required>
                            </div>
							<div class="form-group">
                                <label for="building" class="control-label"><?php echo e(__('global.building')); ?></label>
                                <input id="building" type="text" class="form-control" name="building" required>
                            </div>
							<div class="form-group">
                                <label for="apartment" class="control-label"><?php echo e(__('global.apartment')); ?></label>
                                <input id="apartment" type="text" class="form-control" name="apartment" required>
                            </div>
                            <div class="form-group my-3">
                                <button type="submit" class="button"><?php echo e(__('global.save')); ?></button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3" <?php if(!App::isLocale('en')): ?> style="direction: rtl;" <?php endif; ?>>
                        <?php echo $__env->make('profile_side', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>