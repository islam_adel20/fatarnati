<?php $__env->startSection('content'); ?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-paper-plane"></i></span>
                <h3 class="kt-portlet__head-title">الأشتراكات الجارية</h3>
            </div>
        </div>
        <div class="kt-portlet__body text-center">
			<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
				<thead>
					<tr>
						<th>#</th>
						<th>اسم التاجر</th>
						<th>الموبايل</th>
            <th> الكود</th>
						<th>بداية الأشتراك</th>
            <th>نهاية الأشتراك</th>
            <th>التكلفة</th>
					</tr>
				</thead>
				<tbody>
					<?php $__currentLoopData = $subs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php $__currentLoopData = App\Models\Gallery::where('code', $sub->code)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $saller): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr>
							<td><?php echo e($loop->iteration); ?></td>
							<td><?php echo e($saller['name_ar']); ?></td>
							<td><?php echo e($saller['phone']); ?></td>
              <td><?php echo e($sub->code); ?></td>
              <td> <?php echo e($sub->from); ?></td>
              <td> <?php echo e($sub->to); ?></td>
              <td> <?php echo e($sub->cost); ?></td>
						</tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</tbody>
			</table>
        </div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>