<?php $__env->startSection('content'); ?>
    <!-- begin:: Content -->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-list"></i></span>
                    <h3 class="kt-portlet__head-title">Add Tip</h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <form class="kt-form kt-form--label-left" id="kt_form_1" method="post"
                    action="<?php echo e(route('articles.store')); ?>" enctype="multipart/form-data">
                    <?php echo e(csrf_field()); ?>

                    <div class="kt-portlet__body">
                        <?php if($errors->any()): ?>
                            <div class="alert alert-danger"><?php echo e($errors->first()); ?></div>
                        <?php endif; ?>

                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">Title</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control m-input" name="title" value="<?php echo e(old('title')); ?>" />
                            </div>
                        </div>

                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">Video URL</label>
                            <div class="col-lg-10">
                                <textarea class="form-control m-input" name="text"><?php echo e(old('text')); ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-12 text-right">
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>