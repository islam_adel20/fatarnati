<?php $__env->startSection('content'); ?>

    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area" style="background-image: url('<?php echo e(asset($headerImage->image)); ?>');">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <h3>
                            <?php if(App::getLocale() == 'en'): ?>
                                <?php echo e($cat->title_en); ?>

                            <?php else: ?>
                                <?php echo e($cat->title_ar); ?>

                            <?php endif; ?>
                        </h3>
                        <ul>
                            <li><a href="index.html"><?php echo e(__('global.home')); ?></a></li>
                            <li>
                                <?php if(App::getLocale() == 'en'): ?>
                                    <?php echo e($cat->title_en); ?>

                                <?php else: ?>
                                    <?php echo e($cat->title_ar); ?>

                                <?php endif; ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!--shop  area start-->
    <div class="shop_area shop_reverse mt-70 mb-70">
        <div class="container">
            <div class="row add-success" onclick="$(this).hide();">
                <div class="col-md-6 col-12 mx-auto">
                    <div class="alert alert-success text-center">
                        <b id="add-success-message"></b>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3 col-md-12">
                    <!--sidebar widget start-->
                    <aside class="sidebar_widget">
                        <div class="widget_inner">
                            <div class="widget_list widget_color">
                                <h3><b><?php echo e(__('global.cats')); ?></b></h3>
                                <ul>
                                    <?php $__currentLoopData = get_maincats(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <a href="<?php echo e(url('Products/' . $mcat->id)); ?>">
                                                <?php if(App::getLocale() == 'en'): ?>
                                                    <?php echo e($mcat->title_en); ?>

                                                <?php else: ?>
                                                    <?php echo e($mcat->title_ar); ?>

                                                <?php endif; ?>
                                            </a>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                            <div class="widget_list widget_manu">
                                <h3><b><?php echo e(__('global.parteners')); ?></b></h3>
                                <ul>
                                    <?php $__currentLoopData = $galleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <a href="<?php echo e(url('Gallery/' . $gallery->id)); ?>">
                                                <?php if(App::getLocale() == 'en'): ?>
                                                    <?php echo e($gallery->name_en); ?>

                                                <?php else: ?>
                                                    <?php echo e($gallery->name_ar); ?>

                                                <?php endif; ?>
                                            </a>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                            <div class="widget_list widget_filter">
                                <form action="" method="GET" id="gallery-filter">
                                    <select name="gallery_id" class="form-control">
                                        <option value="0" disabled selected><?php echo e(__('global.select_gallery')); ?></option>
                                        <?php $__currentLoopData = $allGalleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($gallery->id); ?>" onclick="$('#gallery-filter').submit();">
                                                <?php if(App::getLocale() == 'en'): ?>
                                                    <?php echo e($gallery->name_en); ?>

                                                <?php else: ?>
                                                    <?php echo e($gallery->name_ar); ?>

                                                <?php endif; ?>
                                            </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </form>
                            </div>
                            <div class="widget_list widget_filter">
                                <h3><b><?php echo e(__('global.price_filter')); ?></b></h3>
                                <form action="" method="GET">
                                    <div id="slider-range"></div>
                                    <input type="text" name="text" id="amount" />
                                    <input type="hidden" name="first_price" />
                                    <input type="hidden" name="last_price" />
                                    <button type="submit"><?php echo e(__('global.search')); ?></button>
                                </form>
                            </div>
                        </div>
                    </aside>
                    <!--sidebar widget end-->
                </div>
                <div class="col-lg-9 col-md-12 order-md-first">
                    <!--shop wrapper start-->
                    <!--shop toolbar start-->
                    <div class="shop_toolbar_wrapper">
                        <div class="shop_toolbar_btn">
                            <button data-role="grid_3" type="button" class="active btn-grid-3" data-toggle="tooltip"
                                title="3"></button>
                            <button data-role="grid_4" type="button" class=" btn-grid-4" data-toggle="tooltip"
                                title="4"></button>
                        </div>
                        <div class="page_amount">
                            <?php if($products->count() > 12): ?>
                                <?php if(App::getLocale() == 'en'): ?>
                                    <p>Show 1 to 12 of <?php echo e($products->count()); ?> results</p>
                                <?php else: ?>
                                    <p>عرض 1 إلى 12 من أصل <?php echo e($products->count()); ?> نتيجة</p>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!--shop toolbar end-->
                    <div class="row shop_wrapper">
                        <?php if($products->count()): ?>
                            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-12 ">
                                    <div class="single_product">
                                        <div class="product_thumb">
                                            <a class="primary_img" href="<?php echo e(url('Product/' . $product->id)); ?>">
                                                <img src="<?php echo e(asset($product->images[0]->image)); ?>"
                                                    alt="<?php echo e($product->title_en); ?>">
                                            </a>
                                            <div class="label_product">
                                                <?php if($product->discount > 0): ?>
                                                    <span class="label_sale">Sale</span>
                                                <?php endif; ?>
                                                <span class="label_new">New</span>
                                            </div>
                                            <div class="action_links">
                                                <ul>
                                                    <li class="add_to_cart add_to_cart_btn" data-num="<?php echo e($product->id); ?>"><a href="javascript:;"
                                                            data-tippy="<?php echo e(__('global.add_to_cart')); ?>" data-tippy-placement="top"
                                                            data-tippy-arrow="true" data-tippy-inertia="true"> <span
                                                                class="lnr lnr-cart"></span></a></li>
                                                    <li class="quick_button"><a
                                                            href="<?php echo e(url('Product/' . $product->id)); ?>"
                                                            data-tippy="<?php echo e(__('global.show')); ?>" data-tippy-placement="top"
                                                            data-tippy-arrow="true" data-tippy-inertia="true"> <span
                                                                class="lnr lnr-magnifier"></span></a></li>
                                                    <!--<li class="wishlist"><a href="#" data-tippy="أضف لقائمة المفضلة"-->
                                                    <!--        data-tippy-placement="top" data-tippy-arrow="true"-->
                                                    <!--        data-tippy-inertia="true"><span-->
                                                    <!--            class="lnr lnr-heart"></span></a></li>-->
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product_content grid_content">
                                            <h4 class="product_name">
                                                <a href="<?php echo e(url('Product/' . $product->id)); ?>">
                                                    <?php if(App::getLocale() == 'en'): ?>
                                                        <?php echo e($product->title_en); ?>

                                                    <?php else: ?>
                                                        <?php echo e($product->title_ar); ?>

                                                    <?php endif; ?>
                                                </a>
                                            </h4>
                                            <?php if(!empty($product->category)): ?>
                                                <p>
                                                    <a href="<?php echo e(url('Products/' . $product->category->id)); ?>">
                                                        <?php if(App::getLocale() == 'en'): ?>
                                                            <?php echo e($product->category->title_en); ?>

                                                        <?php else: ?>
                                                            <?php echo e($product->category->title_ar); ?>

                                                        <?php endif; ?>
                                                    </a>
                                                </p>
                                            <?php else: ?>
                                                <p style="height: 12px;"></p>
                                            <?php endif; ?>
                                            <div class="price_box">
                                                <?php if($product->discount > 0): ?>
                                                    <span
                                                        class="current_price"><?php echo e($product->price - $product->discount); ?>

                                                        EGP</span>
                                                    <span class="old_price"><?php echo e($product->price); ?> EGP</span>
                                                <?php else: ?>
                                                    <span class="current_price"><?php echo e($product->price); ?> EGP</span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <h4 class="text-center"><?php echo e(__('global.sorry_no_results')); ?></h4>
                        <?php endif; ?>
                    </div>

                    <?php if($products->count() >= 12): ?>
                        <div class="shop_toolbar t_bottom">
                            <div class="pagination">
                                <?php echo e($products->links()); ?>

                            </div>
                        </div>
                    <?php endif; ?>
                    <!--shop toolbar end-->
                    <!--shop wrapper end-->
                </div>

            </div>
        </div>
    </div>
    <!--shop  area end-->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>