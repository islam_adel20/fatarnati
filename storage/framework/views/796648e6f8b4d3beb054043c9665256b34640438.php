<body>

    <!--header area start-->

    <header>
        <div class="header_area container_width color_six p-0">
            <div class="main_header7 border-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="main_header7_inner d-flex align-items-center justify-content-between">
                                <div class="header_account_area">
                                    <div class="header_account_list header_wishlist">
                                        <?php if(App::getLocale() == 'en'): ?>
                                            <a href="<?php echo e(url('switch_lang/ar')); ?>">
                                                <img src="<?php echo e(asset('site/img/icon/egypt.svg')); ?>" alt="">
                                            </a>
                                        <?php else: ?>
                                            <a href="<?php echo e(url('switch_lang/en')); ?>">
                                                <img src="<?php echo e(asset('site/img/icon/uk.png')); ?>" alt="">
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="header_account_list register">
                                        <?php if(Auth::guard('user')->user()): ?>
                                            <ul>
                                                <li><a href="<?php echo e(url('logout')); ?>"><?php echo e(__('global.logout')); ?></a></li>
                                                <li><span>/</span></li>
                                                <li><a href="<?php echo e(url('Profile')); ?>"><?php echo e(__('global.my_account')); ?></a></li>
                                            </ul>
                                        <?php else: ?>
                                            <ul>
                                                <li><a href="<?php echo e(url('register')); ?>"><?php echo e(__('global.register')); ?></a></li>
                                                <li><span>/</span></li>
                                                <li><a href="<?php echo e(url('login')); ?>"><?php echo e(__('global.login')); ?></a></li>
                                            </ul>
                                        <?php endif; ?>
                                    </div>
                                    <!--<div class="header_account_list header_wishlist">-->
                                    <!--    <a href="#"><span class="lnr lnr-heart"></span> <span-->
                                    <!--            class="item_count">3</span> </a>-->
                                    <!--</div>-->
                                    <div class="header_account_list  mini_cart_wrapper">
                                        <a href="javascript:void(0)"><span class="lnr lnr-cart"></span><span
                                                class="item_count"><?php echo e(Cart::content()->count()); ?></span></a>
                                        <?php echo $__env->make('include.cart', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    </div>
                                </div>

                                <div class="col-lg-5 col_search5">
                                    <div class="search_box search_five mobail_s_none">
                                        <form action="<?php echo e(route('search')); ?>">
                                            <input placeholder="<?php echo e(__('global.enter_keyword')); ?>" type="text" name="keyword">
                                            <button type="submit"><span class="lnr lnr-magnifier"></span></button>
                                        </form>
                                    </div>
                                </div>

                                <div class="logo">
                                    <a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset('site/img/logo/logo.jpg')); ?>" alt=""></a>
                                </div>

                                <div class="canvas_open canvas_menu_icon">
                                    <a href="javascript:void(0)"><i class="icon-menu"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header_bottom sticky-header">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-10 offset-lg-1">
                            <!--main menu start-->
                            <div class="main_menu menu_position">
                                <nav>
                                    <ul class="justify-content-center">
                                        <li><a class="active" href="<?php echo e(url('/')); ?>"><?php echo e(__('global.home')); ?></a></li>
                                        <li><a href="<?php echo e(url('/About')); ?>"><?php echo e(__('global.about_asask')); ?></a></li>
                                        
										<?php $__currentLoopData = get_maincats(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li>
                                                <a href="<?php echo e(url('Products/' . $mcat->id)); ?>" style="transition: none;">
                                                    <?php if(App::getLocale() == 'en'): ?>
                                                        <?php echo e($mcat->title_en); ?>

                                                    <?php else: ?>
                                                        <?php echo e($mcat->title_ar); ?>

                                                    <?php endif; ?>
                                                </a>
                                                <?php if($mcat->subs->count()): ?>
                                                    <ul class="sub_menu">
                                                        <?php $__currentLoopData = $mcat->subs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($category->subs->count() == 0): ?>
                                                                <li>
                                                                    <a href="<?php echo e(url('Products/' . $category->id)); ?>" style="transition:none;">
                                                                        <?php if(App::getLocale() == 'en'): ?>
                                                                            <?php echo e($category->title_en); ?>

                                                                        <?php else: ?>
                                                                            <?php echo e($category->title_ar); ?>

                                                                        <?php endif; ?>
                                                                    </a>
                                                                </li>
                                                            <?php else: ?>
                                                                <li class="dropdown-submenu">
                                                                    <a href="<?php echo e(url('Products/' . $category->id)); ?>">
                                                                        <span class="nav-label">
                                                                            <?php if(App::getLocale() == 'en'): ?>
                                                                                <?php echo e($category->title_en); ?>

                                                                            <?php else: ?>
                                                                                <?php echo e($category->title_ar); ?>

                                                                            <?php endif; ?>
                                                                        </span>
                                                                    </a>
                                                                    <ul class="dropdown-menu">
                                                                        <?php $__currentLoopData = $category->subs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <li>
                                                                                <a href="<?php echo e(url('Products/' . $sub->id)); ?>">
                                                                                    <?php if(App::getLocale() == 'en'): ?>
                                                                                        <?php echo e($sub->title_en); ?>

                                                                                    <?php else: ?>
                                                                                        <?php echo e($sub->title_ar); ?>

                                                                                    <?php endif; ?>
                                                                                </a>
                                                                            </li>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                    </ul>
                                                                </li>
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </ul>
                                                <?php endif; ?>
                                            </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        
										<li><a href="<?php echo e(url('/Galleries')); ?>"><?php echo e(__('global.parteners')); ?></a></li>
                                    </ul>
                                </nav>
                            </div>
                            <!--main menu end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!--header area end-->
