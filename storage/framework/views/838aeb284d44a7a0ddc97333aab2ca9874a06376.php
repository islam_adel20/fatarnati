<?php $__env->startSection('content'); ?>
<!-- Category Products Area
============================================ -->
<div class="product-page-area page-area pb-5">
	<div class="container text-center" style="padding: 50px 20px;">
		<h1 style="padding: 50px 0px;"><?php echo e(__('global.checkout')); ?></h1>
		<div class="row">
			<?php if(Cart::content()->count() > 0): ?>
				<form action="<?php echo e(url('Checkout-review')); ?>" method="get" id="checkout-form">
					<div class="row">
						<div class="col-12 col-md-6 text-left">
							<?php if($errors->any()): ?>
								<div class="alert alert-danger"><?php echo e($errors->first()); ?></div>
							<?php endif; ?>

							<p class="mb-2"><b><?php echo e(__('global.name')); ?> : </b><?php echo e(Auth::guard('user')->user()->name); ?></p>
							<p class="mb-2"><b><?php echo e(__('global.email')); ?> : </b><?php echo e(Auth::guard('user')->user()->email); ?></p>
							<p class="mb-2"><b><?php echo e(__('global.phone')); ?> : </b><?php echo e(Auth::guard('user')->user()->phone); ?></p>
							<br>

							<div class="form-group">
								<label><?php echo e(__('global.address')); ?> :</label>
								<select name="address" class="form-control">
									<?php $__currentLoopData = Auth::guard('user')->user()->addresses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $address): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<option value="<?php echo e($address->id); ?>">
											<?php echo e($address->name); ?>

										</option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
							</div>
							<br>
							
							<div class="form-group">
								<label><?php echo e(__('global.discount_code')); ?> :</label>
								<input type="text" name="code" class="form-control" />
							</div>
							<br>

							<div class="form-group">
								<label><?php echo e(__('global.pay_method')); ?> :</label>
								<select name="pay_method" class="form-control">
									<?php $__currentLoopData = $methods; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $method): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<option value="<?php echo e($method->id); ?>">
											<?php if(App::getLocale() == 'en'): ?>
												<?php echo e($method->title_en); ?>

											<?php else: ?>
												<?php echo e($method->title_ar); ?>

											<?php endif; ?>
										</option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
							</div>
							<hr />
							<p class="text-right">
								<button type="submit" class="button"><?php echo e(__('global.review_order')); ?></button>
								<a href="<?php echo e(url('/add-address')); ?>" role="button" class="button"><?php echo e(__('global.add_address')); ?></a>
							</p>
						</div>
						<div class="col-12 col-md-6">
							<table class="table table-responsive" id="shopping_cart_table">
								<tbody>
								<?php $__currentLoopData = Cart::content(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<tr>
										<td>
											<a href="<?php echo e(url('Product/'.$item->options->product)); ?>">
												<img alt="<?php echo e($item->name); ?>" src="<?php echo e(asset($item->options->image)); ?>" style="width:75px;height:75px;">
												<p>
													<?php if(App::getLocale() == 'en'): ?>
														<?php echo e($item->name); ?>

													<?php else: ?>
														<?php echo e($item->options->name_ar); ?>

													<?php endif; ?>
												</p>
											</a>
										</td>
										<td style="vertical-align: middle;">
											<?php if($item->options->size != 0): ?>
												<?php if(App::getLocale() == 'en'): ?>
													<?php echo e(getSize($item->options->size)->title_en); ?>

												<?php else: ?>
													<?php echo e(getSize($item->options->size)->title_ar); ?>

												<?php endif; ?>
											<?php endif; ?>
										</td>
										<td class="text-center" style="vertical-align: middle;">
											x <?php echo e($item->qty); ?>

										<br /> <?php echo e($item->price); ?>  EGP</td>
										<td style="vertical-align: middle;"><?php echo e($item->price * $item->qty); ?>  EGP</td>
									</tr>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<tr>
									<td colspan="3" class="text-right"><?php echo e(__('global.total')); ?> : <?php echo e(Cart::subtotal()); ?> EGP</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
				</form>
			<?php else: ?>
				<h3 class="text-center"><?php echo e(__('global.sorry_no_items')); ?></h3>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>