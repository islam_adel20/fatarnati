<?php $__env->startSection('content'); ?>
<!-- Category Products Area
============================================ -->
<section class="ptb-50" style="padding-bottom: 50px;">
    <div class="container text-center">
        <h1 id="page_title" style="padding: 50px;"><?php echo e(__('global.my_account')); ?></h1>
        
        <div class="row"  <?php if(!App::isLocale('en')): ?> style="direction: rtl;" <?php endif; ?>>
            <div class="col-md-3">
                <?php echo $__env->make('profile_side', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <div class="col-md-9">
                <?php if(App::getLocale() == 'en'): ?>
                    <p class="page_description double_bm">Hello <b><?php echo e(Auth::guard('user')->user()->name); ?></b>, this is your ASASK account<br />
                    Via the different options we will show you, you can modify your personal details, view your orders.</p>
                <?php else: ?>
                    <p style="direction: rtl;">
                        مرحباً <b><?php echo e(Auth::guard('user')->user()->name); ?></b> ، هذا هو حسابك في ASASK
                        <br>
                        باستعمال الاختيارات المتنوعة أدناه ، يمكنك مراجعة معلوماتك الشخصية و الطلبيات التي قمت بها
                    </p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>