<?php $__env->startSection('content'); ?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <div id="home_reports">
            <div class="row">
                <?php if(Auth::guard('admin')->user()->is_super): ?>
                    <div class="col-md-4">
                        <div class="report_box">
                            <p class="report_title">المشرفين</p>
                            <p class="report_number"><?php echo e($admins->count()); ?></p>
                            <i class="fas fa-user"></i>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if(checkPermission('clients')): ?>
                    <div class="col-md-4">
                        <div class="report_box">
                            <p class="report_title">العملاء</p>
                            <p class="report_number"><?php echo e($clients->count()); ?></p>
                            <i class="fas fa-users"></i>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if(checkPermission('products')): ?>
                    <div class="col-md-4">
                        <div class="report_box">
                            <p class="report_title">المنتجات</p>
                            <p class="report_number"><?php echo e($products->count()); ?></p>
                            <i class="fas fa-boxes"></i>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if(checkPermission('orders')): ?>
                    <div class="col-md-4">
                        <div class="report_box">
                            <p class="report_title">الطلبات</p>
                            <p class="report_number"><?php echo e($orders->count()); ?></p>
                            <i class="fas fa-shopping-bag"></i>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if(checkPermission('partners')): ?>
                    <div class="col-md-4">
                        <div class="report_box">
                            <p class="report_title">التجار</p>
                            <p class="report_number"><?php echo e($allGalleries->count()); ?></p>
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>