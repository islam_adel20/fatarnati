<?php $__env->startSection('content'); ?>
<!-- Category Products Area
============================================ -->
<section class="ptb-50">
<div class="container text-center">
    <h1 id="page_title" style="padding: 50px;"><?php echo e(__('global.register')); ?></h1>
    <p class="page_description" style="padding-bottom: 40px;">
        <?php if(App::getLocale() == 'en'): ?>
            By Creating new account In ASASK. You will be able to order your favourite Items and tracking your orders
        <?php else: ?>
            سجل حساب جديد ، و استمتع بكل المميزات و العروض الحصرية
        <?php endif; ?>
    </p>
    <div class="row">
    <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6 col-xs-offset-0 col-sm-offset-1 col-md-offset-2 offset-lg-3">
    <form class="form-horizontal box_shadowed_form" role="form" method="POST" action="<?php echo e(url('/register')); ?>" id="ajforwwe">
        <div id="ajform_res"></div>
        <?php echo e(csrf_field()); ?>


        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="first_name" class="control-label"><?php echo e(__('global.first_name')); ?></label>
                    <input id="first_name" type="text" class="form-control" name="first_name" value="<?php echo e(old('first_name')); ?>" required/>
                    <?php if($errors->has('first_name')): ?>
                        <span class="form-text text-danger">
                            <strong><?php echo e($errors->first('first_name')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>  
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="last_name" class="control-label"><?php echo e(__('global.last_name')); ?></label>
                    <input id="last_name" type="text" class="form-control" name="last_name" value="<?php echo e(old('last_name')); ?>" required/>
                    <?php if($errors->has('last_name')): ?>
                        <span class="form-text text-danger">
                            <strong><?php echo e($errors->first('last_name')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="email" class="control-label"><?php echo e(__('global.email')); ?></label>
                    <input id="email" type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" required/>
                    <?php if($errors->has('email')): ?>
                        <span class="form-text text-danger">
                            <strong><?php echo e($errors->first('email')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="phone" class="control-label"><?php echo e(__('global.phone')); ?></label>
                    <input id="phone" type="text" class="form-control" name="phone" value="<?php echo e(old('phone')); ?>" required/>
                    <?php if($errors->has('phone')): ?>
                        <span class="form-text text-danger">
                            <strong><?php echo e($errors->first('phone')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="password" class="control-label"><?php echo e(__('global.password')); ?></label>
                    <input id="password" type="password" class="form-control" name="password" required>
                    <?php if($errors->has('password')): ?>
                        <span class="form-text text-danger">
                            <strong><?php echo e($errors->first('password')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="password-confirm" class="control-label"><?php echo e(__('global.confirm')); ?></label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
            </div>

        <div class="col-xs-12 my-3">
            <button type="submit" class="button"><?php echo e(__('global.create_account')); ?></button>
        </div>
    </form>
    </div>
    </div>
</div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>