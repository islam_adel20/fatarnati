<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-bell"></i></span>
                <h3 class="kt-portlet__head-title">Add Status</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(route('order_status.store')); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>Name (AR)</label>
							<input class="form-control" type="text" name="title_ar" value="<?php echo e(old('title_ar')); ?>" id="title" />
							<?php if($errors->has('title_ar')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('title_ar')); ?></strong></span><?php endif; ?>
						</div>

            <div class="col-md-6">
							<label>Name (EN)</label>
							<input class="form-control" type="text" name="title_en" value="<?php echo e(old('title_en')); ?>" id="title" />
							<?php if($errors->has('title_en')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('title_en')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>