<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="fas fa-palette"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; تعديل اللون</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(url('admin/colors/'.$color->id)); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				<input type="hidden" name="_method" value="PUT" />
				<div class="form-group">
					<div class="row">
					<div class="col-md-6">
							<label>اسم اللون</label>
							<input class="form-control" type="text" value="<?php echo e($color->title); ?>" name='title'>
							<?php if($errors->has('title')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('title')); ?></strong></span><?php endif; ?>
						</div>

						<div class="col-md-6">
							<label>اللون</label>
							<input class="form-control" type="color" name="color" value="<?php echo e($color->color); ?>">
							<?php if($errors->has('color')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('color')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>