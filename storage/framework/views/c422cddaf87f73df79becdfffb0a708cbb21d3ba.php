<?php $__env->startSection('content'); ?>
<!-- Category Products Area
============================================ -->
<section class="product-page-area ptb-50" style="direction: ltr">
    <div class="container text-center" style="padding: 50px 0px;">
        <h1 style="padding: 50px 0px;">
            <?php if(App::getLocale() == 'en'): ?>
                Order #<?php echo e($order->id); ?> Details
            <?php else: ?>
                #<?php echo e($order->id); ?> تفاصيل الطلبية 
            <?php endif; ?>
        </h1>

        <div class="row" <?php if(!App::isLocale('en')): ?> style="direction: rtl;" <?php endif; ?>>
            <div class="col-md-3">
                <?php echo $__env->make('profile_side', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <div class="col-md-9">
                <div class="row" id="order_badges">
                    <div class="col-xs-12 col-md-6 col-lg-4"><p><b><?php echo e(__('global.status')); ?> : </b> 
                        <?php if(App::getLocale() == 'en'): ?>
                            <?php echo e($order->status_info->title); ?>

                        <?php else: ?>
                            <?php echo e($order->status_info->title_ar); ?>

                        <?php endif; ?>
                    </p></div>
                    <div class="col-xs-12 col-md-6 col-lg-4"><p><b><?php echo e(__('global.pay_method')); ?> : </b> 
                        <?php if(App::getLocale() == 'en'): ?>
                            <?php echo e($order->pay_info->title_en); ?>

                        <?php else: ?>
                            <?php echo e($order->pay_info->title_ar); ?>

                        <?php endif; ?>
                    </p></div>
                    <div class="col-xs-12 col-md-6 col-lg-4"><p><b><?php echo e(__('global.ordered_at')); ?> : </b> <?php echo e(date('Y-m-d', strtotime($order->created_at))); ?></p></div>
                </div>
        
                <hr>
        
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-responsive" id="shopping_cart_table">
                        <thead>
                            <tr>
                                <th class="text-center"><?php echo e(__('global.product')); ?></th>
                                <th class="text-center"><?php echo e(__('global.qty')); ?></th>
                                <th class="text-center"><?php echo e(__('global.price')); ?></th>
                                <th class="text-center"><?php echo e(__('global.total')); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $total = 0; ?>
                        <?php $__currentLoopData = $order->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td class="td-product">
                                    <a href="<?php echo e(url('Product/'.$item->product)); ?>">
                                        <img alt="<?php echo e($item->product_info->title); ?>" src="<?php echo e(asset($item->product_info->images[0]->image)); ?>" style="width:150px;height:150px;">
                                        <span>
                                            <?php if(App::getLocale() == 'en'): ?>
                                                <?php echo e($item->product_info->title_en); ?>

                                            <?php else: ?>
                                                <?php echo e($item->product_info->title_ar); ?>

                                            <?php endif; ?>
                                        </span>
                                    </a>
                                </td>
                                <td><?php echo e($item->qty); ?></td>
                                <td><?php echo e($item->price); ?>  EGP</td>
                                <td><?php echo e($item->price * $item->qty); ?>  EGP <?php $total = $total + ($item->price * $item->qty); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td colspan="3"></td>
                            <td> <?php echo e($total); ?> EGP</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>