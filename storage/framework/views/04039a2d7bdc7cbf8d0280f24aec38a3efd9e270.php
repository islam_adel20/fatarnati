<?php $__env->startSection('content'); ?>
<!-- Category Products Area
============================================ -->
<!-- Category Products Area
============================================ -->
<section class="ptb-50" style="padding-bottom: 50px;">
    <div class="container text-center">
        <div id="change_information">
            <h1  id="page_title" style="padding: 100px 0px;"><?php echo e(__('global.my_addresses')); ?></h1>

            <div class="row" <?php if(!App::isLocale('en')): ?> style="direction: rtl;" <?php endif; ?>>
                <div class="col-md-3">
                    <?php echo $__env->make('profile_side', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                <div class="col-md-9" >
                    <?php if(count(Auth::guard('user')->user()->addresses)): ?>
                        <table class="table table-bordered table-striped table-responsive" id="shopping_cart_table">
                            <thead>
                                <tr class="table-warning">
                                    <td>#</td>
                                    <td><?php echo e(__('global.name')); ?></td>
                                    <td><?php echo e(__('global.city')); ?></td>
                                    <td><?php echo e(__('global.created_at')); ?></td>
                                    <td><?php echo e(__('global.actions')); ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = Auth::guard('user')->user()->addresses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $address): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($loop->iteration); ?></td>
                                        <td><?php echo e($address->name); ?></td>
                                        <td><?php echo e($address->city->name); ?></td>
                                        <td><?php echo e(date('Y-m-d', strtotime($address->created_at))); ?></td>
                                        <td>
                                            <a href="<?php echo e(url('my-address/'.$address->id)); ?>" class="btn btn-success"><?php echo e(__('global.edit')); ?></a>
                                            <a href="<?php echo e(url('delete-address/'.$address->id)); ?>" class="btn btn-danger" onclick="return confirm('<?php echo e(__("global.sure")); ?>');"><?php echo e(__('global.delete')); ?></a>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    <?php else: ?>
                        <p><?php echo e(__('global.no_addresses')); ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>