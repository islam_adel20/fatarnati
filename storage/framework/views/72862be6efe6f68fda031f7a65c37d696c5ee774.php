<?php $__env->startSection('content'); ?>
<!-- Category Products Area
============================================ -->
<section class="ptb-50" style="padding-bottom: 50px;">
    <div class="container text-center">
        <div id="change_information">
            <h1  id="page_title" style="padding: 50px 0px;"><?php echo e(__('global.my_info')); ?></h1>

            <div class="row" <?php if(!App::isLocale('en')): ?> style="direction: rtl;" <?php endif; ?>>
                <div class="col-md-3">
                    <?php echo $__env->make('profile_side', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                <div class="col-md-9">
                    <form class="form-horizontal box_shadowed_form" role="form" method="POST" action="<?php echo e(url('/change_information')); ?>" id="ajform">
                        <?php if(session()->has('success')): ?>
                            <div class="alert alert-success"><?php echo e(session('success')); ?></div>
                        <?php endif; ?>
                        
                        <div id="ajform_res"></div>
                        <?php echo e(csrf_field()); ?>

                        <div classs="container">
                            <div class="form-group">
                                <label for="name" class="control-label"><?php echo e(__('global.first_name')); ?></label>
                                <input id="name" type="text" class="form-control" name="first_name" value="<?php echo e(Auth::guard('user')->user()->first_name); ?>" required />
                                <?php if($errors->has('first_name')): ?>
                                    <span class="form-text text-danger">
                                        <strong><?php echo e($errors->first('first_name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label for="name" class="control-label"><?php echo e(__('global.last_name')); ?></label>
                                <input id="name" type="text" class="form-control" name="last_name" value="<?php echo e(Auth::guard('user')->user()->last_name); ?>" required />
                                <?php if($errors->has('last_name')): ?>
                                    <span class="form-text text-danger">
                                        <strong><?php echo e($errors->first('last_name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label for="email" class="control-label"><?php echo e(__('global.email')); ?></label>
                                <input id="email" type="email" class="form-control" name="email" value="<?php echo e(Auth::guard('user')->user()->email); ?>" required />
                                <?php if($errors->has('email')): ?>
                                    <span class="form-text text-danger">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            
                            <div class="form-group">
                                <label for="phone" class="control-label"><?php echo e(__('global.phone')); ?></label>
                                <input id="phone" type="text" class="form-control" name="phone" value="<?php echo e(Auth::guard('user')->user()->phone); ?>" required />
                                <?php if($errors->has('phone')): ?>
                                    <span class="form-text text-danger">
                                        <strong><?php echo e($errors->first('phone')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group my-3">
                                <button type="submit" class="button"><?php echo e(__('global.save_info')); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section> 
<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>