<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-user-edit"></i></span>
                <h3 class="kt-portlet__head-title">تعديل المشرف</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(url('admin/admins/'.$admin->id)); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				<?php if($errors->any()): ?>
					<div class="alert alert-danger">
						<ul>
							<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<li><?php echo e($error); ?></li>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</ul>
					</div>
				<?php endif; ?>
				<input type="hidden" name="_method" value="PUT" />
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الأسم</label>
							<input class="form-control" type="text" placeholder="الأسم" name="name" value="<?php echo e($admin->name); ?>" id="name" />
						</div>
						<div class="col-md-6">
							<label>اسم المستخدم</label>
							<input class="form-control" type="text" placeholder="اسم المستخدم" name="user_name" value="<?php echo e($admin->user_name); ?>" id="user_name" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>البريد الألكتروني</label>
							<input class="form-control" type="email"  placeholder="البريد الألكتروني" value="<?php echo e($admin->email); ?>" id="email" name="email" />
						</div>
						<div class="col-md-6">
							<label>الموبايل</label>
							<input class="form-control" type="text"  placeholder="الموبايل" name="phone" value="<?php echo e($admin->phone); ?>" id="phone" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الصورة</label>
							<div class="custom-file">
								<input type="file" class="custom-file-input" id="image" name="image">
								<label class="custom-file-label" for="image">اختار الصورة</label>
							</div>
						</div>
						<div class="col-md-2"><?php if($admin->image != ''): ?><img src="<?php echo e(asset($admin->image)); ?>" /><?php endif; ?></div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الصلاحيات</label>
							<div class="row text-left">
								<?php $__currentLoopData = $permissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $permission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<div class="col-md-4 p-3">
										<p><input type="checkbox" name="permissions[]" value="<?php echo e($permission->id); ?>" class="permissions"
											<?php if(in_array($permission->id, $admin->permissions->pluck('id')->toArray())): ?> checked <?php endif; ?>/> <?php echo e($permission->label); ?></p>
									</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</div>
						</div>

            <?php if(Auth::guard('admin')->user()->is_super): ?>
            <div class="col-md-4">
              <label>صلاحية الأدمن</label>
                <select name="is_super" class="form-control">
                    <option value="0" <?php if($admin->is_super == 0): ?> selected <?php endif; ?>> مشرف</option>
                    <option value="1" <?php if($admin->is_super == 1): ?> selected <?php endif; ?>> مدير </option>
                </select>
            </div>
            <?php endif; ?>
					</div>
				</div>

				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>