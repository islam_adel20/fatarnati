<!doctype html>
<html class="no-js" lang="<?php echo e(App::getLocale()); ?>">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>ASASK</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('site/img/logo/logo.jpg')); ?>">

    <!-- CSS 
    ========================= -->
    <!--bootstrap min css-->
    <link rel="stylesheet" href="<?php echo e(asset('site/css/bootstrap.min.css')); ?>">
    <!--owl carousel min css-->
    <link rel="stylesheet" href="<?php echo e(asset('site/css/owl.carousel.min.css')); ?>">
    <!--slick min css-->
    <link rel="stylesheet" href="<?php echo e(asset('site/css/slick.css')); ?>">
    <!--magnific popup min css-->
    <link rel="stylesheet" href="<?php echo e(asset('site/css/magnific-popup.css')); ?>">
    <!--font awesome css-->
    <link rel="stylesheet" href="<?php echo e(asset('site/css/font.awesome.css')); ?>">
    <!--ionicons css-->
    <link rel="stylesheet" href="<?php echo e(asset('site/css/ionicons.min.css')); ?>">
    <!--linearicons css-->
    <link rel="stylesheet" href="<?php echo e(asset('site/css/linearicons.css')); ?>">
    <!--animate css-->
    <link rel="stylesheet" href="<?php echo e(asset('site/css/animate.css')); ?>">
    <!--jquery ui min css-->
    <link rel="stylesheet" href="<?php echo e(asset('site/css/jquery-ui.min.css')); ?>">
    <!--slinky menu css-->
    <link rel="stylesheet" href="<?php echo e(asset('site/css/slinky.menu.css')); ?>">
    <!--plugins css-->
    <link rel="stylesheet" href="<?php echo e(asset('site/css/plugins.css')); ?>">

    <!-- Main Style CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('site/css/style.css')); ?>">

    <?php if(App::getLocale() == 'ar'): ?>
        <!--bootstrap rtl min css-->
        <!-- <link rel="stylesheet" href="<?php echo e(asset('site/css/bootstrap-rtl.min.css')); ?>"> -->
        <!--rtl css-->
        <!-- <link rel="stylesheet" href="<?php echo e(asset('site/css/rtl.css')); ?>"> -->
        <!--custom rtl css-->
        <link rel="stylesheet" href="<?php echo e(asset('site/css/custom-rtl.css')); ?>">
    <?php else: ?>
        <!-- Custom Style CSS -->
        <link rel="stylesheet" href="<?php echo e(asset('site/css/custom.css')); ?>">
    <?php endif; ?>

    <!--modernizr min js here-->
    <script src="<?php echo e(asset('site/js/vendor/modernizr-3.7.1.min.js')); ?>"></script>
</head>