<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-box"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; أضافة منتج</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(url('admin/products')); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الأسم (EN)</label>
							<input class="form-control" type="text" placeholder="اسم المنتج (EN)" name="title_en" value="<?php echo e(old('title_en')); ?>" id="title_en" required />
							<?php if($errors->has('title_en')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('title_en')); ?></strong></span><?php endif; ?>
						</div>
						<div class="col-md-6">
							<label>الأسم (AR)</label>
							<input class="form-control" type="text" placeholder="اسم المنتج (AR)" name="title_ar" value="<?php echo e(old('title_ar')); ?>" id="title_ar" required />
							<?php if($errors->has('title_ar')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('title_ar')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الصور</label>
							<div class="custom-file">
								<input type="file" class="custom-file-input" id="image" name="image[]" multiple>
								<label class="custom-file-label" for="image">Choose file</label>
							</div>
						</div>

						<div class="col-md-6">
							<label>الصورة الرئيسية</label>
							<div class="custom-file">
								<input type="file" class="custom-file-input" id="image" name="photo" required>
								<label class="custom-file-label" for="image">Choose file</label>
							</div>
						</div>

						<div class="col-md-6">
							<label>التاجر</label>
							<select class="form-control" name="gallery_id" id="gallery">
								<option value="" disabled selected>اختار التاجر</option>
								<?php $__currentLoopData = $galleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($gallery->id); ?>"><?php echo e($gallery->name_en); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>السعر (EGP)</label>
							<input class="form-control" type="text" placeholder="سعر المنتج" name="price" value="<?php echo e(old('price')); ?>" id="title" />
							<?php if($errors->has('price')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('price')); ?></strong></span><?php endif; ?>
						</div>
						<div class="col-md-6">
							<label>الخصم (EGP)</label>
							<input class="form-control" type="text" placeholder="الخصم" name="discount" value="<?php echo e(old('discount')); ?>" id="title" />
							<?php if($errors->has('discount')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('discount')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>القسم الرئيسي</label>
							<select class="form-control" name="cat" id="filter-sub" data-url="<?php echo e(route('filter.subs.category')); ?>" required>
								<option value="" disabled selected>اختار القسم الرئيسي</option>
								<?php $__currentLoopData = $cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($cat->id); ?>"><?php echo e($cat->title_ar); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
						</div>

						<div class="col-md-6">
							<label>القسم الفرعي</label>
							<select class="form-control" name="sub_id" id="cat_subs" required></select>
						</div>
					
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الوصف (EN)</label>
							<textarea class="ckeditor" placeholder="Description (EN)" name="text_en" id="text_en"><?php echo e(old('text_en')); ?></textarea>
							<?php if($errors->has('text_en')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('text_en')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الوصف (AR)</label>
							<textarea class="ckeditor" placeholder="Description (AR)" name="text_ar" id="text_ar"><?php echo e(old('text_ar')); ?></textarea>
							<?php if($errors->has('text_en')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('text_en')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>

				<hr>

				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label><b>Sizes</b></label>
							<button class="btn btn-success float-right" id="add-size">
								<i class="fa fa-plus"></i> &nbsp; أضافة مقاس
							</button>

							<table class="table table-bordered mt-5" id="sizes">
								<thead>
									<tr>
										<th>المقاس</th>
										<th>السعر</th>
										<th>حذف</th>
									</tr>
								</thead>
								<tbody>
									<tr class="sizes-placeholder">
										<td colspan="3">لا يوجد مقاسات حتي الأن</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6">
						<label>أختار الألوان</label>
						<select class="form-control" name="color[]" multiple>
							<option value="" disabled selected>أختار الالوان</option>
							<?php $__currentLoopData = $colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($color->id); ?>" style="background:<?php echo e($color->color); ?>;color:#fff"><?php echo e($color->title); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
					</div>
				</div>

				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
	<script>
		$(document).ready(function(){
			$('#add-size').click(function(){
				let sizeBox = `
					<tr class="size-box">
						<td>
							<select class="form-control" name="sizes[]" required>
								<option value="" disabled selected>Choose Size</option>
								<?php $__currentLoopData = $sizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($size->id); ?>"><?php echo e($size->title); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
						</td>
						<td>
							<input type="number" class="form-control" name="prices[]" min="0" required>
						</td>
						<td>
							<button class="btn btn-danger remove-size">
								<i class="fa fa-times"></i> Delete
							</button>
						</td>
					</tr>
				`;

				// first row !
				if ($('.sizes-placeholder').length > 0) {
					$('#sizes tbody').html('');
				}

				// add the box
				$('#sizes tbody').append(sizeBox);

				// scroll to bottom of the page (glitch)
				$("html, body").animate({ scrollTop: $(document).height() }, 1000);
			});

			$(document).on('click', '.remove-size', function(){
				// add the box
				$(this).closest('td').closest('tr').remove();

				// if no rows !
				if ($('.size-box').length == 0) {
					$('#sizes tbody').append(`
						<tr class="sizes-placeholder">
							<td	td colspan="3">No sizes added yet</td>
						</tr>
					`);
				}
			});
		});
	</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>