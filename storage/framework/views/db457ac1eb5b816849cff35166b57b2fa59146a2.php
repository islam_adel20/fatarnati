<?php $__env->startSection('content'); ?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-boxes"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; المنتجات</h3>
            </div>

        </div>
        <div class="kt-portlet__body text-center">
				<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
				<thead>
					<tr>
						<th>#</th>
						<th>الأسم (EN)</th>
						<th>الأسم (AR)</th>
						<th>القسم الرئيسي</th>
            			<th>القسم الفرعي</th>
						<th>التاجر</th>
						<th> الصورة</th>
						<th>الاعدادات</th>
					</tr>
				</thead>
				<tbody>
					<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr>
							<td><?php echo e($loop->iteration); ?></td>
							<td><?php echo e($product->title_en); ?></td>
							<td><?php echo e($product->title_ar); ?></td>
							<td>
							<!-- <?php if(App\Models\Category::where('id', $product->cat)->where('cat', '!=', 0)->count() > 0): ?>
								<?php echo e($product->category->main['title_ar']); ?>

							<?php else: ?>
								<span style="color:red"> يجب تحديد القسم </span>
							<?php endif; ?> -->
							<?php echo e($product->category['title_ar']); ?>

              				</td>

							<td>
								<!-- <?php if(App\Models\Category::where('id', $product->cat)->count() > 0): ?>
									<?php echo e($product->category['title_ar']); ?>

								<?php else: ?>
									<span style="color:red"> يجب تحديد القسم </span>
								<?php endif; ?> -->
								<?php echo e($product->category_sub['title_ar']); ?>

							</td>
											<td>
								<?php if(App\Models\Gallery::where('id', $product->gallery_id)->count() > 0): ?>
									<?php echo e($product->gallery['name_ar']); ?>

								<?php else: ?>
								<span style="color:red"> يجب تحديد التاجر </span>
								<?php endif; ?>
								
							</td>
							<td> 
								<img src="<?php echo e(asset($product->photo)); ?>" width="100px" height="100px">
							</td>
							<td>
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">
										الأعدادات
									</button>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="<?php echo e(route('products.edit', $product->id)); ?>">تعديل</a>
										<a class="dropdown-item" href="<?php echo e(route('products.show', $product->id)); ?>">الصور</a>
										<a class="dropdown-item" data-toggle="modal" href="#myModal-<?php echo e($product->id); ?>">حذف</a>
									</div>
								</div>

								<div class="modal fade" id="myModal-<?php echo e($product->id); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
								<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">حذف المنتج</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
								<form role="form" action="<?php echo e(url('admin/products/'.$product->id)); ?>" class="" method="POST">
								<input name="_method" type="hidden" value="DELETE">
								<?php echo e(csrf_field()); ?>

								<p>هل أنت متأكد من الحذف ؟</p>
								<button type="submit" class="btn btn-danger" name='delete_modal'>حذف</button>
								<button type="button" class="btn btn-success" data-dismiss="modal">الغاء</button>
								</form>
								</div>
								</div>
								</div>
								</div>
							</td>
						</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</tbody>
			</table>
        </div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>