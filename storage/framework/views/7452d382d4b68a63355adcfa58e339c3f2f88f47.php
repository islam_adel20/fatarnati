<?php $__env->startSection('content'); ?>
    <!-- Page Banner
        ============================================ -->
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area" style="background-image: url('<?php echo e(asset($headerImage->image)); ?>');">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <h3>
                            <?php if(App::getLocale() == 'en'): ?>
                                <?php echo e($product->title_en); ?>

                            <?php else: ?>
                                <?php echo e($product->title_ar); ?>

                            <?php endif; ?>
                        </h3>
                        <ul>
                            <li><a href="index.html"><?php echo e(__('global.home')); ?></a></li>
                            <li>
                                <?php if(App::getLocale() == 'en'): ?>
                                    <?php echo e($product->title_en); ?>

                                <?php else: ?>
                                    <?php echo e($product->title_ar); ?>

                                <?php endif; ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!--product details start-->
    <div class="product_details mt-70 mb-70">
        <div class="container">
            <div class="row add-success" onclick="$(this).hide();">
                <div class="col-md-6 col-12 mx-auto">
                    <div class="alert alert-success text-center">
                        <b id="add-success-message"></b>
                    </div>
                </div>
            </div> 
            
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="product-details-tab">
                        <div id="img-1" class="zoomWrapper single-zoom">
                            <a href="#">
                                <img id="zoom1" src="<?php echo e(asset($product->images[0]->image)); ?>" 
                                    data-zoom-image="<?php echo e(asset($product->images[0]->image)); ?>" alt="big-1">
                            </a>
                        </div>
						<?php if($product->images->count() > 1): ?>
							<div class="single-zoom-thumb">
								<ul class="s-tab-zoom owl-carousel single-product-active" id="gallery_01">
									<?php $__currentLoopData = $product->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<li>
											<a href="#" class="elevatezoom-gallery active" data-update=""
												data-image="<?php echo e(asset($image->image)); ?>">
												<img src="<?php echo e(asset($image->image)); ?>" alt="zo-th-1" />
											</a>
										</li>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</ul>
							</div>
						<?php endif; ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="product_d_right">
                        <form action="#">
                            <h1>
                                <a href="#">
                                    <?php if(App::getLocale() == 'en'): ?>
                                        <?php echo e($product->title_en); ?>

                                    <?php else: ?>
                                        <?php echo e($product->title_ar); ?>

                                    <?php endif; ?>
                                </a>

                                <?php if($product->gallery): ?>
                                    <a href="<?php echo e(url('Gallery/' . $product->gallery->id)); ?>" class="badge gallery-title">
                                        <?php if(App::getLocale() == 'en'): ?>
                                            <?php echo e($product->gallery->name_en); ?>

                                        <?php else: ?>
                                            <?php echo e($product->gallery->name_ar); ?>

                                        <?php endif; ?>
                                    </a>
                                <?php endif; ?>
                            </h1>
                            <?php if(!empty($product->category)): ?>
                                <p class="badge gallery-title">
                                    <a href="<?php echo e(url('Products/' . $product->category->id)); ?>">
                                        <?php if(App::getLocale() == 'en'): ?>
                                            <?php echo e($product->category->title_en); ?>

                                        <?php else: ?>
                                            <?php echo e($product->category->title_ar); ?>

                                        <?php endif; ?>
                                    </a>
                                </p>
                            <?php endif; ?>
                            
                            <div class="price_box">
                                <?php if($product->discount > 0): ?>
                                    <span class="current_price"><?php echo e($product->price - $product->discount); ?> EGP</span>
                                    <span class="old_price"><?php echo e($product->price); ?> EGP</span>
                                <?php else: ?>
                                    <span class="current_price"><?php echo e($product->price); ?> EGP</span>
                                <?php endif; ?>

                            </div>
                            <div class="product_desc">
                                <article>
                                    <?php if(App::getLocale() == 'en'): ?>
                                        <?php echo substr($product->text_en, 0, 150); ?>

                                    <?php else: ?>
                                        <?php echo substr($product->text_ar, 0, 150); ?>

                                    <?php endif; ?>
                                </article>
                            </div>
                            <?php if(count($product->sizes)): ?>
                                <div class="product-sizes mb-3 py-3">
                                    <span class="mx-3"><?php echo e(__('global.sizes')); ?></span>
                                    <span>
                                        <?php $__currentLoopData = $product->sizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <button class="btn btn-primary select-size" 
                                                data-size-id="<?php echo e($size->id); ?>"
                                                data-size-price="<?php echo e($size->price); ?>">
                                                <?php if(App::getLocale() == 'en'): ?>
                                                    <?php echo e($size->size_info->title_en); ?>

                                                <?php else: ?>
                                                    <?php echo e($size->size_info->title_ar); ?>

                                                <?php endif; ?>
                                            </button>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </span>
                                </div>
                            <?php endif; ?>
                            <div class="product_variant quantity">
                                <input type="hidden" name="product" value="<?php echo e($product->id); ?>" id="pro" />
                                <span style="margin-left: 10px;"><?php echo e(__('global.qty')); ?></span>
                                <input id="qty" min="1" max="100" value="1" type="number">
                                <input type="hidden" id="size" value="0">
                                <button class="button" id="add_to_cart" type="button"><?php echo e(__('global.add_to_cart')); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--product details end-->

    <!--product info start-->
    <div class="product_d_info mb-65">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="product_d_inner">
                        <div class="product_info_button">
                            <ul class="nav" role="tablist" id="nav-tab">
                                <li>
                                    <a class="active" data-toggle="tab" href="#info" role="tab" aria-controls="info"
                                        aria-selected="false"><?php echo e(__('global.about_product')); ?></a>
                                </li>
                                
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="info" role="tabpanel">
                                <div class="product_info_content">
                                    <article>
                                        <?php if(App::getLocale() == 'en'): ?>
                                            <?php echo $product->text_en; ?>

                                        <?php else: ?>
                                            <?php echo $product->text_ar; ?>

                                        <?php endif; ?>
                                    </article>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--product info end-->

    <!--product area start-->
    <div class="product_area  container_width color_six mb-65">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title section_title6">
                        <h2><?php echo e(__('global.similar_products')); ?></h2>
                    </div>
                </div>
            </div>
            <div class="product_container">
                <div class="row">
                    <div class="col-12">
                        <div class="product_carousel product_column5 owl-carousel">
                            <?php $__currentLoopData = $similarProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="product_items">
                                    <article class="single_product">
                                        <figure>
                                            <div class="product_thumb">
                                                <a class="primary_img" href="<?php echo e(url('Product/' . $product->id)); ?>">
                                                    <img src="<?php echo e(asset($product->images[0]->image)); ?>"
                                                        alt="<?php echo e($product->title_en); ?>">
                                                </a>
                                                <div class="label_product">
                                                    <?php if($product->discount > 0): ?>
                                                        <span class="label_sale">Sale</span>
                                                    <?php endif; ?>
                                                    <span class="label_new">New</span>
                                                </div>
                                                <div class="action_links">
                                                    <ul>
                                                        <li class="add_to_cart add_to_cart_btn" data-num="<?php echo e($product->id); ?>"><a href="javascript:;"
                                                            data-tippy="<?php echo e(__('global.add_to_cart')); ?>" data-tippy-placement="top"
                                                            data-tippy-arrow="true" data-tippy-inertia="true"> <span
                                                                class="lnr lnr-cart"></span></a></li>
                                                        <li class="quick_button"><a
                                                                href="<?php echo e(url('Product/' . $product->id)); ?>"
                                                                data-tippy="<?php echo e(__('global.show')); ?>" data-tippy-placement="top"
                                                                data-tippy-arrow="true" data-tippy-inertia="true"> <span
                                                                    class="lnr lnr-magnifier"></span></a></li>
                                                        <!--<li class="wishlist"><a href="#" data-tippy="أضف لقائمة المفضلة"-->
                                                        <!--        data-tippy-placement="top" data-tippy-arrow="true"-->
                                                        <!--        data-tippy-inertia="true"><span-->
                                                        <!--            class="lnr lnr-heart"></span></a></li>-->
                                                    </ul>
                                                </div>
                                            </div>
                                            <figcaption class="product_content">
                                                <h4 class="product_name">
                                                    <a href="<?php echo e(url('Product/' . $product->id)); ?>">
                                                        <?php if(App::getLocale() == 'en'): ?>
                                                            <?php echo e($product->title_en); ?>

                                                        <?php else: ?>
                                                            <?php echo e($product->title_ar); ?>

                                                        <?php endif; ?>
                                                    </a>
                                                </h4>
                                                <?php if(!empty($product->category)): ?>
                                                    <p>
                                                        <a href="<?php echo e(url('Products/' . $product->category->id)); ?>">
                                                            <?php if(App::getLocale() == 'en'): ?>
                                                                <?php echo e($product->category->title_en); ?>

                                                            <?php else: ?>
                                                                <?php echo e($product->category->title_ar); ?>

                                                            <?php endif; ?>
                                                        </a>
                                                    </p>
                                                <?php else: ?>
                                                    <p style="height: 12px;"></p>
                                                <?php endif; ?>
                                                <div class="price_box">
                                                    <?php if($product->discount > 0): ?>
                                                        <span
                                                            class="current_price"><?php echo e($product->price - $product->discount); ?>

                                                            EGP</span>
                                                        <span class="old_price"><?php echo e($product->price); ?> EGP</span>
                                                    <?php else: ?>
                                                        <span class="current_price"><?php echo e($product->price); ?> EGP</span>
                                                    <?php endif; ?>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </article>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--product area end-->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>