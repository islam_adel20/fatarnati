<?php $__env->startSection('content'); ?>
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area" style="background-image: url('<?php echo e(asset($headerImage->image)); ?>');">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <h3>
                            <?php if(App::getLocale() == 'en'): ?>
                                <?php echo e($gallery->name_en); ?>

                            <?php else: ?>
                                <?php echo e($gallery->name_ar); ?>

                            <?php endif; ?>
                        </h3>
                        <ul>
                            <li><a href="<?php echo e(url('/')); ?>"><?php echo e(__('global.home')); ?></a></li>
                            <li>
                                <?php if(App::getLocale() == 'en'): ?>
                                    <?php echo e($gallery->name_en); ?>

                                <?php else: ?>
                                    <?php echo e($gallery->name_ar); ?>

                                <?php endif; ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!--gallery section area -->
    <section class="about_section">
        <div class="container">
            <div class="row add-success" onclick="$(this).hide();">
                <div class="col-md-6 col-12 mx-auto">
                    <div class="alert alert-success text-center">
                        <b id="add-success-message"></b>
                    </div>
                </div>
            </div> 
            
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <img class="gallery-logo" src="<?php echo e(asset($gallery->image)); ?>" alt="Gallery Logo">
                    <h3>
                        <?php if(App::getLocale() == 'en'): ?>
                            <?php echo e($gallery->name_en); ?>

                        <?php else: ?>
                            <?php echo e($gallery->name_ar); ?>

                        <?php endif; ?>
                    </h3>
                </div>
            </div>

            <hr>

            <div class="product_container" style="direction: ltr;">
                <div class="row">
                    <div class="col-12">
                        <div class="product_carousel product_column5 owl-carousel">
                            <?php $__currentLoopData = $gallery->products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="product_items">
                                    <article class="single_product">
                                        <figure>
                                            <div class="product_thumb">
                                                <a class="primary_img" href="<?php echo e(url('Product/' . $product->id)); ?>">
                                                    <img src="<?php echo e(asset($product->images[0]->image)); ?>"
                                                        alt="<?php echo e($product->title_en); ?>">
                                                </a>
                                                <div class="label_product">
                                                    <?php if($product->discount > 0): ?>
                                                        <span class="label_sale">Sale</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="action_links">
                                                    <ul>
                                                        <li class="add_to_cart add_to_cart_btn" data-num="<?php echo e($product->id); ?>"><a href="javascript:;"
                                                                data-tippy="<?php echo e(__('global.add_to_cart')); ?>" data-tippy-placement="top"
                                                                data-tippy-arrow="true" data-tippy-inertia="true"> <span
                                                                    class="lnr lnr-cart"></span></a></li>
                                                        <li class="quick_button"><a
                                                                href="<?php echo e(url('Product/' . $product->id)); ?>"
                                                                data-tippy="<?php echo e(__('global.show')); ?>" data-tippy-placement="top"
                                                                data-tippy-arrow="true" data-tippy-inertia="true"> <span
                                                                    class="lnr lnr-magnifier"></span></a></li>
                                                        <!--<li class="wishlist"><a href="#" data-tippy="أضف لقائمة المفضلة"-->
                                                        <!--        data-tippy-placement="top" data-tippy-arrow="true"-->
                                                        <!--        data-tippy-inertia="true"><span-->
                                                        <!--            class="lnr lnr-heart"></span></a></li>-->
                                                    </ul>
                                                </div>
                                            </div>
                                            <figcaption class="product_content">
                                                <h4 class="product_name">
                                                    <a href="<?php echo e(url('Product/' . $product->id)); ?>">
                                                        <?php if(App::getLocale() == 'en'): ?>
                                                            <?php echo e($product->title_en); ?>

                                                        <?php else: ?>
                                                            <?php echo e($product->title_ar); ?>

                                                        <?php endif; ?>
                                                    </a>
                                                </h4>
                                                <div class="price_box">
                                                    <?php if($product->discount > 0): ?>
                                                        <span
                                                            class="current_price"><?php echo e($product->price - $product->discount); ?>

                                                            EGP</span>
                                                        <span class="old_price"><?php echo e($product->price); ?> EGP</span>
                                                    <?php else: ?>
                                                        <span class="current_price"><?php echo e($product->price); ?> EGP</span>
                                                    <?php endif; ?>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </article>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--gallery section end-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>