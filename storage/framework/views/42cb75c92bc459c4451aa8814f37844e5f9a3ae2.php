<?php $__env->startSection('content'); ?>
<!-- Category Products Area
============================================ -->
<section class="ptb-50">
<div class="container text-center" style="padding: 75px 10px;">
    <h1 id="page_title" style="padding: 50px 10px;"><?php echo e(__('global.login')); ?></h1>
    <div class="row">
    <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6 col-xs-offset-0 col-sm-offset-1 col-md-offset-2 offset-lg-3">
    <form class="form-horizontal box_shadowed_form text-center" role="form" method="POST" action="<?php echo e(url('/login')); ?>">
        <?php echo e(csrf_field()); ?>

        <?php if(session()->has('success')): ?>
            <div class="alert alert-success"><?php echo e(session('success')); ?></div>
        <?php endif; ?>

        <?php if($errors->any()): ?>
            <div class="alert alert-danger"><?php echo e($errors->first()); ?></div>
        <?php endif; ?>
        
        <div class="form-group <?php echo e($errors->has('email') ? ' has-error' : ''); ?> row mb-4">
            <label for="email" class="col-md-4 control-label"><?php echo e(__('global.email')); ?></label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" autofocus>
            </div>
        </div>

        <div class="form-group <?php echo e($errors->has('password') ? ' has-error' : ''); ?> row mb-4">
            <label for="password" class="col-md-4 control-label"><?php echo e(__('global.password')); ?></label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control" name="password">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-12">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember"> <?php echo e(__('global.remember')); ?>

                    </label>
                    <a class="btn btn-link btn-inverse ml-auto" href="<?php echo e(url('/register')); ?>">
                        <?php echo e(__('global.new_account')); ?>

                    </a>
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="button">
                <?php echo e(__('global.login')); ?>

            </button>
        </div>
    </form>
</div>
</div>
</div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>