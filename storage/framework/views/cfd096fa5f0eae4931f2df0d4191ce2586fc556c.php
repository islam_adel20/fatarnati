<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fa fa-phone"></i></span>
                <h3 class="kt-portlet__head-title">Contact Information</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form kt-form--label-left" id="kt_form_1" method="post"  action="<?php echo e(url('/admin/contact')); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				
				<?php if($errors->any()): ?>
					<div class="alert alert-danger"><?php echo e($errors->first()); ?></div>
				<?php endif; ?>
	
				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">Address (EN)</label>
					<div class="col-lg-10">
						<input type="text" class="form-control m-input" name="address_en" value="<?php echo e($contact->address_en); ?>" />
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">Address (AR)</label>
					<div class="col-lg-10">
						<input type="text" class="form-control m-input" name="address_ar" value="<?php echo e($contact->address_ar); ?>" />
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">Phone</label>
					<div class="col-lg-10">
						<input type="text" class="form-control m-input" name="phone" value="<?php echo e($contact->phone); ?>" />
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">Email</label>
					<div class="col-lg-10">
						<input type="text" class="form-control m-input" name="email" value="<?php echo e($contact->email); ?>" />
						<?php if($errors->has('email')): ?>
							<span class="help-block"><strong><?php echo e($errors->first('email')); ?></strong></span>
						<?php endif; ?> 
					</div>
				</div>
				
				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">Map</label>
					<div class="col-lg-10">
						<textarea name="map" class="form-control map_textarea" dir="ltr"><?php echo e($contact->map); ?></textarea>
					</div>										
				</div>
				<div class="map_frame"><?php echo $contact->map; ?></div>
					
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-12 text-right">
								<button type="submit" class="btn btn-success">Save</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>					
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>