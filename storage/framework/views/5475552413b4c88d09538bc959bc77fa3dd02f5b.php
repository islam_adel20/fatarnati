<?php $__env->startSection('content'); ?>
<!-- Category Products Area
============================================ -->
<section class="product-page-area ptb-50" style="direction: ltr">
    <div class="container text-center" style="padding: 50px 0px;">
        <h1 style="padding: 50px 0px;">
            <?php echo e(__('global.review_order')); ?>

        </h1>

        <form action="<?php echo e(url('checkout')); ?>" method="post" id="checkout-form">
            <?php echo e(csrf_field()); ?>


            <input type="hidden" name="address" value="<?php echo e($data['address']); ?>">
            <input type="hidden" name="code_id" value="<?php echo e($data['code_id']?? 0); ?>">
            <input type="hidden" name="pay_method" value="<?php echo e($data['pay_method']); ?>">

            <?php if($errors->any()): ?>
                <div class="alert alert-danger"><?php echo e($errors->first()); ?></div>
            <?php endif; ?>

            <div class="row" <?php if(!App::isLocale('en')): ?> style="direction: rtl;" <?php endif; ?>>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-responsive" id="shopping_cart_table">
                            <thead>
                                <tr>
                                    <th class="text-center"><?php echo e(__('global.product')); ?></th>
                                    <th class="text-center"><?php echo e(__('global.sizes')); ?></th>
                                    <th class="text-center"><?php echo e(__('global.qty')); ?></th>
                                    <th class="text-center"><?php echo e(__('global.price')); ?></th>
                                    <th class="text-center"><?php echo e(__('global.total')); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = Cart::content(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="td-product">
                                        <a href="<?php echo e(url('Product/'.$item->options->product)); ?>">
                                            <img src="<?php echo e(asset($item->options->image)); ?>" style="width:150px;height:150px;">
                                            <span>
                                                <?php if(App::getLocale() == 'en'): ?>
                                                    <?php echo e($item->name); ?>

                                                <?php else: ?>
                                                    <?php echo e($item->options->name_ar); ?>

                                                <?php endif; ?>
                                            </span>
                                        </a>
                                    </td>
                                    <td>
                                        <?php if($item->options->size != 0): ?>
                                            <?php if(App::getLocale() == 'en'): ?>
                                                <?php echo e(getSize($item->options->size)->title_en); ?>

                                            <?php else: ?>
                                                <?php echo e(getSize($item->options->size)->title_ar); ?>

                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td><?php echo e($item->qty); ?></td>
                                    <td><?php echo e($item->price); ?>  EGP</td>
                                    <td><?php echo e($item->price * $item->qty); ?>  EGP</td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e(__('global.items_total')); ?></td>
                                <td colspan="2"></td>
                                <td> <?php echo e(Cart::subtotal()); ?> EGP</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <hr>

                    <div class="row" >
                        <div class="col-xs-12 col-md-12 col-lg-4"><p><b><?php echo e(__('global.pay_method')); ?> : </b> 
                            <?php if(App::getLocale() == 'en'): ?>
                                <?php echo e($payMethod->title_en); ?>

                            <?php else: ?>
                                <?php echo e($payMethod->title_ar); ?>

                            <?php endif; ?>
                        </p></div>
                    </div>
            
                    <hr>
            
                    <?php
                        $discount = 0;
                        if (!empty($code))
                            $discount = $code->cost;
                    ?>
                    <div class="row" >
                        <div class="col-xs-12 col-md-12 col-lg-4">
                            <?php if(!empty($code)): ?>
                                <p>
                                    <b><?php echo e(__('global.discount_code')); ?> : </b> 
                                    <?php echo e($code->name); ?>

                                </p>
                                <p>
                                    <b><?php echo e(__('global.discount')); ?> : </b> 
                                    <?php echo e($code->cost??0); ?> EGP
                                </p>
                            <?php endif; ?>
                            <?php if($discount > 0): ?>
                                <p>
                                    <b><?php echo e(__('global.total_discount')); ?> : </b> 
                                    <?php echo e($total - (double)$discount); ?> EGP
                                </p>
                            <?php endif; ?>
                        </div>
                    </div>
            
                    <?php if($tax > 0): ?>
                        <hr>

                        <div class="row" >
                            <div class="col-xs-12 col-md-12 col-lg-4">
                                <p>
                                    <b><?php echo e(__('global.tax')); ?> : </b> 
                                    <?php echo e($tax); ?>%
                                </p>
                                <p>
                                    <b><?php echo e(__('global.tax_amount')); ?> : </b> 
                                    <?php echo e(((($total - (double)$discount)*$tax) / 100)); ?> EGP
                                </p>
                                <p>
                                    <b><?php echo e(__('global.total_tax')); ?> : </b> 
                                    <?php echo e(($total - (double)$discount) + ((($total - (double)$discount)*$tax) / 100)); ?> EGP
                                </p>
                            </div>
                        </div>
                    <?php endif; ?>
            
                    <hr>

                    <div class="row" >
                        <div class="col-xs-4 col-md-4 col-lg-4">
                            <p>
                                <b><?php echo e(__('global.address')); ?> : </b> 
                                <?php echo e($address->name); ?>

                            </p>
                        </div>
                        <div class="col-xs-4 col-md-4 col-lg-4">
                            <p>
                                <b><?php echo e(__('global.city')); ?> : </b> 
                                <?php echo e($address->city->name); ?>

                            </p>
                        </div>
                        <div class="col-xs-4 col-md-4 col-lg-4">
                            <p>
                                <b><?php echo e(__('global.area')); ?> : </b> 
                                <?php echo e($address->area); ?>

                            </p>
                        </div>  
                        <div class="col-xs-4 col-md-4 col-lg-4">
                            <p>
                                <b><?php echo e(__('global.street')); ?> : </b> 
                                <?php echo e($address->street); ?>

                            </p>
                        </div>
                        <div class="col-xs-4 col-md-4 col-lg-4">
                            <p>
                                <b><?php echo e(__('global.building')); ?> : </b> 
                                <?php echo e($address->building); ?>

                            </p>
                        </div>
                        <div class="col-xs-4 col-md-4 col-lg-4">
                            <p>
                                <b><?php echo e(__('global.apartment')); ?> : </b> 
                                <?php echo e($address->apartment); ?>

                            </p>
                        </div>
                        <div class="col-xs-4 col-md-4 col-lg-4">
                            <p>
                                <b><?php echo e(__('global.shipment')); ?> : </b> 
                                <?php echo e($address->city->shipment); ?> EGP
                            </p>
                        </div>
                    </div>
            
                    <hr>

                    <div class="row" >
                        <div class="col-xs-12 col-md-12 col-lg-4">
                            <p>
                                <b><?php echo e(__('global.order_total')); ?> : </b>
                                <?php echo e(($total + ((($total - (double)$discount)*$tax) / 100) - (double)$discount + (double)$address->city->shipment)); ?> EGP
                            </p>
                        </div>
                    </div>
            
                    <hr>

                    <button type="submit" class="button"><?php echo e(__('global.place_order')); ?></button>
                </div>
            </div>
        </form>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>