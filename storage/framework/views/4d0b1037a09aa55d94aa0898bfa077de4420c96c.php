<a href="<?php echo e(url('change_information')); ?>">
    <button class="categories-button my-2">
        <i class="fa fa-user"></i> <?php echo e(__('global.my_info')); ?>

    </button>
</a>
<a href="<?php echo e(url('change_password')); ?>">
    <button class="categories-button my-2">
        <i class="fa fa-key"></i> <?php echo e(__('global.change_pass')); ?>

    </button>
</a>
<a href="<?php echo e(url('my-orders')); ?>">
    <button class="categories-button my-2">
        <i class="fa fa-shopping-bag"></i> <?php echo e(__('global.my_orders')); ?>

    </button>
</a>
<a href="<?php echo e(url('my-addresses')); ?>">
    <button class="categories-button my-2">
        <i class="fa fa-map-marker"></i> <?php echo e(__('global.my_addresses')); ?>

    </button>
</a>
<a href="<?php echo e(url('logout')); ?>">
    <button class="categories-button my-2">
        <i class="fa fa-sign-out"></i> <?php echo e(__('global.logout')); ?>

    </button>
</a>