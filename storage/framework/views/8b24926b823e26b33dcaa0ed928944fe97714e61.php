	<!--footer area start-->
	<footer class="footer_widgets color_six">
		<div class="container_width download-app-banner">
			<div class="row">
				<div class="col-md-6 col-12 left">
					<h3 class="mt-3"><?php echo e(__('global.download_app')); ?></h3>
				</div>
				<div class="col-md-6 col-12 right">
					<a href="#">
						<img src="<?php echo e(asset('site/img/icon/appstore.png')); ?>" alt="AppStore">
					</a>
					<a href="#">
						<img src="<?php echo e(asset('site/img/icon/playstore.png')); ?>" alt="AppStore">
					</a>
				</div>
			</div>
		</div>
		<div class="footer_top container_width pt-3">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-12 col-sm-7">
						<div class="widgets_container contact_us">
							<div class="footer_logo text-center">
								<a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset('site/img/logo/logo.jpg')); ?>" alt=""></a>
							</div>
							<p class="footer_desc text-center">
								<?php if(App::getLocale() == 'en'): ?>
									<?php echo e($footer->text_en); ?>

								<?php else: ?>
									<?php echo e($footer->text_ar); ?>

								<?php endif; ?>
							</p>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3><?php echo e(__('global.info')); ?></h3>
							<div class="footer_menu">
								<ul>
									<li><a href="<?php echo e(url('/About')); ?>"><?php echo e(__('global.about_asask')); ?></a></li>
									<li><a href="<?php echo e(url('/Return-Policy')); ?>"><?php echo e(__('global.return_policy')); ?></a></li>
									<li><a href="<?php echo e(url('/Contact')); ?>"><?php echo e(__('global.call_us')); ?></a></li>
									<li><a href="<?php echo e(url('/Terms-and-Conditions')); ?>"> <?php echo e(__('global.terms')); ?></a></li>
									<li><a href="<?php echo e(url('/Privacy')); ?>"><?php echo e(__('global.privacy')); ?></a></li>
									<li><a href="<?php echo e(url('/Tips-and-Guides')); ?>"><?php echo e(__('global.tips')); ?></a></li>
									<li><a href="<?php echo e(url('/Help-Center')); ?>"><?php echo e(__('global.help_center')); ?></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-4">
						<div class="widgets_container widget_menu">
							<h3><?php echo e(__('global.cats')); ?></h3>
							<div class="footer_menu">
								<ul>
									<?php $__currentLoopData = get_maincats(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<li>
											<a href="<?php echo e(url('Products/' . $mcat->id)); ?>">
												<?php if(App::getLocale() == 'en'): ?>
													<?php echo e($mcat->title_en); ?>

												<?php else: ?>
													<?php echo e($mcat->title_ar); ?>

												<?php endif; ?>
											</a>
										</li>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-8">
						<div class="widgets_container widget_newsletter text-center">
							<h3><?php echo e(__('global.subscribe_title')); ?></h3>
							<p class="footer_desc"><?php echo e(__('global.subscribe_now')); ?></p>
							<div class="subscribe_form">
								<form action="<?php echo e(url('/subscribe')); ?>" method="POST" id="mc-form" class="mc-form footer-newsletter">
									<?php echo e(csrf_field()); ?>

									<input id="mc-email" type="email" name="email" autocomplete="off"
										placeholder="<?php echo e(__('global.email')); ?>" />
									<button type="submit"><?php echo e(__('global.subscribe')); ?></</button>
								</form>
								<!-- mailchimp-alerts Start -->
								<div class="mailchimp-alerts text-centre">
									<div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
									<div class="mailchimp-success"></div><!-- mailchimp-success end -->
									<div class="mailchimp-error"></div><!-- mailchimp-error end -->
								</div><!-- mailchimp-alerts end -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer_bottom footer_bottom_bg container_width">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 col-md-7">
						<div class="copyright_area">
							<?php if(App::getLocale() == 'en'): ?>
								<p>All rights reserved to <b>ASASK</b> &copy; <?php echo e(date('Y')); ?> , Design & develop by 
								<a href="https://matrixclouds.com/ar/" target="_blank"><b>Matrix Clouds</b></a></p>
							<?php else: ?>
								<p>جميع الحقوق محفوظة <b>ASASK</b> &copy; <?php echo e(date('Y')); ?> . تصميم و تطوير <a
										href="https://matrixclouds.com/ar/" target="_blank"><b>Matrix Clouds</b></a></p>
							<?php endif; ?>		
						</div>
					</div>
					<div class="col-lg-6 col-md-5">
						<div class="footer_payment footer-social">
							<ul>
								<?php $__currentLoopData = $socials; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $social): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<li><a href="<?php echo e($social->link); ?>" class="fa fa-<?php echo e($social->name); ?>" target="_blank"></a></li>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--footer area end-->

	<!-- JS
	============================================ -->
	<!--jquery min js-->
	<script src="<?php echo e(asset('site/js/vendor/jquery-3.4.1.min.js')); ?>"></script>
	<!--popper min js-->
	<script src="<?php echo e(asset('site/js/popper.js')); ?>"></script>
	<!--bootstrap min js-->
	<script src="<?php echo e(asset('site/js/bootstrap.min.js')); ?>"></script>
	<!--owl carousel min js-->
	<script src="<?php echo e(asset('site/js/owl.carousel.min.js')); ?>"></script>
	<!--slick min js-->
	<script src="<?php echo e(asset('site/js/slick.min.js')); ?>"></script>
	<!--magnific popup min js-->
	<script src="<?php echo e(asset('site/js/jquery.magnific-popup.min.js')); ?>"></script>
	<!--counterup min js-->
	<script src="<?php echo e(asset('site/js/jquery.counterup.min.js')); ?>"></script>
	<!--jquery countdown min js-->
	<script src="<?php echo e(asset('site/js/jquery.countdown.js')); ?>"></script>
	<!--jquery ui min js-->
	<script src="<?php echo e(asset('site/js/jquery.ui.js')); ?>"></script>
	<!--jquery elevatezoom min js-->
	<script src="<?php echo e(asset('site/js/jquery.elevatezoom.js')); ?>"></script>
	<!--isotope packaged min js-->
	<script src="<?php echo e(asset('site/js/isotope.pkgd.min.js')); ?>"></script>
	<!--slinky menu js-->
	<script src="<?php echo e(asset('site/js/slinky.menu.js')); ?>"></script>
	<!--instagramfeed menu js-->
	<script src="<?php echo e(asset('site/js/jquery.instagramFeed.min.js')); ?>"></script>
	<!-- tippy bundle umd js -->
	<script src="<?php echo e(asset('site/js/tippy-bundle.umd.js')); ?>"></script>
	<!-- Plugins JS -->
	<script src="<?php echo e(asset('site/js/plugins.js')); ?>"></script>

	<!-- Main JS -->
	<script src="<?php echo e(asset('site/js/main.js')); ?>"></script>
	<script src="<?php echo e(asset('site/js/custom.js')); ?>"></script>

	
	<?php if(session()->has('subscribe_success')): ?>
		<script>
			alert("<?php echo e(session('subscribe_success')); ?>");
		</script>
	<?php endif; ?>
</body>

</html>
