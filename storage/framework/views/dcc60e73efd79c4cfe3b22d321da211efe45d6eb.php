<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-list"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; تعديل</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(url('admin/categories/'.$category->id)); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				<input type="hidden" name="_method" value="PUT" />
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الأسم (EN)</label>
							<input class="form-control" type="text" placeholder="الأسم (EN)" name="title_en" value="<?php echo e($category->title_en); ?>" id="title_en" />
							<?php if($errors->has('title_en')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('title_en')); ?></strong></span><?php endif; ?>
						</div>
						<div class="col-md-6">
							<label>الأسم (AR)</label>
							<input class="form-control" type="text" placeholder="الأسم (AR)" name="title_ar" value="<?php echo e($category->title_ar); ?>" id="title_ar" />
							<?php if($errors->has('title_ar')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('title_ar')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<?php if($category->cat !== 0): ?>
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<label>الأقسام</label>
								<select name="cat" class="form-control" id="cat">
									<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<option value="<?php echo e($cat->id); ?>"
											<?php if($category->cat == $cat->id): ?>
												selected
											<?php endif; ?>
										>
											<?php echo e($cat->title_en); ?>

										</option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
								<?php if($errors->has('cat')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('cat')); ?></strong></span><?php endif; ?>
							</div>
						</div>
					</div>
				<?php else: ?>
					<input type="hidden" name="cat" value="0">
				<?php endif; ?>
				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">الصورة</label>
					<div class="col-lg-7">
						<div class="custom-file">
							<input type="file" class="custom-file-input" name="image" id="image" />
							<label class="custom-file-label" for="image">اختار الصورة</label>
						</div>
					</div>
					<div class="col-lg-3">
						<img src="<?php echo e(asset($category->image)); ?>">
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>