<?php $__env->startSection('content'); ?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fa fa-star"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; تجار الخدمات</h3>
            </div>
        </div>
        <div class="kt-portlet__body text-center">
			<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
				<thead>
					<tr>
						<th>#</th>
						<th>الأسم</th>
						<th>الأيميل</th>
						<th>الموبايل</th>
            <th>القسم</th>
            <th>تفاصيل أخري</th>
						<th>الاعدادات</th>
					</tr>
				</thead>
				<tbody>
					<?php $__currentLoopData = $saller_servcies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $saller): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr>
							<td><?php echo e($loop->iteration); ?></td>
							<td><?php echo e($saller->name); ?></td>
							<td><?php echo e($saller->email); ?></td>
							<td><?php echo e($saller->phone); ?></td>
              <td>
                    <?php if(App\Models\CategoryServices::where('id', $saller->cat_id)->count() > 0): ?>
                        <?php echo e($saller->cat['name_ar']); ?>

                    <?php else: ?>
                        <span style="color:red"> يرجي تحديد قسم</span>
                    <?php endif; ?>
              </td>
              <td>

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal<?php echo e($saller->id); ?>">
                  عرض
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal<?php echo e($saller->id); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-body">
                        <?php if($saller->image != null): ?>
                        <img src="<?php echo e(asset($saller->image)); ?>" width="50%" style="display:block;margin:auto">
                        <?php else: ?>
                        <h4 class="alert alert-danger"> لا يوجد صورة مرفقة </h4>
                        <?php endif; ?>
                        <p style="margin-top:20px;font-size:16px">
                           <span style="float:right"> الوصف <i class="fas fa-hand-point-left"></i> </span>
                           <span style="color:blue;"> <?php echo e($saller->desc); ?> </span>
                         </p>
                        <hr>

                        <?php if(App\Models\Region::where('city_id', $saller->city_id)->count() > 0): ?>
                        <p style="margin-top:20px;font-size:16px">
                           <span style="float:right"> المحافظة <i class="fas fa-hand-point-left"></i> </span>
                           <span style="color:blue;"> <?php echo e($saller->city->city['name_ar']); ?> </span>
                         </p>
                        <hr>
                        <p style="margin-top:20px;font-size:16px">
                           <span style="float:right"> المدينة <i class="fas fa-hand-point-left"></i> </span>
                           <span style="color:blue;"> <?php echo e($saller->city['name_ar']); ?> </span>
                         </p>
                        <hr>
                        <?php else: ?>
                        <p style="margin-top:20px;font-size:16px">
                           <span style="float:right"> المدينة <i class="fas fa-hand-point-left"></i> </span>
                           <span style="color:red;"> لم يحدد المدينة </span>
                         </p>
                        <hr>
                        <?php endif; ?>

                        <?php if($saller->facebook_link != null): ?>
                        <a href="<?php echo e($saller->facebook_link); ?>" class="btn btn-primary"> الفيس بوك </a>
                        <?php endif; ?>

                        <?php if($saller->instgram_link != null): ?>
                        <a href="<?php echo e($saller->instgram_link); ?>" class="btn btn-info">الأنستجرام </a>
                        <?php endif; ?>

                        <?php if($saller->twitter_link != null): ?>
                        <a href="<?php echo e($saller->twitter_link); ?>" class="btn btn-success"> تويتر </a>
                        <?php endif; ?>

                        <?php if($saller->youtube_link != null): ?>
                        <a href="<?php echo e($saller->youtube_link); ?>" class="btn btn-danger"> اليوتيوب </a>
                        <?php endif; ?>

                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
                      </div>
                    </div>
                  </div>
                </div>

              </td>
							<td>
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">
										الأعدادات
									</button>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="<?php echo e(route('services.edit', $saller->id)); ?>"><i class="fas fa-edit"></i> تعديل</a>
										<a class="dropdown-item" data-toggle="modal" href="#myModal-<?php echo e($saller->id); ?>"><i class="fas fa-trash"></i> حذف</a>
									</div>

                  <!-- Button trigger modal -->
                  <button class="btn btn-success" data-toggle="modal" data-target="#exampleModalsub<?php echo e($saller->id); ?>">
                    تفاصيل الأشتراك
                  </button>

                  <!-- Modal -->
                  <div class="modal fade" id="exampleModalsub<?php echo e($saller->id); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-body">
                          <?php if($saller->code != null): ?>
                          <h4 class="alert alert-success"> الكود :  <?php echo e($saller->code); ?></h4>
                          <h4 class="alert alert-success"> حالة الأشتراك :
                            <?php $__currentLoopData = App\Models\SallerCode::where('code', $saller->code)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $code): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <?php if($code->status == 'expired'): ?>
                                جاري -

                                  [ <?php echo e($code->from); ?> / <?php echo e($code->to); ?> ]
                              <?php elseif($code->status == 'finish'): ?>
                                منتهي
                              <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </h4>
                          <?php else: ?>
                          <h4 class="alert alert-danger text-center"> لا يوجد أشتراك حاليا</h4>
                          <?php endif; ?>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">الغاء</button>
                        </div>
                      </div>
                    </div>
								</div>

								<div class="modal fade" id="myModal-<?php echo e($saller->id); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
								<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">حذف التاجر</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
								<form role="form" action="<?php echo e(url('admin/sallers/services/'.$saller->id)); ?>" class="" method="POST">
								<input name="_method" type="hidden" value="DELETE">
								<?php echo e(csrf_field()); ?>

								<p>هل أنت متأكد من الحذف ؟ </p>
								<button type="submit" class="btn btn-danger" name='delete_modal'>حذف</button>
								<button type="button" class="btn btn-success" data-dismiss="modal">الغاء</button>
								</form>
								</div>
								</div>
								</div>
								</div>
							</td>
						</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</tbody>
			</table>
    </div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>