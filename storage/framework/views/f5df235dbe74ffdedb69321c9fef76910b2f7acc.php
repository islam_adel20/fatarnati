<?php $__env->startSection('content'); ?>
<!--begin::Portlet-->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Add Social Media
            </h3>
        </div>
    </div>

    <!--begin::Form-->
    <form class="kt-form kt-form--label-left" id="kt_form_1" method="post"  action="<?php echo e(url('/admin/social/')); ?>" enctype="multipart/form-data">
        <?php echo e(csrf_field()); ?>

        <div class="kt-portlet__body">
            <?php if($errors->any()): ?>
				<div class="alert alert-danger"><?php echo e($errors->first()); ?></div>
			<?php endif; ?>
            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">Site</label>
                <div class="col-lg-10">
                    <select name="network" id="network" class="form-control">
                        <option value="" disabled="" selected=""> Select Website</option>
                        <option value="facebook" <?php if(old('network') == 'facebook'): ?> selected <?php endif; ?>>Facebook</option>
                        <option value="twitter" <?php if(old('network') == 'twitter'): ?> selected <?php endif; ?>>Twitter</option>
                        <option value="linkedin" <?php if(old('network') == 'linkedin'): ?> selected <?php endif; ?>>Linkedin</option>
                        <option value="pinterest" <?php if(old('network') == 'pinterest'): ?> selected <?php endif; ?>>Pinterest</option>
                        <option value="instagram" <?php if(old('network') == 'instagram'): ?> selected <?php endif; ?>>Instagram</option>
                        <option value="youtube" <?php if(old('network') == 'youtube'): ?> selected <?php endif; ?>>Youtube</option>
                        <option value="google" <?php if(old('network') == 'google'): ?> selected <?php endif; ?>>Google</option>
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">Link</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control m-input" name="link" value="<?php echo e(old('link')); ?>" />
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 text-right">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

        <!--end::Form-->
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>