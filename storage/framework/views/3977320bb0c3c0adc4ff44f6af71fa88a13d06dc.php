<?php $__env->startSection('content'); ?>
<!--begin::Portlet-->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-list"></i></span>
				<h3 class="kt-portlet__head-title">Edit Section</h3>
			</div>
		</div>
		<div class="kt-portlet__body">
		<!--begin::Form-->
			<form class="m-form m-form--label-align-left" method="post" action="<?php echo e(route('sections.update', $section->id)); ?>" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?>

					<input type="hidden" name="_method" value="PUT" />
					
				<div class="kt-portlet__body">
					<?php if($errors->any()): ?>
						<div class="alert alert-danger"><?php echo e($errors->first()); ?></div>
					<?php endif; ?>
					<?php if(in_array('title_en', $data)): ?>	
						<div class="form-group m-form__group row">
							<label class="col-lg-2 col-form-label">Title (EN)</label>
							<div class="col-lg-10">
								<input type="text" class="form-control m-input" name="title_en" value="<?php echo e($section->title_en); ?>" />
							</div>
						</div>			
					<?php endif; ?>		
					<?php if(in_array('title_ar', $data)): ?>	
						<div class="form-group m-form__group row">
							<label class="col-lg-2 col-form-label">Title (AR)</label>
							<div class="col-lg-10">
								<input type="text" class="form-control m-input" name="title_ar" value="<?php echo e($section->title_ar); ?>" />
							</div>
						</div>			
					<?php endif; ?>		
					<?php if(in_array('text_en', $data)): ?>	
						<div class="form-group m-form__group row">
							<label class="col-lg-2 col-form-label">Text (EN)</label>
							<div class="col-lg-10">
								<textarea type="text" class="form-control m-input" name="text_en"><?php echo e($section->text_en); ?></textarea>
							</div>
						</div>			
					<?php elseif(in_array('editor_en', $data)): ?>	
					<div class="form-group m-form__group row">
							<label class="col-lg-2 col-form-label">Text (EN)</label>
							<div class="col-lg-10">
								<textarea type="text" class="form-control m-input ckeditor" name="text_en"><?php echo e($section->text_en); ?></textarea>
							</div>
						</div>			
					<?php endif; ?>
					<?php if(in_array('text_ar', $data)): ?>	
						<div class="form-group m-form__group row">
							<label class="col-lg-2 col-form-label">Text (AR)</label>
							<div class="col-lg-10">
								<textarea type="text" class="form-control m-input" name="text_ar"><?php echo e($section->text_ar); ?></textarea>
							</div>
						</div>			
					<?php elseif(in_array('editor_ar', $data)): ?>	
					<div class="form-group m-form__group row">
							<label class="col-lg-2 col-form-label">Text (AR)</label>
							<div class="col-lg-10">
								<textarea type="text" class="form-control m-input ckeditor" name="text_ar"><?php echo e($section->text_ar); ?></textarea>
							</div>
						</div>			
					<?php endif; ?>

					<?php if(in_array('link', $data)): ?>				
						<div class="form-group m-form__group row">
							<label class="col-lg-2 col-form-label">Link</label>
							<div class="col-lg-10">
								<input type="text" class="form-control m-input" name="link" value="<?php echo e($section->link); ?>" />
							</div>
						</div>
					<?php endif; ?>
					<?php if(in_array('image', $data)): ?>	
						<div class="form-group m-form__group row">
							<label class="col-lg-2 col-form-label">Image  <small class="d-none">(1920 * 1280)</small></label>
							<div class="col-lg-7">
								<div class="custom-file">
									<input type="file" class="custom-file-input" name="image" id="image" />
									<label class="custom-file-label" for="image">Choose file</label>
								</div>
							</div>
							<div class="col-lg-3">
								<img src="<?php echo e(asset($section->image)); ?>">
							</div>
						</div>	
					<?php endif; ?>		
					<?php if(in_array('number', $data)): ?>	
						<div class="form-group m-form__group row">
							<label class="col-lg-2 col-form-label">Tax (%)</label>
							<div class="col-lg-10">
								<input type="number" class="form-control m-input" name="title_en" min="0" step="1" max="99" value="<?php echo e($section->title_en); ?>" />
							</div>
						</div>			
					<?php endif; ?>	
					</div>

					<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<div class="row">
									<div class="col-lg-12 text-right">
										<button type="submit" class="btn btn-success">Save</button>
									</div>
								</div>
							</div>
						</div>
					</form>
		<!--end::Form-->
		</div>
	</div>
</div>			

<?php $__env->stopSection(); ?>



<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>