<?php $__env->startSection('content'); ?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-images"></i></span>
                <h3 class="kt-portlet__head-title"><?php echo e($product->title); ?> Images</h3>
            </div>
            
            <div class="kt-portlet__head-toolbar">
            	<div class="kt-portlet__head-wrapper">
                	<div class="kt-portlet__head-actions">
                    	<a href="<?php echo e(url('admin/products_images/'.$product->id.'/create')); ?>" class="btn btn-brand btn-elevate btn-icon-sm"><i class="fas fa-plus"></i> New Image</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="kt-portlet__body">

            <!--begin::Section-->
            <div class="kt-section">
                
                <div class="kt-section__content">
                        <div class="row">

                        <?php $__currentLoopData = $product->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-sm-4">
                        <div class="slider_solo">
                        <img src="<?php echo e(asset($slider->image)); ?>" style="height:300px; width: 100%;" />
                        <div class="action_btns" style="background: black; text-align: center; padding: 5px;">
                        <a href="<?php echo e(route('products_images.edit', $slider->id)); ?>" class="btn btn-info btn-sm"><i class="fas fa-edit"></i> Edit</a>
                        <?php if(count($product->images) > 1): ?>
                        <a data-toggle="modal" href="#myModal-<?php echo e($slider->id); ?>" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</a>
                        <?php endif; ?>
                        </div>
                        <?php if(count($product->images) > 1): ?>
                        <div class="modal fade" id="myModal-<?php echo e($slider->id); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Delete Image</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            </button>
                        </div>
                        <div class="modal-body">
                        <form role="form" action="<?php echo e(url('admin/products_images/'.$slider->id)); ?>" class="" method="POST">
                        <input name="_method" type="hidden" value="DELETE">
                        <?php echo e(csrf_field()); ?>

                        <p>Are You Sure?</p>
                        <button type="submit" class="btn btn-danger" name='delete_modal'><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                        </form>
                        </div>
                        </div>
                        </div>
                        </div>
                        <?php endif; ?>               
                        </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        
                </div>
            </div>

            <!--end::Section-->
        </div>

        <!--end::Form-->
    </div>

    <!--end::Portlet-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>