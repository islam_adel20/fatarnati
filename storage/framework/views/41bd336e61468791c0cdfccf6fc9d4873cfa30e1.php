<?php $__env->startSection('content'); ?>
    <!-- begin:: Content -->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon"><i class="kt-font-brand fa fa-users"></i></span>
                    <h3 class="kt-portlet__head-title">العميل: <?php echo e($client->name); ?></h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="form-group">
                    <div class='row'>
                        <div class='col-md-4'>
                            <p><b>الأسم : </b><?php echo e($client->name); ?></p>
                        </div>
                        <div class='col-md-4'>
                            <p><b>الموبايل : </b><?php echo e($client->phone ? $client->phone : 'فارغ'); ?></p>
                        </div>
                        <div class='col-md-4'>
                            <p><b>الأيميل : </b><?php echo e($client->email ? $client->email : 'فارغ'); ?></p>
                        </div>
                        <div class='col-md-4'>
                            <p><b> العنوان : </b><?php echo e($client->address ? $client->address : 'فارغ'); ?></p>
                        </div>

                        <div class='col-md-4'>
                            <p><b>البلد : </b><?php echo e($client->country ? $client->country_user['title'] : 'فارغ'); ?></p>
                        </div>

                        <div class='col-md-4'>
                            <p><b>المدينة : </b><?php echo e($client->city ? $client->region['name_ar'] : 'فارغ'); ?></p>
                        </div>
                    </div>

                    <hr />

                </div>
            </div>
        </div>
	</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>