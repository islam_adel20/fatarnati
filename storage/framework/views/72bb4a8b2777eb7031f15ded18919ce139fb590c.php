<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fa fa-star"></i></span>
                <h3 class="kt-portlet__head-title"> &nbsp; أضافة تاجر </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(url('admin/galleries')); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>


				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الأسم (EN)</label>
							<input class="form-control" type="text" placeholder="الأسم (EN)" name="name_en" value="<?php echo e(old('name_en')); ?>" id="name_en" />
							<?php if($errors->has('name_en')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('name_en')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الأسم (AR)</label>
							<input class="form-control" type="text" placeholder="الأسم (AR)" name="name_ar" value="<?php echo e(old('name_ar')); ?>" id="name_ar" />
							<?php if($errors->has('name_ar')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('name_ar')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الموبايل</label>
							<input class="form-control" type="text" placeholder="الموبايل" name="phone" value="<?php echo e(old('phone')); ?>" id="phone" />
							<?php if($errors->has('phone')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('phone')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>العنوان (EN)</label>
							<input class="form-control" type="text" placeholder="العنوان (EN)" name="address_en" value="<?php echo e(old('address_en')); ?>" id="address_en" />
							<?php if($errors->has('address_en')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('address_en')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>العنوان (AR)</label>
							<input class="form-control" type="text" placeholder="العنوان (AR)" name="address_ar" value="<?php echo e(old('address_ar')); ?>" id="address_ar" />
							<?php if($errors->has('address_ar')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('address_ar')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الخريطة</label>
							<input class="form-control" type="text" placeholder="الخريطة" name="map" value="<?php echo e(old('map')); ?>" id="map" />
							<?php if($errors->has('map')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('map')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>

        <div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>كلمة السر</label>
							<input class="form-control" type="password" placeholder="كلمة السر" name="password" value="<?php echo e(old('address_ar')); ?>" id="address_ar" />
							<?php if($errors->has('password')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('password')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>

				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">الصورة</label>
					<div class="col-lg-10">
						<div class="custom-file">
							<input type="file" class="custom-file-input" name="image" id="image" />
							<label class="custom-file-label" for="image">اختار الصورة</label>
						</div>
					</div>
				</div>

        <div class="form-group m-form__group row">
          <label class="col-lg-2 col-form-label">السجل التجاري</label>
          <div class="col-lg-10">
            <div class="custom-file">
              <input type="file" class="custom-file-input" name="commercial_register" id="image" />
              <label class="custom-file-label" for="image">اختار الصورة</label>
            </div>
          </div>
        </div>

        <div class="form-group m-form__group row">
          <label class="col-lg-2 col-form-label">البطاقة القومية</label>
          <div class="col-lg-10">
            <div class="custom-file">
              <input type="file" class="custom-file-input" name="nationalist" id="image" />
              <label class="custom-file-label" for="image">اختار الصورة</label>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              <label>رابط الفيس بوك</label>
              <input class="form-control" type="text" placeholder="الفيسبوك" name="facebook_link" value="<?php echo e(old('facebook_link')); ?>" id="phone" />
              <?php if($errors->has('facebook_link')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('facebook_link')); ?></strong></span><?php endif; ?>
            </div>


            <div class="col-md-6">
              <label>رابط تويتر</label>
              <input class="form-control" type="text" placeholder="تويتر" name="twitter_link" value="<?php echo e(old('twitter_link')); ?>" id="phone" />
              <?php if($errors->has('twitter_link')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('twitter_link')); ?></strong></span><?php endif; ?>
            </div>

            <div class="col-md-6">
              <label>رابط الأنستجرام</label>
              <input class="form-control" type="text" placeholder="انتسجرام" name="instgram_link" value="<?php echo e(old('instgram_link')); ?>" id="phone" />
              <?php if($errors->has('instgram_link')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('instgram_link')); ?></strong></span><?php endif; ?>
            </div>


            <div class="col-md-6">
              <label>رابط اليوتيوب</label>
              <input class="form-control" type="text" placeholder="اليوتيوب" name="youtube_link" value="<?php echo e(old('youtube_link')); ?>" id="phone" />
              <?php if($errors->has('youtube_link')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('youtube_link')); ?></strong></span><?php endif; ?>
            </div>
          </div>
        </div>

				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>