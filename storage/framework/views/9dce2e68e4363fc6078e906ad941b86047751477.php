<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-bell"></i></span>
                <h3 class="kt-portlet__head-title">Edit Status</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(route('terms.update', $term->id)); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				<input type="hidden" name="_method" value="PUT" />
        <div class="form-group">
          <div class="row">
            <div class="col-md-12">
              <label> الشروط و الأحكام (AR)</label>
              <textarea class="form-control ckeditor" rows="7" name="content_ar">
                  <?php echo $term->content_ar; ?>

              </textarea>
              <?php if($errors->has('content_ar')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('content_ar')); ?></strong></span><?php endif; ?>
            </div>

            <div class="col-md-12">
              <hr>
              <label>  الشروط و الأحكام (EN)</label>
              <textarea class="form-control ckeditor" rows="7" name="content_en">
                    <?php echo $term->content_en; ?>

              </textarea>
              <?php if($errors->has('content_en')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('content_en')); ?></strong></span><?php endif; ?>
            </div>

            <div class="col-md-12">
              <hr>
              <label>   تحديد اتجاه الشروط و الأحكام</label>
              <select class="form-control" name="type">
                  <option value="saller" <?php if($term->type == 'saller'): ?> selected <?php endif; ?>> الي التاجر </option>
                  <option value="user" <?php if($term->type == 'user'): ?> selected <?php endif; ?>> الي العميل </option>
              </select>
              <?php if($errors->has('type')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('type')); ?></strong></span><?php endif; ?>
            </div>
          </div>
        </div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>