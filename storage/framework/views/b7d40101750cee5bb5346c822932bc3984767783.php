<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-user"></i></span>
                <h3 class="kt-portlet__head-title">معلومات البروفايل</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(url('admin/profile')); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				<?php if($errors->any()): ?>
					<div class="alert alert-danger">
						<ul>
							<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<li><?php echo e($error); ?></li>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</ul>
					</div>
				<?php endif; ?>
				<?php if(session('alert_message')): ?>
				<div class="alert alert-success"><?php echo e(session('alert_message')); ?></div>
				<?php endif; ?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الأسم</label>
							<input class="form-control" type="text" placeholder="الأسم" name="name" value="<?php echo e(Auth::guard('admin')->user()->name); ?>" id="name" />
						</div>
						<div class="col-md-6">
							<label>اسم المستخدم</label>
							<input class="form-control" type="text" placeholder="اسم المستخدم" name="user_name" value="<?php echo e(Auth::guard('admin')->user()->user_name); ?>" id="user_name" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>البريد الألكتروني</label>
							<input class="form-control" type="email"  placeholder="البريد الألكتروني" value="<?php echo e(Auth::guard('admin')->user()->email); ?>" id="email" name="email" />
						</div>
						<div class="col-md-6">
							<label>الموبايل</label>
							<input class="form-control" type="text"  placeholder="الموبايل" name="phone" value="<?php echo e(Auth::guard('admin')->user()->phone); ?>" id="phone" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الصورة</label>
							<div class="custom-file">
								<input type="file" class="custom-file-input" id="image" name="image">
								<label class="custom-file-label" for="image">اختار الصورة</label>
							</div>
						</div>
						<div class="col-md-2"><?php if(Auth::guard('admin')->user()->image != ''): ?><img src="<?php echo e(asset(Auth::guard('admin')->user()->image)); ?>" /><?php endif; ?></div>
					</div>
				</div>


				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>