<?php $__env->startSection('content'); ?>
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area" style="background-image: url('<?php echo e(asset($headerImage->image)); ?>');">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <h3><?php echo e(__('global.cats')); ?></h3>
                        <ul>
                            <li><a href="<?php echo e(url('/')); ?>"><?php echo e(__('global.home')); ?></a></li>
                            <li><?php echo e(__('global.cats')); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!--categories section area -->
    <section class="about_section">
        <div class="container">
            <div class="row">
                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4 col-12">
                        <a class="cat_img" href="<?php echo e(url('/Products/' . $category->id)); ?>">
                            <img src="<?php echo e(asset($category->image)); ?>" alt="">
                        </a>
                        <h4 class="product_name text-center">
                            <a href="<?php echo e(url('/Products/' . $category->id)); ?>">
                                <?php if(App::getLocale() == 'en'): ?>
                                    <?php echo e($category->title_en); ?>

                                <?php else: ?>
                                    <?php echo e($category->title_ar); ?>

                                <?php endif; ?>
                            </a>
                        </h4>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </section>
    <!--categories section end-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>