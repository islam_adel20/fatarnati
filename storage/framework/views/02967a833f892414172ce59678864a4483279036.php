<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-pic"></i></span>
                <h3 class="kt-portlet__head-title">Add Partner</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(url('admin/galleries')); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">	
							<label>Name (EN)</label>												
							<input class="form-control" type="text" placeholder="Name (EN)" name="name_en" value="<?php echo e(old('name_en')); ?>" id="name_en" />
							<?php if($errors->has('name_en')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('name_en')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">	
							<label>Name (AR)</label>												
							<input class="form-control" type="text" placeholder="Name (AR)" name="name_ar" value="<?php echo e(old('name_ar')); ?>" id="name_ar" />
							<?php if($errors->has('name_ar')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('name_ar')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">	
							<label>Phone</label>												
							<input class="form-control" type="text" placeholder="Phone" name="phone" value="<?php echo e(old('phone')); ?>" id="phone" />
							<?php if($errors->has('phone')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('phone')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">	
							<label>Address (EN)</label>												
							<input class="form-control" type="text" placeholder="Address (EN)" name="address_en" value="<?php echo e(old('address_en')); ?>" id="address_en" />
							<?php if($errors->has('address_en')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('address_en')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">	
							<label>Address (AR)</label>												
							<input class="form-control" type="text" placeholder="Address (AR)" name="address_ar" value="<?php echo e(old('address_ar')); ?>" id="address_ar" />
							<?php if($errors->has('address_ar')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('address_ar')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">	
							<label>Map</label>												
							<input class="form-control" type="text" placeholder="Map" name="map" value="<?php echo e(old('map')); ?>" id="map" />
							<?php if($errors->has('map')): ?><span class="form-text text-danger"><strong><?php echo e($errors->first('map')); ?></strong></span><?php endif; ?>
						</div>
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">Image</label>
					<div class="col-lg-10">
						<div class="custom-file">
							<input type="file" class="custom-file-input" name="image" id="image" />
							<label class="custom-file-label" for="image">Choose file</label>
						</div>
					</div>
				</div>
				
				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>					
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>