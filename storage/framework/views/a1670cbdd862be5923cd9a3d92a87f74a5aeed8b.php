<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-user-plus"></i> </span>
                <h3 class="kt-portlet__head-title">أضافة مشرف</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(url('admin/admins')); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				<?php if($errors->any()): ?>
					<div class="alert alert-danger">
						<ul>
							<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<li><?php echo e($error); ?></li>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</ul>
					</div>
				<?php endif; ?>

				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الأسم</label>
							<input class="form-control" type="text" placeholder="الأسم" name="name" value="<?php echo e(old('name')); ?>" id="name" />
						</div>
						<div class="col-md-6">
							<label>اسم المستخدم</label>
							<input class="form-control" type="text" placeholder="اسم المستخدم" name="user_name" value="<?php echo e(old('user_name')); ?>" id="user_name" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>البريد الألكتروني</label>
							<input class="form-control" type="email"  placeholder="البريد الألكتروني" value="<?php echo e(old('email')); ?>" id="email" name="email" />
						</div>
						<div class="col-md-6">
							<label>الموبايل</label>
							<input class="form-control" type="text"  placeholder="الموبايل" name="phone" value="<?php echo e(old('phone')); ?>" id="phone" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>الصورة</label>
							<div class="custom-file">
								<input type="file" class="custom-file-input" id="image" name="image">
								<label class="custom-file-label" for="image">اختار صورة</label>
							</div>
						</div>

					</div>
				</div>


				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>كلمة السر</label>
							<input class="form-control" type="password" placeholder="كلمة السر" name="password" value="" id="password" />
						</div>
						<div class="col-md-6">
							<label>تأكيد كلمة السر</label>
							<input class="form-control" type="password" placeholder="تأكيد كلمة السر" name="password_confirmation" value="" id="password_confirmation" />
						</div>
					</div>
				</div>

				<hr>

				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<label>الصلاحيات</label>
							<br><br>
							<p>
								<input type="checkbox" onchange="$('.permissions').attr('checked', !$('.permissions').attr('checked'));" /> Select All Permissions
							</p>
							<div class="row text-left">
								<?php $__currentLoopData = $permissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $permission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<div class="col-md-4 p-3">
										<p><input type="checkbox" name="permissions[]" value="<?php echo e($permission->id); ?>" class="permissions" /> <?php echo e($permission->label); ?></p>
									</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</div>
						</div>
					</div>
				</div>

				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>