<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-image"></i></span>
                <h3 class="kt-portlet__head-title">أضافة صورة</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<form class="kt-form" method="post" action="<?php echo e(url('admin/slider')); ?>" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>

				<?php if($errors->any()): ?>
					<div class="alert alert-danger">
						<ul>
							<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<li><?php echo e($error); ?></li>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</ul>
					</div>
				<?php endif; ?>

				<div class="form-group m-form__group row">
					<div class="col-lg-4">
					     <label>صورة الأعلان</label>
						<div class="custom-file">
							<input type="file" class="custom-file-input" id="image" name="image">
							<label class="custom-file-label" for="image">أختار صورة</label>
						</div>
					</div>
					
					<div class="col-lg-4">
						<div class="form-group">
						    <label>عنوان الأعلان</label>
							<input type="text" class="form-control" name="title">
						</div>
					</div>
					
						<div class="col-lg-4">
						<div class="form-group">
						    <label>رابط الأعلان</label>
							<input type="text" class="form-control" name="link">
						</div>
					</div>
					
				</div>
			</div>



				<div class="kt-portlet__foot">
					<div class="kt-form__actions text-right">
						<button type="submit" class="btn btn-success">حفظ</button>
					</div>
				</div>
			</form>
		<!--end::Form-->
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>