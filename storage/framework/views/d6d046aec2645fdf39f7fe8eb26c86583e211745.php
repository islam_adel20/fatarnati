<?php $__env->startSection('content'); ?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-user"></i></span>
                <h3 class="kt-portlet__head-title">Clients</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
			<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Mobile</th>
						<th>Joined At</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>	
						<tr>
							<td><?php echo e($loop->iteration); ?></td>
							<td><?php echo e($client->name); ?></td>
							<td><?php echo e($client->phone); ?></td>
							<td><?php echo e(date('Y-m-d', strtotime($client->created_at))); ?></td>
							<td>
								<a href="<?php echo e(route('clients.show', $client->id)); ?>" class="btn btn-success">
									<i class="fa fa-eye"></i> Show
								</a>
							</td>
						</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</tbody>
			</table>
        </div>
	</div>
</div>					
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>