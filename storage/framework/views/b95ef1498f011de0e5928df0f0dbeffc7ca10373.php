<?php $__env->startSection('content'); ?>
	<!--begin::Portlet-->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Social Media</h3>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin::Section-->
            <div class="kt-section">
                
                <div class="kt-section__content">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                <th>#</th>
                                <th>Site</th>
                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $socail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $section): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <tr>
                                        <td><?php echo e($loop->iteration); ?></td>
                                         <td class="social_media_icon">
                                         <?php if($section->name == 'facebook'): ?>
                                            <i class="fab fa-<?php echo e($section->name); ?>-f"></i>
                                        <?php elseif($section->name == 'linkedin'): ?>
                                            <i class="fab fa-<?php echo e($section->name); ?>-in"></i>
                                        <?php elseif($section->name == 'rss'): ?>
                                            <i class="fas fa-<?php echo e($section->name); ?>"></i>
                                        <?php elseif($section->name == 'google-plus'): ?>
                                            <i class="fab fa-<?php echo e($section->name); ?>-g"></i>
                                        <?php else: ?>
                                            <i class="fab fa-<?php echo e($section->name); ?>"></i>
                                         <?php endif; ?>
                                         </td>
                                         <td>
                                            <button class="btn btn-brand dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" 
                                            aria-haspopup="true" aria-expanded="true">Action</button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="<?php echo e(route('social.edit', $section->id)); ?>"><i class="fa fa-edit"></i> Edit</a>
                                                <a class="dropdown-item" data-toggle="modal" href="#myModal-<?php echo e($section->id); ?>"><i class="fa fa-trash"></i> Delete</a>
                                            </div>
                                                <div class="modal fade" id="myModal-<?php echo e($section->id); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                <div class="modal-content">
                                                <div class="modal-body">
                                                <form role="form" action="<?php echo e(url('/admin/social/'.$section->id)); ?>" class="" method="POST">
                                                <input name="_method" type="hidden" value="DELETE">
                                                <?php echo e(csrf_field()); ?>

                                                <p>are you sure</p>
                                                <button type="submit" class="btn btn-danger" name='delete_modal'><i class="fa fa-trash" aria-hidden="true"></i> delete</button>
                                                </form>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!--end::Section-->
        </div>

        <!--end::Form-->
    </div>

        <!--end::Portlet-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>