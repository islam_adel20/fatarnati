<!--mini cart-->
<div class="mini_cart" style="overflow-y: scroll;">
    <div class="cart_gallery">
        <div class="cart_close">
            <div class="cart_text">
                <h3><?php echo e(__('global.cart')); ?></h3>
            </div>
            
        </div>
        <?php if(Cart::content()->count() > 0): ?>
            <?php $__currentLoopData = Cart::content(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ff): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="cart_item">
                    <div class="cart_img">
                        <a href="<?php echo e(url('Product/'.$ff->options->product)); ?>">
                            <img src="<?php echo e(asset($ff->options->image)); ?>" alt="">
                        </a>
                    </div>
                    <div class="cart_info">
                        <a href="<?php echo e(url('Product/'.$ff->options->product)); ?>">
                            <?php if(App::getLocale() == 'en'): ?>
                                <?php echo e($ff->name); ?>

                            <?php else: ?>
                                <?php echo e($ff->options->name_ar); ?>

                            <?php endif; ?>
                        </a>
                        <p><?php echo e($ff->qty); ?> x <span> <?php echo e($ff->price); ?> EGP </span></p>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php else: ?> 
            <p class="text-center"><b><?php echo e(__('global.sorry_no_items')); ?></b></p>
        <?php endif; ?>
    </div>
    <div class="mini_cart_table">
        <div class="cart_table_border">
            <div class="cart_total mt-10">
                <span><?php echo e(__('global.total')); ?>:</span>
                <span class="price"><?php echo e(Cart::subtotal()); ?> EGP</span>
            </div>
        </div>
    </div>
    <div class="mini_cart_footer">
        <div class="cart_button">
            <a href="<?php echo e(url('Shopping-Cart')); ?>"><i class="fa fa-shopping-cart"></i> <?php echo e(__('global.cart')); ?></a>
        </div>
        <div class="cart_button">
            <a href="<?php echo e(url('Checkout')); ?>"><i class="fa fa-sign-in"></i> <?php echo e(__('global.checkout')); ?></a>
        </div>
    </div>
</div>
<!--mini cart end-->
