<?php $__env->startSection('content'); ?>
    <!--begin::Portlet-->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon"><i class="fab fa-facebook"></i></span>
                    <h3 class="kt-portlet__head-title">
                        تعديل السوشيال ميديا
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
            <form class="kt-form kt-form--label-left" id="kt_form_1" method="post"
                action="<?php echo e(url('/admin/social/' . $social->id)); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <input type="hidden" name="_method" value="PUT" />

                <div class="kt-portlet__body">
                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger"><?php echo e($errors->first()); ?></div>
                    <?php endif; ?>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">مواقع السوشيال ميديا</label>
                        <div class="col-lg-10">
                            <select name="network" id="network" class="form-control">
                                <option value="" disabled="" selected=""> أختار السوشيال ميديا</option>
                                <option value="facebook" <?php if($social->name == 'facebook'): ?> selected <?php endif; ?>>اللفيس بوك</option>
                                <option value="twitter" <?php if($social->name == 'twitter'): ?> selected <?php endif; ?>>تويتر</option>
                                <option value="linkedin" <?php if($social->name == 'linkedin'): ?> selected <?php endif; ?>>لينكد ان</option>
                                <option value="instagram" <?php if($social->name == 'instagram'): ?> selected <?php endif; ?>>الأنستجرام</option>
                                <option value="youtube" <?php if($social->name == 'youtube'): ?> selected <?php endif; ?>>اليوتيوب</option>
                                <option value="google" <?php if($social->name == 'google'): ?> selected <?php endif; ?>>جوجل</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">الرابط</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control m-input" name="link" value="<?php echo e($social->link); ?>" />
                            <?php if($errors->has('link')): ?>
                                <span class="help-block"><strong><?php echo e($errors->first('link')); ?></strong></span>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <button type="submit" class="btn btn-success">حفظ</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>