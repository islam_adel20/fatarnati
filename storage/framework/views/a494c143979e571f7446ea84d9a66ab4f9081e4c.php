<?php $__env->startSection('content'); ?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
			       <div class="kt-portlet__head-label">
				        <span class="kt-portlet__head-icon"><i class="kt-font-brand fas fa-shopping-bag"></i></span>
                <h3 class="kt-portlet__head-title">تفاصيل الطلب</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
				<div class="form-group">
					<div class='row'>
						<div class='col-md-4'><p><b>الأسم : </b><?php echo e($order->client_info->name); ?></p></div>
						<div class='col-md-4'><p><b>الموبايل : </b><?php echo e($order->client_info->phone); ?></p></div>
						<div class='col-md-4'><p><b>البريد الألكتروني : </b><?php echo e($order->client_info->email); ?></p></div>
						<div class='col-md-4'><p><b>المدينة : </b><?php echo e($order->address->city->name_en); ?></p></div>
						<div class='col-md-4'><p><b>المنطقة : </b><?php echo e($order->address->area); ?></p></div>
						<div class='col-md-4'><p><b>الشارع : </b><?php echo e($order->address->street); ?></p></div>
						<div class='col-md-4'><p><b>المبني : </b><?php echo e($order->address->building); ?></p></div>
						<div class='col-md-4'><p><b>الشقة : </b><?php echo e($order->address->apartment); ?></p></div>
						<div class='col-md-4'><p><b>الشحن : </b><?php echo e($order->address->city->shipment); ?> EGP</p></div>
						<div class='col-md-4'><p><b>الخصم : </b><?php echo e($order->discount); ?> EGP</p></div>
						<div class='col-md-4'><p><b>ضريبة : </b><?php echo e($order->tax_value); ?> EGP</p></div>
						<div class='col-md-4'><p><b>طريقة الدفع : </b><?php echo e($order->pay_info->title_en); ?></p></div>
						<div class='col-md-4'>
							<select name="status" class="form-control selling_order_status" num="<?php echo e($order->id); ?>" url="<?php echo e(url('admin/order_status')); ?>">
								<?php $__currentLoopData = $statuss; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($status->id); ?>" <?php if($status->id == $order->status): ?> selected <?php endif; ?>><?php echo e($status->title); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
					</div>
				</div>
				<hr />

				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>المنتج</th>
							<th>المقاس</th>
							<th>العدد</th>
							<th>السعر</th>
							
							<th>الأجمالي</th>
						</tr>
					</thead>
					<tbody>
						<?php $__currentLoopData = $order->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($loop->iteration); ?></td>
								<td><?php echo e($item->product_info->title_en); ?></td>
								<td><?php if($item->size): ?> <?php echo e($item->sizeInfo->size_info->title_en); ?> <?php endif; ?></td>
								<td><?php echo e($item->qty); ?></td>
								<td><?php echo e($item->price); ?> EGP</td>
								
								<td><?php echo e($item->qty * $item->price); ?> EGP</td>
							</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<tr>
						<td colspan="5"></td>
						<td><?php echo e($order->sub_total); ?> EGP</td>
					</tr>
				</table>

		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>