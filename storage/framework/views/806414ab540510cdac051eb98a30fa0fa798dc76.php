<?php $__env->startSection('content'); ?>
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area" style="background-image: url('<?php echo e(asset($headerImage->image)); ?>');">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <h3><?php echo e(__('global.call_us')); ?></h3>
                        <ul>
                            <li><a href="<?php echo e(url('/')); ?>"><?php echo e(__('global.home')); ?></a></li>
                            <li><?php echo e(__('global.call_us')); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!--contact map start-->
    <div class="contact_map">
        <div class="map-area">
            <?php echo $contact_info->map; ?>

        </div>
    </div>
    <!--contact map end-->

    <!--contact area start-->
    <div class="contact_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="contact_message content">
                        <h3><?php echo e(__('global.call_us')); ?></h3>
                        <article>
							<?php if(App::getLocale() == 'en'): ?>
                                <?php echo $footer->text_en; ?>

                            <?php else: ?>
                                <?php echo $footer->text_ar; ?>

                            <?php endif; ?>
						</article>
						<br>
                        <ul>
                            <li><i class="fa fa-map-marker"></i>
                                <?php if(App::getLocale() == 'en'): ?>
                                    <?php echo e($contact_info->address_en); ?>

                                <?php else: ?>
                                    <?php echo e($contact_info->address_ar); ?>

                                <?php endif; ?>
                            </li>
                            <li><i class="fa fa-envelope-o"></i> <a href="#"> <?php echo e($contact_info->email); ?></a></li>
                            <li><i class="fa fa-phone"></i><a href="#"> <?php echo e($contact_info->phone); ?></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="contact_message account_form form">
                        <h3><?php echo e(__('global.write_message')); ?></h3>

                        <?php if(session()->has('success')): ?>
                            <div class="alert alert-success"><?php echo e(session('success')); ?></div>
                        <?php endif; ?>

                        <?php if($errors->any()): ?>
                            <div class="alert alert-danger">
                                <strong><?php echo e($errors->first()); ?></strong>
                            </div>
                        <?php endif; ?>

                        <form method="POST" action="<?php echo e(url('Contact')); ?>">
                            <?php echo e(csrf_field()); ?>


                            <p>
                                <label><?php echo e(__('global.name')); ?></label>
                                <input name="name" placeholder="<?php echo e(__('global.name')); ?> *" type="text" required>
                            </p>
                            <p>
                                <label><?php echo e(__('global.email')); ?></label>
                                <input name="email" placeholder="<?php echo e(__('global.email')); ?> *" type="email" required>
                            </p>
                            <p>
                                <label><?php echo e(__('global.phone')); ?></label>
                                <input name="phone" placeholder="<?php echo e(__('global.phone')); ?> *" type="text" required>
                            </p>
                            <p>
                                <label><?php echo e(__('global.subject')); ?></label>
                                <input name="subject" placeholder="<?php echo e(__('global.subject')); ?> *" type="text" required>
                            </p>
                            <div class="contact_textarea">
                                <label><?php echo e(__('global.msg_body')); ?></label>
                                <textarea placeholder="<?php echo e(__('global.msg_body')); ?> *" name="message" class="form-control2"
                                    required></textarea>
                            </div>
                            <button type="submit"><?php echo e(__('global.msg_send')); ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--contact area end-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('include.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>