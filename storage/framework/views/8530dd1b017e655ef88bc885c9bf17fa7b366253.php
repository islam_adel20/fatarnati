	<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title> فترنتي</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
				active: function() {sessionStorage.fonts = true;}
			});
		</script>
		<link rel="stylesheet" href="<?php echo e(asset('ckeditor/css/samples.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('ckeditor/toolbarconfigurator/lib/codemirror/neo.css')); ?>">

		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/tether/dist/css/tether.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/select2/dist/css/select2.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/nouislider/distribute/nouislider.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/dropzone/dist/dropzone.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/summernote/dist/summernote.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/animate.css/animate.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/toastr/build/toastr.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/morris.js/morris.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/sweetalert2/dist/sweetalert2.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/general/socicon/css/socicon.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/custom/vendors/line-awesome/css/line-awesome.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/custom/vendors/flaticon/flaticon.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/custom/vendors/flaticon2/flaticon.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/custom/vendors/fontawesome5/css/all.min.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/vendors/custom/datatables/datatables.bundle.css')); ?>">

		<!--begin::Global Theme Styles(used by all pages) -->
        <link rel="stylesheet" href="<?php echo e(asset('assets/demo/default/base/style.bundle.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('assets/demo/default/base/custom.css')); ?>">
        <!--end::Global Theme Styles -->
        <!--begin::Layout Skins(used by all pages) -->
		<link rel="stylesheet" href="<?php echo e(asset('assets/demo/default/skins/header/base/light.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/demo/default/skins/header/menu/light.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/demo/default/skins/brand/dark.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/demo/default/skins/aside/dark.css')); ?>">
		<!--end::Layout Skins -->
		<link rel="stylesheet" href="<?php echo e(asset('css/custom_ar.css')); ?>">
		<link href="<?php echo e(asset('favicon.ico')); ?>" rel="shortcut icon" type="image/vnd.microsoft.icon">
		<link rel="shortcut icon" type="image/ico" href="<?php echo e(asset('session1.png')); ?>" />
		<!-- Include this in your blade layout -->
	 <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
		<?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<!-- begin:: Page -->

		<!-- begin:: Header Mobile -->
		<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
			<div class="kt-header-mobile__logo" style="background:#fff;width:50%;overflow:hidden;transform:translateY(0px)">
				<a href="<?php echo e(url('/admin')); ?>">
					<img alt="Logo" src="<?php echo e(asset('site/img/vatrenti.png')); ?>" />
				</a>
			</div>
			<div class="kt-header-mobile__toolbar">
				<button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
			</div>
		</div>
		<!-- end:: Header Mobile -->

		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<!--------------------- Start Side Bar --------------------->
			<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
			<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
				<div class="kt-aside__brand kt-grid__item " id="kt_aside_brand" kt-hidden-height="65" style="">
					<div class="" style="background:#fff;width:100%;">
						<a href="<?php echo e(url('admin')); ?>">
						<img alt="Logo" src="<?php echo e(asset('site/img/vatrenti.png')); ?>" />
						</a>
					</div>
					<div class="kt-aside__brand-tools">
						<button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
							<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<polygon id="Shape" points="0 0 24 0 24 24 0 24"></polygon>
										<path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) "></path>
										<path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) "></path>
									</g>
								</svg></span>
							<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<polygon id="Shape" points="0 0 24 0 24 24 0 24"></polygon>
										<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero"></path>
										<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
									</g>
								</svg></span>
						</button>


					</div>
				</div>
				<!--------------------- Navigation Side Bar ------------------------------------------>
				<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
					<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
						<ul class="kt-menu__nav">
							<!--------------------- Dashboard --------------------->
							<li class="kt-menu__item" aria-haspopup="true">
								<a href="<?php echo e(url('/admin')); ?>" class="kt-menu__link ">
									<span class="kt-menu__link-icon"><i class="fa fa-tachometer-alt"></i></span>
									<span class="kt-menu__link-text">الصفحة الرئيسية</span>
									<span class="kt-menu__link-badge"></span>
								</a>
							</li>

							<?php if(Auth::guard('admin')->user()->is_super): ?>
								<!--------------------- Admins --------------------->
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
										<span class="kt-menu__link-icon"><i class="fa fa-user-alt"></i></span>
										<span class="kt-menu__link-text">المشرفين</span>
										<span class="kt-menu__link-badge"></span>
										<i class="kt-menu__ver-arrow la la-angle-right"></i>
									</a>
									<div class="kt-menu__submenu">
										<span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item" aria-haspopup="true">
												<a href="<?php echo e(url('admin/admins/create')); ?>" class="kt-menu__link ">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i>
													<span class="kt-menu__link-text">أضافة</span>
												</a>
											</li>
											<li class="kt-menu__item " aria-haspopup="true">
												<a href="<?php echo e(url('admin/admins/')); ?>" class="kt-menu__link ">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i>
													<span class="kt-menu__link-text">عرض</span>
												</a>
											</li>
										</ul>
									</div>
								</li>
							<?php endif; ?>


							<?php if(checkPermission('clients')): ?>
								<!--------------------- Clients --------------------->
								<li class="kt-menu__item" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a href="<?php echo e(url('admin/clients')); ?>" class="kt-menu__link">
										<span class="kt-menu__link-icon"><i class=" fa fa-users"></i></span>
										<span class="kt-menu__link-text"> العملاء</span>
										<span class="kt-menu__link-badge"></span>
									</a>
								</li>
							<?php endif; ?>

							<?php if(checkPermission('cities')): ?>
								<!--------------------- Cities --------------------->
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
										<span class="kt-menu__link-icon"><i class=" fa fa-map-marker-alt"></i></span>
										<span class="kt-menu__link-text"> المدن</span>
										<span class="kt-menu__link-badge"></span>
										<i class="kt-menu__ver-arrow la la-angle-right"></i>
									</a>
									<div class="kt-menu__submenu">
										<span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/cities/create')); ?>" class="kt-menu__link ">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">أضافة</span>
												</a>
											</li>
											<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/cities')); ?>" class="kt-menu__link">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">عرض</span>
												</a>
											</li>
										</ul>
									</div>
								</li>
							<?php endif; ?>

							<?php if(checkPermission('regions')): ?>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
										<span class="kt-menu__link-icon"><i class=" fa fa-map-marker-alt"></i></span>
										<span class="kt-menu__link-text"> المناطق</span>
										<span class="kt-menu__link-badge"></span>
										<i class="kt-menu__ver-arrow la la-angle-right"></i>
									</a>
									<div class="kt-menu__submenu">
										<span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/regions/create')); ?>" class="kt-menu__link ">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">أضافة</span>
												</a>
											</li>
											<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/regions')); ?>" class="kt-menu__link">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">عرض</span>
												</a>
											</li>
										</ul>
									</div>
								</li>
							<?php endif; ?>

							<?php if(checkPermission('categories')): ?>
								<!--------------------- Categories --------------------->
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
										<span class="kt-menu__link-icon"><i class=" fa fa-list"></i></span>
										<span class="kt-menu__link-text"> أقسام التجار</span>
										<span class="kt-menu__link-badge"></span>
										<i class="kt-menu__ver-arrow la la-angle-right"></i>
									</a>
									<div class="kt-menu__submenu">
										<span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/categories/create')); ?>" class="kt-menu__link ">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">أضافة</span>
												</a>
											</li>
											<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/categories')); ?>" class="kt-menu__link">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">عرض</span>
												</a>
											</li>
										</ul>
									</div>
								</li>
							<?php endif; ?>

							<?php if(checkPermission('partners')): ?>
								<!--------------------- Galleries --------------------->
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
										<span class="kt-menu__link-icon"><i class=" fa fa-star"></i></span>
										<span class="kt-menu__link-text">التجار</span>
										<span class="kt-menu__link-badge"></span>
										<i class="kt-menu__ver-arrow la la-angle-right"></i>
									</a>
									<div class="kt-menu__submenu">
										<span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/galleries/create')); ?>" class="kt-menu__link ">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">أضافة</span>
												</a>
											</li>
											<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/galleries')); ?>" class="kt-menu__link">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">عرض</span>
												</a>
											</li>
										</ul>
									</div>
								</li>
							<?php endif; ?>


							<?php if(checkPermission('categories_saller_services')): ?>
								<!--------------------- Categories --------------------->
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
										<span class="kt-menu__link-icon"><i class=" fa fa-list"></i></span>
										<span class="kt-menu__link-text"> أقسام الخدمات</span>
										<span class="kt-menu__link-badge"></span>
										<i class="kt-menu__ver-arrow la la-angle-right"></i>
									</a>
									<div class="kt-menu__submenu">
										<span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/saller/servcies/category/create')); ?>" class="kt-menu__link ">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">أضافة</span>
												</a>
											</li>
											<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/saller/servcies/category')); ?>" class="kt-menu__link">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">عرض</span>
												</a>
											</li>
										</ul>
									</div>
								</li>
							<?php endif; ?>

							<?php if(checkPermission('sallers')): ?>
								<!--------------------- Galleries --------------------->
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
										<span class="kt-menu__link-icon"><i class=" fa fa-star"></i></span>
										<span class="kt-menu__link-text">تجار الخدمات</span>
										<span class="kt-menu__link-badge"></span>
										<i class="kt-menu__ver-arrow la la-angle-right"></i>
									</a>
									<div class="kt-menu__submenu">
										<span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/sallers/services/create')); ?>" class="kt-menu__link ">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">أضافة</span>
												</a>
											</li>
											<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/sallers/services')); ?>" class="kt-menu__link">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">عرض</span>
												</a>
											</li>
										</ul>
									</div>
								</li>
							<?php endif; ?>

							<?php if(checkPermission('products')): ?>
								<!--------------------- Products --------------------->
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
										<span class="kt-menu__link-icon"><i class=" fas fa-boxes"></i></span>
										<span class="kt-menu__link-text"> المنتجات</span>
										<span class="kt-menu__link-badge"></span>
										<i class="kt-menu__ver-arrow la la-angle-right"></i>
									</a>
									<div class="kt-menu__submenu">
										<span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/products/create')); ?>" class="kt-menu__link ">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">أضافة</span>
												</a>
											</li>
											<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/products')); ?>" class="kt-menu__link">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">عرض</span>
												</a>
											</li>
										</ul>
									</div>
								</li>
							<?php endif; ?>

							<?php if(checkPermission('orders')): ?>
								<!--------------------- Selling Order --------------------->
								<li class="kt-menu__item" aria-haspopup="true">
									<a href="<?php echo e(url('admin/all/orders')); ?>" class="kt-menu__link">
										<span class="kt-menu__link-icon"><i class=" fas fa-shopping-bag"></i></span>
										<span class="kt-menu__link-text">الطلبات</span>
									</a>
								</li>
							<?php endif; ?>

							<?php if(checkPermission('sizes')): ?>
								<!--------------------- Sizes --------------------->
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
										<span class="kt-menu__link-icon"><i class=" fa fa-balance-scale"></i></span>
										<span class="kt-menu__link-text"> المقاسات</span>
										<span class="kt-menu__link-badge"></span>
										<i class="kt-menu__ver-arrow la la-angle-right"></i>
									</a>
									<div class="kt-menu__submenu">
										<span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/sizes/create')); ?>" class="kt-menu__link ">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">أضافة</span>
												</a>
											</li>
											<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/sizes')); ?>" class="kt-menu__link">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">عرض</span>
												</a>
											</li>
										</ul>
									</div>
								</li>
							<?php endif; ?>

							<?php if(checkPermission('colors')): ?>
								<!--------------------- Sizes --------------------->
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
										<span class="kt-menu__link-icon"><i class="fas fa-palette"></i></span>
										<span class="kt-menu__link-text"> الالوان</span>
										<span class="kt-menu__link-badge"></span>
										<i class="kt-menu__ver-arrow la la-angle-right"></i>
									</a>
									<div class="kt-menu__submenu">
										<span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/colors/create')); ?>" class="kt-menu__link ">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">أضافة</span>
												</a>
											</li>
											<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/colors')); ?>" class="kt-menu__link">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">عرض</span>
												</a>
											</li>
										</ul>
									</div>
								</li>
							<?php endif; ?>

							<?php if(checkPermission('codes')): ?>
								<!--------------------- Discount Codes --------------------->
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
										<span class="kt-menu__link-icon"><i class=" fa fa-tags"></i></span>
										<span class="kt-menu__link-text">أكواد التجار</span>
										<span class="kt-menu__link-badge"></span>
										<i class="kt-menu__ver-arrow la la-angle-right"></i>
									</a>
									<div class="kt-menu__submenu">
										<span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/saller/codes/create')); ?>" class="kt-menu__link ">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">أضافة</span>
												</a>
											</li>
											<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/saller/codes')); ?>" class="kt-menu__link">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">عرض</span>
												</a>
											</li>
										</ul>
									</div>
								</li>
							<?php endif; ?>

							<?php if(checkPermission('subscribers')): ?>

							<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
								<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
									<span class="kt-menu__link-icon"><i class=" fas fa-paper-plane"></i></span>
									<span class="kt-menu__link-text">الأشتركات</span>
									<span class="kt-menu__link-badge"></span>
									<i class="kt-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="kt-menu__submenu">
									<span class="kt-menu__arrow"></span>
									<ul class="kt-menu__subnav">
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="<?php echo e(url('admin/sub/pending')); ?>" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">الأشتراكات الجارية</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="<?php echo e(url('admin/sub/finish')); ?>" class="kt-menu__link">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">الأشتراكات المنتهية</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<?php endif; ?>

							<?php if(checkPermission('terms')): ?>
								<!--------------------- Sizes --------------------->
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
										<span class="kt-menu__link-icon"><i class=" fa fa-balance-scale"></i></span>
										<span class="kt-menu__link-text"> الشروط و الأحكام</span>
										<span class="kt-menu__link-badge"></span>
										<i class="kt-menu__ver-arrow la la-angle-right"></i>
									</a>
									<div class="kt-menu__submenu">
										<span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/terms/create')); ?>" class="kt-menu__link ">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">أضافة</span>
												</a>
											</li>
											<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/terms')); ?>" class="kt-menu__link">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">شروط و أحكام التاجر</span>
												</a>
											</li>

											<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="<?php echo e(url('admin/user/terms')); ?>" class="kt-menu__link">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">شروط و أحكام العميل</span>
												</a>
											</li>
										</ul>
									</div>
								</li>
							<?php endif; ?>

							<?php if(checkPermission('social')): ?>
								<!--------------------- Social Media --------------------->
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
								<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
									<span class="kt-menu__link-icon"><i class=" fab fa-facebook"></i></span>
									<span class="kt-menu__link-text"> السوشيال ميديا</span>
									<span class="kt-menu__link-badge"></span>
									<i class="kt-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="kt-menu__submenu">
									<span class="kt-menu__arrow"></span>
									<ul class="kt-menu__subnav">
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="<?php echo e(url('admin/social/create')); ?>" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">أضافة</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="<?php echo e(url('admin/social')); ?>" class="kt-menu__link">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"></i><span class="kt-menu__link-text">عرض</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<?php endif; ?>

							<?php if(checkPermission('contact')): ?>
								<!--------------------- Contact Us --------------------->
								<li class="kt-menu__item" aria-haspopup="true">
									<a href="<?php echo e(url('admin/contact')); ?>" class="kt-menu__link">
										<span class="kt-menu__link-icon"><i class=" fas fa-phone"></i></span>
										<span class="kt-menu__link-text">تواصل معنا</span>
									</a>
								</li>
							<?php endif; ?>

							<?php if(checkPermission('home')): ?>
								<!--------------------- Home --------------------->
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
										<span class="kt-menu__link-icon"><i class="fa fa-home"></i></span>
										<span class="kt-menu__link-text">الأعدادات</span>
										<span class="kt-menu__link-badge"></span>
										<i class="kt-menu__ver-arrow la la-angle-right"></i>
									</a>
									<div class="kt-menu__submenu">
										<span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item" aria-haspopup="true">
												<a href="<?php echo e(url('admin/slider')); ?>" class="kt-menu__link ">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
													<span class="kt-menu__link-text">سلايدر الأعلانات</span>
												</a>
											</li>
											<li class="kt-menu__item " aria-haspopup="true">
												<a href="<?php echo e(url('/admin/sections/about_home')); ?>" class="kt-menu__link ">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
													<span class="kt-menu__link-text">عن التطبيق</span>
												</a>
											</li>
											<li class="kt-menu__item " aria-haspopup="true">
												<a href="<?php echo e(url('/admin/sections/return_policy')); ?>" class="kt-menu__link ">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
													<span class="kt-menu__link-text">سياسة التطبيق</span>
												</a>
											</li>
										</ul>
									</div>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
				<!-- end:: Aside Menu -->
			</div>
			<!-- end:: Aside -->


				<!-- end:: Aside -->
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

				<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">



<!-- begin:: Header Topbar -->
<div class="kt-header__topbar">


	<!--begin: User Bar -->
	<div class="kt-header__topbar-item kt-header__topbar-item--user ml-auto" style="left:0;position:absolute">
		<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
								<div class="kt-header__topbar-user">
									<span class="kt-header__topbar-username kt-hidden-mobile"><?php echo e(Auth::guard('admin')->user()->name); ?></span>
									<?php if(Auth::guard('admin')->user()->image == ''): ?>
										<img alt="<?php echo e(Auth::guard('admin')->user()->name); ?>" src="<?php echo e(asset('user.png')); ?>" />
									<?php else: ?>
										<img alt="<?php echo e(Auth::guard('admin')->user()->name); ?>" src="<?php echo e(asset(Auth::guard('admin')->user()->image)); ?>" />
									<?php endif; ?>
								</div>
							</div>
		<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

								<!--begin: Head -->
								<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"  style="background-image: url(<?php echo e(asset('assets/media/misc/bg-1.jpg')); ?>)">
									<div class="kt-user-card__avatar">
										<?php if(Auth::guard('admin')->user()->image == ''): ?>
											<img class="kt-hidden" alt="<?php echo e(Auth::guard('admin')->user()->name); ?>" src="<?php echo e(asset('user.png')); ?>" />
											<span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">
												<img alt="<?php echo e(Auth::guard('admin')->user()->name); ?>" src="<?php echo e(asset('user.png')); ?>" />
											</span>
										<?php else: ?>
											<img class="kt-hidden" alt="<?php echo e(Auth::guard('admin')->user()->name); ?>" src="<?php echo e(asset(Auth::guard('admin')->user()->image)); ?>" />
											<span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">
												<img alt="<?php echo e(Auth::guard('admin')->user()->name); ?>" src="<?php echo e(asset(Auth::guard('admin')->user()->image)); ?>" />
											</span>
										<?php endif; ?>
									</div>
									<div class="kt-user-card__name"><?php echo e(Auth::guard('admin')->user()->name); ?></div>
								</div>

								<!--end: Head -->

								<!--begin: Navigation -->
								<div class="kt-notification">
									<a href="<?php echo e(url('admin/profile')); ?>" class="kt-notification__item">
										<div class="kt-notification__item-icon">
											<i class="flaticon2-calendar-3 kt-font-success"></i>
										</div>
										<div class="kt-notification__item-details">
											<div class="kt-notification__item-title kt-font-bold">
												صفحتي الشخصية
											</div>
											<div class="kt-notification__item-time">
												الأعدادات
											</div>
										</div>
									</a>
									<a href="<?php echo e(url('admin/change_password')); ?>" class="kt-notification__item">
										<div class="kt-notification__item-icon">
											<i class="flaticon2-lock kt-font-danger"></i>
										</div>
										<div class="kt-notification__item-details">
											<div class="kt-notification__item-title kt-font-bold">
												تغير كلمة السر
											</div>
										</div>
									</a>


									<div class="kt-notification__custom">
										<a href="<?php echo e(url('admin/logout')); ?>" class="btn btn-label-brand btn-sm btn-bold">Sign Out</a>
									</div>
								</div>
							</div>
	</div>

	<!--end: User Bar -->
</div>

<!-- end:: Header Topbar -->
</div>

	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<?php echo $__env->yieldContent('content'); ?>

	</div>

	</div>

	<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>

		<!-- end::Global Config -->

		<!--begin:: Global Mandatory Vendors -->
		<script src="<?php echo e(asset('assets/vendors/general/jquery/dist/jquery.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/popper.js/dist/umd/popper.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/js-cookie/src/js.cookie.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/moment/min/moment.min.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/sticky-js/dist/sticky.min.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/wnumb/wNumb.js')); ?>"></script>
		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<script src="<?php echo e(asset('assets/vendors/general/jquery-form/dist/jquery.form.min.js')); ?>"></script>
		<script src="<?php echo e(asset('ckeditor/ckeditor.js')); ?>"></script>
		<script src="<?php echo e(asset('ckeditor/js/sample.js')); ?>"></script>

		<script src="<?php echo e(asset('assets/vendors/general/block-ui/jquery.blockUI.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/custom/components/vendors/bootstrap-datepicker/init.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/custom/components/vendors/bootstrap-timepicker/init.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/custom/components/vendors/bootstrap-switch/init.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/select2/dist/js/select2.full.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/ion-rangeslider/js/ion.rangeSlider.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/typeahead.js/dist/typeahead.bundle.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/handlebars/dist/handlebars.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/inputmask/dist/jquery.inputmask.bundle.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/nouislider/distribute/nouislider.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/owl.carousel/dist/owl.carousel.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/autosize/dist/autosize.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/clipboard/dist/clipboard.min.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/dropzone/dist/dropzone.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/summernote/dist/summernote.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/markdown/lib/markdown.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/custom/components/vendors/bootstrap-markdown/init.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/bootstrap-notify/bootstrap-notify.min.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/custom/components/vendors/bootstrap-notify/init.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/jquery-validation/dist/jquery.validate.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/jquery-validation/dist/additional-methods.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/custom/components/vendors/jquery-validation/init.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/toastr/build/toastr.min.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/raphael/raphael.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/morris.js/morris.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/chart.js/dist/Chart.bundle.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/waypoints/lib/jquery.waypoints.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/counterup/jquery.counterup.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/sweetalert2/dist/sweetalert2.min.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/custom/components/vendors/sweetalert2/init.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/jquery.repeater/src/lib.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/jquery.repeater/src/jquery.input.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/jquery.repeater/src/repeater.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/general/dompurify/dist/purify.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/demo/default/base/scripts.bundle.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/app/custom/general/crud/forms/widgets/select2.js')); ?>" type="text/javascript"></script>

		<script src="<?php echo e(asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/app/custom/general/dashboard.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/app/bundle/app.bundle.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/vendors/custom/datatables/datatables.bundle.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/app/custom/general/crud/datatables/data-sources/html.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/app/custom/general/crud/forms/widgets/bootstrap-timepicker.js')); ?>" type="text/javascript"></script>
		<script src="<?php echo e(asset('assets/ckeditor/ckeditor.js')); ?>" type="text/javascript"></script>
		<script src="<?php echo e(asset('js/custom.js')); ?>"></script>
		<!-- The core Firebase JS SDK is always required and must be listed first -->
		<script src="https://www.gstatic.com/firebasejs/8.7.1/firebase-app.js"></script>

		<!-- TODO: Add SDKs for Firebase products that you want to use
			https://firebase.google.com/docs/web/setup#available-libraries -->
		<script src="https://www.gstatic.com/firebasejs/8.7.1/firebase-analytics.js"></script>

		<script>
		// Your web app's Firebase configuration
		// For Firebase JS SDK v7.20.0 and later, measurementId is optional
		var firebaseConfig = {
			apiKey: "AIzaSyDMijInYv6CzO0mVRmEfppEAD_OUFtuivU",
			authDomain: "vatrintiy.firebaseapp.com",
			projectId: "vatrintiy",
			storageBucket: "vatrintiy.appspot.com",
			messagingSenderId: "515085355743",
			appId: "1:515085355743:web:5fc5fbdb120b904b12376a",
			measurementId: "G-LSV774D1JY"
		};
		// Initialize Firebase
		firebase.initializeApp(firebaseConfig);
		firebase.analytics();
		</script>
		<?php echo $__env->yieldContent('js'); ?>

	</body>

	<!-- end::Body -->
</html>
